/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef TYPES_HSAB_H
#define TYPES_HSAB_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */
   
typedef int        	 T_INT;
typedef char         T_CHAR;
typedef float        T_FLOAT;
typedef double       T_DOUBLE;
typedef short        T_SHORT;
typedef unsigned int T_UINT;
typedef unsigned int T_UINT32;
typedef unsigned int T_BOOL;

typedef struct
{
	T_FLOAT re;
	T_FLOAT im;
}T_COMPLEX;


/*-----------------------------------------------------------------------------------------------*
* Message types
*-----------------------------------------------------------------------------------------------*/
typedef enum
{
	MessageTypeDebug,
	MessageTypeInfo,
	MessageTypeWarning,
	MessageTypeError
} tMessageType;




#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */