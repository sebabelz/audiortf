/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_PAL_HSAB_FFT_DATA_H
#define ALG_PAL_HSAB_FFT_DATA_H

typedef struct
{
   T_INT     iNumInputSamples;
	T_INT     iFFTOrder;
	T_INT    *piInternBuffer;
   T_FLOAT  *pfFFTCosineTable;
   T_FLOAT  *pfFFTSineTable;
	T_INT     iTypeFFt; 
	T_FLOAT  *pfInternBuffer;

}tsFFTData;

#endif 