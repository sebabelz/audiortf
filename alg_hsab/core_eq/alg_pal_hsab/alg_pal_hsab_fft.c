/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_pal_hsab_fft.h"
#include "alg_core_hsab_defines.h"
#include <math.h> 
#include <stdlib.h>  
#include <malloc.h> 

/*----------------------------------------------------------------------------------*
 *   Initialization for complex to complex FFT												   *
 *----------------------------------------------------------------------------------*/
void HsabPalInitComplex2ComplexFFT( T_INT iOrderFFT, 
												tsFFTData *psFFTData )
{       
   T_INT   i, k, m;  

	/* Order of complex FFT if input segment is complex */
	psFFTData->iNumInputSamples = (T_INT) 2 * iOrderFFT;
	psFFTData->iFFTOrder = iOrderFFT;
	
	/* Allocate memory for the fft intern buffer (consists of int values) */
	psFFTData->piInternBuffer = (T_INT*) calloc(psFFTData->iFFTOrder, sizeof(T_INT));

  	for (i=0; i<psFFTData->iFFTOrder; i++)
	{
	   psFFTData->piInternBuffer[i] = i;
	}

	/* Allocate memory for the fft intern buffer (consists of float values) */
	psFFTData->pfInternBuffer = (T_FLOAT*) calloc(psFFTData->iNumInputSamples, sizeof(T_FLOAT));

   for (i=1,k=1; i<psFFTData->iFFTOrder; i+=1)
   {
      if (k > i)
      { 
			psFFTData->piInternBuffer[k-1] = i-1; 
			psFFTData->piInternBuffer[i-1] = k-1;
      }
      m = psFFTData->iFFTOrder/2;
      while ((m >= 2) && (k > m))
      {
         k -= m;
         m >>= 1;
      }
      k += m;
   }

	psFFTData->pfFFTCosineTable = (T_FLOAT*) calloc(psFFTData->iFFTOrder, sizeof(T_FLOAT));
	psFFTData->pfFFTSineTable   = (T_FLOAT*) calloc(psFFTData->iFFTOrder, sizeof(T_FLOAT));

	/* Init cosine and sine table */
	for (i = 0; i<psFFTData->iFFTOrder; i++)
	{  
	   psFFTData->pfFFTCosineTable[i] = (T_FLOAT) cos( (T_FLOAT) 2 * PI / (T_FLOAT) psFFTData->iFFTOrder * (T_FLOAT) i );
		psFFTData->pfFFTSineTable[i]   = (T_FLOAT) sin( (T_FLOAT) 2 * PI / (T_FLOAT) psFFTData->iFFTOrder * (T_FLOAT) i );
	}

}
					
/*----------------------------------------------------------------------------------*
 *   Initialization for real to complex FFT / complex to real FFT					      *															      *
 *----------------------------------------------------------------------------------*/
void HsabPalInitReal2ComplexFFT( T_INT iOrderFFT, 
											tsFFTData *psFFTData )
{     
   int i, k, m;            

	/* Complex FFT of half order if input segment is real */
	psFFTData->iNumInputSamples = iOrderFFT;
	psFFTData->iFFTOrder = psFFTData->iNumInputSamples >> 1; 

	/* Allocate memory for the fft intern buffer (consists of int values) */
	psFFTData->piInternBuffer = (T_INT*) calloc(psFFTData->iNumInputSamples, sizeof(T_INT));

  	for (i=0; i<psFFTData->iNumInputSamples; i++)
	{
	   psFFTData->piInternBuffer[i] = i;
	}

	/* Allocate memory for the fft intern buffer (consists of float values) */
	psFFTData->pfInternBuffer = (T_FLOAT*) calloc(psFFTData->iNumInputSamples, sizeof(T_FLOAT));

   for (i=3, k=psFFTData->iFFTOrder+1; i<psFFTData->iNumInputSamples; i+=2)
   {
      if (k > i)
      { 
			psFFTData->piInternBuffer[k-1] = i-1; 
			psFFTData->piInternBuffer[i-1] = k-1;
			psFFTData->piInternBuffer[k]   = i; 
			psFFTData->piInternBuffer[i]   = k;
      }
      m = psFFTData->iFFTOrder;
      while ((m >= 2) && (k > m))
      {
         k -= m;
         m >>= 1;
      }
      k += m;
   }

	psFFTData->pfFFTCosineTable = (T_FLOAT*) calloc(psFFTData->iFFTOrder, sizeof(T_FLOAT));
	psFFTData->pfFFTSineTable   = (T_FLOAT*) calloc(psFFTData->iFFTOrder, sizeof(T_FLOAT));

	/* Init cosine and sine table */
	for (i = 0; i<psFFTData->iFFTOrder; i++)
	{  
	   psFFTData->pfFFTCosineTable[i] = (T_FLOAT) cos( (T_FLOAT) PI / (T_FLOAT) psFFTData->iFFTOrder * (T_FLOAT) i );
		psFFTData->pfFFTSineTable[i]   = (T_FLOAT) sin( (T_FLOAT) PI / (T_FLOAT) psFFTData->iFFTOrder * (T_FLOAT) i );
	}
}


/*----------------------------------------------------------------------------------*
 *   Compute complex FFT of real input									                     *
 *----------------------------------------------------------------------------------*/
void HsabPalReal2ComplexFFT( T_FLOAT   *pfInputData, 
									  tsFFTData *psFFTData, 
									  T_COMPLEX *pfSpecDataOut )
{
	T_INT   mmax, istep;        
   T_INT   i, j, k, m;            
   T_INT   iIndex, iStepVar;            /* used for sine and cosine tables */
   T_FLOAT ftempr, ftempi;              /* temp variables for Danielson-Lanczos method      */
   T_FLOAT fw_real, fw_imag;            /* real and imag. part of complex exponential func. */
   T_FLOAT ftempVar1, ftempVar2;        /* correcting factors (real and imaginary part) of the complex exponetial function.          */
	T_FLOAT ftempVar3, ftempVar4;

  /*----------------------------------------------------------------------------------*
   *     Bit reversel (re-sort of the input signal)                                   *
	*     bit patterns of the indices are mirrored                                     *
	*     (even number->real value, odd number->imaginary value)                       *
   *----------------------------------------------------------------------------------*/	
	for ((i = 0); i < psFFTData->iNumInputSamples; i++)
   {
		k = psFFTData->piInternBuffer[i];
      psFFTData->pfInternBuffer[i] = pfInputData[k]; 
   }

  /*----------------------------------------------------------------------------------*
   *         Formula of Danielson-Lanczos (according numerical receipis)              *
   *----------------------------------------------------------------------------------*/
   mmax = 2;
	iStepVar = psFFTData->iFFTOrder >> 1;

   while (psFFTData->iNumInputSamples > mmax) /* Loop over all stages */
   {
      istep = mmax << 1;
      iIndex = 0U;

      for (m=1; m<mmax; m+=2)
      {
         /* read complex values from tables */
			j = iIndex * (T_INT) 2;
			fw_real = psFFTData->pfFFTCosineTable[j];
         fw_imag = psFFTData->pfFFTSineTable[j]; 

			for (i=m; i<psFFTData->iNumInputSamples; i+=istep)
         {
            k       = i + mmax;
            ftempr  = (fw_real * psFFTData->pfInternBuffer[k-1]) + (fw_imag * psFFTData->pfInternBuffer[k]);
            ftempi  = (fw_real * psFFTData->pfInternBuffer[k])    - (fw_imag * psFFTData->pfInternBuffer[k-1]);
            psFFTData->pfInternBuffer[k-1]  = psFFTData->pfInternBuffer[i-1] - ftempr;
            psFFTData->pfInternBuffer[k]    = psFFTData->pfInternBuffer[i]    - ftempi;
            psFFTData->pfInternBuffer[i-1] += ftempr;
            psFFTData->pfInternBuffer[i]   += ftempi;
         }
         iIndex += iStepVar;
      }
      mmax = istep;
		iStepVar = iStepVar >> 1;
   }

  /*----------------------------------------------------------------------------------*
	*   FFT correction -> necessary when applying a FFT of length N                    *
	*   to a real valued signal of length 2N (see Kammayer/Kroschel                    *
	*   Digitale Signalverarbeitung ). The used equation is                            *
	*   In(n) = 0.5*[In(n)+In*(N-n)] + exp(-j*pi*n/N)*(1)/(2j)*[Out(n)-Out*(N-n)],     *
	*   whereas In(n) is the input spectrum and Out(n) is the corrected output spectrum)*
   *----------------------------------------------------------------------------------*/
   pfSpecDataOut[0].re = psFFTData->pfInternBuffer[0] + psFFTData->pfInternBuffer[1];
	pfSpecDataOut[0].im = (T_FLOAT) 0.0;
   
	pfSpecDataOut[psFFTData->iFFTOrder].re = psFFTData->pfInternBuffer[0] - psFFTData->pfInternBuffer[1];
   pfSpecDataOut[psFFTData->iFFTOrder].im = (T_FLOAT) 0.0;

   k = psFFTData->iFFTOrder >> 1;
   pfSpecDataOut[k].re =  psFFTData->pfInternBuffer[psFFTData->iFFTOrder];
   pfSpecDataOut[k].im = -psFFTData->pfInternBuffer[psFFTData->iFFTOrder+1]; 

	for (i=2,k=1; i<psFFTData->iFFTOrder; i+=2,k++)
	{
		j = psFFTData->iNumInputSamples - i;
		m = j >> 1;

		fw_real = psFFTData->pfFFTCosineTable[k];
		fw_imag = psFFTData->pfFFTSineTable[k];

		ftempVar3 = psFFTData->pfInternBuffer[i] - psFFTData->pfInternBuffer[j];
		ftempVar4 = psFFTData->pfInternBuffer[i+1] + psFFTData->pfInternBuffer[j+1];
		ftempVar1 = (fw_imag * ftempVar3) - (fw_real * ftempVar4);
		ftempVar2 = (fw_real * ftempVar3) + (fw_imag * ftempVar4);
 
		ftempVar3 = psFFTData->pfInternBuffer[i] + psFFTData->pfInternBuffer[j];
		ftempVar4 = psFFTData->pfInternBuffer[i+1U] - psFFTData->pfInternBuffer[j+1];

		pfSpecDataOut[k].re =  (ftempVar3 - ftempVar1) * (T_FLOAT) 0.5f;
		pfSpecDataOut[k].im =  (ftempVar4 - ftempVar2) * (T_FLOAT) 0.5f;
		pfSpecDataOut[m].re =  (ftempVar3 + ftempVar1) * (T_FLOAT) 0.5f;
		pfSpecDataOut[m].im = -(ftempVar4 + ftempVar2) * (T_FLOAT) 0.5f;
	}
}

/*----------------------------------------------------------------------------------*
 *   Compute complex FFT of complex input									                  *
 *----------------------------------------------------------------------------------*/
void HsabPalComplex2ComplexFFT( T_COMPLEX  *pfInputData, 
								        tsFFTData  *psFFTData, 
									     T_COMPLEX  *pfResultData,
										  T_INT       iFlagFFT )
{
	T_INT mmax, istep;        
   T_INT i, k, m;            
   T_INT iIndex, iStepVar;     /* used for sine and cosine tables */
   T_FLOAT ftempr, ftempi;     /* temp variables for Danielson-Lanczos method      */
   T_FLOAT fw_real, fw_imag;   /* real and imag. part of complex exponential func. */
  
  /*----------------------------------------------------------------------------------*
   *     Bit reversel (re-sort of the input signal)                                   *
	*     bit patterns of the indices are mirrored                                     *
	*     (even number->real value, odd number->imaginary value)                       *
   *----------------------------------------------------------------------------------*/	
	for (i=0,k=0; i<psFFTData->iFFTOrder; i++,k+=2)
   {
		m = psFFTData->piInternBuffer[i];
      psFFTData->pfInternBuffer[k] = pfInputData[m].re;
		psFFTData->pfInternBuffer[k+1] = pfInputData[m].im;
   }

  /*----------------------------------------------------------------------------------*
   *         Formula of Danielson-Lanczos (according numerical receipis)              *
   *----------------------------------------------------------------------------------*/
   mmax = 2;
	iStepVar = psFFTData->iFFTOrder >> 1;

   while (psFFTData->iNumInputSamples > mmax) /* Loop over all stages */
   {
      istep = mmax << 1;
      iIndex = 0U;

      for (m = 1U; m < mmax; m += 2U)
      {
         /* read complex values from tables */
			fw_real = psFFTData->pfFFTCosineTable[iIndex];
         fw_imag = iFlagFFT * psFFTData->pfFFTSineTable[iIndex]; 

			for (i=m; i<psFFTData->iNumInputSamples; i+=istep)
         {
            k       = i + mmax;
            ftempr  = (fw_real * psFFTData->pfInternBuffer[k-1U]) + (fw_imag * psFFTData->pfInternBuffer[k]);
            ftempi  = (fw_real * psFFTData->pfInternBuffer[k])    - (fw_imag * psFFTData->pfInternBuffer[k-1]);
            psFFTData->pfInternBuffer[k-1U]  = psFFTData->pfInternBuffer[i-1U] - ftempr;
            psFFTData->pfInternBuffer[k]     = psFFTData->pfInternBuffer[i]    - ftempi;
            psFFTData->pfInternBuffer[i-1U] += ftempr;
            psFFTData->pfInternBuffer[i]    += ftempi;
         }
         iIndex += iStepVar;
      }
      mmax = istep;
		iStepVar = iStepVar >> 1;
   }

	for (i=0,k=0; i<psFFTData->iNumInputSamples; i+=2,k++)
   {		
		pfResultData[k].re = psFFTData->pfInternBuffer[i];
		pfResultData[k].im = psFFTData->pfInternBuffer[i+1];
	}
}

/*----------------------------------------------------------------------------------*
 *   Compute complex IFFT: complex input and real output   				               *
 *----------------------------------------------------------------------------------*/
void HsabPalComplex2RealIFFT( T_COMPLEX  *pfInputData, 
								      tsFFTData  *psFFTData, 
									   T_FLOAT    *pfResultData )
{
	T_INT mmax, istep;        
   T_INT i, j, k, m;            
   T_INT iIndex, iStepVar;              /* used for sine and cosine tables */
   T_FLOAT ftempr, ftempi;              /* temp variables for Danielson-Lanczos method      */
   T_FLOAT fw_real, fw_imag;            /* real and imag. part of complex exponential func. */
   T_FLOAT ftempVar1, ftempVar2;        /* correcting factors (real and imaginary part) of the complex exponetial function.          */
	T_FLOAT ftempVar3, ftempVar4, fFFTOrderInv;

  /*----------------------------------------------------------------------------------*
	*   FFT correction -> necessary when applying a FFT of length N                    *
	*   to a real valued signal of length 2N (see Kammayer/Kroschel                    *
	*   Digitale Signalverarbeitung ). The used equation is                            *
	*   Out(n) = 0.5*[In(n)+In*(N-n)] + exp(-j*pi*n/N)*(1)/(2j)*[In(n)-In*(N-n)],      *
	*   whereas In(n) is the input spectrum and Out(n) is the corrected output spectrum) *
   *----------------------------------------------------------------------------------*/
	psFFTData->pfInternBuffer[1] = (pfInputData[0].re - pfInputData[psFFTData->iFFTOrder].re) * (T_FLOAT) 0.5;
	psFFTData->pfInternBuffer[0] = (pfInputData[0].re + pfInputData[psFFTData->iFFTOrder].re) * (T_FLOAT) 0.5;
	
   k = psFFTData->iFFTOrder >> 1;
   psFFTData->pfInternBuffer[psFFTData->iFFTOrder] = pfInputData[k].re;
   psFFTData->pfInternBuffer[psFFTData->iFFTOrder+1U] = -pfInputData[k].im; 

	for (i=2,k=1; i < psFFTData->iFFTOrder; i+=2, k++)
	{
		j = psFFTData->iNumInputSamples - i;
		m = j >> 1;

		fw_real = -psFFTData->pfFFTCosineTable[k];
		fw_imag =  psFFTData->pfFFTSineTable[k];

		ftempVar3 = pfInputData[k].re - pfInputData[m].re;
		ftempVar4 = pfInputData[k].im + pfInputData[m].im;

		ftempVar1 = (fw_imag * ftempVar3) - (fw_real * ftempVar4);
		ftempVar2 = (fw_real * ftempVar3) + (fw_imag * ftempVar4);

	   ftempVar3 = pfInputData[k].re + pfInputData[m].re;
		ftempVar4 = pfInputData[k].im - pfInputData[m].im;

		psFFTData->pfInternBuffer[i]   =  (ftempVar3 - ftempVar1) * (T_FLOAT) 0.5;
		psFFTData->pfInternBuffer[i+1] =  (ftempVar4 - ftempVar2) * (T_FLOAT) 0.5;
		psFFTData->pfInternBuffer[j]   =  (ftempVar3 + ftempVar1) * (T_FLOAT) 0.5;
		psFFTData->pfInternBuffer[j+1] = -(ftempVar4 + ftempVar2) * (T_FLOAT) 0.5;
	}

  /*----------------------------------------------------------------------------------*
   *     Bit reversel (re-sort of the input signal)                                   *
	*     bit patterns of the indices are mirrored                                     *
	*     (even number->real value, odd number->imaginary value)                       *
   *----------------------------------------------------------------------------------*/	
	for (i=0; i<psFFTData->iNumInputSamples; i++)
   {
		k = psFFTData->piInternBuffer[i];
      pfResultData[i] = psFFTData->pfInternBuffer[k]; 
   }

  /*----------------------------------------------------------------------------------*
   *         Formula of Danielson-Lanczos (according numerical receipis)              *
   *----------------------------------------------------------------------------------*/
   mmax = 2;
	iStepVar = psFFTData->iFFTOrder >> 1;

   while (psFFTData->iNumInputSamples > mmax) /* Loop over all stages */
   {
      istep = mmax << 1;
      iIndex = 0;

      for (m=1; m<mmax; m+=2)
      {
         /* read complex values from tables */
			j = iIndex * 2;
			fw_real =  psFFTData->pfFFTCosineTable[j];
         fw_imag = -psFFTData->pfFFTSineTable[j]; 

			for (i=m; i<psFFTData->iNumInputSamples; i+=istep)
         {
            k       = i + mmax;
            ftempr  = (fw_real * pfResultData[k-1]) + (fw_imag * pfResultData[k]);
            ftempi  = (fw_real * pfResultData[k])   - (fw_imag * pfResultData[k-1]);
            pfResultData[k-1]  = pfResultData[i-1]  - ftempr;
            pfResultData[k]    = pfResultData[i]    - ftempi;
            pfResultData[i-1] += ftempr;
            pfResultData[i]   += ftempi;
         }
         iIndex += iStepVar;
      }
      mmax = istep;
		iStepVar = iStepVar >> 1;
   }

  /*----------------------------------------------------------------------------------*
   *         Devide by the FFT Order (necessary for the IFFT)                         *
   *----------------------------------------------------------------------------------*/
	fFFTOrderInv = (T_FLOAT) 1.0 / (T_FLOAT) psFFTData->iFFTOrder;
	
	for (m=0; m<psFFTData->iNumInputSamples; m+=1)
   {
      pfResultData[m] *= fFFTOrderInv;
	}

}

/*----------------------------------------------------------------------------------*
 *   Deinit function for complex to complex FFT   								            *
 *----------------------------------------------------------------------------------*/
void HsabPalDeInitComplex2ComplexFFT( tsFFTData *psFFTData )
{
	free(psFFTData->pfFFTCosineTable);
	free(psFFTData->pfFFTSineTable);
	free(psFFTData->piInternBuffer);
	free(psFFTData->pfInternBuffer);
}

/*----------------------------------------------------------------------------------*
 *   Deinit function for real to complex FFT / complex to real FFT    					*
 *----------------------------------------------------------------------------------*/
void HsabPalDeInitReal2ComplexFFT( tsFFTData *psFFTData )
{
	free(psFFTData->pfFFTCosineTable);
	free(psFFTData->pfFFTSineTable);
	free(psFFTData->piInternBuffer);
	free(psFFTData->pfInternBuffer);
}