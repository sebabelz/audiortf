/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_pal_hsab_set_vec.h"

/*---------------------------------------------------------------------------------------------*
 * Input buffer "fBuffer" of size "float*uiSize" is set zo zero 
 *---------------------------------------------------------------------------------------------*/
void HsabPalSetFloatVecToZero( T_FLOAT* fBuffer, T_INT iSize )
{
	T_INT i;

	for (i=0; i<iSize; i++)
	{
      fBuffer[i] = (T_FLOAT) 0.0; 
	}
}

/*---------------------------------------------------------------------------------------------*
 * Input buffer "fBuffer" of size "complex*uiSize" is set zo zero 
 *---------------------------------------------------------------------------------------------*/
void HsabPalSetComplexVecToZero( T_COMPLEX* CmplxBuffer, T_INT iSize )
{
	T_INT i;

	for (i=0; i<iSize; i++)
	{
      CmplxBuffer[i].re = (T_FLOAT) 0.0;
		CmplxBuffer[i].im = (T_FLOAT) 0.0;
	}
}

/*---------------------------------------------------------------------------------------------*
 * Scale float vector of size "float*uiSize" 
 *---------------------------------------------------------------------------------------------*/
void HsabPalScaleFloatVec( T_FLOAT* fBuffer, T_FLOAT fSacleFactor, T_INT iSize )
{
	int i;

	for (i=0; i<iSize; i++)
	{
      fBuffer[i] *= fSacleFactor; 
	}
}