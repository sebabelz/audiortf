/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_DEFINES_H
#define ALG_CORE_HSAB_DEFINES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_signals.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_anasynfb_types.h"

/*---------------------------------------------------------------------------------*
 * General defines 
 *---------------------------------------------------------------------------------*/
#define PI                    3.141592653589793
#define SYS_EPS                          1e-15f
#define ACOUSTICVELOCITY                    340
#define FABS_FACTOR                        0.8f
#define MAX_INT_16                        32767
#define MAX_INT_16_DB        90.308733622833979

/*---------------------------------------------------------------------------------*
 * TRUE/FALSE 
 *---------------------------------------------------------------------------------*/
#define HSAB_TRUE                 1
#define HSAB_FALSE                0  

/*---------------------------------------------------------------------------------*
 * Macro defines 
 *---------------------------------------------------------------------------------*/
#define FABS(A) (((A) > 0.0f) ? (A) : -(A)) 
#define MAX(A,B) (((A) > (B)) ? (A) : (B)) 
#define MIN(A,B) (((A) < (B)) ? (A) : (B)) 
#define CLAMP(A,LO,HI) (((A) < (LO)) ? (LO) : (((A) > (HI)) ? (HI) : (A)))

/*---------------------------------------------------------------------------------*
 * On/off switches
 *---------------------------------------------------------------------------------*/
#define HSAB_OFF                              0
#define HSAB_ON                               1

/*---------------------------------------------------------------------------------*
 * Colors
 *---------------------------------------------------------------------------------*/
#define HSAB_DEBUG_INFO                       0
#define HSAB_DEBUG_ERROR                      1
#define HSAB_DEBUG_WARNING                    2

/*---------------------------------------------------------------------------------*
 * Memory management
 *---------------------------------------------------------------------------------*/
typedef struct {

  /*-------------------------------------------------------------------------------*
   * Total memory
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytes;
   T_INT iCurrentDynMemInBytes;
   T_INT iMaxDynMemInBytes;

  /*-------------------------------------------------------------------------------*
   * Memory used for parameters
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesParameters;
   T_INT iCurrentDynMemInBytesParameters;
   T_INT iMaxDynMemInBytesParameters;

  /*-------------------------------------------------------------------------------*
   * Memory used for signals
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesSignals;
   T_INT iCurrentDynMemInBytesSignals;
   T_INT iMaxDynMemInBytesSignals;

   /*-------------------------------------------------------------------------------*
   * Memory used for analysis and synthesis filterbank
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesAnaSynFb;
   T_INT iCurrentDynMemInBytesAnaSynFb;
   T_INT iMaxDynMemInBytesAnaSynFb;

   /*-------------------------------------------------------------------------------*
   * Memory used for sample rate conversion
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesSrc;
   T_INT iCurrentDynMemInBytesSrc;
   T_INT iMaxDynMemInBytesSrc;

   /*-------------------------------------------------------------------------------*
   * Memory used for equalizer
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesEq;
   T_INT iCurrentDynMemInBytesEq;
   T_INT iMaxDynMemInBytesEq;
 
} tHsabAlgCoreMemManagement;






/*---------------------------------------------------------------------------------*
 * Main
 *---------------------------------------------------------------------------------*/
typedef struct { 

  /*-------------------------------------------------------------------------------*
   * Main memory management
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreMemManagement        HsabAlgCoreMemManagement;

  /*-------------------------------------------------------------------------------*
   * Main parameters
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreParameters           HsabAlgCoreParams;

  /*-------------------------------------------------------------------------------*
   * Main signals
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreSignals              HsabAlgCoreSignals;

   /*-------------------------------------------------------------------------------*
   * Analysis and synthesis filterbank front seats
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreAnaSynFb				HsabAlgCoreAnaSynFbFs;

   /*-------------------------------------------------------------------------------*
   * Analysis and synthesis filterbank rear seats
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreAnaSynFb				HsabAlgCoreAnaSynFbRs;

   /*-------------------------------------------------------------------------------*
   * Equalizer rear seats
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreEq				    HsabAlgCoreEqRs;

   /*-------------------------------------------------------------------------------*
   * Equalizer front seats
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreEq				    HsabAlgCoreEqFs;


}tHsabAlgCoreMain;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

