/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

/*---------------------------------------------------------------------------------*
 * Function: HsabAlgCoreSendDebugMessage
 *
 * Sends a debug message to the graphical user interface
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSendDebugMessage( T_CHAR* pcMessage,
                                  T_INT   iColor )
{
   T_INT iLocColor;
   switch (iColor)
   {
      case (T_INT) HSAB_DEBUG_INFO:
      { 
         //iLocColor = debug_gray;
         break;
      }
      case (T_INT) HSAB_DEBUG_ERROR:
      { 
         //iLocColor = debug_red;
         break;
      }
      case (T_INT) HSAB_DEBUG_WARNING:
      { 
         //iLocColor = debug_blue;
         break;
      }
      default:
      { 
         //iLocColor = debug_black;
      }
   } 
   //send debug message
}