/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_core_hsab_memory_allocation.h"
//#include "alg_core_hsab_messages.h"
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_anasynfb.h"
#include "alg_core_hsab_eq.h"
#include <stdlib.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------*
* Initialization of memory management
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreInitMemMangement(tHsabAlgCoreMemManagement *psMemManagement)
{
	T_INT iError = (T_INT)0;

	/*-------------------------------------------------------------------------------*
	* Total memory
	*-------------------------------------------------------------------------------*/
	psMemManagement->iStaticMemInBytes = (T_INT) sizeof(tHsabAlgCoreMain);
	psMemManagement->iCurrentDynMemInBytes = (T_INT)0;
	psMemManagement->iMaxDynMemInBytes = (T_INT)0;

	/*-------------------------------------------------------------------------------*
	* Memory used for parameters
	*-------------------------------------------------------------------------------*/
	psMemManagement->iStaticMemInBytesParameters = (T_INT) sizeof(tHsabAlgCoreParameters);
	psMemManagement->iCurrentDynMemInBytesParameters = (T_INT)0;
	psMemManagement->iMaxDynMemInBytesParameters = (T_INT)0;

	/*-------------------------------------------------------------------------------*
	* Memory used for analysis and synthesis filterbank
	*-------------------------------------------------------------------------------*/
	psMemManagement->iStaticMemInBytesAnaSynFb = (T_INT) sizeof(tHsabAlgCoreAnaSynFb);
	psMemManagement->iCurrentDynMemInBytesAnaSynFb = (T_INT)0;
	psMemManagement->iMaxDynMemInBytesAnaSynFb = (T_INT)0;


	/*-------------------------------------------------------------------------------*
	* Memory used for signals
	*-------------------------------------------------------------------------------*/
	psMemManagement->iStaticMemInBytesSignals = (T_INT) sizeof(tHsabAlgCoreSignals);
	psMemManagement->iCurrentDynMemInBytesSignals = (T_INT)0;
	psMemManagement->iMaxDynMemInBytesSignals = (T_INT)0;


	/*-------------------------------------------------------------------------------*
	* Memory used for equalizer
	*-------------------------------------------------------------------------------*/
	psMemManagement->iStaticMemInBytesEq = (T_INT) sizeof(tHsabAlgCoreEq);
	psMemManagement->iCurrentDynMemInBytesEq = (T_INT)0;
	psMemManagement->iMaxDynMemInBytesEq = (T_INT)0;

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;

}

/*---------------------------------------------------------------------------------*
* Memory allocation for vectors
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreAlloc1DArray(void**         ppPtr,
	const T_INT    iDim1,
	const T_INT    iSize,
	const T_CHAR*  pcModuleName,
	const T_CHAR*  pcVariableName,
	T_INT*         piCurrentMem,
	T_INT*         piMaxMem,
	T_INT*         piCurrentMemAlgPart,
	T_INT*         piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab Error: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory
	*-------------------------------------------------------------------------------*/
	*ppPtr = malloc(iSize * iDim1);
	if (*ppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError );  
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSize * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSize * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}

/*---------------------------------------------------------------------------------*
* Memory allocation for vectors combined with setting all elements to zero
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreCalloc1DArray(void**         ppPtr,
	const T_INT    iDim1,
	const T_INT    iSize,
	const T_CHAR*  pcModuleName,
	const T_CHAR*  pcVariableName,
	T_INT*         piCurrentMem,
	T_INT*         piMaxMem,
	T_INT*         piCurrentMemAlgPart,
	T_INT*         piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab Error: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory and set elements to zero
	*-------------------------------------------------------------------------------*/
	*ppPtr = calloc(iDim1, iSize);
	if (*ppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError);  
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSize * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSize * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}

/*---------------------------------------------------------------------------------*
* Memory allocation for matrices
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreAlloc2DArray(void***       pppPtr,
	const T_INT   iDim1,
	const T_INT   iDim2,
	const T_INT   iSize,
	const T_INT   iSizeP,
	const T_CHAR* pcModuleName,
	const T_CHAR* pcVariableName,
	T_INT*        piCurrentMem,
	T_INT*        piMaxMem,
	T_INT*        piCurrentMemAlgPart,
	T_INT*        piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_INT i;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab ERROR: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory
	*-------------------------------------------------------------------------------*/
	*pppPtr = malloc(iSizeP * iDim1);
	if (*pppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSizeP * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSizeP * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}
	for (i = 0; i < iDim1; i++)
	{
		/*----------------------------------------------------------------------------*
		* (Try to) allocate memory
		*----------------------------------------------------------------------------*/
		(*pppPtr)[i] = malloc(iSize * iDim2);
		if ((*pppPtr)[i] == 0)
		{
			/*-------------------------------------------------------------------------*
			* Send error message if memory allocation fails
			*-------------------------------------------------------------------------*/
			//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
			iError = (T_INT)1;
		}
		else
		{
			/*-------------------------------------------------------------------------*
			* Update memory statistics if memory allocation was successful
			*-------------------------------------------------------------------------*/
			*piCurrentMem += iSize * iDim2;
			if (*piCurrentMem > *piMaxMem)
			{
				*piMaxMem = *piCurrentMem;
			}
			*piCurrentMemAlgPart += iSize * iDim2;
			if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
			{
				*piMaxMemAlgPart = *piCurrentMemAlgPart;
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}


/*---------------------------------------------------------------------------------*
* Memory allocation for matrices combined with setting all elements to zero
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreCalloc2DArray(void***       pppPtr,
	const T_INT   iDim1,
	const T_INT   iDim2,
	const T_INT   iSize,
	const T_INT   iSizeP,
	const T_CHAR* pcModuleName,
	const T_CHAR* pcVariableName,
	T_INT*        piCurrentMem,
	T_INT*        piMaxMem,
	T_INT*        piCurrentMemAlgPart,
	T_INT*        piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_INT i;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab ERROR: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory
	*-------------------------------------------------------------------------------*/
	*pppPtr = calloc(iDim1, iSizeP);
	if (*pppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSizeP * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSizeP * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}
	for (i = 0; i < iDim1; i++)
	{
		/*----------------------------------------------------------------------------*
		* (Try to) allocate memory
		*----------------------------------------------------------------------------*/
		(*pppPtr)[i] = calloc(iDim2, iSize);
		if ((*pppPtr)[i] == 0)
		{
			/*-------------------------------------------------------------------------*
			* Send error message if memory allocation fails
			*-------------------------------------------------------------------------*/
			//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
			iError = (T_INT)1;
		}
		else
		{
			/*-------------------------------------------------------------------------*
			* Update memory statistics if memory allocation was successful
			*-------------------------------------------------------------------------*/
			*piCurrentMem += iSize * iDim2;
			if (*piCurrentMem > *piMaxMem)
			{
				*piMaxMem = *piCurrentMem;
			}
			*piCurrentMemAlgPart += iSize * iDim2;
			if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
			{
				*piMaxMemAlgPart = *piCurrentMemAlgPart;
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}

/*---------------------------------------------------------------------------------*
* Memory allocation for three dimensional arrays
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreAlloc3DArray(void****      ppppPtr,
	const T_INT   iDim1,
	const T_INT   iDim2,
	const T_INT   iDim3,
	const T_INT   iSize,
	const T_INT   iSizeP,
	const T_CHAR* pcModuleName,
	const T_CHAR* pcVariableName,
	T_INT*        piCurrentMem,
	T_INT*        piMaxMem,
	T_INT*        piCurrentMemAlgPart,
	T_INT*        piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_INT i, n;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab ERROR: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory
	*-------------------------------------------------------------------------------*/
	*ppppPtr = malloc(iSizeP * iDim1);
	if (*ppppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSizeP * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSizeP * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}

	for (i = 0; i < iDim1; i++)
	{
		/*----------------------------------------------------------------------------*
		* (Try to) allocate memory
		*----------------------------------------------------------------------------*/
		(*ppppPtr)[i] = malloc(iSizeP * iDim2);
		if ((*ppppPtr)[i] == 0)
		{
			/*-------------------------------------------------------------------------*
			* Send error message if memory allocation fails
			*-------------------------------------------------------------------------*/
			//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
			iError = (T_INT)1;
		}
		else
		{
			/*-------------------------------------------------------------------------*
			* Update memory statistics if memory allocation was successful
			*-------------------------------------------------------------------------*/
			*piCurrentMem += iSize * iDim2;
			if (*piCurrentMem > *piMaxMem)
			{
				*piMaxMem = *piCurrentMem;
			}
			*piCurrentMemAlgPart += iSize * iDim2;
			if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
			{
				*piMaxMemAlgPart = *piCurrentMemAlgPart;
			}
		}
		for (n = 0; n < iDim2; n++)
		{
			(*ppppPtr)[i][n] = malloc(iSize * iDim3);
			if ((*ppppPtr)[i][n] == 0)
			{
				/*----------------------------------------------------------------------*
				* Send error message if memory allocation fails
				*----------------------------------------------------------------------*/
				//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
				iError = (T_INT)1;
			}
			else
			{
				/*----------------------------------------------------------------------*
				* Update memory statistics if memory allocation was successful
				*----------------------------------------------------------------------*/
				*piCurrentMem += iSize * iDim3;
				if (*piCurrentMem > *piMaxMem)
				{
					*piMaxMem = *piCurrentMem;
				}
				*piCurrentMemAlgPart += iSize * iDim3;
				if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
				{
					*piMaxMemAlgPart = *piCurrentMemAlgPart;
				}
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}

/*---------------------------------------------------------------------------------*
* Memory allocation for three dimensional arrays combined with setting all
* elements to zero
*---------------------------------------------------------------------------------*/
T_INT HsabAlgCoreCalloc3DArray(void****      ppppPtr,
	const T_INT   iDim1,
	const T_INT   iDim2,
	const T_INT   iDim3,
	const T_INT   iSize,
	const T_INT   iSizeP,
	const T_CHAR* pcModuleName,
	const T_CHAR* pcVariableName,
	T_INT*        piCurrentMem,
	T_INT*        piMaxMem,
	T_INT*        piCurrentMemAlgPart,
	T_INT*        piMaxMemAlgPart)
{
	T_INT iError = (T_INT)0;
	T_INT i, n;
	T_CHAR message[256];

	/*-------------------------------------------------------------------------------*
	* Generate error messge
	*-------------------------------------------------------------------------------*/
	sprintf(message, "Hsab ERROR: Allocation error in module %s for the variable %s! ", pcModuleName, pcVariableName);

	/*-------------------------------------------------------------------------------*
	* (Try to) allocate memory
	*-------------------------------------------------------------------------------*/
	*ppppPtr = calloc(iDim1, iSizeP);
	if (*ppppPtr == 0)
	{
		/*----------------------------------------------------------------------------*
		* Send error message if memory allocation fails
		*----------------------------------------------------------------------------*/
		//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
		iError = (T_INT)1;
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Update memory statistics if memory allocation was successful
		*----------------------------------------------------------------------------*/
		*piCurrentMem += iSizeP * iDim1;
		if (*piCurrentMem > *piMaxMem)
		{
			*piMaxMem = *piCurrentMem;
		}
		*piCurrentMemAlgPart += iSizeP * iDim1;
		if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
		{
			*piMaxMemAlgPart = *piCurrentMemAlgPart;
		}
	}

	for (i = 0; i < iDim1; i++)
	{
		/*----------------------------------------------------------------------------*
		* (Try to) allocate memory
		*----------------------------------------------------------------------------*/
		(*ppppPtr)[i] = calloc(iDim2, iSizeP);
		if ((*ppppPtr)[i] == 0)
		{
			/*-------------------------------------------------------------------------*
			* Send error message if memory allocation fails
			*-------------------------------------------------------------------------*/
			//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
			iError = (T_INT)1;
		}
		else
		{
			/*-------------------------------------------------------------------------*
			* Update memory statistics if memory allocation was successful
			*-------------------------------------------------------------------------*/
			*piCurrentMem += iSize * iDim2;
			if (*piCurrentMem > *piMaxMem)
			{
				*piMaxMem = *piCurrentMem;
			}
			*piCurrentMemAlgPart += iSize * iDim2;
			if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
			{
				*piMaxMemAlgPart = *piCurrentMemAlgPart;
			}
		}
		for (n = 0; n < iDim2; n++)
		{
			(*ppppPtr)[i][n] = calloc(iDim3, iSize);
			if ((*ppppPtr)[i][n] == 0)
			{
				/*----------------------------------------------------------------------*
				* Send error message if memory allocation fails
				*----------------------------------------------------------------------*/
				//HsabAlgCoreSendDebugMessage(message, HsabMessageError ); 
				iError = (T_INT)1;
			}
			else
			{
				/*----------------------------------------------------------------------*
				* Update memory statistics if memory allocation was successful
				*----------------------------------------------------------------------*/
				*piCurrentMem += iSize * iDim3;
				if (*piCurrentMem > *piMaxMem)
				{
					*piMaxMem = *piCurrentMem;
				}
				*piCurrentMemAlgPart += iSize * iDim3;
				if (*piCurrentMemAlgPart > *piMaxMemAlgPart)
				{
					*piMaxMemAlgPart = *piCurrentMemAlgPart;
				}
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;
}

/*---------------------------------------------------------------------------------*
* Free memory of vectors
*---------------------------------------------------------------------------------*/
void HsabAlgCoreFree1DArray(void**           ppPtr,
	const T_INT      iDim1,
	const T_INT      iSize,
	T_INT*           piCurrentMem,
	T_INT*           piCurrentMemAlgPart)
{
	/*-------------------------------------------------------------------------------*
	* Free memory and set pointer to null
	*-------------------------------------------------------------------------------*/
	free(*ppPtr);
	*ppPtr = 0;

	/*-------------------------------------------------------------------------------*
	* Update memory statistics
	*-------------------------------------------------------------------------------*/
	*piCurrentMem -= iSize * iDim1;
	*piCurrentMemAlgPart -= iSize * iDim1;
}

/*---------------------------------------------------------------------------------*
* Free memory of matrices
*---------------------------------------------------------------------------------*/
void HsabAlgCoreFree2DArray(void***          pppPtr,
	const T_INT      iDim1,
	const T_INT      iDim2,
	const T_INT      iSize,
	const T_INT      iSizeP,
	T_INT*           piCurrentMem,
	T_INT*           piCurrentMemAlgPart)
{
	T_INT i;

	for (i = 0; i < iDim1; i++)
	{
		/*----------------------------------------------------------------------------*
		* Free memory
		*----------------------------------------------------------------------------*/
		free((*pppPtr)[i]);
		
		/*----------------------------------------------------------------------------*
		* Update memory statistics
		*----------------------------------------------------------------------------*/
		*piCurrentMem -= iSize * iDim2;
		*piCurrentMemAlgPart -= iSize * iDim2;
	}

	/*-------------------------------------------------------------------------------*
	* Free memory and set pointer to null
	*-------------------------------------------------------------------------------*/
	free(*pppPtr);
	*pppPtr = 0;

	/*-------------------------------------------------------------------------------*
	* Update memory statistics
	*-------------------------------------------------------------------------------*/
	*piCurrentMem -= iSizeP * iDim1;
	*piCurrentMemAlgPart -= iSizeP * iDim1;
}

/*---------------------------------------------------------------------------------*
* Free memory of three dimensional arrays
*---------------------------------------------------------------------------------*/
void HsabAlgCoreFree3DArray(void ****        ppppPtr,
	const T_INT      iDim1,
	const T_INT      iDim2,
	const T_INT      iDim3,
	const T_INT      iSize,
	const T_INT      iSizeP,
	T_INT*           piCurrentMem,
	T_INT*           piCurrentMemAlgPart)
{
	T_INT i, n;

	for (i = 0; i < iDim1; i++)
	{
		for (n = 0; n < iDim2; n++)
		{
			/*-------------------------------------------------------------------------*
			* Free memory
			*-------------------------------------------------------------------------*/
			free((*ppppPtr)[i][n]);

			/*-------------------------------------------------------------------------*
			* Update memory statistics
			*-------------------------------------------------------------------------*/
			*piCurrentMem -= iSize * iDim3;
			*piCurrentMemAlgPart -= iSize * iDim3;
		}

		/*----------------------------------------------------------------------------*
		* Free memory
		*----------------------------------------------------------------------------*/
		free((*ppppPtr)[i]);

		/*----------------------------------------------------------------------------*
		* Update memory statistics
		*----------------------------------------------------------------------------*/
		*piCurrentMem -= iSizeP * iDim2;
		*piCurrentMemAlgPart -= iSizeP * iDim2;
	}

	/*-------------------------------------------------------------------------------*
	* Free memory and set pointer to null
	*-------------------------------------------------------------------------------*/
	free(*ppppPtr);
	*ppppPtr = 0;

	/*-------------------------------------------------------------------------------*
	* Update memory statistics
	*-------------------------------------------------------------------------------*/
	*piCurrentMem -= iSizeP * iDim1;
	*piCurrentMemAlgPart -= iSizeP * iDim1;
}
