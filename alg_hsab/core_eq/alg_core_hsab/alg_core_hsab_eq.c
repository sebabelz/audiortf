/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Manuel Diener
* (c) 2016-2019
*-----------------------------------------------------------------------------------------------*/

#include "alg_core_hsab_eq.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_memory_allocation.h"
#include <math.h>


void HsabAlgCoreEqInit(tHsabAlgCoreEq            *psEq,
					   tHsabAlgCoreParameters    *psParams,
					   T_INT                      iNumLoudSpeak,
					   tHsabAlgCoreMemManagement *psMemManagement)
{
	T_INT   iError = (T_INT) 0;
	T_INT   n, m, k;
	T_FLOAT fV0;
	T_FLOAT fThreedBFreq;
	T_INT   iSamplingRate;

	/*-------------------------------------------------------------------------------*
	* Main initialization
	*-------------------------------------------------------------------------------*/
	psEq->iMainControl   = (T_INT) HSAB_ON;
	psEq->fGainActMax    = (T_FLOAT)  0.002;
	psEq->fGainActMin    = (T_FLOAT) -0.002;
	psEq->fMaxGaindB	 = (T_FLOAT) 22.0;
	psEq->fMinGaindB	 = (T_FLOAT) -24;
	psEq->iNumChannels = iNumLoudSpeak;
	iSamplingRate = psParams->iSampleRateInt;

	/*-------------------------------------------------------------------------------*
	* Init Peak filter
	*-------------------------------------------------------------------------------*/
	psEq->iNumPeakFilt = 8;	//change default parameters if altered
	
   /*-------------------------------------------------------------------------------*
	* Gain control
	*-------------------------------------------------------------------------------*/
    HsabAlgCoreCalloc1DArray((void **)&(psEq->pfGainLspdB),
			psEq->iNumChannels,
			sizeof(T_FLOAT),
			"psEq",
			"pfGainLspdB",
			&(psMemManagement->iCurrentDynMemInBytes),
			&(psMemManagement->iMaxDynMemInBytes),
			&(psMemManagement->iCurrentDynMemInBytesEq),
			&(psMemManagement->iMaxDynMemInBytesEq));
	
	for (n = 0; n<psEq->iNumChannels; n++)
	{
		psEq->pfGainLspdB[n] = (T_FLOAT) 0.0;
	}
	psEq->fGainMasterdB = (T_FLOAT)  0.0;

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfGainLsp),

			psEq->iNumChannels,
			sizeof(T_FLOAT),
			"psEq",
			"pfGainLs",
			&(psMemManagement->iCurrentDynMemInBytes),
			&(psMemManagement->iMaxDynMemInBytes),
			&(psMemManagement->iCurrentDynMemInBytesEq),
			&(psMemManagement->iMaxDynMemInBytesEq));

	for (n = 0; n<psEq->iNumChannels; n++)
	{
		psEq->pfGainLsp[n] = (T_FLOAT) pow((T_FLOAT) 10.0, ((T_FLOAT)psEq->pfGainLspdB[n] / (T_FLOAT) 20.0));
	}

	psEq->fMasterVolume = (T_FLOAT) 1.0;
   /*-------------------------------------------------------------------------------*
	* Parameters of low shelving filters
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvGaindB),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvGaindB",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));
	
	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvBandwidthHz),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvBandwidthHz",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->piLowShelvActivation),
		psEq->iNumChannels,
		sizeof(T_INT),
		"psEq",
		"piLowShelvActivation",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvGain),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvGain",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvAllpassAlpha),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvAllpassAlpha",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvAllpassMemInput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvApMemInput",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfLowShelvAllpassMemOutput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfLowShelvApMemOutput",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	//------------------------------------------------------------------------------
	//Initialisation of low shelv parameters (for use with hard coded parameters)
	//------------------------------------------------------------------------------
	for (n = 0; n<psEq->iNumChannels; n++)
	{
		psEq->pfLowShelvGaindB[n] = (T_FLOAT)  0.0;
		if (psEq->pfLowShelvGaindB[n] >= psEq->fMaxGaindB) {
			psEq->pfLowShelvGaindB[n] = psEq->fMaxGaindB;
		}
		if (psEq->pfLowShelvGaindB[n] <= psEq->fMinGaindB) {
			psEq->pfLowShelvGaindB[n] = psEq->fMinGaindB;
		}
		psEq->pfLowShelvBandwidthHz[n] = (T_FLOAT) 500.0;
	}

	/*-------------------------------------------------------------------------------*
	* Parameters of high shelving filters
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc1DArray(
		(void **)&(psEq->pfHighShelvGaindB),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvGaindB",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray(
		(void **)&(psEq->pfHighShelvBandwidthHz),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvBandwidthHz",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray(
		(void **)&(psEq->piHighShelvActivation),
		psEq->iNumChannels,
		sizeof(T_INT),
		"psEq",
		"piHighShelvActivation",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfHighShelvGain),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvGain",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfHighShelvAllpassAlpha),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvAllpassAlpha",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfHighShelvAllpassMemInput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvApMemInput",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc1DArray((void **)&(psEq->pfHighShelvAllpassMemOutput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		"psEq",
		"pfHighShelvApMemOutput",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	//-----------------------------------------------------------------------------
	//Initalisation of high shelv parameters (for use with hard coded parameters)
	//-----------------------------------------------------------------------------
	for (n = 0; n<psEq->iNumChannels; n++)
	{
		psEq->pfHighShelvGaindB[n] = (T_FLOAT)  0.0;
		if (psEq->pfHighShelvGaindB[n] >= psEq->fMaxGaindB) {
			psEq->pfHighShelvGaindB[n] = psEq->fMaxGaindB;
		}
		if (psEq->pfHighShelvGaindB[n] <= psEq->fMinGaindB) {
			psEq->pfHighShelvGaindB[n] = psEq->fMinGaindB;
		}
		psEq->pfHighShelvBandwidthHz[n] = (T_FLOAT) 1000.0;
	}

	/*-------------------------------------------------------------------------------*
	* Parameters of peak filters
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltGaindB),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltGaindB",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltCenterFreqHz),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltCenterFreqHz",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltBandwidthHz),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltBandwidthHz",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppiPeakFiltActivation),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_INT),
		sizeof(T_INT*),
		"psEq",
		"ppiPeakFiltActivation",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltGain),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltGain",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassAlpha),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassAlpha",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassBeta),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassBeta",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryInput1),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassMemoryInput1",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryInput2),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassMemoryInput2",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryOutput1),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassMemoryOutput1",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	HsabAlgCoreCalloc2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryOutput2),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psEq",
		"ppfPeakFiltAllpassMemoryOutput2",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq),
		&(psMemManagement->iMaxDynMemInBytesEq));

	//----------------------------------------------------------------------------
	//Initalisation of peak filter parameters (for use with hard coded parameters)
	//----------------------------------------------------------------------------
	for (n = 0;n < psEq->iNumChannels;n++) {
		psEq->ppfPeakFiltCenterFreqHz[n][0] = (T_FLOAT) 125.0;
		psEq->ppfPeakFiltCenterFreqHz[n][1] = (T_FLOAT) 950.0;
		psEq->ppfPeakFiltCenterFreqHz[n][2] = (T_FLOAT) 2000.0;
		psEq->ppfPeakFiltCenterFreqHz[n][3] = (T_FLOAT) 5000.0;
		psEq->ppfPeakFiltCenterFreqHz[n][4] = (T_FLOAT) 8500.0;
		psEq->ppfPeakFiltCenterFreqHz[n][5] = (T_FLOAT) 12300.0;
		psEq->ppfPeakFiltCenterFreqHz[n][6] = (T_FLOAT) 16750.0;
		psEq->ppfPeakFiltCenterFreqHz[n][7] = (T_FLOAT) 21600.0;

		psEq->ppfPeakFiltBandwidthHz[n][0] = (T_FLOAT) 100.0;
		psEq->ppfPeakFiltBandwidthHz[n][1] = (T_FLOAT) 700.0;
		psEq->ppfPeakFiltBandwidthHz[n][2] = (T_FLOAT) 350.0;
		psEq->ppfPeakFiltBandwidthHz[n][3] = (T_FLOAT) 525.0;
		psEq->ppfPeakFiltBandwidthHz[n][4] = (T_FLOAT) 200.0;
		psEq->ppfPeakFiltBandwidthHz[n][5] = (T_FLOAT) 450.0;
		psEq->ppfPeakFiltBandwidthHz[n][6] = (T_FLOAT) 400.0;
		psEq->ppfPeakFiltBandwidthHz[n][7] = (T_FLOAT) 300.0;

		psEq->ppfPeakFiltGaindB[n][0] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][1] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][2] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][3] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][4] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][5] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][6] = (T_FLOAT) 0.0;
		psEq->ppfPeakFiltGaindB[n][7] = (T_FLOAT) 0.0;

		for (int k = 0; k < psEq->iNumPeakFilt; k++) {
			if (psEq->ppfPeakFiltGaindB[n][k] >= psEq->fMaxGaindB) {
				psEq->ppfPeakFiltGaindB[n][k] = psEq->fMaxGaindB;
			}
			if (psEq->ppfPeakFiltGaindB[n][k] <= psEq->fMinGaindB) {
				psEq->ppfPeakFiltGaindB[n][k] = psEq->fMinGaindB;
			}
		}
	}
}

/*---------------------------------------------------------------------------------*
* Process function of the allpass based equalization
*---------------------------------------------------------------------------------*/
void HsabAlgCoreEqProcess							//process funktion of the equalizer
	(T_FLOAT				  **ppfInBuffer,		//input/output buffer
	tHsabAlgCoreEq            *psEq,				//struct of equalizer
	tHsabAlgCoreParameters    *psParams)			//struct of parameters

{

	T_INT   k, n, m;	//counter varibles
	T_FLOAT fAllpassAlpha, fAllpassBeta, fAllpassIn1, fAllpassIn2, fAllpassOut1, fAllpassOut2, fAllpassTmp, fAllpassGain, fAllpassOutTemp, fV0;
	
	if (psEq->iEqualizerActivation == 1)
	{
		//-------------------------------------------------------------------------------
		// Set gain factor   /   apply gain factor to OutBuffer
		//-------------------------------------------------------------------------------
		psEq->fGainMaster = (T_FLOAT)pow((T_FLOAT) 10.0, ((T_FLOAT)psEq->fGainMasterdB / (T_FLOAT) 20.0));
		psEq->fGainMaster *= psEq->fMasterVolume;
		for (n = 0; n < psEq->iNumChannels; n++)
		{
			fAllpassGain = (T_FLOAT)(psEq->fGainMaster * psEq->pfGainLsp[n]);
			for (m = 0; m < psParams->iFrameshiftInt; m++)
			{
				ppfInBuffer[n][m] *= fAllpassGain;
			}
		}
	
		//------------------------------------------------------------------------------
		//Initialisation of low shelv parameters
		//------------------------------------------------------------------------------
		for (n = 0; n<psEq->iNumChannels; n++)
		{
			if (psEq->pfLowShelvGaindB[n] == (T_FLOAT) 0.0)
			{
				psEq->piLowShelvActivation[n] = (T_INT)0;
			}
			else
			{
				psEq->piLowShelvActivation[n] = (T_INT)1;
			}
		}
		for (n = 0; n<psEq->iNumChannels; n++)
		{
			// V
			fV0 = (T_FLOAT)pow((T_FLOAT) 10.0, (psEq->pfLowShelvGaindB[n] / (T_FLOAT) 20.0));
			psEq->pfLowShelvGain[n] = (T_FLOAT) 0.5 * (fV0 - (T_FLOAT) 1.0); //pfLowShelvGain replaces gamma

			// a
			if (fV0 >= (T_FLOAT) 1.0)
			{
				psEq->pfLowShelvAllpassAlpha[n] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->pfLowShelvBandwidthHz[n] / psParams->fSampleRateInt) - (T_FLOAT) 1.0)
				/ ((T_FLOAT)tan((T_FLOAT)PI * psEq->pfLowShelvBandwidthHz[n] / psParams->fSampleRateInt) + (T_FLOAT) 1.0);
			}
			else
			{
				psEq->pfLowShelvAllpassAlpha[n] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->pfLowShelvBandwidthHz[n] / psParams->fSampleRateInt) - fV0) /
					((T_FLOAT)tan((T_FLOAT)PI * psEq->pfLowShelvBandwidthHz[n] / psParams->fSampleRateInt) + fV0);
			}
		}
		
		//-------------------------------------------------------------------------------
		// Apply low shelving filter
		//---------------------------------------------------------------------------------
		for (n = 0; n < psEq->iNumChannels; n++)
		{
			//----------------------------------------------------------------------------
			// Set local parameters
			//----------------------------------------------------------------------------
			fAllpassAlpha = psEq->pfLowShelvAllpassAlpha[n];
			fAllpassOut1 = psEq->pfLowShelvAllpassMemOutput[n];
			fAllpassIn1 = psEq->pfLowShelvAllpassMemInput[n];
			fAllpassGain = psEq->pfLowShelvGain[n];

			//----------------------------------------------------------------------------
			// Compute low shelving filter only if gain differs from 0 dB
			//----------------------------------------------------------------------------
			if (((fAllpassGain > psEq->fGainActMax) || (fAllpassGain < psEq->fGainActMin))
				&& (psEq->piLowShelvActivation[n] == (T_INT)1))
			{
				//-------------------------------------------------------------------------
				// Loop over all samples in current frame
				//-------------------------------------------------------------------------
				for (m = 0; m < psParams->iFrameshiftInt; m++)
				{
					//----------------------------------------------------------------------
					// Compute allpass output
					//----------------------------------------------------------------------
					fAllpassOut1 = (ppfInBuffer[n][m] - fAllpassOut1) * fAllpassAlpha + fAllpassIn1;

					//----------------------------------------------------------------------
					// Store new input
					//----------------------------------------------------------------------
					fAllpassIn1 = ppfInBuffer[n][m];

					//----------------------------------------------------------------------
					// Compute output
					//----------------------------------------------------------------------
					ppfInBuffer[n][m] += fAllpassGain * (fAllpassIn1 + fAllpassOut1);
				}

				//-------------------------------------------------------------------------
				// Store local parameters
				//-------------------------------------------------------------------------
				psEq->pfLowShelvAllpassMemOutput[n] = fAllpassOut1;
				psEq->pfLowShelvAllpassMemInput[n] = fAllpassIn1;
			}
		}

		//-----------------------------------------------------------------------------
		//Initalisation of high shelv parameters
		//-----------------------------------------------------------------------------
		for (n = 0; n<psEq->iNumChannels; n++) {
			if (psEq->pfHighShelvGaindB[n] == (T_FLOAT) 0.0) {
				psEq->piHighShelvActivation[n] = (T_INT)0;
			}
			else {
				psEq->piHighShelvActivation[n] = (T_INT)1;
			}
		}

		for (n = 0; n<psEq->iNumChannels; n++) {
			// V
			fV0 = (T_FLOAT)pow((T_FLOAT) 10.0, (psEq->pfHighShelvGaindB[n] / (T_FLOAT) 20.0));
			psEq->pfHighShelvGain[n] = (T_FLOAT) 0.5 * (fV0 - (T_FLOAT) 1.0); // pfHighShelvGain replaces gamma

			// a
			if (fV0 >= (T_FLOAT) 1.0) {
				psEq->pfHighShelvAllpassAlpha[n] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->pfHighShelvBandwidthHz[n] / psParams->fSampleRateInt) - (T_FLOAT) 1.0)
				/ ((T_FLOAT)tan((T_FLOAT)PI * psEq->pfHighShelvBandwidthHz[n] / psParams->fSampleRateInt) + (T_FLOAT) 1.0);
			}
			else {
				psEq->pfHighShelvAllpassAlpha[n] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->pfHighShelvBandwidthHz[n] / psParams->fSampleRateInt) - fV0) /
				((T_FLOAT)tan((T_FLOAT)PI * psEq->pfHighShelvBandwidthHz[n] / psParams->fSampleRateInt) + fV0);
			}
		}

		//-------------------------------------------------------------------------------
		// Apply high shelving filter
		//---------------------------------------------------------------------------------
		for (n = 0; n < psEq->iNumChannels; n++)
		{
			//----------------------------------------------------------------------------
			// Set local parameters
			//----------------------------------------------------------------------------
			fAllpassAlpha = psEq->pfHighShelvAllpassAlpha[n];
			fAllpassOut1 = psEq->pfHighShelvAllpassMemOutput[n];
			fAllpassIn1 = psEq->pfHighShelvAllpassMemInput[n];
			fAllpassGain = psEq->pfHighShelvGain[n];

			//----------------------------------------------------------------------------
			// Compute high shelving filter only if gain differs from 0 dB
			//----------------------------------------------------------------------------
			if (((fAllpassGain > psEq->fGainActMax) || (fAllpassGain < psEq->fGainActMin))
				&& (psEq->piHighShelvActivation[n] == (T_INT)1))
			{
				//-------------------------------------------------------------------------
				// Loop over all samples in current frame
				//-------------------------------------------------------------------------
				for (m = 0; m < psParams->iFrameshiftInt; m++)
				{

					//----------------------------------------------------------------------
					// Compute allpass output
					//----------------------------------------------------------------------
					fAllpassOut1 = (fAllpassOut1 - ppfInBuffer[n][m]) * fAllpassAlpha + fAllpassIn1;

					//----------------------------------------------------------------------
					// Store new input
					//----------------------------------------------------------------------
					fAllpassIn1 = ppfInBuffer[n][m];

					//----------------------------------------------------------------------
					// Compute output
					//----------------------------------------------------------------------
					ppfInBuffer[n][m] += fAllpassGain * (fAllpassIn1 - fAllpassOut1);
				}

				//-------------------------------------------------------------------------
				// Store local parameters
				//-------------------------------------------------------------------------
				psEq->pfHighShelvAllpassMemOutput[n] = fAllpassOut1;
				psEq->pfHighShelvAllpassMemInput[n] = fAllpassIn1;
			}
		}


		//---------------------------------------------------------------------------------
		// Initialisation of peak filter parameters
		//---------------------------------------------------------------------------------
		for (n = 0;n < psEq->iNumChannels;n++) {
			for (k = 0; k < psEq->iNumPeakFilt; k++) {
				if (psEq->ppfPeakFiltGaindB[n][k] == 0.0) {
					psEq->ppiPeakFiltActivation[n][k] = (T_INT)0;
				}
				else {
					psEq->ppiPeakFiltActivation[n][k] = (T_INT)1;
				}
			}
		}

		for (n = 0; n<psEq->iNumChannels; n++)
		{
			for (k = 0; k < psEq->iNumPeakFilt;k++) {

				// V/gamma
				fV0 = (T_FLOAT)pow((T_FLOAT) 10.0, (psEq->ppfPeakFiltGaindB[n][k] / (T_FLOAT) 20.0));
				psEq->ppfPeakFiltGain[n][k] = (T_FLOAT) 0.5 * (fV0 - (T_FLOAT) 1.0); //ppfPeakfiltGain replaces gamma

				// a
				if (fV0 >= (T_FLOAT) 1.0)
				{
					psEq->ppfPeakFiltAllpassAlpha[n][k] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->ppfPeakFiltBandwidthHz[n][k] / psParams->fSampleRateInt) - (T_FLOAT) 1.0)
						/ ((T_FLOAT)tan((T_FLOAT)PI * psEq->ppfPeakFiltBandwidthHz[n][k] / psParams->fSampleRateInt) + (T_FLOAT) 1.0);
				}
				else
				{
					psEq->ppfPeakFiltAllpassAlpha[n][k] = (T_FLOAT)((T_FLOAT)tan((T_FLOAT)PI * psEq->ppfPeakFiltBandwidthHz[n][k] / psParams->fSampleRateInt) - fV0) /
						((T_FLOAT)tan((T_FLOAT)PI * psEq->ppfPeakFiltBandwidthHz[n][k] / psParams->fSampleRateInt) + fV0);
				}

				// b
				psEq->ppfPeakFiltAllpassBeta[n][k] = (T_FLOAT)(-(T_FLOAT)cos((T_FLOAT)2.0 * (T_FLOAT)PI*psEq->ppfPeakFiltCenterFreqHz[n][k] / psParams->fSampleRateInt)*((T_FLOAT)1.0 - psEq->ppfPeakFiltAllpassAlpha[n][k]));
			}
		}

		//-------------------------------------------------------------------------------
		// Apply peak filter
		//-------------------------------------------------------------------------------
		for (n = 0; n < psEq->iNumChannels; n++) {
			//--------------------------------------------------------------------------
			//Loop over all peak filters
			//--------------------------------------------------------------------------
			for (k = 0; k < psEq->iNumPeakFilt; k++) {
				//----------------------------------------------------------------------
				//Set local parameters
				//----------------------------------------------------------------------
				fAllpassAlpha = psEq->ppfPeakFiltAllpassAlpha[n][k];
				fAllpassBeta = psEq->ppfPeakFiltAllpassBeta[n][k];
				fAllpassOut1 = psEq->ppfPeakFiltAllpassMemoryOutput1[n][k];
				fAllpassOut2 = psEq->ppfPeakFiltAllpassMemoryOutput2[n][k];
				fAllpassIn1 = psEq->ppfPeakFiltAllpassMemoryInput1[n][k];
				fAllpassIn2 = psEq->ppfPeakFiltAllpassMemoryInput2[n][k];
				fAllpassGain = psEq->ppfPeakFiltGain[n][k];

				//-----------------------------------------------------------------------
				//Compute peak filter only if gain differs from 0 dB
				//-----------------------------------------------------------------------
				if (((fAllpassGain > psEq->fGainActMax) || (fAllpassGain < psEq->fGainActMin)) && (psEq->ppiPeakFiltActivation[n][k] == (T_INT)1)) {

					//-------------------------------------------------------------------
					//Loop over all samples in current frame
					//-------------------------------------------------------------------
					for (m = 0; m < psParams->iFrameshiftInt; m++) {

						//---------------------------------------------------------------
						//Temporarily store new outputs
						//---------------------------------------------------------------
						fAllpassOutTemp = fAllpassOut1;

						//---------------------------------------------------------------
						//Compute allpass output
						//---------------------------------------------------------------
						fAllpassOut1 = (fAllpassOut2 - ppfInBuffer[n][m])*fAllpassAlpha + (fAllpassIn1 - fAllpassOut1)*fAllpassBeta + fAllpassIn2;

						//---------------------------------------------------------------
						//Store new inputs
						//---------------------------------------------------------------
						fAllpassIn2 = fAllpassIn1;
						fAllpassIn1 = ppfInBuffer[n][m];

						//---------------------------------------------------------------
						//Compute output
						//---------------------------------------------------------------
						ppfInBuffer[n][m] += fAllpassGain * (fAllpassIn1 - fAllpassOut1);

						//---------------------------------------------------------------
						//Store output
						//---------------------------------------------------------------
						fAllpassOut2 = fAllpassOutTemp;
					}

					//-------------------------------------------------------------------
					//Store local parameters
					//-------------------------------------------------------------------
					psEq->ppfPeakFiltAllpassMemoryOutput1[n][k] = fAllpassOut1;
					psEq->ppfPeakFiltAllpassMemoryOutput2[n][k] = fAllpassOut2;
					psEq->ppfPeakFiltAllpassMemoryInput1[n][k] = fAllpassIn1;
					psEq->ppfPeakFiltAllpassMemoryInput2[n][k] = fAllpassIn2;
				}
			}
		}

	}
}

/*---------------------------------------------------------------------------------*
* Deinitialization of allocated memory
*---------------------------------------------------------------------------------*/
void HsabAlgCoreEqDeInit(tHsabAlgCoreEq *psEq,
						 tHsabAlgCoreMemManagement  *psMemManagement)
{
	/*-------------------------------------------------------------------------------*
	* Gain control
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree1DArray((void **)&(psEq->pfGainLspdB),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfGainLsp),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	/*-------------------------------------------------------------------------------*
	* Parameters of the low shelving filters
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvGaindB),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvBandwidthHz),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->piLowShelvActivation),
		psEq->iNumChannels,
		sizeof(T_INT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvGain),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvAllpassAlpha),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvAllpassMemInput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfLowShelvAllpassMemOutput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	//--------------------------------------------------------------------------------
	//Parameters of high shelving filters
	//--------------------------------------------------------------------------------
	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvGaindB),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvBandwidthHz),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->piHighShelvActivation),
		psEq->iNumChannels,
		sizeof(T_INT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvGain),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvAllpassAlpha),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvAllpassMemInput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree1DArray((void **)&(psEq->pfHighShelvAllpassMemOutput),
		psEq->iNumChannels,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	/*-------------------------------------------------------------------------------*
	* Parameters of peak filters
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltCenterFreqHz),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));
	
	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltGaindB),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltBandwidthHz),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppiPeakFiltActivation),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_INT),
		sizeof(T_INT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltGain),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassAlpha),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassBeta),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryInput1),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryInput2),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryOutput1),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));

	HsabAlgCoreFree2DArray((void ***)&(psEq->ppfPeakFiltAllpassMemoryOutput2),
		psEq->iNumChannels,
		psEq->iNumPeakFilt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesEq));
}