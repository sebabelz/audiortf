/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_core_hsab_defines.h"
#include "defines_hsab.h"


/*---------------------------------------------------------------------------------*
 * Initialization
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreParametersInit( tHsabAlgCoreParameters    *Params,
                                tHsabAlgCoreMemManagement *MemManagement )
{
	T_INT iError = (T_INT) 0;

	/*-------------------------------------------------------------------------------*
	* Main control
	*-------------------------------------------------------------------------------*/
	Params->iMainControl = (T_INT) HSAB_ON;

	/*-------------------------------------------------------------------------------*
	* Sample rate and frameshift
	*-------------------------------------------------------------------------------*/
	Params->iSampleRateExt   =  (T_INT)48000;
	Params->fSampleRateExt   =  (T_FLOAT)Params->iSampleRateExt;
	Params->iUpDownSamplFac  =  (T_INT)1;
	Params->fSampleRateInt   =  Params->fSampleRateExt / (T_FLOAT)Params->iUpDownSamplFac;
	Params->iSampleRateInt   =  (T_INT)Params->fSampleRateInt;
	Params->iFrameshiftExt   =  (T_INT)64;
	Params->iFrameshiftInt   =  (T_INT)((T_FLOAT)Params->iFrameshiftExt / (T_FLOAT)Params->iUpDownSamplFac);

	/*-------------------------------------------------------------------------------*
	* FFT size for the signal enhancement
	*-------------------------------------------------------------------------------*/
	Params->iFftOrder = (T_INT) 256;

	/*-------------------------------------------------------------------------------*
	* Set amount of input channels
	*-------------------------------------------------------------------------------*/
	Params->iNumMicInFrontSeat = (T_INT) 2;
	Params->iNumMicInRearSeat  = (T_INT) 2;
	Params->iNumAllIn		   = Params->iNumMicInFrontSeat + Params->iNumMicInRearSeat;

   /*-------------------------------------------------------------------------------*
	* Set amount of output channels
	*-------------------------------------------------------------------------------*/
	Params->iNumLspOutFrontSeat = (T_INT) 2;
	Params->iNumLspOutRearSeat  = (T_INT) 2;
	Params->iNumAllOut = Params->iNumLspOutFrontSeat + Params->iNumLspOutRearSeat;

   /*-------------------------------------------------------------------------------*
	* Set number of supported seats
	*-------------------------------------------------------------------------------*/
	Params->iNumFrontSeats = (T_INT) 2;
	Params->iNumRearSeats  = (T_INT) 2;

   /*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;

}


