/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_SIGNALS_H
#define ALG_CORE_HSAB_SIGNALS_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Signals 
 *---------------------------------------------------------------------------------*/

typedef struct { 

	/*-------------------------------------------------------------------------------*
	* External microphone inputs
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfMicInFrontSeat;
	T_FLOAT **ppfMicInRearSeat;

	/*-------------------------------------------------------------------------------*
	* External loudspeaker outputs
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfLspOutFrontSeat;
	T_FLOAT **ppfLspOutRearSeat;

	/*-------------------------------------------------------------------------------*
	* Internal inputs for the ICC after sample rate conversion
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfMicInAlgCoreFrontSeat;
	T_FLOAT **ppfMicInAlgCoreRearSeat;

	/*-------------------------------------------------------------------------------*
	* Internal outputs for the ICC before sample rate conversion
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfLspOutAlgCoreFrontSeat;
	T_FLOAT **ppfLspOutAlgCoreRearSeat;

	/*-------------------------------------------------------------------------------*
	* Outputs after synthesis filterbank (front seats) 
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfSynFbAlgCoreFrontSeat;

	/*-------------------------------------------------------------------------------*
	* Frame counter
	*-------------------------------------------------------------------------------*/
	T_INT iFrameCounter;

	/*-------------------------------------------------------------------------------*
	* Spectrum Microphone Input
	*-------------------------------------------------------------------------------*/
	T_COMPLEX **ppCmplxHsabSpecMicInRearSeat;
	T_COMPLEX **ppCmplxHsabSpecMicInFrontSeat;

} tHsabAlgCoreSignals; 

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */