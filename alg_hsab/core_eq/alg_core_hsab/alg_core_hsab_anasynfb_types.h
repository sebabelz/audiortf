/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* edited by Manuel Diener
* (c) 2016-2019
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_ANASYNFB_TYPES_H
#define ALG_CORE_HSAB_ANASYNFB_TYPES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_pal_hsab_fft_data.h"

/*-------------------------------------------------------------------------------*
* Parameters and variables of the analysis filterbank
*-------------------------------------------------------------------------------*/
typedef struct {
	/*---------------------------------------------------------------------------*
	* Main parameters
	*----------------------------------------------------------------------------*/
	T_INT		iMainControl;
	T_INT		iNumChannels;
	T_INT		iFftOrder;

	/*---------------------------------------------------------------------------*
	* Input buffer
	*----------------------------------------------------------------------------*/
	T_FLOAT		**ppfInputBuffer;
	T_FLOAT		**ppfOutputBuffer;

	/*-------------------------------------------------------------------------------*
	* Fft Data
	*-------------------------------------------------------------------------------*/
	tsFFTData	psFFTData;

}tHsabAlgCoreAnaSynFb;

/*---------------------------------------------------------------------------------*
* Parameters and variables of the equalizer
*---------------------------------------------------------------------------------*/
typedef struct
{

	/*-------------------------------------------------------------------------------*
	* Main control
	*-------------------------------------------------------------------------------*/
	T_INT   iMainControl;
	T_INT   iNumPeakFilt;	//Number of Peak Filter; adjust default parameters in alg_core_hsab_eq.c if changed
	T_INT   iNumChannels;	//Number of Audio Channels; adjust default parameters in alg_core_hsab_eq.c if changed
	T_FLOAT fGainActMax;
	T_FLOAT fGainActMin;
	T_FLOAT fMaxGaindB;		//Max Gain in dB that can be adjusted
	T_FLOAT fMinGaindB;		//Min Gain in dB that can be adjusted

	/*-------------------------------------------------------------------------------*
	* Gain settings
	*-------------------------------------------------------------------------------*/
	T_FLOAT *pfGainLspdB;
	T_FLOAT *pfGainLsp;
	T_FLOAT fGainMasterdB;	//max gain of all filters in dB
	T_FLOAT fGainMaster;	//max gain of all filters
	T_FLOAT fMasterVolume;	//max gain of all filters in percent

	/*-------------------------------------------------------------------------------*
	* Low shelving filters
	*-------------------------------------------------------------------------------*/
	T_FLOAT *pfLowShelvGaindB;				//gain of low shelving filter in dB
	T_FLOAT *pfLowShelvBandwidthHz;			//bandwidth of low shelving filter in Hz
	T_INT   *piLowShelvActivation;			//activation variable of low shelving filter
	T_FLOAT *pfLowShelvGain;				//gain of low shelving filter
	T_FLOAT *pfLowShelvAllpassAlpha;		//parameter alpha of low shelving filter
	T_FLOAT *pfLowShelvAllpassMemInput;		//input storage of low shelving filter
	T_FLOAT *pfLowShelvAllpassMemOutput;	//output storage of low shelving filter

	/*-------------------------------------------------------------------------------*
	* High shelving filters
	*-------------------------------------------------------------------------------*/
	T_FLOAT *pfHighShelvGaindB;				//gain of high shelving filter in dB
	T_FLOAT *pfHighShelvBandwidthHz;		//bandwidth of high shelving filter in Hz
	T_INT   *piHighShelvActivation;			//activation variable of high shelving filter
	T_FLOAT *pfHighShelvGain;				//gain of high shelving filter
	T_FLOAT *pfHighShelvAllpassAlpha;		//parameter alpha of low shelving filter
	T_FLOAT *pfHighShelvAllpassMemInput;	//input storage of high shelving filter
	T_FLOAT *pfHighShelvAllpassMemOutput;	//output storage of high shelving filter

	/*-------------------------------------------------------------------------------*
	* Peak filters
	*-------------------------------------------------------------------------------*/
	T_FLOAT **ppfPeakFiltGaindB;			//gain of peak filter in dB
	T_FLOAT **ppfPeakFiltBandwidthHz;		//bandwidth of peak filter in Hz
	T_INT   **ppiPeakFiltActivation;		//activation variable of peak filter
	T_FLOAT **ppfPeakFiltCenterFreqHz;		//center frequency of peak filter in Hz

	/*-----------------------------------------------------------------------*
	* Peak filters: varibles and parameters
	*-----------------------------------------------------------------------*/
	T_FLOAT **ppfPeakFiltAllpassAlpha;			//parameter alpha of peak filter
	T_FLOAT **ppfPeakFiltAllpassBeta;			//parameter beta of peak filter
	T_FLOAT **ppfPeakFiltAllpassMemoryOutput1;	//output storage 1 of peak filter
	T_FLOAT **ppfPeakFiltAllpassMemoryOutput2;	//output storage 2 of peak filter
	T_FLOAT **ppfPeakFiltAllpassMemoryInput1;	//input storage 1 of peak filter
	T_FLOAT **ppfPeakFiltAllpassMemoryInput2;	//input storage 1 of peak filter
	T_FLOAT **ppfPeakFiltGain;
	
	//--------------------------------------------------------------------------
	//Filter activatio varible
	//--------------------------------------------------------------------------
	T_INT   iEqualizerActivation;			//activation variable of equalizer

}tHsabAlgCoreEq;

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */