/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_PARAMETERS_H
#define ALG_CORE_HSAB_PARAMETERS_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */
   
#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Parameters
 *---------------------------------------------------------------------------------*/
typedef struct {

	/*-------------------------------------------------------------------------------*
	* Main control
	*-------------------------------------------------------------------------------*/
	T_INT   iMainControl;

	/*-------------------------------------------------------------------------------*
	* Number of input channels
	*-------------------------------------------------------------------------------*/
	T_INT   iNumMicInFrontSeat;
	T_INT   iNumMicInRearSeat;
	T_INT   iNumAllIn;

	/*-------------------------------------------------------------------------------*
	* Number of output channels
	*-------------------------------------------------------------------------------*/
	T_INT   iNumLspOutFrontSeat;
	T_INT   iNumLspOutRearSeat;
	T_INT   iNumAllOut;

	/*-------------------------------------------------------------------------------*
	* Number of supported seats
	*-------------------------------------------------------------------------------*/
	T_INT   iNumFrontSeats;
	T_INT   iNumRearSeats;

	/*-------------------------------------------------------------------------------*
	* Sample rate and frameshift (in- and external)
	*-------------------------------------------------------------------------------*/
	T_INT   iSampleRateExt;
	T_FLOAT fSampleRateExt;
	T_INT   iFrameshiftExt;
	T_INT   iUpDownSamplFac;
	T_INT   iSampleRateInt;
	T_FLOAT fSampleRateInt;
	T_INT   iFrameshiftInt;

	/*-------------------------------------------------------------------------------*
	* FFT size for the signal enhancement structures
	*-------------------------------------------------------------------------------*/
	T_INT   iFftOrder;


} tHsabAlgCoreParameters;



#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */