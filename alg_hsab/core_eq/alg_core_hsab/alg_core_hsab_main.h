/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_MAIN_H
#define ALG_CORE_HSAB_MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_anasynfb.h"

/*---------------------------------------------------------------------------------*
 * Main 
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreInit(	tHsabAlgCoreMain *Main);

void HsabAlgCoreDeInit(	tHsabAlgCoreMain *Main);

void HsabAlgCoreProcess(tHsabAlgCoreMain *psMain);

void HsabAlgCoreReset( tHsabAlgCoreMain *Main );

/*---------------------------------------------------------------------------------*
 * Parameters
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreParametersInit( tHsabAlgCoreParameters    *Params,
                                tHsabAlgCoreMemManagement *MemManagement );

/*---------------------------------------------------------------------------------*
 * Signals
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSignalsInit( tHsabAlgCoreSignals       *Signals,
                             tHsabAlgCoreParameters    *Parameters,
                             tHsabAlgCoreMemManagement *MemManagement );

void HsabAlgCoreSignalsReset( tHsabAlgCoreSignals    *Signals,
                              tHsabAlgCoreParameters *Parameters );

void HsabAlgCoreSignalsDeInit( tHsabAlgCoreSignals       *Signals,
                               tHsabAlgCoreParameters    *Parameters,
                               tHsabAlgCoreMemManagement *MemManagement );

/*-------------------------------------------------------------------------------*
* Downsampling of the Microphone Input
*-------------------------------------------------------------------------------*/
void HsabAlgCoreMainDs(	T_FLOAT **ppfMicIn,
						T_FLOAT **ppfMicOut,
						T_INT iNumSeat, // for different front and rear seat numbers
						tHsabAlgCoreParameters *Params);

/*-------------------------------------------------------------------------------*
* Upsampling of the Loutspeaker Output
*-------------------------------------------------------------------------------*/
void HsabAlgCoreMainUs(	T_FLOAT **ppfLspIn,
						T_FLOAT **ppfLspOut,
						T_INT iNumSeat, // for different front and rear seat numbers
						tHsabAlgCoreParameters *Params);


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
