/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2018
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_EQ_H
#define AL8G_CORE_HSAB_EQ_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */


#include "alg_core_hsab_defines.h"
#include "types_hsab.h"
#include "alg_pal_hsab_fft.h"









	void HsabAlgCoreEqInit(tHsabAlgCoreEq            *psEq,
		tHsabAlgCoreParameters    *psParams,
		T_INT                      iNumLoudSpeak,
		tHsabAlgCoreMemManagement *psMemManagement);

	/*---------------------------------------------------------------------------------*
	* Process function of the allpass based equalization
	*---------------------------------------------------------------------------------*/
	void HsabAlgCoreEqProcess(T_FLOAT				   **ppfInOutBuffer,
		tHsabAlgCoreEq            *psEq,
		tHsabAlgCoreParameters    *psParams);


	/*---------------------------------------------------------------------------------*
	* Deinitialization of allocated memory
	*---------------------------------------------------------------------------------*/
	void HsabAlgCoreEqDeInit(tHsabAlgCoreEq *psEq,
		tHsabAlgCoreMemManagement  *psMemManagement);



#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */