/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_core_hsab_anasynfb.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_memory_allocation.h"
#include <math.h>


/*-------------------------------------------------------------------------------*
* Init of the analysis filterbank
* Function parameters:
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psMemManagement:	Pointer to memory-management struct
*  - iNumChannels:		Number of microphone channels
*  - iFftOrder:			size of FFT
*-------------------------------------------------------------------------------*/
T_INT HsabAlgCoreAnaSynFbInit(	tHsabAlgCoreAnaSynFb *psAnaSynFb,
								tHsabAlgCoreMemManagement *psMemManagement,
								T_INT iNumChannels,
								T_INT iFftOrder)
{
	
	T_INT iError = (T_INT)0;

	//Basic parameters
	psAnaSynFb->iMainControl = (T_INT)HSAB_ON;
	psAnaSynFb->iNumChannels = iNumChannels;
	psAnaSynFb->iFftOrder = iFftOrder;

	//Memory for storing the time-domain input data
	if ((T_INT)0 == iError){

		iError = HsabAlgCoreCalloc2DArray((void ***)&(psAnaSynFb->ppfInputBuffer),
			psAnaSynFb->iNumChannels,
			psAnaSynFb->iFftOrder,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"psAnaSynFb",
			"ppfInputBuffer",
			&(psMemManagement->iCurrentDynMemInBytes),
			&(psMemManagement->iMaxDynMemInBytes),
			&(psMemManagement->iCurrentDynMemInBytesAnaSynFb),
			&(psMemManagement->iMaxDynMemInBytesAnaSynFb));
	}

	if ((T_INT)0 == iError){

		iError = HsabAlgCoreCalloc2DArray((void ***)&(psAnaSynFb->ppfOutputBuffer),
			psAnaSynFb->iNumChannels,
			psAnaSynFb->iFftOrder,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"psAnaSynFb",
			"ppfInputBuffer",
			&(psMemManagement->iCurrentDynMemInBytes),
			&(psMemManagement->iMaxDynMemInBytes),
			&(psMemManagement->iCurrentDynMemInBytesAnaSynFb),
			&(psMemManagement->iMaxDynMemInBytesAnaSynFb));
	
	}

	if ((T_INT)1 == iError)	return iError;

	//Init function for real to complex or complex to real FFT				
	HsabPalInitReal2ComplexFFT(psAnaSynFb->iFftOrder,
		&(psAnaSynFb->psFFTData));

	return iError;

}

/*-------------------------------------------------------------------------------*
* Main process of the analysis filterbank
* Function parameters:
*  - ppfMicIn:			incoming Microphone signal
*  - ppcOutput:			Complex FFT Output
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psParams:			Pointer to parameters struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbProcessAna(	T_FLOAT **ppfMicIn,
									T_COMPLEX **ppcOutput,
									tHsabAlgCoreAnaSynFb *psAnaSynFb,
									tHsabAlgCoreParameters *psParams)
{
	T_INT k, m, n;
	//Update Input buffer and perform FFT
	for (k = 0; k < psAnaSynFb->iNumChannels; k++)//switch channels
	{
		//Move old data
		for (m = 0, n = psParams->iFrameshiftInt; n < psAnaSynFb->iFftOrder; m++, n++)
		{
			psAnaSynFb->ppfInputBuffer[k][m] = psAnaSynFb->ppfInputBuffer[k][n];	//data in n is moved to m
		}

		//add new data
		psAnaSynFb->ppfInputBuffer[k][m] = ppfMicIn[k][0];

		for (m++, n = 1; m < psAnaSynFb->iFftOrder; m++, n++)	
		{
			psAnaSynFb->ppfInputBuffer[k][m] = ppfMicIn[k][n];
		}

		//compute FFT
		HsabPalReal2ComplexFFT(	psAnaSynFb->ppfInputBuffer[k],	
								&(psAnaSynFb->psFFTData),
								ppcOutput[k]);				
	}
}

/*-------------------------------------------------------------------------------*
* Main process of the synthesis filterbank
* Function parameters:
*  - ppfLspOut:			processed Loudspeaker output
*  - ppcInput:			Complex IFFT Input
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psParams:			Pointer to parameters struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbProcessSyn(	T_FLOAT **ppfLspOut,
									T_COMPLEX **ppcInput,
									tHsabAlgCoreAnaSynFb *psAnaSynFb,
									tHsabAlgCoreParameters *psParams)
{
	T_INT m, k, n;

	for (k = 0; k < psAnaSynFb->iNumChannels; k++)//switch channels
	{
		//compute IFFT
		HsabPalComplex2RealIFFT(ppcInput[k],
			&(psAnaSynFb->psFFTData),
			psAnaSynFb->ppfOutputBuffer[k]);

		//add data to output
		for (m = 0, n = psAnaSynFb->iFftOrder - psParams->iFrameshiftInt; m < psParams->iFrameshiftInt; m++, n++)
		{
			ppfLspOut[k][m] = psAnaSynFb->ppfOutputBuffer[k][n];
		}
	}
}

/*-------------------------------------------------------------------------------*
* Deinitialization of the analysis filterbank
* Function parameters:
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psMemManagement:	Pointer to memory-management struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbDeInit(	tHsabAlgCoreAnaSynFb *psAnaSynFb,
								tHsabAlgCoreMemManagement *psMemManagement)
{
	//Memory for storing the time-domain input data
	
	HsabAlgCoreFree2DArray(	(void***)&psAnaSynFb->ppfInputBuffer,
							psAnaSynFb->iNumChannels,
							psAnaSynFb->iFftOrder,
							sizeof(T_FLOAT),
							sizeof(T_FLOAT*),
							&(psMemManagement->iCurrentDynMemInBytes),
							&(psMemManagement->iCurrentDynMemInBytesAnaSynFb));

	HsabAlgCoreFree2DArray(	(void***)&psAnaSynFb->ppfOutputBuffer,
							psAnaSynFb->iNumChannels,
							psAnaSynFb->iFftOrder,
							sizeof(T_FLOAT),
							sizeof(T_FLOAT*),
							&(psMemManagement->iCurrentDynMemInBytes),
							&(psMemManagement->iCurrentDynMemInBytesAnaSynFb));
							
	//Deinit function for real to complex or complex to real FFT				
	HsabPalDeInitReal2ComplexFFT(&(psAnaSynFb->psFFTData));					

}
