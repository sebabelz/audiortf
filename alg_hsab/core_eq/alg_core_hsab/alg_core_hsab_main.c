
/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_main.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_anasynfb.h"
#include "alg_core_hsab_eq.h"
#include "alg_pal_hsab_fft.h"
#include "alg_core_hsab_memory_allocation.h"
#include <stdio.h>

/*---------------------------------------------------------------------------------*
 * Main init function
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreInit(	tHsabAlgCoreMain *Main)
{
   
  /*-------------------------------------------------------------------------------*
   * Memory management
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreInitMemMangement( &(Main->HsabAlgCoreMemManagement) );

  /*-------------------------------------------------------------------------------*
   * Parameters                                                       
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreParametersInit( &(Main->HsabAlgCoreParams),
                              &(Main->HsabAlgCoreMemManagement) );

   /*-------------------------------------------------------------------------------*
   * Signals
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreSignalsInit( &(Main->HsabAlgCoreSignals), 
                           &(Main->HsabAlgCoreParams),
                           &(Main->HsabAlgCoreMemManagement) );

   /*-------------------------------------------------------------------------------*
   * Init of the analysis filterbank for front seats (overlap-save)
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbInit(	&(Main->HsabAlgCoreAnaSynFbFs),
							&(Main->HsabAlgCoreMemManagement),
							Main->HsabAlgCoreParams.iNumFrontSeats,
							Main->HsabAlgCoreParams.iFftOrder);

   /*-------------------------------------------------------------------------------*
   * Init of the analysis filterbank for rear seats (overlap-save)
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbInit(	&(Main->HsabAlgCoreAnaSynFbRs),
							&(Main->HsabAlgCoreMemManagement),
							Main->HsabAlgCoreParams.iNumRearSeats,
							Main->HsabAlgCoreParams.iFftOrder);


   /*-------------------------------------------------------------------------------*
   * Equalizer - front to rear
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreEqInit( &(Main->HsabAlgCoreEqFs),
					  &(Main->HsabAlgCoreParams),
					  Main->HsabAlgCoreParams.iNumLspOutFrontSeat,
					  &(Main->HsabAlgCoreMemManagement) );


   /*-------------------------------------------------------------------------------*
   * Equalizer - rear to front
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreEqInit(&(Main->HsabAlgCoreEqRs),
	   &(Main->HsabAlgCoreParams),
	   Main->HsabAlgCoreParams.iNumLspOutRearSeat,
	   &(Main->HsabAlgCoreMemManagement));
}

/*---------------------------------------------------------------------------------*
 * Main reset function
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreReset( tHsabAlgCoreMain *Main )
{

}

/*---------------------------------------------------------------------------------*
 * Main algorithmic process function
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreProcess(tHsabAlgCoreMain* psMain)				
{
	
   T_INT k;

   /*-------------------------------------------------------------------------------*
   * Main process of the analysis filterbank - front to rear
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbProcessAna(	psMain->HsabAlgCoreSignals.ppfMicInAlgCoreFrontSeat,
									psMain->HsabAlgCoreSignals.ppCmplxHsabSpecMicInFrontSeat,
									&(psMain->HsabAlgCoreAnaSynFbFs),
									&(psMain->HsabAlgCoreParams));
									
   /*-------------------------------------------------------------------------------*
   * Main process of the synthesis filterbank - front to rear
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbProcessSyn(psMain->HsabAlgCoreSignals.ppfLspOutAlgCoreFrontSeat,
									psMain->HsabAlgCoreSignals.ppCmplxHsabSpecMicInFrontSeat,
									&(psMain->HsabAlgCoreAnaSynFbFs),
									&(psMain->HsabAlgCoreParams));


   /*-------------------------------------------------------------------------------*
   * Postprocessing - front to rear
   *-------------------------------------------------------------------------------*/
   if ((T_INT)HSAB_ON == psMain->HsabAlgCoreEqFs.iMainControl)
   {
	   /*----------------------------------------------------------------------------*
	   * Equalizer
	   *----------------------------------------------------------------------------*/
	   HsabAlgCoreEqProcess(psMain->HsabAlgCoreSignals.ppfLspOutAlgCoreFrontSeat,
							   &(psMain->HsabAlgCoreEqFs),
							   &(psMain->HsabAlgCoreParams));

   }



   /*-------------------------------------------------------------------------------*
   * Main process of the analysis filterbank - rear to front
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbProcessAna(psMain->HsabAlgCoreSignals.ppfMicInAlgCoreRearSeat,
	   psMain->HsabAlgCoreSignals.ppCmplxHsabSpecMicInRearSeat,
	   &(psMain->HsabAlgCoreAnaSynFbRs),
	   &(psMain->HsabAlgCoreParams));

	/*-------------------------------------------------------------------------------*
	* Main process of the synthesis filterbank - rear to front
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreAnaSynFbProcessSyn(	psMain->HsabAlgCoreSignals.ppfLspOutAlgCoreRearSeat,
									psMain->HsabAlgCoreSignals.ppCmplxHsabSpecMicInRearSeat,
									&(psMain->HsabAlgCoreAnaSynFbRs),
									&(psMain->HsabAlgCoreParams));


	/*-------------------------------------------------------------------------------*
   * Postprocessing - rear to front
   *-------------------------------------------------------------------------------*/
	if ((T_INT)HSAB_ON == psMain->HsabAlgCoreEqRs.iMainControl)
	{
		/*----------------------------------------------------------------------------*
		* Equalizer
		*----------------------------------------------------------------------------*/
		HsabAlgCoreEqProcess(psMain->HsabAlgCoreSignals.ppfLspOutAlgCoreRearSeat,
			&(psMain->HsabAlgCoreEqRs),
			&(psMain->HsabAlgCoreParams));

	}

}

/*---------------------------------------------------------------------------------*
 * Main deinit function
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreDeInit(	tHsabAlgCoreMain *Main)
{

   /*-------------------------------------------------------------------------------*
   * Deinitialization of the signal structure
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreSignalsDeInit( &(Main->HsabAlgCoreSignals), 
                             &(Main->HsabAlgCoreParams),
                             &(Main->HsabAlgCoreMemManagement) );
   /*-------------------------------------------------------------------------------*
   * Deinitialization of the analysis filterbank front seats
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbDeInit(	&(Main->HsabAlgCoreAnaSynFbFs),
								&(Main->HsabAlgCoreMemManagement) );

   /*-------------------------------------------------------------------------------*
   * Deinitialization of the analysis filterbank rear seats
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreAnaSynFbDeInit(	&(Main->HsabAlgCoreAnaSynFbRs),
								&(Main->HsabAlgCoreMemManagement) );

  /*-------------------------------------------------------------------------------*
   * Deinitialization of the equalizer - front to rear
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreEqDeInit( &(Main->HsabAlgCoreEqFs),
						&(Main->HsabAlgCoreMemManagement) );


   /*-------------------------------------------------------------------------------*
   * Deinitialization of the equalizer - rear to front
   *-------------------------------------------------------------------------------*/
   HsabAlgCoreEqDeInit(&(Main->HsabAlgCoreEqRs),
	   &(Main->HsabAlgCoreMemManagement));
								

}
