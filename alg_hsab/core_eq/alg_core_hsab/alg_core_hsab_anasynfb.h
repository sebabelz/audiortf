/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_ANASYNFB_H
#define ALG_CORE_HSAB_ANASYNFB_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */


#include "alg_core_hsab_defines.h"
#include "types_hsab.h"
#include "alg_pal_hsab_fft.h"




/*-------------------------------------------------------------------------------*
* Init of the analysis filterbank
* Function parameters:
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psMemManagement:	Pointer to memory-management struct
*  - iNumChannels:		Number of microphone channels
*  - iFftOrder:			size of FFT
*-------------------------------------------------------------------------------*/
T_INT HsabAlgCoreAnaSynFbInit(	tHsabAlgCoreAnaSynFb *psAnaSynFb,
								tHsabAlgCoreMemManagement *psMemManagement,
								T_INT iNumChannels,
								T_INT iFftOrder);
										
/*-------------------------------------------------------------------------------*
* Main process of the analysis filterbank
* Function parameters:
*  - ppfMicIn:			incoming Microphone signal
*  - ppcOutput:			Complex FFT Output
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psParams:			Pointer to parameters struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbProcessAna(	T_FLOAT **ppfMicIn,
									T_COMPLEX **ppcOutput,
									tHsabAlgCoreAnaSynFb *psAnaSynFb,
									tHsabAlgCoreParameters *psParams);

/*-------------------------------------------------------------------------------*
* Main process of the synthesis filterbank
* Function parameters:
*  - ppfLspOut:			processed Loudspeaker output
*  - ppcInput:			Complex IFFT Input
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psParams:			Pointer to parameters struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbProcessSyn(	T_FLOAT **ppfLspOut,
									T_COMPLEX **ppcInput,
									tHsabAlgCoreAnaSynFb *psAnaSynFb,
									tHsabAlgCoreParameters *psParams);

/*-------------------------------------------------------------------------------*
* Deinitialization of the analysis filterbank
* Function parameters:
*  - psAnaSynFb:		Pointer to analysis-synthesis-filterbank struct
*  - psMemManagement:	Pointer to memory-management struct
*-------------------------------------------------------------------------------*/
void HsabAlgCoreAnaSynFbDeInit(tHsabAlgCoreAnaSynFb *psAnaSynFb,
	tHsabAlgCoreMemManagement *psMemManagement);
								


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */