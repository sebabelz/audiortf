/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_parameters.h"
#include "alg_pal_hsab_set_vec.h"

/*---------------------------------------------------------------------------------*
 * Initialization
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSignalsInit( tHsabAlgCoreSignals       *Signals,
                             tHsabAlgCoreParameters    *Params,
                             tHsabAlgCoreMemManagement *MemManagement )
{

	T_INT iError = (T_INT)0;

	/*-------------------------------------------------------------------------------*
	* Memory managment
	*-------------------------------------------------------------------------------*/
	MemManagement->iStaticMemInBytesSignals += (T_INT) sizeof(tHsabAlgCoreSignals);

	/*-------------------------------------------------------------------------------*
	* Allocate memory for external microphone inputs and fill memory with zeros
	*-------------------------------------------------------------------------------*/
	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfMicInFrontSeat),
			Params->iNumMicInFrontSeat,
			Params->iFrameshiftExt,
			sizeof(float),
			sizeof(float*),
			"Signals",
			"ppfMicInFrontSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}


	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfMicInRearSeat),
			Params->iNumMicInRearSeat,
			Params->iFrameshiftExt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfMicInRearSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	/*-------------------------------------------------------------------------------*
	* Allocate memory for internal inputs and fill memory with zeros
	*-------------------------------------------------------------------------------*/
	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfMicInAlgCoreFrontSeat),
			Params->iNumMicInFrontSeat,
			Params->iFrameshiftInt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfMicInAlgCoreFrontSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfMicInAlgCoreRearSeat),
			Params->iNumMicInRearSeat,
			Params->iFrameshiftInt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfMicInAlgCoreRearSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	

	/*-------------------------------------------------------------------------------*
	* Allocate memory for internal outputs and fill memory with zeros
	*-------------------------------------------------------------------------------*/
	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfLspOutAlgCoreFrontSeat),
			Params->iNumLspOutFrontSeat,
			Params->iFrameshiftInt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfLspOutAlgCoreFrontSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}


	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfLspOutAlgCoreRearSeat),
			Params->iNumLspOutRearSeat,
			Params->iFrameshiftInt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfLspOutAlgCoreRearSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}



	

	/*-------------------------------------------------------------------------------*
	* Allocate memory for external outputs and fill memory with zeros
	*-------------------------------------------------------------------------------*/
	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfLspOutFrontSeat),
			Params->iNumLspOutFrontSeat,
			Params->iFrameshiftExt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfLspOutFrontSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppfLspOutRearSeat),
			Params->iNumLspOutRearSeat,
			Params->iFrameshiftExt,
			sizeof(T_FLOAT),
			sizeof(T_FLOAT*),
			"Signals",
			"ppfLspOutRearSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	/*-------------------------------------------------------------------------------*
	* Allocate memory for spectrum Microphone Input
	*-------------------------------------------------------------------------------*/
	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppCmplxHsabSpecMicInFrontSeat),
			Params->iNumMicInFrontSeat,
			Params->iFftOrder/2+1,
			sizeof(T_COMPLEX),
			sizeof(T_COMPLEX*),
			"Signals",
			"ppCmplxHsabSpecMicIn",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	if ((T_INT)0 == iError)
	{
		iError = HsabAlgCoreCalloc2DArray((void ***)&(Signals->ppCmplxHsabSpecMicInRearSeat),
			Params->iNumMicInRearSeat,
			Params->iFftOrder/2+1,
			sizeof(T_COMPLEX),
			sizeof(T_COMPLEX*),
			"Signals",
			"ppCmplxHsabSpecMicInRearSeat",
			&(MemManagement->iCurrentDynMemInBytes),
			&(MemManagement->iMaxDynMemInBytes),
			&(MemManagement->iCurrentDynMemInBytesSignals),
			&(MemManagement->iMaxDynMemInBytesSignals));
	}

	/*-------------------------------------------------------------------------------*
	* Terminate function
	*-------------------------------------------------------------------------------*/
	return iError;

}

/*---------------------------------------------------------------------------------*
 * Initialization
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSignalsReset( tHsabAlgCoreSignals    *Signals,
                              tHsabAlgCoreParameters *Params )
{
   T_INT m;

   /*-------------------------------------------------------------------------------*
   * Fill the signal arrays with zeros
   *-------------------------------------------------------------------------------*/
   for (m = 0; m<Params->iNumMicInFrontSeat; m++)
   {
	   HsabPalSetFloatVecToZero(Signals->ppfMicInFrontSeat[m], Params->iFrameshiftExt);
	   HsabPalSetFloatVecToZero(Signals->ppfMicInAlgCoreFrontSeat[m], Params->iFrameshiftInt);
   }
   for (m = 0; m<Params->iNumMicInRearSeat; m++)
   {
	   HsabPalSetFloatVecToZero(Signals->ppfMicInRearSeat[m], Params->iFrameshiftExt);
	   HsabPalSetFloatVecToZero(Signals->ppfMicInAlgCoreRearSeat[m], Params->iFrameshiftInt);
   }

   for (m = 0; m<Params->iNumLspOutFrontSeat; m++)
   {
	   HsabPalSetFloatVecToZero(Signals->ppfLspOutFrontSeat[m], Params->iFrameshiftExt);
	   HsabPalSetFloatVecToZero(Signals->ppfLspOutAlgCoreFrontSeat[m], Params->iFrameshiftInt);
   }
   for (m = 0; m<Params->iNumLspOutRearSeat; m++)
   {
	   HsabPalSetFloatVecToZero(Signals->ppfLspOutRearSeat[m], Params->iFrameshiftExt);
	   HsabPalSetFloatVecToZero(Signals->ppfLspOutAlgCoreRearSeat[m], Params->iFrameshiftInt);
   }
   for (m = 0; m < Params->iNumMicInFrontSeat; m++)															
   {
	   HsabPalSetComplexVecToZero(Signals->ppCmplxHsabSpecMicInFrontSeat[m], Params->iFftOrder);				
	   HsabPalSetComplexVecToZero(Signals->ppCmplxHsabSpecMicInRearSeat[m], Params->iFftOrder);					
   }

}

/*---------------------------------------------------------------------------------*
 * Deinitialization
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSignalsDeInit( tHsabAlgCoreSignals       *Signals,
                               tHsabAlgCoreParameters    *Params,
                               tHsabAlgCoreMemManagement *MemManagement )
{
	/*-------------------------------------------------------------------------------*
	* External microphone inputs
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfMicInFrontSeat),
		Params->iNumMicInFrontSeat,
		Params->iFrameshiftExt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfMicInRearSeat),
		Params->iNumMicInRearSeat,
		Params->iFrameshiftExt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	/*-------------------------------------------------------------------------------*
	* Internal inputs for the ICC after sample rate conversion
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfMicInAlgCoreFrontSeat),
		Params->iNumMicInFrontSeat,
		Params->iFrameshiftInt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfMicInAlgCoreRearSeat),
		Params->iNumMicInRearSeat,
		Params->iFrameshiftInt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	/*-------------------------------------------------------------------------------*
	*  Internal outputs for the ICC before sample rate conversion
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfLspOutAlgCoreFrontSeat),
		Params->iNumLspOutFrontSeat,
		Params->iFrameshiftInt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfLspOutAlgCoreRearSeat),
		Params->iNumLspOutRearSeat,
		Params->iFrameshiftInt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	/*-------------------------------------------------------------------------------*
	* Free memory for external loudspeaker outputs
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfLspOutFrontSeat),
		Params->iNumLspOutFrontSeat,
		Params->iFrameshiftExt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	HsabAlgCoreFree2DArray((void ***)&(Signals->ppfLspOutRearSeat),
		Params->iNumLspOutRearSeat,
		Params->iFrameshiftExt,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	/*-------------------------------------------------------------------------------*
	* Free memory for spectrum Microphone Input
	*-------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(Signals->ppCmplxHsabSpecMicInFrontSeat),
		Params->iNumMicInFrontSeat,
		Params->iFftOrder/2+1,
		sizeof(T_COMPLEX),
		sizeof(T_COMPLEX*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));

	HsabAlgCoreFree2DArray((void ***)&(Signals->ppCmplxHsabSpecMicInRearSeat),
		Params->iNumMicInRearSeat,
		Params->iFftOrder/2+1,
		sizeof(T_COMPLEX),
		sizeof(T_COMPLEX*),
		&(MemManagement->iCurrentDynMemInBytes),
		&(MemManagement->iCurrentDynMemInBytesSignals));
		
}