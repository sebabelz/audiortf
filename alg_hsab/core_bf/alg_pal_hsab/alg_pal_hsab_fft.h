/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_PAL_HSAB_FFT_H
#define ALG_PAL_HSAB_FFT_H

#include "alg_core_hsab_parameters.h"
#include "alg_pal_hsab_fft_data.h"

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

/*----------------------------------------------------------------------------------*
 *   Init function for complex to complex FFT   								            *
 *----------------------------------------------------------------------------------*/
void HsabPalInitComplex2ComplexFFT( T_INT iOrderFFT, 
												tsFFTData *psFFTData );

/*----------------------------------------------------------------------------------*
 *   Compute complex FFT of the complex input									            *
 *----------------------------------------------------------------------------------*/
void HsabPalComplex2ComplexFFT( T_COMPLEX  *pfInputData, 
								        tsFFTData  *psFFTData, 
									     T_COMPLEX  *pfResultData,
										  T_INT       iFlagFFT );

/*----------------------------------------------------------------------------------*
 *   Deinit function for complex to complex FFT   								            *
 *----------------------------------------------------------------------------------*/
void HsabPalDeInitComplex2ComplexFFT( tsFFTData *psFFTData );

/*----------------------------------------------------------------------------------*
 *   Init function for real to complex or complex to real FFT   							*
 *----------------------------------------------------------------------------------*/
void HsabPalInitReal2ComplexFFT( T_INT iOrderFFT,          
											tsFFTData *psFFTData );

/*----------------------------------------------------------------------------------*
 *   Compute complex FFT of the real input									               *
 *----------------------------------------------------------------------------------*/
void HsabPalReal2ComplexFFT( T_FLOAT   *pfInputData, 
									  tsFFTData *psFFTData, 
									  T_COMPLEX *pfSpecDataOut );

/*----------------------------------------------------------------------------------*
 *   Compute complex IFFT of the complex input									            *
 *----------------------------------------------------------------------------------*/
void HsabPalComplex2RealIFFT( T_COMPLEX  *pfInputData, 
								      tsFFTData  *psFFTData, 
									   T_FLOAT    *pfResultData );

/*----------------------------------------------------------------------------------*
 *   Deinit function for real to complex or complex to real FFT 			            *
 *----------------------------------------------------------------------------------*/
void HsabPalDeInitReal2ComplexFFT( tsFFTData *psFFTData );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

