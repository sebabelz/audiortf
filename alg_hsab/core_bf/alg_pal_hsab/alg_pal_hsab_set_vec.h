/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_PAL_HSAB_SET_VEC_H
#define ALG_PAL_HSAB_SET_VEC_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

/*------------------------------------------------------------------------------------------------*
 * Set float input vector to zero 
 *------------------------------------------------------------------------------------------------*/
 void HsabPalSetFloatVecToZero( T_FLOAT* fBuffer, T_INT iSize );

/*------------------------------------------------------------------------------------------------*
 * Set complex input vector to zero 
 *------------------------------------------------------------------------------------------------*/
 void HsabPalSetComplexVecToZero( T_COMPLEX* CmplxBuffer, T_INT iSize );

/*------------------------------------------------------------------------------------------------*
 * Scale input vector
 *------------------------------------------------------------------------------------------------*/
 void HsabPalScaleFloatVec( T_FLOAT* fBuffer, T_FLOAT fSacleFactor, T_INT iSize );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

