/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_SRC_H
#define ALG_CORE_HSAB_SRC_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "alg_core_hsab_src_defines.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_memory_allocation.h"

/*---------------------------------------------------------------------------------*
 * Sample rate conversion
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSrcUpDownInit( tHsabAlgCoreSrc           *psHsabAlgCoreSrc,
                               tHsabAlgCoreParameters    *psParams,    
                               tHsabAlgCoreMemManagement *psMemManagement,
                               T_INT                     iNumChannelsDown,
                               T_INT                     iNumChannelsUp );

void HsabAlgCoreSrcDownProcess( T_FLOAT               **ppfInput,
                                T_FLOAT               **ppfOutput,
                                tHsabAlgCoreSrc        *psHsabAlgCoreSrc,
                                tHsabAlgCoreParameters *psParams );

void HsabAlgCoreSrcUpProcess( T_FLOAT               **ppfInput,
                              T_FLOAT               **ppfOutput,
                              tHsabAlgCoreSrc        *psHsabAlgCoreSrc,
                              tHsabAlgCoreParameters *psParams );

void HsabAlgCoreSrcUpDownDeInit( tHsabAlgCoreSrc           *psHsabAlgCoreSrc,
                                 tHsabAlgCoreParameters    *psParams,    
                                 tHsabAlgCoreMemManagement *psMemManagement );

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
