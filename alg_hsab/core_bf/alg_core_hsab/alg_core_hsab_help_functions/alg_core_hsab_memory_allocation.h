/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_MEMORY_ALLOCATION_H
#define ALG_CORE_HSAB_MEMORY_ALLOCATION_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

/*---------------------------------------------------------------------------------*
 * Memory allocation 
 *---------------------------------------------------------------------------------*/
 void HsabAlgCoreInitMemMangement( tHsabAlgCoreMemManagement *MemManagement );

 void HsabAlgCoreAlloc1DArray( void**         ppPtr,
                               const T_INT    iDim1, 
                               const T_INT    iSize, 
                               const T_CHAR*  pcModuleName, 
                               const T_CHAR*  pcSignalName,
                               T_INT*         piCurrentMem,
                               T_INT*         piMaxMem,
                               T_INT*         piCurrentMemAlgPart,
                               T_INT*         piMaxMemAlgPart );

 void HsabAlgCoreCalloc1DArray( void**         ppPtr,
                                const T_INT    iDim1, 
                                const T_INT    iSize, 
                                const T_CHAR*  pcModuleName, 
                                const T_CHAR*  pcSignalName,
                                T_INT*         piCurrentMem,
                                T_INT*         piMaxMem,
                                T_INT*         piCurrentMemAlgPart,
                                T_INT*         piMaxMemAlgPart );

 void HsabAlgCoreAlloc2DArray( void***         pppPtr,
                               const T_INT     iDim1, 
                               const T_INT     iDim2, 
                               const T_INT     iSize,
                               const T_INT     iSizeP, 
                               const T_CHAR*   pcModuleName, 
                               const T_CHAR*   pcSignalName,
                               T_INT*          piCurrentMem,
                               T_INT*          piMaxMem,
                               T_INT*          piCurrentMemAlgPart,
                               T_INT*          piMaxMemAlgPart );

 void HsabAlgCoreCalloc2DArray( void***         pppPtr,
                                const T_INT     iDim1, 
                                const T_INT     iDim2, 
                                const T_INT     iSize,
                                const T_INT     iSizeP, 
                                const T_CHAR*   pcModuleName, 
                                const T_CHAR*   pcSignalName,
                                T_INT*          piCurrentMem,
                                T_INT*          piMaxMem,
                                T_INT*          piCurrentMemAlgPart,
                                T_INT*          piMaxMemAlgPart );

 void HsabAlgCoreAlloc3DArray( void****         ppppPtr,
                               const T_INT      iDim1, 
                               const T_INT      iDim2, 
                               const T_INT      iDim3,
                               const T_INT      iSize, 
                               const T_INT      iSizeP, 
                               const T_CHAR*    pcModuleName, 
                               const T_CHAR*    pcSignalName,
                               T_INT*           piCurrentMem,
                               T_INT*           piMaxMem,
                               T_INT*           piCurrentMemAlgPart,
                               T_INT*           piMaxMemAlgPart );

 void HsabAlgCoreCalloc3DArray( void****         ppppPtr,
                                const T_INT      iDim1, 
                                const T_INT      iDim2, 
                                const T_INT      iDim3,
                                const T_INT      iSize, 
                                const T_INT      iSizeP, 
                                const T_CHAR*    pcModuleName, 
                                const T_CHAR*    pcSignalName,
                                T_INT*           piCurrentMem,
                                T_INT*           piMaxMem,
                                T_INT*           piCurrentMemAlgPart,
                                T_INT*           piMaxMemAlgPart );

/*---------------------------------------------------------------------------------*
 * Free memory  
 *---------------------------------------------------------------------------------*/
 void HsabAlgCoreFree1DArray( void**           ppPtr,
                              const T_INT      iDim1, 
                              const T_INT      iSize, 
                              T_INT*           piCurrentMem,
                              T_INT*           piCurrentMemAlgPart ); 

 void HsabAlgCoreFree2DArray( void***          pppPtr,
                              const T_INT      iDim1, 
                              const T_INT      iDim2, 
                              const T_INT      iSize, 
                              const T_INT      iSizeP, 
                              T_INT*           piCurrentMem,
                              T_INT*           piCurrentMemAlgPart );

 void HsabAlgCoreFree3DArray( void****         ppppPtr,
                              const T_INT      iDim1, 
                              const T_INT      iDim2, 
                              const T_INT      iDim3,
                              const T_INT      iSize, 
                              const T_INT      iSizeP, 
                              T_INT*           piCurrentMem,
                              T_INT*           piCurrentMemAlgPart );

/*---------------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
