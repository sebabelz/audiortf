/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_MESSAGES_H
#define ALG_CORE_HSAB_MESSAGES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
 
/*---------------------------------------------------------------------------------*
 * Functions
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSendDebugMessage( T_CHAR* pcMessage,
                                  T_INT   iColor );

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */