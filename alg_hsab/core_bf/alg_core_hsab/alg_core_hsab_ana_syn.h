/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_ANA_SYN_H   
#define ALG_CORE_HSAB_ANA_SYN_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_ana_syn_defines.h"

/*---------------------------------------------------------------------------------*
 * Process function of the analysis filterbank
 *---------------------------------------------------------------------------------*/
 void HsabAnaFilterBankProcess( T_FLOAT                   **ppfInputSamples,
										  tHsabAlgCoreParameters     *psParams,
										  tHsabAnaSynFiltBankData    *psAnaSynFiltBankData,
										  T_COMPLEX                 **ppCmplxSpecDataOut );

/*---------------------------------------------------------------------------------*
 * Process function of the synthesis filterbank
 *---------------------------------------------------------------------------------*/
 void HsabSynFilterBankProcess( T_COMPLEX                 **ppCmplxSpecDataIn,
										  tHsabAlgCoreParameters     *psParams,
										  tHsabAnaSynFiltBankData    *psAnaSynFiltBankData,
										  T_FLOAT                   **ppfoutputSamples );

/*---------------------------------------------------------------------------------*
 * Initialization of the analysis and synthesis filterbank
 *---------------------------------------------------------------------------------*/
 void HsabAnaSynFilterBankInit( tHsabAlgCoreParameters     *psParams,
	                             tHsabAnaSynFiltBankData    *psAnaSynFiltBankData,
										  T_INT                       iNumChannelAna,
                                T_INT                       iNumChannelSyn,
									     tHsabAlgCoreMemManagement  *MemManagement );

/*---------------------------------------------------------------------------------*
 * Deinitialization of the analysis and synthesis filterbank
 *---------------------------------------------------------------------------------*/
 void HsabAnaSynFilterBankDeInit( tHsabAlgCoreParameters     *psParams,
	                               tHsabAnaSynFiltBankData    *psAnaSynFiltBankData,
									       tHsabAlgCoreMemManagement  *MemManagement );

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

