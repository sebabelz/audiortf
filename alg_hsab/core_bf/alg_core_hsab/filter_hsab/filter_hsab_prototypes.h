/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef FILTER_HSAB_PROTOTYPES_H
#define FILTER_HSAB_PROTOTYPES_H

#include "types_hsab.h"

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

  /*------------------------------------------------------------------------------------------------*
   * Prototype filter
   *------------------------------------------------------------------------------------------------*/
   const T_FLOAT fPrototypeFilter1536_fft512[];

  /*------------------------------------------------------------------------------------------------*
   * Discrete prolate sequence with frame shift 160 for synthesis filterbank
   *------------------------------------------------------------------------------------------------*/
	const T_FLOAT pfSynProlateSequenceFS160_fft512[];

  /*------------------------------------------------------------------------------------------------*
   * Discrete prolate sequence with frame shift 160 for synthesis filterbank
   *------------------------------------------------------------------------------------------------*/
	const T_FLOAT pfAnaProlateSequenceFS160_fft512[];

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

