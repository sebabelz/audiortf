/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_ANA_SYN_DEFINES_H   
#define ALG_CORE_HSAB_ANA_SYN_DEFINES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "alg_pal_hsab_fft_data.h"

/*---------------------------------------------------------------------------------*
 * Data struct for the FFT
 *---------------------------------------------------------------------------------*/
typedef struct {

  /*-------------------------------------------------------------------------------*
   * Used FFT order 
   *-------------------------------------------------------------------------------*/
   T_INT         iFFTLength;

  /*-------------------------------------------------------------------------------*
   * Real to complex FFT / complex to real FFT
   *-------------------------------------------------------------------------------*/
	tsFFTData     sReal2CmplxFFTData;

} tHsabAlgoCoreParameterFFT;


/*---------------------------------------------------------------------------------*
 * Data struct for analysis filterbank
 *---------------------------------------------------------------------------------*/
 typedef struct { 

  /*-------------------------------------------------------------------------------*
   * Intern ring buffer (saves the input samples)  
   *-------------------------------------------------------------------------------*/
   T_INT         iMainControl;

  /*-------------------------------------------------------------------------------*
   * Process mode (narrowband or wideband)  
   *-------------------------------------------------------------------------------*/
   T_INT         iProcessMode;

  /*-------------------------------------------------------------------------------*
   * Intern ring buffer (saves the input samples)  
   *-------------------------------------------------------------------------------*/
   T_FLOAT     **ppfRingBufferAna;

  /*-------------------------------------------------------------------------------*
   * Intern ring buffer (saves the input samples)  
   *-------------------------------------------------------------------------------*/
   T_FLOAT      *pfFFTInputBuffer;
   
  /*-------------------------------------------------------------------------------*
   * Position index for the intern buffer 
   *-------------------------------------------------------------------------------*/
	T_INT         iPositionIndex;

  /*-------------------------------------------------------------------------------*
   * Prototype filter used for the analysis filterbank (window function)
   *-------------------------------------------------------------------------------*/
	T_FLOAT      *pfPrototypeFilterAna;

  /*-------------------------------------------------------------------------------*
   * Prototype filter used for the synthesis filterbank (window function)
   *-------------------------------------------------------------------------------*/
	T_FLOAT      *pfPrototypeFilterSyn;

  /*-------------------------------------------------------------------------------*
   * Prototype filter used for the filterbank (window function)
   *-------------------------------------------------------------------------------*/
	T_FLOAT      **ppfRingBufferSyn;

  /*-------------------------------------------------------------------------------*
   * Length of the prototype filter used for the filterbank
   *-------------------------------------------------------------------------------*/
	T_INT         iPrototypLength;

  /*-------------------------------------------------------------------------------*
   * Number of channels for analysis filterbank
   *-------------------------------------------------------------------------------*/
	T_INT         iNumChannelAna;

  /*-------------------------------------------------------------------------------*
   * Number of channels for synthesis filterbank
   *-------------------------------------------------------------------------------*/
	T_INT         iNumChannelSyn;

  /*-------------------------------------------------------------------------------*
   * Type of the prototype filter 
	* --------->  iTypePrototypFilter = 0 for Hann window
	* --------->  iTypePrototypFilter = 1 for Prototype window of length 1536 (3*512)
	* --------->  iTypePrototypFilter = 2 for prolate sequences
   *-------------------------------------------------------------------------------*/
	T_INT         iTypePrototypFilter; 

  /*-------------------------------------------------------------------------------*
   * Number of used order fft used for prototype filter                            * 
	* (iNumberFFTOrder = iPrototypLength/FFT_order)                                 *
   *-------------------------------------------------------------------------------*/
	T_INT         iNumberFFTOrder;

  /*-------------------------------------------------------------------------------*
   * Parameter struct for the FFT/IFFT
   *-------------------------------------------------------------------------------*/
	tHsabAlgoCoreParameterFFT   sParamFFT; 

 } tHsabAnaSynFiltBankData;
		  

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

