/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_EQ_H
#define ALG_CORE_HSAB_EQ_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_eq_defines.h"
#include "alg_core_hsab_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function for EQ
 *---------------------------------------------------------------------------------*/
   void HsabEqInit(  tHsabEq                    *psEq,
                     tHsabAlgCoreParameters     *psParams,
                     tHsabAlgCoreMemManagement  *psMemManagement,
                     T_INT                      iNumChannels);

/*---------------------------------------------------------------------------------*
 * Process function for the EQ
 *---------------------------------------------------------------------------------*/
   void HsabEqProcess(  T_FLOAT                 **ppfInBuffer,
                        tHsabEq                 *psEq, 
                        tHsabAlgCoreParameters  *psParams, 
                        T_FLOAT                 **ppfOutBuffer);

/*---------------------------------------------------------------------------------*
 * Deinit function for the EQ
 *---------------------------------------------------------------------------------*/
   void HsabEqDeInit(tHsabEq                    *psEq,
                     tHsabAlgCoreMemManagement  *psMemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
