/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_EQ_DEFINES_H
#define ALG_CORE_HSAB_EQ_DEFINES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Data struct for the FFT
 *---------------------------------------------------------------------------------*/
typedef struct {

  /*-------------------------------------------------------------------------------*
   * Main control
   *-------------------------------------------------------------------------------*/
   T_INT   iMainControl;
   T_INT   iNumPeakFilter;
   T_INT   iNumChannels;
   T_FLOAT fGainActMax;
   T_FLOAT fGainActMin;

  /*-------------------------------------------------------------------------------*
   * User adjustable parameters for the gain settings
   *-------------------------------------------------------------------------------*/
   T_FLOAT *pfGainLs_dB;
   T_FLOAT  fGainMaster_dB;

   T_FLOAT *pfGainLs;
   T_FLOAT  fGainMaster;

  /*-------------------------------------------------------------------------------*
   * User adjustable parameters for the low shelving filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT *pfLowShelvGain_dB;
   T_FLOAT *pfLowShelvBandwidth_Hz;
   T_INT   *piLowShelvActivation;

  /*-------------------------------------------------------------------------------*
   * Variables and internal parameters for the low shelving filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT *pfLowShelvGain;
   T_FLOAT *pfLowShelvApAlpha;
   T_FLOAT *pfLowShelvApMemInput;
   T_FLOAT *pfLowShelvApMemOutput;

  /*-------------------------------------------------------------------------------*
   * User adjustable parameters for the peak filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT **ppfPeakFiltGain_dB;
   T_FLOAT **ppfPeakFiltCenterFreq_Hz;
   T_FLOAT **ppfPeakFiltBandwidth_Hz;
   T_INT   **ppiPeakFiltActivation;

  /*-------------------------------------------------------------------------------*
   * Variables and internal parameters for the peak filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT **ppfPeakFiltGain;
   T_FLOAT **ppfPeakFiltApAlpha;
   T_FLOAT **ppfPeakFiltApBeta;
   T_FLOAT **ppfPeakFiltApMemInput1;
   T_FLOAT **ppfPeakFiltApMemInput2;
   T_FLOAT **ppfPeakFiltApMemOutput1;
   T_FLOAT **ppfPeakFiltApMemOutput2;

  /*-------------------------------------------------------------------------------*
   * User adjustable parameters for the high shelving filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT *pfHighShelvGain_dB;
   T_FLOAT *pfHighShelvBandwidth_Hz;
   T_INT   *piHighShelvActivation;

  /*-------------------------------------------------------------------------------*
   * Variables and internal parameters for the low shelving filters
   *-------------------------------------------------------------------------------*/
   T_FLOAT *pfHighShelvGain;
   T_FLOAT *pfHighShelvApAlpha;
   T_FLOAT *pfHighShelvApMemInput;
   T_FLOAT *pfHighShelvApMemOutput;

} tHsabEq;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

