/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_POWER_EST_H
#define ALG_CORE_HSAB_POWER_EST_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_power_est_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function of noise estimation
 *---------------------------------------------------------------------------------*/
 void HsabNoiseEstimateInit( tHsabAlgCoreParameters     *psParams,
									  tHsabPowerEstData          *psPowerEstData,
									  T_INT                       iNumChannel, 
									  tHsabAlgCoreMemManagement  *MemManagement ); 

/*---------------------------------------------------------------------------------*
 * Deinit function of noise estimation
 *---------------------------------------------------------------------------------*/
 void HsabNoiseEstimateDeInit( tHsabAlgCoreParameters     *psParams,
									    tHsabPowerEstData          *psPowerEstData,                
									    tHsabAlgCoreMemManagement  *MemManagement );
                             
/*---------------------------------------------------------------------------------*
 * Process function of noise estimation
 *---------------------------------------------------------------------------------*/
 void HsabNoiseEstimateProcess( T_COMPLEX                 **ppCmplxSpecDataIn,
									     tHsabAlgCoreParameters     *psParams,
									     tHsabPowerEstData          *psPowerEstData );

/*---------------------------------------------------------------------------------*
 * Reset function of the noise estimation
 *---------------------------------------------------------------------------------*/
 void HsabNoiseEstimateReset( tHsabAlgCoreParameters     *psParams,
									   tHsabPowerEstData          *psPowerEstData );

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
