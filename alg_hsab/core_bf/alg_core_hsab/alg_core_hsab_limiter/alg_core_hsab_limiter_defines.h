/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_LIMITER_DEFINES_H   
#define ALG_CORE_HSAB_LIMITER_DEFINES_H 

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

typedef struct {

  /*-------------------------------------------------------------------------------*
   * Main control parameter    
   *-------------------------------------------------------------------------------*/
   T_INT       iMainControl;

  /*-------------------------------------------------------------------------------*
   * Number of used channel  
   *-------------------------------------------------------------------------------*/
   T_INT       iNumChannel;

  /*-------------------------------------------------------------------------------*
   * Limiter external parameters
   *-------------------------------------------------------------------------------*/
   T_INT       iTimeConstRiseDBS;
   T_INT       iTimeConstFallDBS;
   T_INT       iMaxShortTermMagBelowMaxDB;
   T_INT       iHardLimitBelowMaxX100DB;

  /*-------------------------------------------------------------------------------*
   * Limiter internal parameters
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fTimeConstRise;
   T_FLOAT     fTimeConstFall;
   T_FLOAT     fMaxShortTermMag;
   T_FLOAT     fMaxMag;

  /*-------------------------------------------------------------------------------*
   * Limiter internal signals
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfShortTermMag; 

} tHsabLimiterData;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

