/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_LIMITER_H
#define ALG_CORE_HSAB_LIMITER_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_limiter_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function for limiter
 *---------------------------------------------------------------------------------*/
 void HsabLimiterInit(  tHsabAlgCoreParameters     *psParams,
                        tHsabLimiterData           *psLimiterData,
                        T_INT                      iNumChannel, 
                        tHsabAlgCoreMemManagement  *MemManagement );

/*---------------------------------------------------------------------------------*
 * Process function for the limiter
 *---------------------------------------------------------------------------------*/
 void HsabLimiterProcess(  T_FLOAT                 **ppDataIn,
                           tHsabAlgCoreParameters  *psParams,
                           tHsabLimiterData        *psLimiterData,
                           T_FLOAT                 **ppDataOut );

/*---------------------------------------------------------------------------------*
 * Deinit function for the limiter
 *---------------------------------------------------------------------------------*/
 void HsabLimiterDeInit(tHsabLimiterData           *psLimiteData,
                        tHsabAlgCoreMemManagement  *MemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
