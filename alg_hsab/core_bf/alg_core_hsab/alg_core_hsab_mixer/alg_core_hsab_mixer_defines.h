/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#ifndef ALG_CORE_HSAB_MIXER_DEFINES_H
#define ALG_CORE_HSAB_MIXER_DEFINES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */
   
#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Parameters and variables of the noise estimation
 *---------------------------------------------------------------------------------*/
typedef struct 
{
  /*-------------------------------------------------------------------------------*                              
   * Main parameters
   *-------------------------------------------------------------------------------*/ 
   T_INT     iMainControl;

  /*-------------------------------------------------------------------------------*                              
   * Related internal parameters
   *-------------------------------------------------------------------------------*/ 
   T_INT     iNumSeats;
   T_INT     iNumRemoteOut;
   T_INT     iFftOrderHalfPlusOne;
   T_FLOAT   fNumSeatsInvSqrt;

  /*-------------------------------------------------------------------------------*                              
   * Parameters of the mixer
   *-------------------------------------------------------------------------------*/
   T_INT       iMixMainControl;
   T_FLOAT     fMixLowLimitFreqRangeLow;
   T_FLOAT     fMixHighLimitFreqRangeLow;
   T_FLOAT     fMixLowLimitFreqRangeCenter;
   T_FLOAT     fMixHighLimitFreqRangeCenter;
   T_FLOAT     fMixLowLimitFreqRangeHigh;
   T_FLOAT     fMixHighLimitFreqRangeHigh;
   T_FLOAT     fMixAttOfInactiveChannelsInDezibel;
   T_FLOAT     fMixAttIncInDezibelPerSecond;
   T_FLOAT     fMixAttDecInDezibelPerSecond;
   T_FLOAT     fMaxGainNoiseEqualizationInDezibel;
   T_FLOAT     fMinGainNoiseEqualizationInDezibel;
   T_FLOAT     fGainIncNoiseEqualizationInDezibelPerSecond;
   T_FLOAT     fGainDecNoiseEqualizationInDezibelPerSecond;
   T_FLOAT     fMixThresholdForSpeechActivityInDezibel;
   T_FLOAT     fMixDistanceToSecondLoudestSpeakerInDezibel;
   T_FLOAT     fMixDistanceToSecondLoudestSpeakerFastInDezibel;
   T_FLOAT     fMixActCounterIncStd;
   T_FLOAT     fMixActCounterIncFast;
   T_FLOAT     fMixActCounterDecStd;
   T_FLOAT     fMixActCounterMax;

  /*-------------------------------------------------------------------------------*                              
   * Variables of the mixer
   *-------------------------------------------------------------------------------*/
   T_INT       iMixLowLimitFreqRangeLow;
   T_INT       iMixHighLimitFreqRangeLow;
   T_INT       iMixLowLimitFreqRangeCenter;
   T_INT       iMixHighLimitFreqRangeCenter;
   T_INT       iMixLowLimitFreqRangeHigh;
   T_INT       iMixHighLimitFreqRangeHigh;

   T_FLOAT     fMixAttOfInactiveChannels;
   T_FLOAT     fMaxGainNoiseEqualizationLinear;
   T_FLOAT     fMinGainNoiseEqualizationLinear;
   T_FLOAT     fGainIncNoiseEqualizationLinear;
   T_FLOAT     fGainDecNoiseEqualizationLinear;
   T_FLOAT     fMixThresholdForSpeechActivityLinear;
   T_FLOAT     fMixDistanceToSecondLoudestSpeakerLinear;
   T_FLOAT     fMixDistanceToSecondLoudestSpeakerFastLinear;

   T_FLOAT     **ppfAvSignalMag;
   T_FLOAT     **ppfAvNoiseMag;
   T_FLOAT     **ppfAvMagEqualFactor;
   T_FLOAT     *pfMixMixerWeigths;
   T_FLOAT     *pfMixAcitivityCounter;

   T_FLOAT     fMixAttInc;
   T_FLOAT     fMixAttDec;


} tHsabAlgCoreMixer;

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
