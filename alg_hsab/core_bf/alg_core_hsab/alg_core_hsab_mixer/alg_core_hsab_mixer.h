/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_MIXER_H
#define ALG_CORE_HSAB_MIXER_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_mixer_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function for the mixer
 *---------------------------------------------------------------------------------*/
   T_INT HsabMixerInit( tHsabAlgCoreMixer          *psHsabMixerData,
                        tHsabAlgCoreParameters     *psParams,
                        tHsabAlgCoreMemManagement  *psMemManagement,
                        T_INT                      iNumSeats,
                        T_INT                      iNumRemoteOut,
                        T_INT                      iFftOrder );

/*---------------------------------------------------------------------------------*
 * Process function for the mixer
 *---------------------------------------------------------------------------------*/
   void HsabMixerProcess(  T_COMPLEX            **ppcInput,
                           T_COMPLEX            **ppcOutput,
                           tHsabAlgCoreMixer    *psHsabMixerData,
                           T_FLOAT              **ppfSigPropEstSpecSigPow,
                           T_FLOAT              **ppfSigPropEstSpecNoiseMag );

/*---------------------------------------------------------------------------------*
 * Deinit function for the mixer
 *---------------------------------------------------------------------------------*/
   void HsabMixerDeInit(tHsabAlgCoreMixer          *psHsabMixerData,
                        tHsabAlgCoreMemManagement  *psMemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
