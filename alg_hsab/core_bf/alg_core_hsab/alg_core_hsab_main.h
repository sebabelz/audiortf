/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_MAIN_H
#define ALG_CORE_HSAB_MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

/*---------------------------------------------------------------------------------*
 * Main 
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreInit( tHsabAlgCoreMain *Main );

void HsabAlgCoreDeInit( tHsabAlgCoreMain *Main);

void HsabAlgCoreProcess( tHsabAlgCoreMain *Main );

void HsabAlgCoreReset( tHsabAlgCoreMain *Main );

/*---------------------------------------------------------------------------------*
 * Parameters
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreParametersInit( tHsabAlgCoreParameters    *Params,
                                tHsabAlgCoreMemManagement *MemManagement );

/*---------------------------------------------------------------------------------*
 * Signals
 *---------------------------------------------------------------------------------*/
void HsabAlgCoreSignalsInit( tHsabAlgCoreSignals       *Signals,
                             tHsabAlgCoreParameters    *Parameters,
                             tHsabAlgCoreMemManagement *MemManagement );

void HsabAlgCoreSignalsReset( tHsabAlgCoreSignals    *Signals,
                              tHsabAlgCoreParameters *Parameters );

void HsabAlgCoreSignalsDeInit( tHsabAlgCoreSignals       *Signals,
                               tHsabAlgCoreParameters    *Parameters,
                               tHsabAlgCoreMemManagement *MemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
