/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_AGC_DEFINES_H   
#define ALG_CORE_HSAB_AGC_DEFINES_H 

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Data struct for the FFT
 *---------------------------------------------------------------------------------*/
typedef struct {

  /*-------------------------------------------------------------------------------*
   * Main control parameter    
   *-------------------------------------------------------------------------------*/
   T_INT       iMainControl;

  /*-------------------------------------------------------------------------------*
   * Number of used channel  
   *-------------------------------------------------------------------------------*/
   T_INT       iNumChannel;

  /*-------------------------------------------------------------------------------*
   * AGC external parameters
   *-------------------------------------------------------------------------------*/
   T_INT       iDesMagBelowFullScaleDB;
   T_INT       iGainMinDB;
   T_INT       iGainMaxDB;
   T_INT       iGainIncX100DBS;
   T_INT       iGainDecX100DBS;

  /*-------------------------------------------------------------------------------*
   * AGC internal parameters
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fDesPowFullScale;
   T_FLOAT     fGainMin;
   T_FLOAT     fGainMax;
   T_FLOAT     fGainInc;
   T_FLOAT     fGainDec;

  /*-------------------------------------------------------------------------------*
   * AGC internal signals
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfBroadBandGain;

} tHsabAgcData;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

