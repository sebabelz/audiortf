/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_AGC_H
#define ALG_CORE_HSAB_AGC_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_agc_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function for AGC
 *---------------------------------------------------------------------------------*/
 void HsabAgcInit(   tHsabAlgCoreParameters     *psParams,
                     tHsabAgcData               *psAgcData,
                     T_INT                      iNumChannel, 
                     tHsabAlgCoreMemManagement  *MemManagement );

/*---------------------------------------------------------------------------------*
 * Process function for the AGC
 *---------------------------------------------------------------------------------*/
 void HsabAgcProcess(   T_FLOAT                 **ppfInData,
                        tHsabAlgCoreParameters  *psParams,
                        T_FLOAT                 *pfPeakPowBroadband,
                        tHsabAgcData            *psAgcData,
                        T_FLOAT                 **ppfOutData );

/*---------------------------------------------------------------------------------*
 * Deinit function for the AGC
 *---------------------------------------------------------------------------------*/
 void HsabAgcDeInit( tHsabAlgCoreParameters     *psParams,
                     tHsabAgcData               *psAgcData,
                     tHsabAlgCoreMemManagement  *MemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
