/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_PARAMETERS_H
#define ALG_CORE_HSAB_PARAMETERS_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */
   
#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Parameters
 *---------------------------------------------------------------------------------*/
typedef struct {

   T_INT    iSampleRateExternal;
   T_FLOAT  fSampleRateExternal;
   T_INT    iSampleRateInternal;
   T_FLOAT  fSampleRateInternal;
   T_INT    iFrameshiftExternal;
   T_INT    iFrameshiftInternal;
   T_INT    iUpAndDownSamplingFactor;
   T_INT    iNumMicIn; 
   T_INT    iNumTtsIn;
   T_INT    iNumRefInFromAmp;
   T_INT    iNumOutToAmpl;
   T_INT    iNumRemoteIn;
   T_INT    iNumRemoteOut;
   T_INT    iNumAllIn;
   T_INT    iNumAllOut;
   T_INT    iFFTLength;
   T_INT    iNumProcSubb;

  /*-------------------------------------------------------------------------------*
   * Number of supported seats
   *-------------------------------------------------------------------------------*/
   T_INT    iNumSeats;

  /*-------------------------------------------------------------------------------*
   * Half of FFT length plus one
   *-------------------------------------------------------------------------------*/
   T_INT    iFFTHalfPlusOne;

  /*-------------------------------------------------------------------------------*
   * Set handsfree or recognition mode
   *-------------------------------------------------------------------------------*/
   T_INT    iProcessMode;

} tHsabAlgCoreParameters;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */