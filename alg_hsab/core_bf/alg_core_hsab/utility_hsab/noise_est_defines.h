/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef NOISE_EST_DEFINES_H
#define NOISE_EST_DEFINES_H

#ifdef __cplusplus 
extern "C"
{
#endif /* C++ */
   
#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Parameters and variables of the noise estimation
 *---------------------------------------------------------------------------------*/
typedef struct 
{
  /*-------------------------------------------------------------------------------*                              
   * Main parameters
   *-------------------------------------------------------------------------------*/ 
   T_INT     iMainControl;
   T_INT     iNumChannels;
   T_INT     iNumProcSubb;

  /*-------------------------------------------------------------------------------*                              
   * Short-term smoothing of input magnitudes 
   *-------------------------------------------------------------------------------*/
   T_FLOAT    fTimeConstSmMagdBperSecond;
   T_FLOAT    fTimeConstSmMag;
   T_FLOAT  **ppfSmoothedMag;

  /*-------------------------------------------------------------------------------*                              
   * Noise estimation
   *-------------------------------------------------------------------------------*/
   T_FLOAT   fNoiseIncStddBPerSecond;
   T_FLOAT   fNoiseIncStd;
   T_FLOAT   fNoiseDecStddBPerSecond;
   T_FLOAT   fNoiseDecStd;
   T_FLOAT   fNoiseIncFastdBPerSecond;
   T_FLOAT   fNoiseIncFast;
   T_FLOAT   fNoiseDecFastdBPerSecond;
   T_FLOAT   fNoiseDecFast;
   T_FLOAT   fSwitchToFastEstIncSeconds;
   T_INT     iSwitchToFastEstInc;
   T_FLOAT   fSwitchToFastEstDecSeconds;
   T_INT     iSwitchToFastEstDec;
   T_INT   **ppiCounter;
   T_FLOAT **ppfEstNoiseMag;

} tHsabAlgCoreNoiseEst;

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
