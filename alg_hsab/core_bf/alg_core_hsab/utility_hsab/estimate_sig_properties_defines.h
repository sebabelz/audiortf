/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ESTIMATE_SIG_PROPERTIES_DEFINES_H
#define ESTIMATE_SIG_PROPERTIES_DEFINES_H

#ifdef __cplusplus 
extern "C"
{
#endif /* C++ */
   
#include "types_hsab.h"

/*---------------------------------------------------------------------------------*
 * Parameters, variables and results of the signal properties estimation
 *---------------------------------------------------------------------------------*/

typedef struct 
{
   T_INT    iComputeSigPowSpec;              /* This has to be set externally */
   T_INT    iComputeNoisePowSpec;            /* This has to be set externally */
   T_INT    iComputeSnrSpec;                 /* This has to be set externally */
   T_INT    iComputeSpeechActivitySpec;      /* This has to be set externally */
   T_INT    iComputeSigPowBroadBand;         /* This has to be set externally */
   T_INT    iComputeNoisePowBroadBand;       /* This has to be set externally */
   T_INT    iComputeSnrBroadBand;            /* This has to be set externally */
   T_INT    iComputeSpeechActivityBroadBand; /* This has to be set externally */
   T_INT    iComputePeakPowSpec;             /* This has to be set externally */
   T_INT    iComputePeakSnrSpec;             /* This has to be set externally */
   T_INT    iComputePeakPowBroadBand;        /* This has to be set externally */
   T_INT    iComputePeakSnrBroadBand;        /* This has to be set externally */

   T_INT    iNumChannels;                    /* This has to be set externally */

   T_INT    *piSigPowSpecIncX100DBS;
   T_INT    *piSigPowSpecDecX100DBS;
   T_INT    *piNoisePowSpecIncX100DBS;
   T_INT    *piNoisePowSpecDecX100DBS;
   T_INT    *piSnrSpecIncX100DBS;
   T_INT    *piSnrSpecDecX100DBS;
   T_INT    *piSpeechActivitySpecThreshX100DB;
   T_INT    iSigPowBroadBandIncX100DBS;
   T_INT    iSigPowBroadBandDecX100DBS;
   T_INT    iSigPowBroadBandStartHz;
   T_INT    iSigPowBroadBandEndHz;
   T_INT    iNoisePowBroadBandIncX100DBS;
   T_INT    iNoisePowBroadBandDecX100DBS;
   T_INT    iNoisePowBroadBandStartHz;
   T_INT    iNoisePowBroadBandEndHz;
   T_INT    iSnrBroadBandIncX100DBS;
   T_INT    iSnrBroadBandDecX100DBS;
   T_INT    iSpeechActivityBroadBandThreshX100DB;
   T_INT    *piPeakPowSpecIncX100DBS;
   T_INT    *piPeakPowSpecDecX100DBS;
   T_INT    *piPeakSnrSpecIncX100DBS;
   T_INT    *piPeakSnrSpecDecX100DBS;
   T_INT    iPeakPowBroadBandIncX100DBS;
   T_INT    iPeakPowBroadBandDecX100DBS;
   T_INT    iPeakPowBroadBandStartHz;
   T_INT    iPeakPowBroadBandEndHz;
   T_INT    iPeakSnrBroadBandIncX100DBS;
   T_INT    iPeakSnrBroadBandDecX100DBS;
   T_INT    iFastAdaptMS;

} tHsabEstimateSigPropertiesParam;

typedef struct 
{
   T_INT    iNumChannels;
   T_INT    iNumProcSubb;
   T_INT    iFrameShift;
   T_INT    iFreqDomain;

   T_FLOAT  *pfSigPowSpecInc;
   T_FLOAT  *pfSigPowSpecDec;
   T_FLOAT  *pfNoisePowSpecInc;
   T_FLOAT  *pfNoisePowSpecDec;
   T_FLOAT  *pfSnrSpecInc;
   T_FLOAT  *pfSnrSpecDec;
   T_FLOAT  *pfSpeechActivitySpecThresh;
   T_FLOAT  fSigPowBroadBandInc;
   T_FLOAT  fSigPowBroadBandDec;
   T_INT    iSigPowBroadBandStartBin;
   T_INT    iSigPowBroadBandEndBin;
   T_FLOAT  fSigPowBroadBandInvAmountBins;
   T_FLOAT  fNoisePowBroadBandInc;
   T_FLOAT  fNoisePowBroadBandDec;
   T_INT    iNoisePowBroadBandStartBin;
   T_INT    iNoisePowBroadBandEndBin;
   T_FLOAT  fNoisePowBroadBandInvAmountBins;
   T_FLOAT  fSnrBroadBandInc;
   T_FLOAT  fSnrBroadBandDec;
   T_FLOAT  fSpeechActivityBroadBandThresh;
   T_FLOAT  *pfPeakPowSpecInc;
   T_FLOAT  *pfPeakPowSpecDec;
   T_FLOAT  *pfPeakSnrSpecInc;
   T_FLOAT  *pfPeakSnrSpecDec;
   T_FLOAT  fPeakPowBroadBandInc;
   T_FLOAT  fPeakPowBroadBandDec;
   T_INT    iPeakPowBroadBandStartBin;
   T_INT    iPeakPowBroadBandEndBin;
   T_FLOAT  fPeakPowBroadBandInvAmountBins;
   T_FLOAT  fPeakSnrBroadBandInc;
   T_FLOAT  fPeakSnrBroadBandDec;
   T_INT    iFastAdapt;
   T_FLOAT  fInvNumProcSubb;
   T_FLOAT  fInvFrameShift;

} tHsabEstimateSigPropertiesVar;

typedef struct 
{
   T_FLOAT        **ppfSigPowSpec;
   T_FLOAT        **ppfNoisePowSpec;
   T_FLOAT        **ppfSnrSpec;
   T_INT          **ppiSpeechActivitySpec;
   T_FLOAT        *pfSigPowBroadBand;
   T_FLOAT        *pfNoisePowBroadBand;
   T_FLOAT        *pfSnrBroadBand;
   T_INT          *piSpeechActivityBroadBand;
   T_FLOAT        **ppfPeakPowSpec;
   T_FLOAT        **ppfPeakSnrSpec;
   T_FLOAT        *pfPeakPowBroadBand;
   T_FLOAT        *pfPeakSnrBroadBand;

} tHsabEstimateSigPropertiesResult;

typedef struct 
{
   tHsabEstimateSigPropertiesParam     psParam;
   tHsabEstimateSigPropertiesVar       psVar;
   tHsabEstimateSigPropertiesResult    psResult;

} tHsabEstimateSigProperties;

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
