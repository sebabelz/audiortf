/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ESTIMATE_SIG_PROPERTIES_H
#define ESTIMATE_SIG_PROPERTIES_H

#ifdef __cplusplus 
extern "C"
{
#endif /* C++ */

#include "estimate_sig_properties_defines.h"
#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
   
/*---------------------------------------------------------------------------------*
 * Init function of the signal properties estimation
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesInit(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                 tHsabAlgCoreParameters     *psParams,
                                 tHsabAlgCoreMemManagement  *psMemManagement,
                                 T_INT                      iNumChannels );

/*---------------------------------------------------------------------------------*
 * Deinit function of the signal properties estimation
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesDeInit(tHsabEstimateSigProperties *psEstimateSigProperties,
                                 tHsabAlgCoreParameters     *psParams,
                                 T_INT                      iNumChannels,
                                 tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation process function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesProcess(  T_COMPLEX                  **ppcInput,
                                    T_FLOAT                    **ppfInput,
                                    T_UINT32                   ui32FrameCounter,
                                    tHsabEstimateSigProperties *psEstimateSigProperties );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation check parameter dependencies function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesCheckParamDepend(  tHsabEstimateSigProperties *psEstimateSigProperties );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation parameter memory allocation function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesAllocMemoryPar( tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation variable memory allocation function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesAllocMemoryVar( tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation result memory free function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesAllocMemoryRes( tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          T_INT                      iNumChannels,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation parameter memory free function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesFreeMemoryPar(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation variable memory free function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesFreeMemoryVar(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation result memory allocation function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesFreeMemoryRes(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          T_INT                      iNumChannels,
                                          tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation set scalar variables function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesSetScalarVar(tHsabEstimateSigProperties *psEstimateSigProperties,
                                       tHsabAlgCoreParameters     *psParams);


/*---------------------------------------------------------------------------------*
 * Signal properties estimation set vector variables function
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesSetVecVar(tHsabEstimateSigProperties *psEstimateSigProperties,
                                    tHsabAlgCoreParameters     *psParams,
                                    T_INT                      iNumProcSubb);


/*---------------------------------------------------------------------------------*
 * Signal properties estimation initialization of result signals
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesInitRes(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                    tHsabAlgCoreParameters     *psParams,
                                    T_INT                      iNumChannels,
                                    tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Signal properties estimation default vector parameters
 *---------------------------------------------------------------------------------*/
void EstimateSigPropertiesDefaultVecPar(  tHsabEstimateSigProperties *psEstimateSigProperties,
                                          tHsabAlgCoreParameters     *psParams,
                                          T_INT                      iNumProcSubb);


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
