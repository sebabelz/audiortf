/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#ifndef NOISE_EST_H
#define NOISE_EST_H

#ifdef __cplusplus 
extern "C"
{
#endif /* C++ */

#include "noise_est_defines.h"
#include "types_hsab.h"
#include "alg_core_hsab_defines.h"
   
/*---------------------------------------------------------------------------------*
 * Init function of the noise estimation
 *---------------------------------------------------------------------------------*/
void NoiseEstInit( tHsabAlgCoreNoiseEst       *psNoiseEst, 
                   tHsabAlgCoreParameters     *psParams,    
                   tHsabAlgCoreMemManagement  *psMemManagement,                                   
                   T_INT                       iNumChannels );

/*---------------------------------------------------------------------------------*
 * Deinit function of the noise estimation
 *---------------------------------------------------------------------------------*/
void NoiseEstDeInit( tHsabAlgCoreNoiseEst       *psNoiseEst,
                     tHsabAlgCoreMemManagement  *psMemManagement );


/*---------------------------------------------------------------------------------*
 * Noise estimation process function
 *---------------------------------------------------------------------------------*/
void NoiseEstProcess( T_COMPLEX            **ppcInput,
                      tHsabAlgCoreNoiseEst  *psNoiseEst );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
