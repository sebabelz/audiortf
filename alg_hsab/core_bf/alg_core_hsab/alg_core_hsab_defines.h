/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_DEFINES_H
#define ALG_CORE_HSAB_DEFINES_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_signals.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_ana_syn_defines.h"
#include "alg_core_hsab_power_est_defines.h"
#include "alg_core_hsab_src_defines.h"
#include "alg_core_hsab_bf_defines.h"
#include "alg_core_hsab_mixer_defines.h"
#include "alg_core_hsab_agc_defines.h"
#include "alg_core_hsab_limiter_defines.h"
#include "alg_core_hsab_eq_defines.h"
#include "noise_est_defines.h"
#include "estimate_sig_properties_defines.h"

/*---------------------------------------------------------------------------------*
 * General defines 
 *---------------------------------------------------------------------------------*/
#define PI                    3.141592653589793
#define SYS_EPS                          1e-15f
#define ACOUSTICVELOCITY                    340
#define FABS_FACTOR                        0.8f
#define MAX_INT_16                        32767
#define MAX_INT_16_DB        90.308733622833979

/*---------------------------------------------------------------------------------*
 * TRUE/FALSE 
 *---------------------------------------------------------------------------------*/
#define HSAB_TRUE                 1
#define HSAB_FALSE                0  

/*---------------------------------------------------------------------------------*
 * Macro defines 
 *---------------------------------------------------------------------------------*/
#define FABS(A) (((A) > 0.0f) ? (A) : -(A)) 
#define MAX(A,B) (((A) > (B)) ? (A) : (B)) 
#define MIN(A,B) (((A) < (B)) ? (A) : (B)) 
#define CLAMP(A,LO,HI) (((A) < (LO)) ? (LO) : (((A) > (HI)) ? (HI) : (A)))

/*---------------------------------------------------------------------------------*
 * On/off switches
 *---------------------------------------------------------------------------------*/
#define HSAB_OFF                              0
#define HSAB_ON                               1

/*---------------------------------------------------------------------------------*
 * Colors
 *---------------------------------------------------------------------------------*/
#define HSAB_DEBUG_INFO                       0
#define HSAB_DEBUG_ERROR                      1
#define HSAB_DEBUG_WARNING                    2

/*---------------------------------------------------------------------------------*
 * Memory management
 *---------------------------------------------------------------------------------*/
typedef struct {

  /*-------------------------------------------------------------------------------*
   * Total memory
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytes;
   T_INT iCurrentDynMemInBytes;
   T_INT iMaxDynMemInBytes;

  /*-------------------------------------------------------------------------------*
   * Memory used for parameters
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesParameters;
   T_INT iCurrentDynMemInBytesParameters;
   T_INT iMaxDynMemInBytesParameters;

  /*-------------------------------------------------------------------------------*
   * Memory used for signals
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesSignals;
   T_INT iCurrentDynMemInBytesSignals;
   T_INT iMaxDynMemInBytesSignals;

  /*-------------------------------------------------------------------------------*
   * Memory used for analysis and synthesis filterbank
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesAnaSyn;
   T_INT iCurrentDynMemInBytesAnaSyn;
   T_INT iMaxDynMemInBytesAnaSyn;

  /*-------------------------------------------------------------------------------*
   * Memory used for sample rate conversion
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesSrc;
   T_INT iCurrentDynMemInBytesSrc;
   T_INT iMaxDynMemInBytesSrc;

  /*-------------------------------------------------------------------------------*
   * Memory used for beamforming
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesBf;
   T_INT iCurrentDynMemInBytesBf; 
   T_INT iMaxDynMemInBytesBf;

  /*-------------------------------------------------------------------------------*
   * Memory used for agc
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesAgc;
   T_INT iCurrentDynMemInBytesAgc;
   T_INT iMaxDynMemInBytesAgc;

  /*-------------------------------------------------------------------------------*
   * Memory used for limiter
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesLimiter;
   T_INT iCurrentDynMemInBytesLimiter; 
   T_INT iMaxDynMemInBytesLimiter;

  /*-------------------------------------------------------------------------------*
   * Memory used for eq
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesEq;
   T_INT iCurrentDynMemInBytesEq; 
   T_INT iMaxDynMemInBytesEq;

  /*-------------------------------------------------------------------------------*
   * Memory used for mixer
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesMixer;
   T_INT iCurrentDynMemInBytesMixer;
   T_INT iMaxDynMemInBytesMixer;

  /*-------------------------------------------------------------------------------*
   * Memory used for signal properties estimation
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesEstimateSigProperties;
   T_INT iCurrentDynMemInBytesEstimateSigProperties;
   T_INT iMaxDynMemInBytesEstimateSigProperties;

  /*-------------------------------------------------------------------------------*
   * Memory used for noise estimation in receiving path
   *-------------------------------------------------------------------------------*/
   T_INT iStaticMemInBytesNoiseEst;
   T_INT iCurrentDynMemInBytesNoiseEst;
   T_INT iMaxDynMemInBytesNoiseEst;
 
} tHsabAlgCoreMemManagement;

/*---------------------------------------------------------------------------------*
 * Main
 *---------------------------------------------------------------------------------*/
typedef struct { 

  /*-------------------------------------------------------------------------------*
   * Main memory management
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreMemManagement        HsabAlgCoreMemManagement;

  /*-------------------------------------------------------------------------------*
   * Main parameters
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreParameters           HsabAlgCoreParams;

  /*-------------------------------------------------------------------------------*
   * Main signals
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreSignals              HsabAlgCoreSignals;

  /*-------------------------------------------------------------------------------*
   * Main analysis and synthesis filterbank in sending path
   *-------------------------------------------------------------------------------*/
   tHsabAnaSynFiltBankData          HsabAnaSynFiltBankDataSend;

  /*-------------------------------------------------------------------------------*
   * Main data structure for sample rate conversion in sending path
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreSrc                  HsabAlgCoreSrcSend;

  /*-------------------------------------------------------------------------------*
   * Main data structure for the BF
   *-------------------------------------------------------------------------------*/
   tHsabBfData						HsabBfData;

  /*-------------------------------------------------------------------------------*
   * Main data structure for generic mixer
   *-------------------------------------------------------------------------------*/
   tHsabAlgCoreMixer                HsabAlgCoreMixer;

  /*-------------------------------------------------------------------------------*
   * Main data structure for the agc
   *-------------------------------------------------------------------------------*/
   tHsabAgcData                     HsabAgcData;

  /*-------------------------------------------------------------------------------*
   * Main data structure for the limiter
   *-------------------------------------------------------------------------------*/
   tHsabLimiterData                 HsabLimiterData;

  /*-------------------------------------------------------------------------------*
   * Main data structure for the eq
   *-------------------------------------------------------------------------------*/
   tHsabEq                          HsabEq;

  /*-------------------------------------------------------------------------------*
   * Main data structure for noise estimation in receiving path
   *-------------------------------------------------------------------------------*/
   tHsabPowerEstData                HsabAgcPowerDataSend;

  /*-------------------------------------------------------------------------------*
   * Main data structure for signal properties estimation for AGC (time domain)
   *-------------------------------------------------------------------------------*/
   tHsabEstimateSigProperties       HsabEstSigPropAgcTime;

  /*-------------------------------------------------------------------------------*
   * Main data structure for signal properties estimation for mixer (frequency domain)
   *-------------------------------------------------------------------------------*/
   tHsabEstimateSigProperties       HsabEstSigPropMix;

} tHsabAlgCoreMain;


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */

