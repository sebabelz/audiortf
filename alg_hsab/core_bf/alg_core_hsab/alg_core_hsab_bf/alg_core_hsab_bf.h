/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_BF_H
#define ALG_CORE_HSAB_BF_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "alg_core_hsab_bf_defines.h"

/*---------------------------------------------------------------------------------*
 * Init function for BF
 *---------------------------------------------------------------------------------*/
 void HsabBfInit(	tHsabAlgCoreParameters      *psParams,
                    tHsabBfData					*psBfData,
                    T_INT						iNumMicIn,
                    tHsabAlgCoreMemManagement	*MemManagement );

/*---------------------------------------------------------------------------------*
 * Process function for the BF
 *---------------------------------------------------------------------------------*/
 void HsabBfProcess(    T_COMPLEX               **ppCmplxSpecDataIn,
                        tHsabBfData              *psBfData,
						tHsabAlgCoreParameters   *psParams,
                        T_COMPLEX               **ppCmplxSpecDataOut );

/*---------------------------------------------------------------------------------*
 * Delay alignement of the input signal and sum (delay & sum)
 *---------------------------------------------------------------------------------*/
void HsabDelayAlignmentSum( T_COMPLEX                **ppCmplxSpecDataIn,
						    tHsabBfData	              *psBfData,
							tHsabAlgCoreParameters    *psParams,
					        T_COMPLEX				  *pCmplxFbfOutSpec );

/*---------------------------------------------------------------------------------*
 * Fixed blocking matrix
 *---------------------------------------------------------------------------------*/
 void HsabBfFixedBlockMat( tHsabAlgCoreParameters    *psParams,
						   tHsabBfData               *psBfData );

/*---------------------------------------------------------------------------------*
 * Control for adaptive blocking matrix
 *---------------------------------------------------------------------------------*/
 void HsabBfAdaptiveBlockMat( tHsabAlgCoreParameters    *psParams,
                              tHsabBfData               *psBfData );

 /*---------------------------------------------------------------------------------*
 * Step-size control for adaptive beamforming
 *---------------------------------------------------------------------------------*/
void HsabBfStepSizeControlIc( T_COMPLEX                 **ppCmplxFbfOutSpec,
                              tHsabAlgCoreParameters    *psParams,
                              tHsabBfData               *psBfData );

/*---------------------------------------------------------------------------------*
 * Power Control for the Postfilter
 *---------------------------------------------------------------------------------*/
void HsabBfPfCtrl( T_COMPLEX                 **ppCmplxSpecDataOut,
                   tHsabAlgCoreParameters    *psParams,
                   tHsabBfData               *psBfData );

/*---------------------------------------------------------------------------------*
 * Deinit function for the BF
 *---------------------------------------------------------------------------------*/
 void HsabAgcDeInit( tHsabAlgCoreParameters     *psParams,
                     tHsabBfData                *psBfData,
                     tHsabAlgCoreMemManagement  *MemManagement );


#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
