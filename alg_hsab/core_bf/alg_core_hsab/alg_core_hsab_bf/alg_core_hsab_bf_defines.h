/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef ALG_CORE_HSAB_BF_DEFINES_H   
#define ALG_CORE_HSAB_BF_DEFINES_H 

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"
#include "noise_est_defines.h"


typedef struct {

   T_INT		   iFilterLengthAbm;

   // Stepsize for the NLMS of the adaptive Blocking Matrix 
   T_FLOAT     fMaxStepsizeAbm;
   T_FLOAT     fStepsizeAbm;

   // Internal threshold for the power ratio of the adaptive Blocking Matrix
   T_FLOAT     fIntThreshPowRatioAbm;

   // Output signal of adaptive filter of BM
   T_COMPLEX   **ppCmplxAbmFiltOutSpec;

   // Filter coefficients for the adaptive BM
   T_COMPLEX   ***pppCmplxFilterCoeffAbm;

   // Pointer index for the adaptive blocking matrix memory
   T_INT       iPointMemAbm;

   // Filter Norm of adaptive BM
   T_FLOAT     *pfFilterNormAbm;

   // internal signals
   T_COMPLEX   **ppCmplxAbmInBuffer;
   T_COMPLEX   *pCmplxAbmInBufferMem;


} tHsabBfAbmData;


typedef struct {

  /*-------------------------------------------------------------------------------*
   * Power ratio threshold  
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fThreshPowRatio;
  /*-------------------------------------------------------------------------------*
   * IIR smoothing constant 
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fSmoothConst;
  /*-------------------------------------------------------------------------------*
   * SNR upper limit
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fLimitSnr;

  /*-------------------------------------------------------------------------------*
   * Magnitude ratio of fixed BF and BM
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fMagRatio;
  /*-------------------------------------------------------------------------------*
   * Decrease and increase constants 
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fDeltaDecStart, fDeltaIncStart;
   T_FLOAT     fDeltaDec, fDeltaInc;
  /*-------------------------------------------------------------------------------*
   * Frequency boundaries for the power ratio
   *-------------------------------------------------------------------------------*/
   T_INT       iLowFreqBoundPowRatio, iUppFreqBoundPowRatio;
  /*-------------------------------------------------------------------------------*
   * Short time power of FBF output
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfShortTimePowFbf;
  /*-------------------------------------------------------------------------------*
   * Short time power of BM output
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfShortTimePowBm;
  /*-------------------------------------------------------------------------------*
   * Power of BM output averaged over all paths
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfAverPowBm;

  /*-------------------------------------------------------------------------------*
   * BM power level correction factor
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfPowCorrBm;

} tHsabBfStepSizeCtrl;


typedef struct {

  /*-------------------------------------------------------------------------------*
   * Decrease and increase constants 
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fDeltaDecStart, fDeltaIncStart;
   T_FLOAT     fDeltaDec, fDeltaInc;

  /*-------------------------------------------------------------------------------*
   * Pf power level correction factor
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfPowCorrPf;

  /*-------------------------------------------------------------------------------*
   * Memory for the smoothed power at the adaptive beamformer output
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfShortTimePowAbf;

  /*-------------------------------------------------------------------------------*
   * Smoothed constant for the power at the adaptive beamformer output
   *-------------------------------------------------------------------------------*/
   T_FLOAT      fSmoothConstPowAbf;

  /*-------------------------------------------------------------------------------*
   * Ratio of smoothed power
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfSmPowRatio;

  /*-------------------------------------------------------------------------------*
   * Overestimation factor
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fOverEst;

  /*-------------------------------------------------------------------------------*
   * Spectral floor
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fSpecFloor;

  /*-------------------------------------------------------------------------------*
   * Spectral floor in dB for hands-free mode
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fSpecFloorHandsFreedB;

  /*-------------------------------------------------------------------------------*
   * Spectral floor in dB for recognition mode
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fSpecFloorRecogndB;

  /*-------------------------------------------------------------------------------*
   * Overestimation factor in dB for hands-free mode
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fOverEstHandsFreedB;

  /*-------------------------------------------------------------------------------*
   * Overestimation factor in dB for recognition mode
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fOverEstRecogndB;

  /*-------------------------------------------------------------------------------*
   * optimal Wiener Filter
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfOptWienFilt;

} tHsabBfPfData;


typedef struct {

  /*-------------------------------------------------------------------------------*
   * Main control parameter    
   *-------------------------------------------------------------------------------*/
   T_INT		   iMainControl;

  /*-------------------------------------------------------------------------------*
   * Distance between microphones in meter
   *-------------------------------------------------------------------------------*/
   T_FLOAT		fDistMic;

  /*-------------------------------------------------------------------------------*
   * Steering direction in degree
   *-------------------------------------------------------------------------------*/
   T_FLOAT	   fSteerAngle;

  /*-------------------------------------------------------------------------------*
   * Delay between microphones
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfDelayMic;

  /*-------------------------------------------------------------------------------*
   * Delay values for phase compensation
   *-------------------------------------------------------------------------------*/
   T_COMPLEX   **ppCmplxDelayCompFilt;

  /*-------------------------------------------------------------------------------*
   * Number of outputs of the blocking matrix BM
   *-------------------------------------------------------------------------------*/
   T_INT       iNumOutBm;

  /*-------------------------------------------------------------------------------*
   * Filter length of the interference canceller
   *-------------------------------------------------------------------------------*/
   T_INT       iFilterLengthIc;

   /*-------------------------------------------------------------------------------*
   * Pointer index for the interference canceller memory
   *-------------------------------------------------------------------------------*/
   T_INT       iPointMemIc;

   /*-------------------------------------------------------------------------------*
   * Maximal stepsize for the NLMS of the interference canceller
   *-------------------------------------------------------------------------------*/
   T_FLOAT      fMaxStepsizeIc;

  /*-------------------------------------------------------------------------------*
   * Stepsize for the NLMS of the interference canceller
   *-------------------------------------------------------------------------------*/
   T_FLOAT      fStepsizeIc;

  /*-------------------------------------------------------------------------------*
   * Internal threshold for the power ratio of the interference canceller
   *-------------------------------------------------------------------------------*/
   T_FLOAT      fIntThreshPowRatioIc;

  /*-------------------------------------------------------------------------------*
   * Counter for the main loop
   *-------------------------------------------------------------------------------*/
   T_INT       iCount;

  /*-------------------------------------------------------------------------------*
   * Inverse of number MicIn channels
   *-------------------------------------------------------------------------------*/
   T_FLOAT     fInvNumMicIn;

  /*-------------------------------------------------------------------------------*
   * Filter coefficients for the interference canceller
   *-------------------------------------------------------------------------------*/
   T_COMPLEX   ***pppCmplxFilterCoeffIc;

  /*-------------------------------------------------------------------------------*
   * Norm of the filter coefficients of the interference canceller for each path
   *-------------------------------------------------------------------------------*/
   T_FLOAT     **ppfFilterNormIcPath;

  /*-------------------------------------------------------------------------------*
   * Norm of the filter coefficients of the interference canceller
   *-------------------------------------------------------------------------------*/
   T_FLOAT     *pfFilterNormIc;
   
  /*-------------------------------------------------------------------------------*
   * BF external parameters
   *-------------------------------------------------------------------------------*/
   T_INT		iFilterLengthAbmExt;
   T_INT        iUseAdaptBf;
   T_INT		iUseAbm;
   T_INT        iUsePf;

  /*-------------------------------------------------------------------------------*
   * BF internal signals
   *-------------------------------------------------------------------------------*/
   T_COMPLEX   **ppCmplxDelayCompOutSpec;
   T_COMPLEX   **ppCmplxFbfOutSpec;
   T_COMPLEX   **ppCmplxBmOutSpec;
   T_COMPLEX   ***pppCmplxIcInBuffer;
   T_COMPLEX   **ppCmplxIcInBufferMem;
   T_COMPLEX   *pCmplxIcOutSpec;

  /*-------------------------------------------------------------------------------*
   * Parameters needed for the stepsize control
   *-------------------------------------------------------------------------------*/
   tHsabBfAbmData       sBfAbmData;

  /*-------------------------------------------------------------------------------*
   * Parameters needed for the stepsize control
   *-------------------------------------------------------------------------------*/
   tHsabBfStepSizeCtrl  sStepSizeCtrl;

  /*-------------------------------------------------------------------------------*
   * Parameters needed for the Posfilter
   *-------------------------------------------------------------------------------*/
   tHsabBfPfData        sBfPfData;

   /*---------------------------------------------------------------------------------*
    * Noise estimate of the fixed beamformer
    *---------------------------------------------------------------------------------*/
   tHsabAlgCoreNoiseEst  sNoiseEstDataFbf;


} tHsabBfData;



#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */