/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef DEFINES_HSAB_H
#define DEFINES_HSAB_H

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */
   
#define MESSAGE_LENGTH                      256
#define CONFIG_KEY_LENGTH                   256

/*---------------------------------------------------------------------------------*
 * Set recognition or handsfree mode
 *---------------------------------------------------------------------------------*/
 enum PROCESSMODE
 {
 	HSAB_HANDSFREE_MODE = 0,
    HSAB_RECOGNITION_MODE,
 };

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */