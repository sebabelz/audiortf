/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_ADDER_H
#define RTF_HSAB_ADDER_H

#include <QtWidgets>
#include "types_hsab.h"

class Adder: public QWidget
{
   public:

      Adder(QWidget *parent)
         : QWidget(parent)
         , mIsActive(true)
      {
      }

      void paintEvent ( QPaintEvent* )
      {
         QPainter p(this);
         p.setRenderHint(QPainter::Antialiasing, true);

         T_INT w  = width();
         T_INT h  = height();
         T_INT w2 = width()/2;
         T_INT h2 = height()/2;
         T_INT d  = 6; 
         T_INT s  = 2;

         if(mIsActive)
         {
            p.setPen(Qt::black);
         }
         else
         {
            p.setPen(Qt::gray);  
         }

         p.setBrush(Qt::transparent);
         p.drawEllipse( QRectF( s, s, w-2*s, h-2*s) );

         if(mIsActive)
         {
            p.setBrush(Qt::black);
         }
         else
         {
            p.setBrush(Qt::gray);
         }

         p.drawLine( w2, d, w2, h-d );
         p.drawLine( d, h2, w-d, h2 );  
      }

      bool setActive(bool isActive)
      {
         mIsActive=isActive;
         repaint();
      }

   protected:
      bool mIsActive;
};


#endif