/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

/*
Informatik IV Projekt 2018
programmed by s160921, Christian Wagner
*/

#ifndef RTF_HSAB_BEAMFORMER_ALGORITHMIC_PARAM_H
#define RTF_HSAB_BEAMFORMER_ALGORITHMIC_PARAM_H

#include <QtWidgets>
#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

class TabAlgoStructureBeamformer;

class TabAlgoStructureBeamformerParamBeamformer : public QWidget
{

   Q_OBJECT

   public:
      TabAlgoStructureBeamformerParamBeamformer(	tHsabBfData           *psHsabBfData,
							                              tHsabAlgCoreParameters     *psParams,
                                                   TabAlgoStructureBeamformer *tabStructureAGC );

	  

   protected:
      void resizeEvent(QResizeEvent *event);

   public slots:
		void setBfMode(int iModeSelect);
		void setDistMicM();
		void setSteerAngleD();
		void setStepsize();


   signals:

   private:
		TabAlgoStructureBeamformer  *mpTabAlgoStructureBeamformer; 
		tHsabAlgCoreParameters      *mpsParams;
		tHsabBfData                 *mpsHsabBfData;
		
		QLabel						*mLabelSelectBfMode;
		QComboBox					*mComboBoxSelectBfMode;
		
		QLabel						*mLabelParamStepsize;
		QDoubleSpinBox				*mDoubleSpinBoxParamStepsize;

		QLabel						*mLabelParamSteerAngle;
		QDoubleSpinBox				*mDoubleSpinBoxParamSteerAngle;

		QLabel						*mLabelParamDistMic;
		QDoubleSpinBox				*mDoubleSpinBoxParamDistMic;

};


class TabAlgoStructureBeamformerParamBlockingMatrix : public QWidget
{

	Q_OBJECT

public:
	TabAlgoStructureBeamformerParamBlockingMatrix(tHsabBfData           *psHsabBfData,
		tHsabAlgCoreParameters     *psParams,
		TabAlgoStructureBeamformer *tabStructureAGC);

protected:
	void resizeEvent(QResizeEvent *event);

	public slots:
		void setModeSelectBlockMat(int iModeSelect);
		void setMaxStepsizeAbm();

signals:

private:
	TabAlgoStructureBeamformer * mpTabAlgoStructureBeamformer;
	tHsabAlgCoreParameters      *mpsParams;
	tHsabBfData                 *mpsHsabBfData;
	
	QLabel						*mLabelModeSelectBlockMat;
	QComboBox					*mComboBoxModeSelectBlockMat;

	QLabel						*mLabelParamMaxStepsizeAbm;
	QDoubleSpinBox					*mDoubleSpinBoxParamMaxStepsizeAbm;


};

#endif
