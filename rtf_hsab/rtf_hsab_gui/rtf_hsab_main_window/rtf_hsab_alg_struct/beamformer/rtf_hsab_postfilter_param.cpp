/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

/*
Informatik IV Projekt 2018
programmed by s160921, Christian Wagner
*/

#include <QtWidgets>
#include "types_hsab.h"
#include "defines_hsab.h"
#include "rtf_hsab_postfilter_algorithmic_main.h"
#include "rtf_hsab_postfilter_param.h"


TabAlgoStructurePostfilterParamPostfilter::TabAlgoStructurePostfilterParamPostfilter(  tHsabBfData                *psHsabBfData, 
												                                       tHsabAlgCoreParameters     *psParams,
                                                                                       TabAlgoStructurePostfilter *tabStructurePostfilter )

{
   T_INT   iLeftOffset          = 20;
   T_INT   iTopOffset           = 20;
   T_INT   iWidgetHeigth        = 22;
   T_INT   iVerSpaceBetwWidgets = 8;

   QString Text;

  /*---------------------------------------------------------------------------------------------*
   * Save address of parameter and beamformer struct
   *---------------------------------------------------------------------------------------------*/
   mpsHsabBfData = psHsabBfData;
   mpsParams     = psParams;

  
  /*-------------------------------------------------------------------------------*
   * Mode selection (hands-free or recognition)
   *-------------------------------------------------------------------------------*/
   mLabelModeSelection = new QLabel( this );
   mLabelModeSelection->setGeometry(iLeftOffset -5, iTopOffset + 10, 110, iWidgetHeigth);
   mLabelModeSelection->setText("Mode selection:  ");
   mLabelModeSelection->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
   
   mComboBoxModeSelection = new QComboBox( this );
   mComboBoxModeSelection->setGeometry(iLeftOffset + 115, iTopOffset + 10, 110, iWidgetHeigth);
	mComboBoxModeSelection->addItem("Hands-free");
	mComboBoxModeSelection->addItem("Voice recognition");
   mComboBoxModeSelection->setCurrentIndex(psHsabBfData->iUsePf);
   connect(mComboBoxModeSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(setModeSelectNoiseSupp()));

  /*-------------------------------------------------------------------------------*
	* Maximum attenuation
	*-------------------------------------------------------------------------------*/
	mLabelParamMaxAttGaindB = new QLabel( this );
	mLabelParamMaxAttGaindB->setGeometry(iLeftOffset -15, iTopOffset + 40, 120, iWidgetHeigth);
	mLabelParamMaxAttGaindB->setText("Max. attenuation in dB:  ");
	mLabelParamMaxAttGaindB->setAlignment(Qt::AlignRight |Qt::AlignVCenter);
   
	mDoubleSpinBoxParamMaxAttGaindB = new QDoubleSpinBox( this );
	mDoubleSpinBoxParamMaxAttGaindB->setGeometry(iLeftOffset + 115, iTopOffset + 40, 110, iWidgetHeigth);
	mDoubleSpinBoxParamMaxAttGaindB->setSingleStep((T_DOUBLE) 0.5);
	mDoubleSpinBoxParamMaxAttGaindB->setMinimum((T_DOUBLE)   0.0);
	mDoubleSpinBoxParamMaxAttGaindB->setMaximum((T_DOUBLE)  40.0);
	mDoubleSpinBoxParamMaxAttGaindB->setValue(getMaxAtt());
	connect(mDoubleSpinBoxParamMaxAttGaindB, SIGNAL(valueChanged(double)), this, SIGNAL(setMaxAttGaindB(double)));

  /*-------------------------------------------------------------------------------*
	* Overestimation of the background noise
	*-------------------------------------------------------------------------------*/
	mLabelParamOverestGaindB = new QLabel( this );
	mLabelParamOverestGaindB->setGeometry(iLeftOffset -5, iTopOffset + 70, 110, iWidgetHeigth);
	mLabelParamOverestGaindB->setText("Overestimation in dB:  ");
	mLabelParamOverestGaindB->setAlignment(Qt::AlignRight |Qt::AlignVCenter);
   
	mDoubleSpinBoxParamOverestGaindB = new QDoubleSpinBox( this );
	mDoubleSpinBoxParamOverestGaindB->setGeometry(iLeftOffset + 115, iTopOffset + 70, 110, iWidgetHeigth);
	mDoubleSpinBoxParamOverestGaindB->setSingleStep((T_DOUBLE) 0.5);
	mDoubleSpinBoxParamOverestGaindB->setMinimum((T_DOUBLE) -100.0);
	mDoubleSpinBoxParamOverestGaindB->setMaximum((T_DOUBLE)  100.0);
   mDoubleSpinBoxParamOverestGaindB->setValue(getOverEstFac());
   connect(mDoubleSpinBoxParamOverestGaindB, SIGNAL(valueChanged(double)), this, SIGNAL(setOverEstdB(double)));

}

//Holt aktuellen "Max. Attenuation" Parameter in Abhängigkeit des gewählten Modes (Hands-free, Voice recognition)
float TabAlgoStructurePostfilterParamPostfilter::getMaxAtt()
{
   if( HSAB_RECOGNITION_MODE == mpsParams->iProcessMode )
   {
      return mpsHsabBfData->sBfPfData.fSpecFloorRecogndB;
   }
   else
   {
      return mpsHsabBfData->sBfPfData.fSpecFloorHandsFreedB;
   }
}

//Holt aktuellen "Overestimation" Parameter in Abhängigkeit des gewählten Modes (Hands-free, Voice recognition)
float TabAlgoStructurePostfilterParamPostfilter::getOverEstFac()
{
   if( HSAB_RECOGNITION_MODE == mpsParams->iProcessMode )
   {
      return mpsHsabBfData->sBfPfData.fOverEstRecogndB;
   }
   else
   {
      return mpsHsabBfData->sBfPfData.fOverEstHandsFreedB;
   }
}


//Je nach Modeauswahl wird 0 bzw. 1 im alg. Kern gesetzt.
//Spinboxen werden mit aktuellen werten des alg. Kerns gefüllt
void TabAlgoStructurePostfilterParamPostfilter::setModeSelectNoiseSupp()
{
	if (mComboBoxModeSelection->currentIndex() == 0) 
	{
		mpsParams->iProcessMode = 0;
		mDoubleSpinBoxParamMaxAttGaindB->setValue(getMaxAtt());
		mDoubleSpinBoxParamOverestGaindB->setValue(getOverEstFac());

	}
	if (mComboBoxModeSelection->currentIndex() == 1)
	{
		mpsParams->iProcessMode = 1;
		mDoubleSpinBoxParamMaxAttGaindB->setValue(getMaxAtt());
		mDoubleSpinBoxParamOverestGaindB->setValue(getOverEstFac());
	}

}

/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructurePostfilterParamPostfilter::resizeEvent ( QResizeEvent * event )
{

   T_INT iButtonWidth         =  90;
   if (width() < 1000)
   {
      iButtonWidth = (T_INT) ((T_FLOAT) 90.0 * (width()-(T_FLOAT) 400.0) / (T_FLOAT) 600.0 );
   }

	T_INT   iLeftOffset          = 20;
   T_INT   iTopOffset           = 20;
   T_INT   iWidgetHeigth        = 22;

	T_INT   iRightOffset         =  90;

   T_INT   iWidth               =  width();
   if (iWidth > (T_INT) 1200)
   {
      iLeftOffset        += (iWidth - (T_INT) 1200) / 2;
      iRightOffset       += (iWidth - (T_INT) 1200) / 2;
      iWidth              = 1200;
   }

   T_INT iMinSizeForChangingText        = 1000;

  /*---------------------------------------------------------------------------------------------*
   * Mode selection
   *---------------------------------------------------------------------------------------------*/
   /*mLabelModeSelection->setGeometry(iLeftOffset-50,iTopOffset,145,iWidgetHeigth);
   if (width() < iMinSizeForChangingText) 
   {
      mLabelModeSelection->setText("Noise\nest.");
   }
   else
   {
      mLabelModeSelection->setText("Mode selection:");
   }
   
	mComboBoxModeSelection->setGeometry(iLeftOffset+110,iTopOffset,220,iWidgetHeigth);*/

	//mGridLayout->addWidget( mLabelModeSelection, 0, 0 );
	//mGridLayout->addWidget( mComboBoxModeSelection, 0, 1 );
}