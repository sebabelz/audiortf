/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include <math.h>
#include "defines_hsab.h"
#include "rtf_hsab_connector.h"
#include "rtf_hsab_splitter.h"
#include "alg_core_hsab_defines.h"
#include "rtf_hsab_postfilter_algorithmic_main.h"
#include "rtf_hsab_postfilter_param.h"


TabAlgoStructurePostfilter::TabAlgoStructurePostfilter(  tHsabAlgCoreMain *psHsabAlgCore,
                                                         const int         iProVis, 
                                                         QWidget          *parent )
     : QWidget(parent)
{

   T_INT iButtonWidth         =  90;
	T_INT iButtonHeight        =  45;
	T_INT iCheckBoxWidth       =  45;
   T_INT iCheckBoxHeight      =  18;
   T_INT iProgressBarWidth    =  64;
   T_INT iProgressBarHeight   =  18;
	T_INT iRightOffset         = 120;
	T_INT iLeftOffset          =  90;
	T_INT iTopOffset           =  15;
	T_INT iHorSpaceBetwButtons =  (T_INT) (T_FLOAT(1200-iLeftOffset-iRightOffset-6*iButtonWidth) / (T_FLOAT) 5.0);
	T_INT iVerSpaceBetwButtons =  15;
   T_INT iSplitterLength      =  10;
   T_INT iLeftRightBoundary   =  74;

   T_INT ixpos[12];
	T_INT iypos[8];
	 
	ixpos[0]  = iLeftOffset;
	ixpos[1]  = iLeftOffset + 1 * iButtonWidth;
   ixpos[2]  = iLeftOffset + 1 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[3]  = iLeftOffset + 2 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[4]  = iLeftOffset + 2 * iButtonWidth + 2 * iHorSpaceBetwButtons;
	ixpos[5]  = iLeftOffset + 3 * iButtonWidth + 2 * iHorSpaceBetwButtons;
   ixpos[6]  = iLeftOffset + 3 * iButtonWidth + 3 * iHorSpaceBetwButtons;
	ixpos[7]  = iLeftOffset + 4 * iButtonWidth + 3 * iHorSpaceBetwButtons;
   ixpos[8]  = iLeftOffset + 4 * iButtonWidth + 4 * iHorSpaceBetwButtons;
	ixpos[9]  = iLeftOffset + 5 * iButtonWidth + 4 * iHorSpaceBetwButtons;
   ixpos[10] = iLeftOffset + 5 * iButtonWidth + 5 * iHorSpaceBetwButtons;
	ixpos[11] = iLeftOffset + 6 * iButtonWidth + 5 * iHorSpaceBetwButtons;

	iypos[0] = iTopOffset;
   iypos[1] = iTopOffset + 1 * iButtonHeight;
 	iypos[2] = iTopOffset + 1 * iButtonHeight + 1 * iVerSpaceBetwButtons;
	iypos[3] = iTopOffset + 2 * iButtonHeight + 1 * iVerSpaceBetwButtons;
	iypos[4] = iTopOffset + 2 * iButtonHeight + 2 * iVerSpaceBetwButtons;
	iypos[5] = iTopOffset + 3 * iButtonHeight + 2 * iVerSpaceBetwButtons;
	iypos[6] = iTopOffset + 3 * iButtonHeight + 3 * iVerSpaceBetwButtons;
	iypos[7] = iTopOffset + 4 * iButtonHeight + 3 * iVerSpaceBetwButtons;

  /*---------------------------------------------------------------------------------------------*
   * Flag for visualization of the development progress
   *---------------------------------------------------------------------------------------------*/
   miProVis = iProVis;

  /*---------------------------------------------------------------------------------------------*
   * Save address of algorithmic core struct
   *---------------------------------------------------------------------------------------------*/
   mpsHsabAlgCore = psHsabAlgCore;

  /*---------------------------------------------------------------------------------------------*
   * Noise estimation
   *---------------------------------------------------------------------------------------------*/
   mPushButtonNoiseEst = new QPushButton(tr("Noise\nestimation"), this);
   mPushButtonNoiseEst->setGeometry(ixpos[0] - iLeftOffset + 160, iypos[2] + 70, iButtonWidth, iButtonHeight);

   mConnectorNoiseEstHor1 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorNoiseEstHor1->setGeometry(ixpos[0]-iLeftOffset+130, iypos[2] + 70, 30, iButtonHeight);

   mConnectorNoiseEstVer1 = new Connector(this, Connector::Vertical, Connector::None);
	mConnectorNoiseEstVer1->setGeometry(ixpos[0] - iLeftOffset + 120, iypos[0]+iButtonHeight/2 + 70, 20, iButtonHeight+iVerSpaceBetwButtons);

   mSplitterNoiseEst1 = new Splitter(this);	
   mSplitterNoiseEst1->setGeometry(ixpos[0] - iLeftOffset+125, iypos[0] + iButtonHeight / 2 - iSplitterLength / 2 + 70, iSplitterLength, iSplitterLength);

   mConnectorNoiseEstHor2 = new Connector(this, Connector::Horizontal, Connector::None);
	mConnectorNoiseEstHor2->setGeometry(ixpos[0] - iLeftOffset + 250, iypos[2] + 70,iButtonWidth/2+iHorSpaceBetwButtons-30, iButtonHeight);
	

 
   mConnectorNoiseEstVer2 = new Connector(this, Connector::Vertical, Connector::Up);
	mConnectorNoiseEstVer2->setGeometry(ixpos[0] - iLeftOffset + 345, iypos[1] + 70, 20, iButtonHeight/2+iVerSpaceBetwButtons);

   mCheckBoxNoiseEst = new QCheckBox(tr("On"),this);
	mCheckBoxNoiseEst->setGeometry(ixpos[0] - iLeftOffset + 255, iypos[2] + 70, iCheckBoxWidth, iCheckBoxHeight);
	mCheckBoxNoiseEst->setChecked(true);
   connect(mCheckBoxNoiseEst, SIGNAL(toggled(bool)), this, SLOT(setNoiseEstSend(bool)));

   mProgressBarNoiseEst = new QProgressBar(this);
   mProgressBarNoiseEst->setGeometry(ixpos[0] - iLeftOffset +255, iypos[3]-iProgressBarHeight + 70, iProgressBarWidth, iProgressBarHeight);
   mProgressBarNoiseEst->setValue(100);			   
   if (miProVis != (T_INT) 1)
   {
      mProgressBarNoiseEst->setVisible(false); 
   }
    
  /*---------------------------------------------------------------------------------------------*
   * Noise suppression
   *---------------------------------------------------------------------------------------------*/
   mPushButtonNs = new QPushButton(tr("Spatial\nfiltering"), this);
   mPushButtonNs->setGeometry(ixpos[0] - iLeftOffset+310, iypos[0] + 70, iButtonWidth, iButtonHeight);

   mConnectorNsHor1 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorNsHor1->setGeometry(ixpos[0] - iLeftOffset+400, iypos[0] + 70, iHorSpaceBetwButtons, iButtonHeight);

	mConnectorNsHor2 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorNsHor2->setGeometry(ixpos[0] - iLeftOffset+40, iypos[0]+70, 2*iHorSpaceBetwButtons+iButtonWidth, iButtonHeight);

   mCheckBoxNs = new QCheckBox(tr("On"),this);
	mCheckBoxNs->setGeometry(ixpos[0] - iLeftOffset + 405, iypos[0] + 70, iCheckBoxWidth, iCheckBoxHeight);
	mCheckBoxNs->setChecked(mpsHsabAlgCore->HsabBfData.iUsePf);
   connect(mCheckBoxNs, SIGNAL(toggled(bool)), this, SLOT(setPostfilter(bool)));

   mProgressBarNs = new QProgressBar(this);
   mProgressBarNs->setGeometry(ixpos[0] - iLeftOffset + 405, iypos[1]-iProgressBarHeight + 70, iProgressBarWidth, iProgressBarHeight);
   mProgressBarNs->setValue(100);  

   if (miProVis != (T_INT) 1)
   {
       mProgressBarNs->setVisible(false); 
   }

  /*---------------------------------------------------------------------------------------------*
   * Parameter adjustment
   *---------------------------------------------------------------------------------------------*/
   mTabWidgetPostfilterParameter = new QTabWidget(this);
   mTabWidgetPostfilterParameter->setGeometry(ixpos[0] - iLeftOffset + 605, iypos[0], 260, 245);

  /*-------------------------------------------------------------------------------*
   * Noise suppression
   *-------------------------------------------------------------------------------*/
	mParamTabPostfilter = new TabAlgoStructurePostfilterParamPostfilter( &(mpsHsabAlgCore->HsabBfData),
		                                                                  &(mpsHsabAlgCore->HsabAlgCoreParams),
																			               this );

	mTabWidgetPostfilterParameter->addTab(mParamTabPostfilter, tr("Spatial postfilter"));
	
	connect(mParamTabPostfilter, SIGNAL(setMaxAttGaindB(double)), this, SLOT(setMaxAttGaindB(double)));
   connect(mParamTabPostfilter, SIGNAL(setOverEstdB(double)), this, SLOT(setOverEstdB(double)));

}


void TabAlgoStructurePostfilter::setMaxAttGaindB(double fMaxAttValue)
{
   if( HSAB_HANDSFREE_MODE == mpsHsabAlgCore->HsabAlgCoreParams.iProcessMode )
   {
      mpsHsabAlgCore->HsabBfData.sBfPfData.fSpecFloorHandsFreedB = fMaxAttValue;
      mpsHsabAlgCore->HsabBfData.sBfPfData.fSpecFloor = 
         (T_FLOAT) pow( (T_FLOAT) 10, (T_FLOAT) - fMaxAttValue / (T_FLOAT) 20.0 );
   }
   if( HSAB_RECOGNITION_MODE == mpsHsabAlgCore->HsabAlgCoreParams.iProcessMode )
   {
      mpsHsabAlgCore->HsabBfData.sBfPfData.fSpecFloorRecogndB = fMaxAttValue;
      mpsHsabAlgCore->HsabBfData.sBfPfData.fSpecFloor = 
         (T_FLOAT) pow( (T_FLOAT) 10, (T_FLOAT) - fMaxAttValue / (T_FLOAT) 20.0 );
   }
}

void TabAlgoStructurePostfilter::setOverEstdB(double fMaxAttValue)
{
   if( HSAB_HANDSFREE_MODE == mpsHsabAlgCore->HsabAlgCoreParams.iProcessMode )
   {
      mpsHsabAlgCore->HsabBfData.sBfPfData.fOverEstHandsFreedB = fMaxAttValue;
      mpsHsabAlgCore->HsabBfData.sBfPfData.fOverEst = 
         (T_FLOAT) pow( (T_FLOAT) 10, (T_FLOAT) - fMaxAttValue / (T_FLOAT) 20.0 );
   }
   if( HSAB_RECOGNITION_MODE == mpsHsabAlgCore->HsabAlgCoreParams.iProcessMode )
   {
      mpsHsabAlgCore->HsabBfData.sBfPfData.fOverEstRecogndB = fMaxAttValue;
      mpsHsabAlgCore->HsabBfData.sBfPfData.fOverEst = 
         (T_FLOAT) pow( (T_FLOAT) 10, (T_FLOAT) - fMaxAttValue / (T_FLOAT) 20.0 );
   }
}




void TabAlgoStructurePostfilter::setPostfilter(bool bNewState)
{
   mpsHsabAlgCore->HsabBfData.iUsePf = (T_INT) bNewState;
}

/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructurePostfilter::resizeEvent ( QResizeEvent * event )
{

}


