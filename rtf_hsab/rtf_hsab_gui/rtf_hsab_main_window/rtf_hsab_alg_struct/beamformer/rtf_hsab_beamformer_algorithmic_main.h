/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_BEAMFORMER_ALGORITHMIC_MAIN_H
#define RTF_HSAB_BEAMFORMER_ALGORITHMIC_MAIN_H

#include <QtWidgets>
#include "alg_core_hsab_defines.h"
#include "types_hsab.h"

class Connector;
class Splitter;
class Adder;
class TabAlgoStructureBeamformerParamBeamformer;
class TabAlgoStructureBeamformerParamBlockingMatrix;


class TabAlgoStructureBeamformer : public QWidget
{

   Q_OBJECT

   public:
      TabAlgoStructureBeamformer(   tHsabAlgCoreMain *psHsabAlgCore,
                                    const int         iProVis, 
                                    QWidget          *parent = 0 );

   signals:
	  

   protected:
		void resizeEvent(QResizeEvent *event);
		

   public slots:
		
      
      


   private: 

      tHsabAlgCoreMain  *mpsHsabAlgCore;
	  tHsabBfData       *mpsHsabBfData;

	  T_INT              miProVis;

	  QTabWidget        *mTabWidgetBeamformerParameter;
	  TabAlgoStructureBeamformerParamBeamformer  *mParamTabBeamformer;
	  TabAlgoStructureBeamformerParamBlockingMatrix  *mParamTabBlockingMatrix;
	  
	  
      Connector        *mConnectorBeamfHor1;
      Connector        *mConnectorBeamfHor2;
      Connector        *mConnectorBeamfHor3;
      QPushButton      *mPushButtonBeamf1;

      Connector        *mConnectorBlockMat1;
      Connector        *mConnectorBlockMat2;
      Connector        *mConnectorBlockMat3;
      Connector        *mConnectorBlockMat4;
      QPushButton      *mPushButtonBlockMat;

      Connector        *mConnectorIntferCancHor1;
      Connector        *mConnectorIntferCancHor2;
      Connector        *mConnectorIntferCancVer1;
      QPushButton      *mPushButtonIntferCanc;

      Splitter         *mSplitter1;
      Splitter         *mSplitter2;
      Splitter         *mSplitter3;

      Splitter         *mSplitter4;
      Splitter         *mSplitter5;
      Splitter         *mSplitter6;

      Splitter         *mSplitter7;
      Splitter         *mSplitter8;
      Splitter         *mSplitter9;

      Adder				  *mAdderIntfer;

      QCheckBox        *mCheckBoxFixedBeamf;
      QCheckBox        *mCheckBoxBlockMat;
      QCheckBox        *mCheckBoxIntferCanc;

      QProgressBar     *mProgressBarIntferCanc;
      QProgressBar     *mProgressBarFixedBeamf;
      QProgressBar     *mProgressBarBlockMat;


};




#endif