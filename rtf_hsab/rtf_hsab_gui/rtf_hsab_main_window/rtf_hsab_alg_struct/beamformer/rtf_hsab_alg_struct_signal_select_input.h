/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_ALG_STRUCT_SIGNAL_SELECT_INPUT_H
#define RTF_HSAB_ALG_STRUCT_SIGNAL_SELECT_INPUT_H

#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_audio_io.h"

#include <QCheckBox>


    class MicInComboBoxWidget : public QComboBox
    {
        Q_OBJECT
    public:
        MicInComboBoxWidget(int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void CurrentIndexMic(int);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };

	    class MicInCheckBoxWidget : public QCheckBox
    {
        Q_OBJECT
    public:
        MicInCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void activateMicComboBox(bool bAct);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };


 class PhoneInComboBoxWidget : public QComboBox
    {
        Q_OBJECT
    public:
        PhoneInComboBoxWidget(int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:
		void CurrentIndexPhone(int);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };

 class PhoneInCheckBoxWidget : public QCheckBox
    {
        Q_OBJECT
    public:
        PhoneInCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void activatePhoneComboBox(bool bAct);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };



class TabAlgoStructureSigSelectInput: public QWidget

{
    Q_OBJECT

    public:
        TabAlgoStructureSigSelectInput (  AudioIO *pAudioIO, tSession *ps,
                                    QWidget          *parent = 0 );

   protected:

   signals:
		

    public slots:
		
private:
	 T_INT iChannelNumber;

	 QScrollArea     *mScrollArea;
     QHBoxLayout     *SigSelectInputLayout;
	 
	 QString  sChannelNameMic; 
	 MicInComboBoxWidget  *mMicInComboBoxWidget;
	 MicInCheckBoxWidget  *mMicInCheckBoxWidget;
	 QVector<MicInComboBoxWidget*>   mQVectorMicInComboBoxWidget;

	 QString  sChannelNamePhone;
     PhoneInComboBoxWidget  *mPhoneInComboBoxWidget;
	 PhoneInCheckBoxWidget  *mPhoneInCheckBoxWidget;
	 QVector<PhoneInComboBoxWidget*>   mQVectorPhoneInComboBoxWidget;
};



#endif
