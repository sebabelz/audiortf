/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_POSTFILTER_PARAM_H
#define RTF_HSAB_POSTFILTER_PARAM_H

#include <QtWidgets>
#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

class TabAlgoStructurePostfilter;

class TabAlgoStructurePostfilterParamPostfilter : public QWidget
{

   Q_OBJECT

   public:
      TabAlgoStructurePostfilterParamPostfilter(   tHsabBfData                *psHsabBfData, 
							                              tHsabAlgCoreParameters     *psParams,
                                                   TabAlgoStructurePostfilter *tabStructurePostfilter );

      float					getMaxAtt();
      float					getOverEstFac();	        

   protected:
      void resizeEvent(QResizeEvent *event);

   public slots:
	  void setModeSelectNoiseSupp();
		

   signals:      
      void	setMaxAttGaindB(double);
      void	setOverEstdB(double);

   private:

		tHsabAlgCoreParameters      *mpsParams;
      tHsabBfData                 *mpsHsabBfData;
		TabAlgoStructurePostfilter  *mpTabAlgoStructurePostfilter; 

      QLabel							 *mLabelModeSelection;
      QLabel							 *mLabelParamMaxAttGaindB;
      QLabel							 *mLabelParamOverestGaindB;

	  QComboBox				*mComboBoxModeSelection;
	  QDoubleSpinBox		*mDoubleSpinBoxParamMaxAttGaindB;
	  QDoubleSpinBox		*mDoubleSpinBoxParamOverestGaindB;
};

#endif
