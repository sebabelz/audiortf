/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_POSTFILTER_ALGORITHMIC_MAIN_H
#define RTF_HSAB_POSTFILTER_ALGORITHMIC_MAIN_H

#include <QtWidgets>
#include "alg_core_hsab_defines.h"
#include "types_hsab.h"

class Connector;
class Splitter;
class TabAlgoStructurePostfilterParamPostfilter;

class TabAlgoStructurePostfilter : public QWidget
{

   Q_OBJECT

   public:
      TabAlgoStructurePostfilter(   tHsabAlgCoreMain *psHsabAlgCore,
                                    const int         iProVis, 
                                    QWidget          *parent = 0 );

   signals:

   protected:
		void resizeEvent(QResizeEvent *event);

   public slots:
      void setMaxAttGaindB(double fMaxAttValue);
      void setOverEstdB(double fMaxAttValue);
      void setPostfilter(bool bNewState);

   private: 

      tHsabAlgCoreMain  *mpsHsabAlgCore;
		T_INT              miProVis;
      QTabWidget        *mTabWidgetPostfilterParameter;
		TabAlgoStructurePostfilterParamPostfilter  *mParamTabPostfilter;
      tHsabBfData       *mpsHsabBfData;

      QPushButton      *mPushButtonNoiseEst;
      Connector        *mConnectorNoiseEstHor1;
      Connector        *mConnectorNoiseEstHor2;
      Connector        *mConnectorNoiseEstVer1;
      Connector        *mConnectorNoiseEstVer2;
      Splitter         *mSplitterNoiseEst1;
      QCheckBox        *mCheckBoxNoiseEst;
      QProgressBar     *mProgressBarNoiseEst;

      QPushButton      *mPushButtonNs;
      Connector        *mConnectorNsHor1;
      Connector        *mConnectorNsHor2;
      QCheckBox        *mCheckBoxNs;
      QProgressBar     *mProgressBarNs;	

      QPushButton      *mPushButtonResEchoSupp;
      Connector        *mConnectorResEchoSuppHor1;
      QCheckBox        *mCheckBoxResEchoSupp;
      QProgressBar     *mProgressBarResEchoSupp;	

};




#endif