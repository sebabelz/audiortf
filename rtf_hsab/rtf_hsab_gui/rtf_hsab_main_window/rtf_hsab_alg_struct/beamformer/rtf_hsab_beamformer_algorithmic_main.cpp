/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtWidgets>
#include <math.h>
#include "defines_hsab.h"
#include "rtf_hsab_splitter.h"
#include "rtf_hsab_connector.h"
#include "rtf_hsab_adder.h"
#include "alg_core_hsab_defines.h"
#include "rtf_hsab_beamformer_algorithmic_main.h"
#include "rtf_hsab_beamformer_algorithmic_param.h"

TabAlgoStructureBeamformer::TabAlgoStructureBeamformer(  tHsabAlgCoreMain *psHsabAlgCore,
                                                         const int         iProVis, 
                                                         QWidget          *parent )
     : QWidget(parent)
{

   int iButtonWidth         =  90;
   int iButtonHeight = 90;
   int iRightOffset = 120;
	T_INT iLeftOffset          =  90;
	T_INT iTopOffset           =  15;
	T_INT iHorSpaceBetwButtons =  (T_INT) (T_FLOAT(1200-iLeftOffset-iRightOffset-6*iButtonWidth) / (T_FLOAT) 5.0);
	T_INT iVerSpaceBetwButtons =  15;
   T_INT iSplitterLength      =  10;
   T_INT iLeftRightBoundary   =  74;
   T_INT iAdderLength         =  22;
   T_INT iCheckBoxWidth       =  45;
   T_INT iCheckBoxHeight      =  18;
   T_INT iProgressBarWidth    =  58;
   T_INT iProgressBarHeight   =  18;
   
   T_INT   iWidgetHeigth        = 22;

   T_INT ixpos[12];
	T_INT iypos[8];
	 
	ixpos[0]  = iLeftOffset;
	ixpos[1]  = iLeftOffset + 1 * iButtonWidth;
   ixpos[2]  = iLeftOffset + 1 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[3]  = iLeftOffset + 2 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[4]  = iLeftOffset + 2 * iButtonWidth + 2 * iHorSpaceBetwButtons;
	ixpos[5]  = iLeftOffset + 3 * iButtonWidth + 2 * iHorSpaceBetwButtons;
   ixpos[6]  = iLeftOffset + 3 * iButtonWidth + 3 * iHorSpaceBetwButtons;
	ixpos[7]  = iLeftOffset + 4 * iButtonWidth + 3 * iHorSpaceBetwButtons;
   ixpos[8]  = iLeftOffset + 4 * iButtonWidth + 4 * iHorSpaceBetwButtons;
	ixpos[9]  = iLeftOffset + 5 * iButtonWidth + 4 * iHorSpaceBetwButtons;
   ixpos[10] = iLeftOffset + 5 * iButtonWidth + 5 * iHorSpaceBetwButtons;
	ixpos[11] = iLeftOffset + 6 * iButtonWidth + 5 * iHorSpaceBetwButtons;

	iypos[0] = iTopOffset;
   iypos[1] = iTopOffset + 1 * iButtonHeight;
 	iypos[2] = iTopOffset + 1 * iButtonHeight + 1 * iVerSpaceBetwButtons;
	iypos[3] = iTopOffset + 2 * iButtonHeight + 1 * iVerSpaceBetwButtons;
	iypos[4] = iTopOffset + 2 * iButtonHeight + 2 * iVerSpaceBetwButtons;
	iypos[5] = iTopOffset + 3 * iButtonHeight + 2 * iVerSpaceBetwButtons;
	iypos[6] = iTopOffset + 3 * iButtonHeight + 3 * iVerSpaceBetwButtons;
	iypos[7] = iTopOffset + 4 * iButtonHeight + 3 * iVerSpaceBetwButtons;

  /*---------------------------------------------------------------------------------------------*
   * Flag for visualization of the development progress
   *---------------------------------------------------------------------------------------------*/
   miProVis = iProVis;

  /*---------------------------------------------------------------------------------------------*
   * Save address of algorithmic core struct
   *---------------------------------------------------------------------------------------------*/
   mpsHsabAlgCore = psHsabAlgCore;

  /*---------------------------------------------------------------------------------------------*
   * Fixed beamforming
   *---------------------------------------------------------------------------------------------*/
   mConnectorBeamfHor1 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBeamfHor1->setGeometry(ixpos[0] - iLeftOffset + 40, iypos[0] + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mPushButtonBeamf1 = new QPushButton(tr("Fixed\nbeamformer"), this);
   mPushButtonBeamf1->setGeometry(ixpos[1] - iLeftOffset + 40, iypos[0] + 20, iButtonWidth, iButtonHeight);

   mConnectorBeamfHor2 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBeamfHor2->setGeometry(ixpos[2] - iLeftOffset + 40, iypos[0] + 20, 2*iHorSpaceBetwButtons+iButtonWidth-iAdderLength/2, iButtonHeight);

   mConnectorBeamfHor3 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBeamfHor3->setGeometry(ixpos[0] - iLeftOffset + 40, iypos[1]-iButtonHeight/2 + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mSplitter1 = new Splitter(this);
	mSplitter1->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[0]+iButtonHeight/2-iSplitterLength-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);
   
	mSplitter2 = new Splitter(this);
	mSplitter2->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[0]+iButtonHeight/2-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mSplitter3 = new Splitter(this);
	mSplitter3->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[0]+iButtonHeight/2+iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mCheckBoxFixedBeamf = new QCheckBox(tr("On"),this);
	mCheckBoxFixedBeamf->setGeometry(ixpos[2]+5 - iLeftOffset + 40, iypos[0]+23 + 20, iCheckBoxWidth, iCheckBoxHeight);


   mProgressBarFixedBeamf = new QProgressBar(this);
   mProgressBarFixedBeamf->setGeometry(ixpos[2]+5 - iLeftOffset + 40, iypos[0]+50 + 20, iProgressBarWidth, iProgressBarHeight);
   mProgressBarFixedBeamf->setValue(100);   

	if (miProVis != (T_INT) 1)
   {
      mProgressBarFixedBeamf->setVisible(false); 
   }

  /*---------------------------------------------------------------------------------------------*
   * Blocking matrix
   *---------------------------------------------------------------------------------------------*/
   mConnectorBlockMat1 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBlockMat1->setGeometry(ixpos[0] - iLeftOffset + 40, iypos[2] + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mPushButtonBlockMat = new QPushButton(tr("Blocking\nmatrix"), this);
   mPushButtonBlockMat->setGeometry(ixpos[1] - iLeftOffset + 40, iypos[2] + 20, iButtonWidth, iButtonHeight);

   mConnectorBlockMat2 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBlockMat2->setGeometry(ixpos[2] - iLeftOffset + 40, iypos[2] + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mConnectorBlockMat3 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBlockMat3->setGeometry(ixpos[0] - iLeftOffset + 40, iypos[3]-iButtonHeight/2 + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mConnectorBlockMat4 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorBlockMat4->setGeometry(ixpos[2] - iLeftOffset + 40, iypos[3]-iButtonHeight/2 + 20, iHorSpaceBetwButtons, iButtonHeight/2);

   mSplitter4 = new Splitter(this);
	mSplitter4->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2-iSplitterLength-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);
   
	mSplitter5 = new Splitter(this);
	mSplitter5->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mSplitter6 = new Splitter(this);
	mSplitter6->setGeometry(ixpos[0]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2+iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mCheckBoxBlockMat = new QCheckBox(tr("On"),this);
	mCheckBoxBlockMat->setGeometry(ixpos[2]+5 - iLeftOffset + 40, iypos[2] + 20, iCheckBoxWidth, iCheckBoxHeight);
	connect(mCheckBoxBlockMat, SIGNAL(toggled(bool)), this, SLOT(setModeSelectBlockMat(bool)));

   mProgressBarBlockMat = new QProgressBar(this);
   mProgressBarBlockMat->setGeometry(ixpos[2]+5 - iLeftOffset + 40, iypos[3]-iProgressBarHeight + 20, iProgressBarWidth, iProgressBarHeight);
   mProgressBarBlockMat->setValue(100);   

	if (miProVis != (T_INT) 1)
   {
      mProgressBarBlockMat->setVisible(false); 
   }

  /*---------------------------------------------------------------------------------------------*
   * Interference canceller
   *---------------------------------------------------------------------------------------------*/
	mPushButtonIntferCanc = new QPushButton(tr("Interference\ncanceller"), this);
   mPushButtonIntferCanc->setGeometry(ixpos[3] - iLeftOffset + 40, iypos[2]+20, iButtonWidth, iButtonHeight);

   mConnectorIntferCancHor1 = new Connector(this, Connector::Horizontal, Connector::None);
	mConnectorIntferCancHor1->setGeometry(ixpos[4] - iLeftOffset + 40, iypos[2] + 20, iHorSpaceBetwButtons, iButtonHeight);

   mConnectorIntferCancVer1 = new Connector(this, Connector::Vertical, Connector::Up);
	mConnectorIntferCancVer1->setGeometry(ixpos[4] - iLeftOffset + 40, iypos[0]+iButtonHeight/2+iAdderLength/2 + 20, 2*iHorSpaceBetwButtons, iButtonHeight+iVerSpaceBetwButtons-iAdderLength/2);

	mAdderIntfer = new Adder(this);
	mAdderIntfer->setGeometry(ixpos[5]-iAdderLength/2 - iLeftOffset + 40, iypos[0]+iButtonHeight/2-iAdderLength/2 + 20, iAdderLength, iAdderLength);
   
   mConnectorIntferCancHor2 = new Connector(this, Connector::Horizontal, Connector::Right);
	mConnectorIntferCancHor2->setGeometry(ixpos[5]+iAdderLength/2 - iLeftOffset + 40, iypos[0] + 20, iHorSpaceBetwButtons, iButtonHeight);

   mSplitter7 = new Splitter(this);
	mSplitter7->setGeometry(ixpos[2]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2-iSplitterLength-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);
   
	mSplitter8 = new Splitter(this);
	mSplitter8->setGeometry(ixpos[2]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2-iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mSplitter9 = new Splitter(this);
	mSplitter9->setGeometry(ixpos[2]+iButtonWidth/2-iSplitterLength/2 - iLeftOffset + 40, iypos[2]+iButtonHeight/2+iSplitterLength/2 + 20, iSplitterLength, iSplitterLength);

   mCheckBoxIntferCanc = new QCheckBox(tr("On"),this);
	mCheckBoxIntferCanc->setGeometry(ixpos[4]+5 - iLeftOffset + 40, iypos[2]+23 + 20, iCheckBoxWidth, iCheckBoxHeight);

   mProgressBarIntferCanc = new QProgressBar(this);
   mProgressBarIntferCanc->setGeometry(ixpos[4]+5 - iLeftOffset + 40, iypos[2]+50 + 20, iProgressBarWidth, iProgressBarHeight);
   mProgressBarIntferCanc->setValue(100);   

	if (miProVis != (T_INT) 1)
   {
      mProgressBarIntferCanc->setVisible(false); 
   }

	/*---------------------------------------------------------------------------------------------*
	* Parameter adjustment
	*---------------------------------------------------------------------------------------------*/
	mTabWidgetBeamformerParameter = new QTabWidget(this);
	mTabWidgetBeamformerParameter->setGeometry(ixpos[0] - iLeftOffset + 605, iypos[0], 260, 245);


	mParamTabBeamformer = new TabAlgoStructureBeamformerParamBeamformer(&(mpsHsabAlgCore->HsabBfData),&(mpsHsabAlgCore->HsabAlgCoreParams),this);
	mParamTabBlockingMatrix = new TabAlgoStructureBeamformerParamBlockingMatrix(&(mpsHsabAlgCore->HsabBfData), &(mpsHsabAlgCore->HsabAlgCoreParams), this);
	

	mTabWidgetBeamformerParameter->addTab(mParamTabBeamformer, tr("Beamformer"));
	mTabWidgetBeamformerParameter->addTab(mParamTabBlockingMatrix, tr("Blocking Matrix"));

	
}






/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructureBeamformer::resizeEvent ( QResizeEvent * event )
{

 
}



//void TabAlgoStructureBeamformer::setBfMode(bool bModeSelect)
//{
//
//	if (bModeSelect == false) 
//	{
//		mpsHsabBfData->iMainControl = 0;
//		mComboBoxSelectBfMode->setCurrentIndex(2);
//	}
//
//	else 
//	{
//		mpsHsabBfData->iUseAdaptBf = 0;
//		mpsHsabBfData->iMainControl = 1;
//		mComboBoxSelectBfMode->setCurrentIndex(0);
//	}
//}


//void TabAlgoStructureBeamformer::setModeSelectBlockMat(bool bModeSelect)
//{
//	if (bModeSelect == false) 
//	{
//
//	}
//	
//	
//	mpsHsabBfData->iUseAbm = bModeSelect;
//
//	if (mpsHsabBfData->iUseAbm == 0)
//	{
//		mpsHsabBfData->iNumOutBm = mpsParams->iNumMicIn - 1;
//	}
//	else
//	{
//		mpsHsabBfData->iNumOutBm = mpsParams->iNumMicIn;
//	}
//}