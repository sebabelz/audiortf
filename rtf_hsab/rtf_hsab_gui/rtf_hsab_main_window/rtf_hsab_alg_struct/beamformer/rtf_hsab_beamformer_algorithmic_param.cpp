/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

/*
Informatik IV Projekt 2018
programmed by s160921, Christian Wagner
*/

#include <QtWidgets>
#include "types_hsab.h"
#include "defines_hsab.h"
#include "rtf_hsab_beamformer_algorithmic_main.h"
#include "rtf_hsab_beamformer_algorithmic_param.h"



//BEAMFORMER Parameter Class

TabAlgoStructureBeamformerParamBeamformer::TabAlgoStructureBeamformerParamBeamformer(	tHsabBfData            *psHsabBfData,
												                                       tHsabAlgCoreParameters     *psParams,
                                                                                       TabAlgoStructureBeamformer *tabStructureBeamformer )

{
	T_INT   iLeftOffset = 20;
	T_INT   iTopOffset = 20;
	T_INT   iWidgetHeigth = 22;
	T_INT   iVerSpaceBetwWidgets = 8;

   QString Text;



   /*---------------------------------------------------------------------------------------------*
   * Save address of parameter and beamformer struct
   *---------------------------------------------------------------------------------------------*/
   mpsHsabBfData = psHsabBfData;
   mpsParams = psParams;
   
   /*-------------------------------------------------------------------------------*
   * Mode selection (fixed and adaptive beamforimg)
   *-------------------------------------------------------------------------------*/
   mLabelSelectBfMode = new QLabel(this);
   mLabelSelectBfMode->setGeometry(iLeftOffset + 5, iTopOffset+10, 100, iWidgetHeigth);
   mLabelSelectBfMode->setText("Mode selection:  ");
   mLabelSelectBfMode->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

   mComboBoxSelectBfMode = new QComboBox(this);
   mComboBoxSelectBfMode->setGeometry(iLeftOffset + 115, iTopOffset+10, 110, iWidgetHeigth);
   mComboBoxSelectBfMode->addItem("Fixed");
   mComboBoxSelectBfMode->addItem("Adaptive");
   mComboBoxSelectBfMode->addItem("Disable");
   mComboBoxSelectBfMode->setCurrentIndex(psHsabBfData->iUseAdaptBf);
   connect(mComboBoxSelectBfMode, SIGNAL(currentIndexChanged(int)), this, SLOT(setBfMode(int)));


   /*-------------------------------------------------------------------------------*
   * Stepsize
   *-------------------------------------------------------------------------------*/
   mLabelParamStepsize = new QLabel(this);
   mLabelParamStepsize->setGeometry(iLeftOffset+5, iTopOffset + 40, 100, iWidgetHeigth);
   mLabelParamStepsize->setText("Stepsize:  ");
   mLabelParamStepsize->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

   mDoubleSpinBoxParamStepsize = new QDoubleSpinBox(this);
   mDoubleSpinBoxParamStepsize->setGeometry(iLeftOffset + 115, iTopOffset + 40, 110, iWidgetHeigth);
   
   mDoubleSpinBoxParamStepsize->setSingleStep((T_DOUBLE) 0.1);
   mDoubleSpinBoxParamStepsize->setMinimum((T_DOUBLE) 0.0);
   mDoubleSpinBoxParamStepsize->setMaximum((T_DOUBLE)  2.0);
   mDoubleSpinBoxParamStepsize->setValue(mpsHsabBfData->fStepsizeIc);
   connect(mDoubleSpinBoxParamStepsize, SIGNAL(valueChanged(double)), this, SLOT(setStepsize()));


   /*-------------------------------------------------------------------------------*
   * SteerAngle
   *-------------------------------------------------------------------------------*/
   mLabelParamSteerAngle = new QLabel(this);
   mLabelParamSteerAngle->setGeometry(iLeftOffset -15, iTopOffset + 70, 120, iWidgetHeigth);
   mLabelParamSteerAngle->setText("SteerAngle in Degree:  ");
   mLabelParamSteerAngle->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

   mDoubleSpinBoxParamSteerAngle = new QDoubleSpinBox(this);
   mDoubleSpinBoxParamSteerAngle->setGeometry(iLeftOffset + 115, iTopOffset + 70, 110, iWidgetHeigth);
   
   mDoubleSpinBoxParamSteerAngle->setSingleStep((T_DOUBLE) 1.0);
   mDoubleSpinBoxParamSteerAngle->setMinimum((T_DOUBLE) 0.0);
   mDoubleSpinBoxParamSteerAngle->setMaximum((T_DOUBLE)  180.0);
   mDoubleSpinBoxParamSteerAngle->setValue(mpsHsabBfData->fSteerAngle);
   connect(mDoubleSpinBoxParamSteerAngle, SIGNAL(valueChanged(double)), this, SLOT(setSteerAngleD()));


   /*-------------------------------------------------------------------------------*
   * DistMic
   *-------------------------------------------------------------------------------*/
   mLabelParamDistMic = new QLabel(this);
   mLabelParamDistMic->setGeometry(iLeftOffset + 5, iTopOffset + 100, 100, iWidgetHeigth);
   mLabelParamDistMic->setText("DistMic in m:  ");
   mLabelParamDistMic->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

   mDoubleSpinBoxParamDistMic = new QDoubleSpinBox(this);
   mDoubleSpinBoxParamDistMic->setGeometry(iLeftOffset + 115, iTopOffset + 100, 110, iWidgetHeigth);

   
   mDoubleSpinBoxParamDistMic->setSingleStep((T_DOUBLE) 0.05);
   mDoubleSpinBoxParamDistMic->setMinimum((T_DOUBLE) 0.0);
   mDoubleSpinBoxParamDistMic->setMaximum((T_DOUBLE)  100.0);   
   mDoubleSpinBoxParamDistMic->setValue(mpsHsabBfData->fDistMic);
   connect(mDoubleSpinBoxParamDistMic, SIGNAL(valueChanged(double)), this, SLOT(setDistMicM()));

   
}

/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructureBeamformerParamBeamformer::resizeEvent ( QResizeEvent * event )
{

   T_INT iButtonWidth         =  90;
   if (width() < 1000)
   {
      iButtonWidth = (T_INT) ((T_FLOAT) 90.0 * (width()-(T_FLOAT) 400.0) / (T_FLOAT) 600.0 );
   }

	T_INT   iLeftOffset          = 20;
   T_INT   iTopOffset           = 20;
   T_INT   iWidgetHeigth        = 22;

	T_INT iRightOffset         =  90;

   T_INT iWidth               =  width();
   if (iWidth > (T_INT) 1200)
   {
      iLeftOffset        += (iWidth - (T_INT) 1200) / 2;
      iRightOffset       += (iWidth - (T_INT) 1200) / 2;
      iWidth              = 1200;
   }

   T_INT iMinSizeForChangingText        = 1000;

 
}

//Funktionen werden bei ValueChanged bzw. IndexChanged Event der jeweiligen GUI Komponente (SpinBox, ComboBox) aufgerufen
//Setzt aktuellen Mode des Beamformer und f�llt SpinBoxen mit aktuellem Parameter
void TabAlgoStructureBeamformerParamBeamformer::setBfMode(int iModeSelect)
{
	mpsHsabBfData->iUseAdaptBf = iModeSelect;
	mpsHsabBfData->iMainControl = 1;
	if (mpsHsabBfData->iUseAdaptBf == 2)
	{
		mpsHsabBfData->iMainControl = 0;
	}
	mDoubleSpinBoxParamStepsize->setValue(mpsHsabBfData->fStepsizeIc);
	mDoubleSpinBoxParamSteerAngle->setValue(mpsHsabBfData->fSteerAngle);
	mDoubleSpinBoxParamDistMic->setValue(mpsHsabBfData->fDistMic);
}

//Setzt Parameter DistMic des alg. Kern
void TabAlgoStructureBeamformerParamBeamformer::setDistMicM()
{
	mpsHsabBfData->fDistMic = mDoubleSpinBoxParamDistMic->value();
}

//Setzt Parameter SteerAngle des alg. Kern
void TabAlgoStructureBeamformerParamBeamformer::setSteerAngleD()
{
	mpsHsabBfData->fSteerAngle = mDoubleSpinBoxParamSteerAngle->value();
}

//Setzt Parameter StepSize des alg. Kern
void TabAlgoStructureBeamformerParamBeamformer::setStepsize()
{
	mpsHsabBfData->fStepsizeIc = mDoubleSpinBoxParamStepsize->value();
}






//BLOCKING MATRIX



TabAlgoStructureBeamformerParamBlockingMatrix::TabAlgoStructureBeamformerParamBlockingMatrix(tHsabBfData            *psHsabBfData,
	tHsabAlgCoreParameters     *psParams,
	TabAlgoStructureBeamformer *tabStructureBeamformer)

{
	T_INT   iLeftOffset = 20;
	T_INT   iTopOffset = 20;
	T_INT   iWidgetHeigth = 22;
	T_INT   iVerSpaceBetwWidgets = 8;

	QString Text;

	/*---------------------------------------------------------------------------------------------*
	* Save address of parameter and beamformer struct
	*---------------------------------------------------------------------------------------------*/
	mpsHsabBfData = psHsabBfData;
	mpsParams = psParams;

	

	/*-------------------------------------------------------------------------------*
	* Mode selection (blocking matrix)
	*-------------------------------------------------------------------------------*/
	mLabelModeSelectBlockMat = new QLabel(this);
	mLabelModeSelectBlockMat->setGeometry(iLeftOffset + 5, iTopOffset+10, 100, iWidgetHeigth);
	mLabelModeSelectBlockMat->setText("Mode selection:  ");
	mLabelModeSelectBlockMat->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	mComboBoxModeSelectBlockMat = new QComboBox(this);
	mComboBoxModeSelectBlockMat->setGeometry(iLeftOffset + 115, iTopOffset + 10, 110, iWidgetHeigth);
	mComboBoxModeSelectBlockMat->addItem("Fixed");
	mComboBoxModeSelectBlockMat->addItem("Adaptive");
	mComboBoxModeSelectBlockMat->setCurrentIndex(psHsabBfData->iUseAbm);
	connect(mComboBoxModeSelectBlockMat, SIGNAL(currentIndexChanged(int)), this, SLOT(setModeSelectBlockMat(int)));
		


	/*-------------------------------------------------------------------------------*
	* max. Stepsize
	*-------------------------------------------------------------------------------*/
	mLabelParamMaxStepsizeAbm = new QLabel(this);
	mLabelParamMaxStepsizeAbm->setGeometry(iLeftOffset + 5, iTopOffset + 40, 100, iWidgetHeigth);
	mLabelParamMaxStepsizeAbm->setText("max. Stepsize:  ");
	mLabelParamMaxStepsizeAbm->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	mDoubleSpinBoxParamMaxStepsizeAbm = new QDoubleSpinBox(this);
	mDoubleSpinBoxParamMaxStepsizeAbm->setGeometry(iLeftOffset + 115, iTopOffset + 40, 110, iWidgetHeigth);

	mDoubleSpinBoxParamMaxStepsizeAbm->setSingleStep((T_DOUBLE) 0.1);
	mDoubleSpinBoxParamMaxStepsizeAbm->setMinimum((T_DOUBLE) 0.0);
	mDoubleSpinBoxParamMaxStepsizeAbm->setMaximum((T_DOUBLE) 2.0);
	mDoubleSpinBoxParamMaxStepsizeAbm->setValue(mpsHsabBfData->sBfAbmData.fMaxStepsizeAbm);
	connect(mDoubleSpinBoxParamMaxStepsizeAbm, SIGNAL(valueChanged(double)), this, SLOT(setMaxStepsizeAbm()));

}

/*------------------------------------------------------------------------------------------------*
* Resize tab
*------------------------------------------------------------------------------------------------*/
void TabAlgoStructureBeamformerParamBlockingMatrix::resizeEvent(QResizeEvent * event)
{

	T_INT iButtonWidth = 90;
	if (width() < 1000)
	{
		iButtonWidth = (T_INT)((T_FLOAT) 90.0 * (width() - (T_FLOAT) 400.0) / (T_FLOAT) 600.0);
	}

	T_INT   iLeftOffset = 20;
	T_INT   iTopOffset = 20;
	T_INT   iWidgetHeigth = 22;

	T_INT iRightOffset = 90;

	T_INT iWidth = width();
	if (iWidth > (T_INT)1200)
	{
		iLeftOffset += (iWidth - (T_INT)1200) / 2;
		iRightOffset += (iWidth - (T_INT)1200) / 2;
		iWidth = 1200;
	}

	T_INT iMinSizeForChangingText = 1000;


}

//Funktionen werden bei ValueChanged bzw. IndexChanged Event der jeweiligen GUI Komponente (SpinBox, ComboBox) aufgerufen
//Setzt aktuellen Mode der Blocking Matrix und f�llt SpinBox mit aktuellem Parameter
void TabAlgoStructureBeamformerParamBlockingMatrix::setModeSelectBlockMat(int iModeSelect)
{
	mpsHsabBfData->iUseAbm = iModeSelect;

	if (mpsHsabBfData->iUseAbm == 0)
	{
		mpsHsabBfData->iNumOutBm = 0;
	}
	else
	{
		mpsHsabBfData->iNumOutBm = mpsParams->iNumMicIn;
	}
		
	mDoubleSpinBoxParamMaxStepsizeAbm->setValue(mpsHsabBfData->sBfAbmData.fMaxStepsizeAbm);
}

//Setzt Parameter Max. StepSize des alg. Kern
void TabAlgoStructureBeamformerParamBlockingMatrix::setMaxStepsizeAbm()
{
	mpsHsabBfData->sBfAbmData.fMaxStepsizeAbm = mDoubleSpinBoxParamMaxStepsizeAbm->value();
}
