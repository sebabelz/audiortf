/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/ 

#ifndef RTF_HSAB_ALG_STRUCT_SIGNAL_SELECT_OUTPUT_H
#define RTF_HSAB_ALG_STRUCT_SIGNAL_SELECT_OUTPUT_H

#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_audio_io.h"

#include <QCheckBox>

class LspOutComboBoxWidget : public QComboBox
    {
        Q_OBJECT
    public:
        LspOutComboBoxWidget(int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void CurrentIndexLsp(int);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };

	    class LspOutCheckBoxWidget : public QCheckBox
    {
        Q_OBJECT
    public:
        LspOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void activateLspOutComboBox(bool bAct);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };

  class PhoneOutComboBoxWidget : public QComboBox
    {
        Q_OBJECT
    public:
        PhoneOutComboBoxWidget(int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:
		void CurrentIndexPhone(int);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };

 class PhoneOutCheckBoxWidget : public QCheckBox
    {
        Q_OBJECT
    public:
        PhoneOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

    void activatePhoneComboBox(bool bAct);

    private:
       T_INT   mChannelNumber;
	   AudioIO *mpAudioIO;
	   tSession *mps;
    };



class TabAlgoStructureSigSelectOutput: public QWidget

{
    Q_OBJECT

    public:
        TabAlgoStructureSigSelectOutput (  AudioIO *pAudioIO, tSession *ps,
                                    QWidget          *parent = 0 );

   protected:

   signals:
		

    public slots:

private:
	 T_INT iChannelNumber;

	 QScrollArea     *mScrollArea;
     QHBoxLayout     *SigSelectOutputLayout;

	 QString  sChannelNameLsp;
	 LspOutCheckBoxWidget *mLspOutCheckBoxWidget;
	 LspOutComboBoxWidget *mLspOutComboBoxWidget;
	 QVector<LspOutComboBoxWidget*>   mQVectorLspOutComboBoxWidget;
	    
	 QString  sChannelNamePhone;
     PhoneOutComboBoxWidget  *mPhoneOutComboBoxWidget;
	 PhoneOutCheckBoxWidget  *mPhoneOutCheckBoxWidget;
	 QVector<PhoneOutComboBoxWidget*>   mQVectorPhoneOutComboBoxWidget;
	    
};



#endif
