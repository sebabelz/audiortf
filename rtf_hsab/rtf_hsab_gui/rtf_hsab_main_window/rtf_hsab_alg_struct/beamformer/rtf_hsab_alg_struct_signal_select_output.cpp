/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include "rtf_hsab_alg_struct_signal_select_output.h"

LspOutComboBoxWidget::LspOutComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
   T_INT j;
   setEditable(true);
   setFixedWidth(200);
   setFixedHeight(20);
   for (j = 0; j < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; j++)
   {
      addItem(tr("Handsfree loudspeaker output %2").arg(j));
   }
   setCurrentIndex((T_INT)iChannelNumber);
   connect(this, SIGNAL(activated(int)), this, SLOT(CurrentIndexLsp(int)));
}

LspOutCheckBoxWidget::LspOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
    setText(sChannelName);
	setChecked(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(activateLspOutComboBox(bool)));
}

PhoneOutComboBoxWidget::PhoneOutComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
   T_INT j;
   setEditable(true);
   setFixedWidth(200);
   setFixedHeight(20);
   for (j = 0; j < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; j++)
   {
      addItem(tr("Handsfree phone output %1").arg(j));
   }
   setCurrentIndex((T_INT)iChannelNumber);
   connect(this, SIGNAL(activated(int)), this, SLOT(CurrentIndexPhone(int)));
}

PhoneOutCheckBoxWidget::PhoneOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
    setText(sChannelName);
	setChecked(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(activatePhoneComboBox(bool)));
}


TabAlgoStructureSigSelectOutput::TabAlgoStructureSigSelectOutput( AudioIO *pAudioIO, tSession *ps,
																   QWidget   *parent )
     : QWidget ( parent )

{
  /*-----------------------------------------------------------------------------------------------*
   * Main tab layout
   *-----------------------------------------------------------------------------------------------*/
	    QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);
	    mScrollArea = new QScrollArea(this);
		SubLayoutCenter->addWidget(mScrollArea);
        QWidget *mWidget     = new QWidget(mScrollArea);

        mScrollArea->setWidget(mWidget);
        mScrollArea->setWidgetResizable(true);
   
        QHBoxLayout *SigSelectOutputLayout = new QHBoxLayout(mWidget);
	    mWidget->setLayout(SigSelectOutputLayout);

  /*-----------------------------------------------------------------------------------------------*
   * Loudspeaker output sub-layout
   *-----------------------------------------------------------------------------------------------*/
		T_INT i;
		iChannelNumber = (T_INT) 0;
		QGroupBox   *GroupBoxLspOut        = new QGroupBox("Loudspeaker output");
		GroupBoxLspOut->setCheckable(true);
		QGridLayout *GridLayoutLspOut = new QGridLayout(GroupBoxLspOut);

		

		for(i=0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
		{
		    sChannelNameLsp = QString("Loudspeaker signal %2: ").arg(i);
			mLspOutCheckBoxWidget = new LspOutCheckBoxWidget(sChannelNameLsp,iChannelNumber, mWidget, pAudioIO,ps);
		    GridLayoutLspOut->setRowMinimumHeight( i, 20 );
		    GridLayoutLspOut->addWidget(mLspOutCheckBoxWidget,i,0);

		   mLspOutComboBoxWidget = new LspOutComboBoxWidget(iChannelNumber, mWidget, pAudioIO,ps);
		   mQVectorLspOutComboBoxWidget.append(mLspOutComboBoxWidget);
		   GridLayoutLspOut->addWidget(mQVectorLspOutComboBoxWidget[iChannelNumber],i,1);
		   iChannelNumber++;
		   
		}
		GridLayoutLspOut->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxLspOut->setLayout(GridLayoutLspOut);




  /*-----------------------------------------------------------------------------------------------*
   * Phone output sub-layout
   *-----------------------------------------------------------------------------------------------*/
	    T_INT k;
		iChannelNumber = (T_INT) 0;
		QGridLayout *GridLayoutPhone = new QGridLayout(this);
	    QGroupBox   *GroupBoxPhone        = new QGroupBox(tr("Phone output"));
		GroupBoxPhone->setCheckable(true);

		for (k = 0; k < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; k++)
		{
			sChannelNamePhone = QString("Phone uplink signal %2: ").arg(k);
			mPhoneOutCheckBoxWidget = new PhoneOutCheckBoxWidget(sChannelNamePhone,iChannelNumber, mWidget, pAudioIO,ps);
		    GridLayoutPhone->setRowMinimumHeight( k, 20 );
		    GridLayoutPhone->addWidget(mPhoneOutCheckBoxWidget,k,0);

		   mPhoneOutComboBoxWidget = new PhoneOutComboBoxWidget(iChannelNumber, mWidget, pAudioIO,ps);
		   mQVectorPhoneOutComboBoxWidget.append(mPhoneOutComboBoxWidget);
		   GridLayoutPhone->addWidget(mQVectorPhoneOutComboBoxWidget[iChannelNumber],k,1);
		   iChannelNumber++;
		}

		GridLayoutPhone->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxPhone->setLayout(GridLayoutPhone );


	/*------------------------------------------------------------------------------------------*
     * Add all sub-layouts to main Tab layout
     *------------------------------------------------------------------------------------------*/
	      SigSelectOutputLayout->addWidget(GroupBoxLspOut);
          SigSelectOutputLayout->addWidget(GroupBoxPhone);
}
   


void LspOutCheckBoxWidget::activateLspOutComboBox(bool bAct)
{
	mpAudioIO->setLspOutSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void LspOutComboBoxWidget::CurrentIndexLsp(int k)
{
	mpAudioIO->mapLspOutSignalChannelSpec(k, mChannelNumber, mps);
}

void PhoneOutCheckBoxWidget::activatePhoneComboBox(bool bAct)
{
	mpAudioIO->setPhoneOutSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void PhoneOutComboBoxWidget::CurrentIndexPhone(int k)
{
	mpAudioIO->mapPhoneOutSignalChannelSpec(k, mChannelNumber, mps);
}