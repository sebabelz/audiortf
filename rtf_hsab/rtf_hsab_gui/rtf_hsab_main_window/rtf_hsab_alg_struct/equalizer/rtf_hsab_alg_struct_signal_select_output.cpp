/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include "rtf_hsab_alg_struct_signal_select_output.h"

LspOutComboBoxWidget::LspOutComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{

}

LspOutCheckBoxWidget::LspOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{

}

PhoneOutComboBoxWidget::PhoneOutComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{

}

PhoneOutCheckBoxWidget::PhoneOutCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
    setText(sChannelName);
	setChecked(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(activatePhoneComboBox(bool)));
}


TabAlgoStructureSigSelectOutput::TabAlgoStructureSigSelectOutput( AudioIO *pAudioIO, tSession *ps,
																   QWidget   *parent )
     : QWidget ( parent )

{
  /*-----------------------------------------------------------------------------------------------*
   * Main tab layout
   *-----------------------------------------------------------------------------------------------*/
	    QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);
	    mScrollArea = new QScrollArea(this);
		SubLayoutCenter->addWidget(mScrollArea);
        QWidget *mWidget     = new QWidget(mScrollArea);

        mScrollArea->setWidget(mWidget);
        mScrollArea->setWidgetResizable(true);
   
        QHBoxLayout *SigSelectOutputLayout = new QHBoxLayout(mWidget);
	    mWidget->setLayout(SigSelectOutputLayout);

  /*-----------------------------------------------------------------------------------------------*
   * Loudspeaker output sub-layout
   *-----------------------------------------------------------------------------------------------*/
		T_INT i;
		iChannelNumber = (T_INT) 0;
		QGroupBox   *GroupBoxLspOut        = new QGroupBox("Loudspeaker output");
		GroupBoxLspOut->setCheckable(true);
		QGridLayout *GridLayoutLspOut = new QGridLayout(GroupBoxLspOut);

		GridLayoutLspOut->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxLspOut->setLayout(GridLayoutLspOut);




  /*-----------------------------------------------------------------------------------------------*
   * Phone output sub-layout
   *-----------------------------------------------------------------------------------------------*/
	    T_INT k;
		iChannelNumber = (T_INT) 0;
		QGridLayout *GridLayoutPhone = new QGridLayout(this);
	    QGroupBox   *GroupBoxPhone        = new QGroupBox(tr("Phone output"));
		GroupBoxPhone->setCheckable(true);

		GridLayoutPhone->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxPhone->setLayout(GridLayoutPhone );


	/*------------------------------------------------------------------------------------------*
     * Add all sub-layouts to main Tab layout
     *------------------------------------------------------------------------------------------*/
	      SigSelectOutputLayout->addWidget(GroupBoxLspOut);
          SigSelectOutputLayout->addWidget(GroupBoxPhone);
}
   


void LspOutCheckBoxWidget::activateLspOutComboBox(bool bAct)
{
	mpAudioIO->setLspOutSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void LspOutComboBoxWidget::CurrentIndexLsp(int k)
{
	mpAudioIO->mapLspOutSignalChannelSpec(k, mChannelNumber, mps);
}

void PhoneOutCheckBoxWidget::activatePhoneComboBox(bool bAct)
{
	mpAudioIO->setPhoneOutSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void PhoneOutComboBoxWidget::CurrentIndexPhone(int k)
{
	mpAudioIO->mapPhoneOutSignalChannelSpec(k, mChannelNumber, mps);
}