/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_BEAMFORMER_ALGORITHMIC_PARAM_H
#define RTF_HSAB_BEAMFORMER_ALGORITHMIC_PARAM_H

#include <QtWidgets>
#include "types_hsab.h"
#include "alg_core_hsab_defines.h"

class TabAlgoStructureBeamformer;

class TabAlgoStructureBeamformerParamBeamformer : public QWidget
{

   Q_OBJECT

   public:
      TabAlgoStructureBeamformerParamBeamformer(   tHsabAlgCoreMain           *psHsabAlgCore, 
							                              tHsabAlgCoreParameters     *psParams,
                                                   TabAlgoStructureBeamformer *tabStructureAGC );

   protected:
      void resizeEvent(QResizeEvent *event);

   public slots:

   signals:

   private:
		TabAlgoStructureBeamformer      *mpTabAlgoStructureBeamformer; 

};

#endif
