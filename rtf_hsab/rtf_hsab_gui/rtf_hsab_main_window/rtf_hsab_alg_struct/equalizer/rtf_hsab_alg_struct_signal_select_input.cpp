/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include "rtf_hsab_alg_struct_signal_select_input.h"
#include <QtWidgets>



MicInComboBoxWidget::MicInComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
   T_INT j;
   setEditable(true);
   setFixedWidth(200);
   setFixedHeight(20);
   for (j = 0; j < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; j++)
   {
      addItem(tr("Standard Mic %2").arg(j));
   }
   setCurrentIndex((T_INT)iChannelNumber);
   connect(this, SIGNAL(activated(int)), this, SLOT(CurrentIndexMic(int)));
}

MicInCheckBoxWidget::MicInCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
    setText(sChannelName);
	setChecked(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(activateMicComboBox(bool)));
}

PhoneInComboBoxWidget::PhoneInComboBoxWidget(int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QComboBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
   //T_INT j;
   //setEditable(true);
   //setFixedWidth(200);
   //setFixedHeight(20);
   //for (j = 0; j < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn; j++)
   //{
   //   addItem(tr("Phone in %2").arg(j));
   //}
   //setCurrentIndex((T_INT)iChannelNumber);
   //connect(this, SIGNAL(activated(int)), this, SLOT(CurrentIndexPhone(int)));
}

PhoneInCheckBoxWidget::PhoneInCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
		, mps(ps)
	    , mpAudioIO(pAudioIO)
{
    setText(sChannelName);
	setChecked(true);
	connect(this, SIGNAL(toggled(bool)), this, SLOT(activatePhoneComboBox(bool)));
}
 
TabAlgoStructureSigSelectInput::TabAlgoStructureSigSelectInput( AudioIO *pAudioIO, tSession *ps,
																   QWidget   *parent )
     : QWidget ( parent )


{
  /*-----------------------------------------------------------------------------------------------*
   * Main tab layout
   *-----------------------------------------------------------------------------------------------*/
	    QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);
	    mScrollArea = new QScrollArea(this);
		SubLayoutCenter->addWidget(mScrollArea);
        QWidget *mWidget     = new QWidget(mScrollArea);

        mScrollArea->setWidget(mWidget);
        mScrollArea->setWidgetResizable(true);
   
        QHBoxLayout *SigSelectInputLayout = new QHBoxLayout(mWidget);
	    mWidget->setLayout(SigSelectInputLayout);

  /*-----------------------------------------------------------------------------------------------*
   * Microphone input sub-layout
   *-----------------------------------------------------------------------------------------------*/
		T_INT i;
		iChannelNumber = (T_INT) 0;
		QGroupBox   *GroupBoxMic        = new QGroupBox("Microphone input");
		GroupBoxMic->setCheckable(true);
		QGridLayout *GridLayoutMic = new QGridLayout(GroupBoxMic);
		
		for (i = 0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
		{
			sChannelNameMic = QString("Microphone input %1:").arg(i);
			mMicInCheckBoxWidget = new MicInCheckBoxWidget(sChannelNameMic ,iChannelNumber, mWidget, pAudioIO,ps);
		    GridLayoutMic->setRowMinimumHeight( i, 20 );
		    GridLayoutMic->addWidget(mMicInCheckBoxWidget,i,0);

		   mMicInComboBoxWidget = new MicInComboBoxWidget(iChannelNumber, mWidget, pAudioIO,ps);
		   mQVectorMicInComboBoxWidget.append(mMicInComboBoxWidget);
		   GridLayoutMic->addWidget(mQVectorMicInComboBoxWidget[iChannelNumber],i,1);
		   iChannelNumber++;
		}

		GridLayoutMic->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxMic->setLayout(GridLayoutMic);


  /*-----------------------------------------------------------------------------------------------*
   * Phone input sub-layout
   *-----------------------------------------------------------------------------------------------*/
	    T_INT k;
		iChannelNumber = (T_INT) 0;
		QGridLayout *GridLayoutPhone = new QGridLayout(this);
	    QGroupBox   *GroupBoxPhone        = new QGroupBox(tr("Phone input"));
		GroupBoxPhone->setCheckable(true);

		for (k = 0; k < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInRearSeat; k++)
		{
			sChannelNamePhone = QString("Phone input %1:").arg(k);
			mPhoneInCheckBoxWidget = new PhoneInCheckBoxWidget(sChannelNamePhone,iChannelNumber, mWidget, pAudioIO,ps);
		    GridLayoutPhone->setRowMinimumHeight( k, 20 );
		    GridLayoutPhone->addWidget(mPhoneInCheckBoxWidget,k,0);

		   mPhoneInComboBoxWidget = new PhoneInComboBoxWidget(iChannelNumber, mWidget, pAudioIO,ps);
		   mQVectorPhoneInComboBoxWidget.append(mPhoneInComboBoxWidget);
		   GridLayoutPhone->addWidget(mQVectorPhoneInComboBoxWidget[iChannelNumber],k,1);
		   iChannelNumber++;
		}
		GridLayoutPhone->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), 6, 2);
		GroupBoxPhone->setLayout(GridLayoutPhone );


	/*------------------------------------------------------------------------------------------*
     * Add all sub-layouts to main Tab layout
     *------------------------------------------------------------------------------------------*/
	      SigSelectInputLayout->addWidget(GroupBoxMic);
          SigSelectInputLayout->addWidget(GroupBoxPhone);
}
   
void MicInCheckBoxWidget::activateMicComboBox(bool bAct)
{
	mpAudioIO->setMicInSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void MicInComboBoxWidget::CurrentIndexMic(int k)
{
	mpAudioIO->mapMicInSignalChannelSpec(k, mChannelNumber, mps);
}

void PhoneInCheckBoxWidget::activatePhoneComboBox(bool bAct)
{
	mpAudioIO->setPhoneInSignalSelActivationChannelSpec(bAct, mChannelNumber, mps);
}

void PhoneInComboBoxWidget::CurrentIndexPhone(int k)
{
	mpAudioIO->mapPhoneInSignalChannelSpec(k, mChannelNumber, mps);
}