/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Manuel Diener
* (c) 2016-2019
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_EQ_H
#define RTF_HSAB_EQ_H

#include <QtWidgets>
#include "alg_core_hsab_defines.h"
#include "types_hsab.h"

#define NumPeakFilt ((int)(mpsHsabAlgCore->HsabAlgCoreEqFs.iNumPeakFilt))		// number of peak filter
#define regcnt	(NumPeakFilt+2)	// number of peak filter + high/low shelving filter		
#define NumCustomFilt 3			// number of custom peak filter; do not alter!!!
#define dBGainMax (mpsHsabAlgCore->HsabAlgCoreEqFs.fMaxGaindB)		// max Value of gain that can be set
#define dBGainMin (mpsHsabAlgCore->HsabAlgCoreEqFs.fMinGaindB)		// min Value of gain that can be set

class EqRegulator : public QWidget
{

private:
	Q_OBJECT

public:
	EqRegulator();
	EqRegulator(tHsabAlgCoreMain *core_var, QWidget *parent, int i, int w, int h,
		float pdBGain, int pcentFreq, float pbandwidth);
	EqRegulator(tHsabAlgCoreMain *core_var, QWidget *parent, int i, QString filt, int w, int h,
		float pdBGain, int pcentFreq, float pbandwidth);
	void resizeNew(int w, int h);
	void setDBGain(float db);
	void setCentFreq(int cent);
	void setBandwidth(float bw);
	void setMasterVolume(int mv);
	void setMasterVolume_alg_core(int mv);	//sets the Master Volume in the algorithmic core

    float getDBGain() { return dBGain; }
    float getCentFreq() { return centFreq; }
    float getBandwidth() { return bandwidth; }

private:
	void paintEvent(QPaintEvent *pe);
	void adjustFilter(int);	//adjusts filter in algorithmic core
	void readAll();

	public slots:
	void EqRegEventReturnPressed();
	void EqRegEventButActiveClicked();
	void wheelEvent(QWheelEvent *qwe);
	void mouseMoveEvent(QMouseEvent *qme);

private:
	tHsabAlgCoreMain * mpsHsabAlgCore;

	QPushButton * butNumberBand;
	QLineEdit * editDBGain;
	QLineEdit * editCentFreq;
	QLineEdit * editBandwidth;

	int channel;		// channelnumber of filter
	float dBGain;		// gain of peak filter in dB
	float centFreq;		// center frequency of peak filter
	float bandwidth;	// bandwidth of peak filter
	bool active;		// equalizer on
	int masterVolume;	// master volume in percent
};

class TabEqualizerTab : public QWidget
{
	Q_OBJECT

public:
    TabEqualizerTab(tHsabAlgCoreMain *psHsabAlgCore, const int iProVis, QWidget *parent = nullptr);
	~TabEqualizerTab();

protected:
	void resetAllDBGain();
	void resetAllCentFreq();
	void resetAllBandwidth();

signals:

protected:
	void resizeEvent(QResizeEvent *event);
	void paintEvent(QPaintEvent* ev);
	void wheelEvent(QWheelEvent *qwe);
	void mouseMoveEvent(QMouseEvent *qme);


	public slots:
	void EqEventOnOff(int state);
	void EqEventMasterVolumeReturn();
	void EqEventPresetButtonPress();
	void EqEventPresetSaveButtonPress();
	void EqEventResetButtonPress();

private:
	tHsabAlgCoreMain * mpsHsabAlgCore;

	//funktions for initialisation of parameters with default values
	void default_initialisation();

	//member variables
	EqRegulator **eqreg;				//array of filters

	QPushButton *resbut[6];
	QCheckBox *enable;
	QPushButton *savebut[6];
	QPushButton *prebut[6];
	QLineEdit *masedit;

	float *CenterFreqs_default;		//Default center frequencies from the algorithmic core
	float *Bandwidth_default;		//Default bandwidths from the algorithmic core
	float *GaindB_default;			//Default gain in dB from the algorithmic core
	float *Low_Pass_Para;			//Parameter for low pass filter button
	float *High_Pass_Para;			//Parameter for high pass filter button
	float *Band_Pass_Para;			//Parameter for band pass filter button
	int	masterVolume;				// master volume in percent
	float **custommemDBGain;		//dBGain of custom settings for filters
	float **custommemCentFreq;		//Center frequency of custom settings for filters
	float **custommemBandwidth;		//Bandwidth of custom settings for filters
};

#endif
