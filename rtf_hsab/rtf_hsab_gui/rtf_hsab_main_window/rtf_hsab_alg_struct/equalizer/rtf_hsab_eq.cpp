/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Manuel Diener
* (c) 2016-2019
*-----------------------------------------------------------------------------------------------*/

#include <QtWidgets>
#include <QDebug>
#include <math.h>
#include "defines_hsab.h"
#include "rtf_hsab_splitter.h"
#include "rtf_hsab_connector.h"
#include "rtf_hsab_adder.h"
#include "alg_core_hsab_defines.h"
#include "rtf_hsab_eq.h"
#include "rtf_hsab_memory_allocation.h"
#include <iostream>
#include <fstream>

void TabEqualizerTab::default_initialisation() {
	//adjusting center frequencies for peak filter
		for (int i = 0; i < regcnt; i++) {
			if (i < 2) {
				this->CenterFreqs_default[i] = 0;
			}
			else {
				this->CenterFreqs_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltCenterFreqHz[0][i-2];
			}
		}


	//adjusting bandwidths for peak filter
		for (int i = 0; i < regcnt; i++) {
			if (i == 0) {
				this->Bandwidth_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.pfLowShelvBandwidthHz[0];
			}
			else if (i == 1) {
				this->Bandwidth_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.pfHighShelvBandwidthHz[0];
			}
			else {
				this->Bandwidth_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltBandwidthHz[0][i-2];
			}
		}


	//adjusting GaindB for peak filter
		for (int i = 0; i < regcnt; i++) {
			if (i == 0) {
				this->GaindB_default/*[j]*/[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.pfLowShelvGaindB[0];
			}
			else if (i == 1) {
				this->GaindB_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.pfHighShelvGaindB[0];
			}
			else {
				this->GaindB_default[i] = mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltGaindB[0][i-2];
			}
		}


	//adjusting Low_Pass_Para; arbitrary algorithm
		{
			int j = (regcnt - 2) / 3;
			for (int i = 0; i < regcnt; i++) {
				if (i == 0 || i == 1) {
					Low_Pass_Para[i] = 0.0;
				}
				else if (i > regcnt / 2 + 1) {
					this->Low_Pass_Para[i] = dBGainMin;
				}
				else if (i <= regcnt / 2 + 1 && i > regcnt / 4 + 1) {
					if (j == 0) {
						j++;
					}
					this->Low_Pass_Para[i] = dBGainMin / j;
					j--;
				}
				else {
					this->Low_Pass_Para[i] = 0.0;
				}
			}
		}

	//adjusting High_Pass_Para; arbitrary algorithm
		{
			int j = 2;
			for (int i = 0; i < regcnt; i++) {
				if (i == 0 || i == 1) {
					High_Pass_Para[i] = 0.0;
				}
				else if (i > regcnt / 2 + 1) {
					this->High_Pass_Para[i] = 0.0;
				}
				else if (i <= regcnt / 2 + 1 && i > regcnt / 4 + 1) {
					this->High_Pass_Para[i] = dBGainMin / j;
					j++;
				}
				else {
					this->High_Pass_Para[i] = dBGainMin;
				}
			}
		}

	//adjusting Band_Pass_Para; arbitrary algorithm
	for (int i = 0; i < regcnt; i++) {
		if (i == 0 || i == 1) {
			Band_Pass_Para[i] = 0.0;
		}
		else if (i > regcnt / 4 + 1 && i < (regcnt - regcnt / 4)) {
			this->Band_Pass_Para[i] = 0.0;
		}
		else {
			this->Band_Pass_Para[i] = dBGainMin;
		}
	}

}

TabEqualizerTab::TabEqualizerTab(tHsabAlgCoreMain *psHsabAlgCore, const int iProVis, QWidget *parent)
	: QWidget(parent)
{
	mpsHsabAlgCore = psHsabAlgCore;

	HsabRtfAlloc2DArray(&(this->custommemDBGain), NumCustomFilt, regcnt);
	HsabRtfAlloc2DArray(&(this->custommemCentFreq), NumCustomFilt, regcnt);
	HsabRtfAlloc2DArray(&(this->custommemBandwidth), NumCustomFilt, regcnt);
	HsabRtfAlloc2DArray(&(this->eqreg), regcnt, 0);
	HsabRtfAlloc1DArray(&(this->CenterFreqs_default), regcnt);
	HsabRtfAlloc1DArray(&(this->Bandwidth_default), regcnt);
	HsabRtfAlloc1DArray(&(this->GaindB_default), regcnt);
	HsabRtfAlloc1DArray(&(this->Low_Pass_Para), regcnt);
	HsabRtfAlloc1DArray(&(this->High_Pass_Para), regcnt);
	HsabRtfAlloc1DArray(&(this->Band_Pass_Para), regcnt);

	this->default_initialisation();

	int width = 880;
	int height = 273;
	masterVolume = 100;

	enable = new QCheckBox("Equalizer Off", this);
	enable->setFont(QFont("Arial", 9, 2, false));
	enable->setGeometry(5, 0, 150, 35);
	connect(enable, SIGNAL(stateChanged(int)), this, SLOT(EqEventOnOff(int)));

	QString txt[6] = { "High Pass", "Band Pass", "Low Pass", "Custom 1", "Custom 2", "Custom 3" };
	for (int i = 0; i < 6; i++) {
		prebut[i] = new QPushButton(txt[i], this);
		prebut[i]->setGeometry(5, 40 + i * (height - 40) / 6 - (i<3 ? 5 : 0), 90, (height - 40) / 6 - 5);
		connect(prebut[i], SIGNAL(pressed()), this, SLOT(EqEventPresetButtonPress()));

		if (i > 2) {
			savebut[i] = new QPushButton("Save", this);
			savebut[i]->setGeometry(100, 40 + i * (height - 40) / 6, 35, (height - 40) / 6 - 5);
			connect(savebut[i], SIGNAL(pressed()), this, SLOT(EqEventPresetSaveButtonPress()));
		}
	}

	for (int i = 0; i < regcnt; i++) {
		for (int n = 0; n < NumCustomFilt; n++) {
			this->custommemDBGain[n][i] = 0.0;
			this->custommemCentFreq[n][i] = CenterFreqs_default[i];
			this->custommemBandwidth[n][i] = Bandwidth_default[i];
		}
		eqreg[i] = new EqRegulator(psHsabAlgCore, this, i /*filter number*/, (width - 300) / (regcnt), height, this->GaindB_default[i] /*GaindB*/, 
			this->CenterFreqs_default[i] /*Center frequency*/, this->Bandwidth_default[i] /*Bandwidth*/);
	}
	
	QLabel *maslab = new QLabel("Master\nVolume", this);
	maslab->setAlignment(Qt::AlignCenter);
	maslab->setGeometry(145, 0, 50, height / 10);

	masedit = new QLineEdit(QString::number(masterVolume), this);
	masedit->setAlignment(Qt::AlignCenter);
	masedit->setGeometry(145, height / 10, 50, height / 10);
	connect(masedit, SIGNAL(returnPressed()), this, SLOT(EqEventMasterVolumeReturn()));

	QString text[] = { "dB Gain" , "Center Frequency" , "Bandwidth" };
	for (int i = 0; i < 3; i++) {
		resbut[i] = new QPushButton(text[i], this);
		resbut[i]->setGeometry(200, 7 * height / 10 + i * height / 10, 100, height / 10);
		connect(resbut[i], SIGNAL(pressed()), this, SLOT(EqEventResetButtonPress()));
	}
	resbut[0]->setGeometry(200, height / 10, 100, height / 10);
}

TabEqualizerTab::~TabEqualizerTab(){
	HsabRtfFree2DArray(&(this->eqreg), regcnt, 0);
	HsabRtfFree2DArray(&(this->custommemDBGain), NumCustomFilt, regcnt);
	HsabRtfFree2DArray(&(this->custommemCentFreq), NumCustomFilt, regcnt);
	HsabRtfFree2DArray(&(this->custommemBandwidth), NumCustomFilt, regcnt);
	HsabRtfFree1DArray(&(this->CenterFreqs_default));
	HsabRtfFree1DArray(&(this->Bandwidth_default));
	HsabRtfFree1DArray(&(this->GaindB_default));
	HsabRtfFree1DArray(&(this->Low_Pass_Para));
	HsabRtfFree1DArray(&(this->High_Pass_Para));
	HsabRtfFree1DArray(&(this->Band_Pass_Para));
}

void TabEqualizerTab::paintEvent(QPaintEvent* ev)
{
	QPainter pa(this);
	float perc = masterVolume / 100.0;
	if (perc < 0) perc = 0;
	if (perc > 1) perc = 1;
	pa.fillRect(145, (int)(4.0 * height() * (1 - perc) / 5) + height() / 5, 50, (int)(perc * 4.0 * height() / 5 + 3), QColor(0, 200, 255));
	pa.setPen(QPen(Qt::black, 2));
	pa.drawLine(143, 0, 143, size().height());
	pa.drawLine(197, 0, 197, size().height());
	pa.setPen(QPen(Qt::black, 1));
	QFont font("Arial", 10);
	QFontMetrics qfm(font);
	pa.setFont(font);
	for (int i = dBGainMin; i <= dBGainMax; i++) {
		int y = (height() / 5) + (i - dBGainMin) * 3 * height() / 5 / (dBGainMax - dBGainMin);
		if (i % 6 == 0) {
			pa.drawLine(285, y, 300, y);
			QString str = QString::number(-i + dBGainMin + dBGainMax);
			y += qfm.height() / 3;
			if (y < height() / 5 + qfm.height()) y = height() / 5 + 2 * qfm.height() / 3;
			if (y > 4 * height() / 5 - qfm.height()) y = 4 * height() / 5 - qfm.height() / 6;
			pa.drawText(280 - qfm.width(str), y, str);
		}
		else if (i % 2 == 0) {
			pa.drawLine(292, y, 300, y);
		}
		else {
			pa.drawLine(296, y, 300, y);
		}
	}

	for (int i = 0; i <= 100; i++) {
		int y = (height() / 5) + (i * 4 * height() / 5 / 100);
		if (i % 25 == 0) {
			pa.drawLine(158, y, 182, y);
		}
		else if (i % 5 == 0) {
			pa.drawLine(166, y, 174, y);
		}
	}

	pa.rotate(-90);
	pa.drawText(-160, 250, "dB Gain");
}

//------------------------------------------------------------------------------------------------
// Resize tab
//------------------------------------------------------------------------------------------------
void TabEqualizerTab::resizeEvent(QResizeEvent *event)
{
	for (int i = 0; i < regcnt; i++) {
		eqreg[i]->resizeNew((event->size().width() - 300) / (regcnt), event->size().height());
	}
}

void TabEqualizerTab::wheelEvent(QWheelEvent *qwe) {
	if (qwe->x() > 145 && qwe->x() < 195 && qwe->y() > height() / 5 && qwe->y() < height()) {
		masterVolume += (qwe->angleDelta().y() > 0 ? 1 : -1);
		if (masterVolume > 100) masterVolume = 100;
		if (masterVolume < 0) masterVolume = 0;
		masedit->setText(QString::number(masterVolume));
		EqEventMasterVolumeReturn();
	}
}

void TabEqualizerTab::mouseMoveEvent(QMouseEvent *qme) {
	if (qme->x() > 145 && qme->x() < 195 && qme->y() > height() / 5 && qme->y() < height()) {
		int masVolNeu = 100 - 100 * (qme->y() - height() / 5) / (4 * height() / 5);
		masedit->setText(QString::number(masVolNeu));
		EqEventMasterVolumeReturn();
	}
}

void TabEqualizerTab::EqEventMasterVolumeReturn() {
	bool ok;
	int neuVal = masedit->text().toInt(&ok);
	if (ok) {
		masterVolume = neuVal;
		if (masterVolume > 100) masterVolume = 100;
		if (masterVolume < 0) masterVolume = 0;
		for (int i = 0; i < regcnt; i++) {
			eqreg[i]->setMasterVolume(masterVolume);
		}
		masedit->setText(QString::number(masterVolume));
	}
	repaint();
}

void TabEqualizerTab::EqEventResetButtonPress() {
	if (resbut[0]->isDown()) resetAllDBGain();
	if (resbut[1]->isDown()) resetAllCentFreq();
	if (resbut[2]->isDown()) resetAllBandwidth();
}

void TabEqualizerTab::resetAllDBGain() {
	for (int i = 0; i < regcnt; i++) eqreg[i]->setDBGain(GaindB_default[i]);
}

void TabEqualizerTab::resetAllCentFreq() {
	for (int i = 0; i < regcnt; i++) eqreg[i]->setCentFreq(CenterFreqs_default[i]);
}

void TabEqualizerTab::resetAllBandwidth() {
	for (int i = 0; i < regcnt; i++) eqreg[i]->setBandwidth(Bandwidth_default[i]);
}


void TabEqualizerTab::EqEventPresetButtonPress() {
	
	//High pass
	if (prebut[0]->isDown()) {
		for (int i = 0; i < regcnt; i++) eqreg[i]->setDBGain(High_Pass_Para[i]);		//loop starts at 2 because High_Pass_Para has no effect on High/Low Filter
		resetAllCentFreq();
		resetAllBandwidth();
	}

	//Band pass
	if (prebut[1]->isDown()) {
		for (int i = 0; i < regcnt; i++) eqreg[i]->setDBGain(Band_Pass_Para[i]);		//loop starts at 2 because Band_Pass_Para has no effect on High/Low Filter
		resetAllCentFreq();
		resetAllBandwidth();
	}

	//Low pass
	if (prebut[2]->isDown()) {
		for (int i = 0; i < regcnt; i++) eqreg[i]->setDBGain(Low_Pass_Para[i]);			//loop starts at 2 because Low_Pass_Para has no effect on High/Low Filter
		resetAllCentFreq();
		resetAllBandwidth();
	}

	//custom 1
	if (prebut[3]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			eqreg[i]->setDBGain(custommemDBGain[0][i]);
			eqreg[i]->setCentFreq(custommemCentFreq[0][i]);
			eqreg[i]->setBandwidth(custommemBandwidth[0][i]);
		}
	}

	//custom 2
	if (prebut[4]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			eqreg[i]->setDBGain(custommemDBGain[1][i]);
			eqreg[i]->setCentFreq(custommemCentFreq[1][i]);
			eqreg[i]->setBandwidth(custommemBandwidth[1][i]);
		}
	}

	//custom 3
	if (prebut[5]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			eqreg[i]->setDBGain(custommemDBGain[2][i]);
			eqreg[i]->setCentFreq(custommemCentFreq[2][i]);
			eqreg[i]->setBandwidth(custommemBandwidth[2][i]);
		}
	}

}


void TabEqualizerTab::EqEventPresetSaveButtonPress() {
	//save custom 1
	if (savebut[3]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			custommemDBGain[0][i] = eqreg[i]->getDBGain();
			custommemCentFreq[0][i] = eqreg[i]->getCentFreq();
			custommemBandwidth[0][i] = eqreg[i]->getBandwidth();
		}
	}

	//save custom 2
	else if (savebut[4]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			custommemDBGain[1][i] = eqreg[i]->getDBGain();
			custommemCentFreq[1][i] = eqreg[i]->getCentFreq();
			custommemBandwidth[1][i] = eqreg[i]->getBandwidth();
		}
	}

	//save custom 3
	else if (savebut[5]->isDown()) {
		for (int i = 0; i < regcnt; i++) {
			custommemDBGain[2][i] = eqreg[i]->getDBGain();
			custommemCentFreq[2][i] = eqreg[i]->getCentFreq();
			custommemBandwidth[2][i] = eqreg[i]->getBandwidth();
		}
	}
}

void TabEqualizerTab::EqEventOnOff(int state)
{
	if (enable->isChecked()) {
		enable->setText("Equalizer On");
		mpsHsabAlgCore->HsabAlgCoreEqFs.iEqualizerActivation = 1;
		mpsHsabAlgCore->HsabAlgCoreEqRs.iEqualizerActivation = 1;
	}
	else {
		enable->setText("Equalizer Off");
		mpsHsabAlgCore->HsabAlgCoreEqFs.iEqualizerActivation = 0;
		mpsHsabAlgCore->HsabAlgCoreEqRs.iEqualizerActivation = 0;
	}
}



EqRegulator::EqRegulator() {}


EqRegulator::EqRegulator(tHsabAlgCoreMain *core_var, QWidget *parent, int i, int w, int h,
	float adBGain, int acentFreq, float abandwidth) : QWidget(parent)
{
	mpsHsabAlgCore = core_var;
	setParent(parent);

	setGeometry(300 + i * w, 0, w - 1, h);

	channel = i;
	dBGain = adBGain;
	centFreq = acentFreq;
	bandwidth = abandwidth;
	
	if (i == 0) {
		butNumberBand = new QPushButton("Low-Shelv", this);
		butNumberBand->setFont(QFont("Arial", 8));
		butNumberBand->setGeometry(1, 1, w - 2, h / 10 - 2);
		connect(butNumberBand, SIGNAL(clicked()), this, SLOT(EqRegEventButActiveClicked()));
	}
	else if (i == 1) {
		butNumberBand = new QPushButton("High-Shelv", this);
		butNumberBand->setFont(QFont("Arial", 8));
		butNumberBand->setGeometry(1, 1, w - 2, h / 10 - 2);
		connect(butNumberBand, SIGNAL(clicked()), this, SLOT(EqRegEventButActiveClicked()));
	}
	else {
		butNumberBand = new QPushButton(QString::number(i - 1), this);
		butNumberBand->setFont(QFont("Arial", 12));
		butNumberBand->setGeometry(1, 1, w - 2, h / 10 - 2);
		connect(butNumberBand, SIGNAL(clicked()), this, SLOT(EqRegEventButActiveClicked()));
	}

	setMinimumWidth(35);
	editDBGain = new QLineEdit(QString::number(dBGain), this);
	editDBGain->setAlignment(Qt::AlignCenter);
	editDBGain->setGeometry(1, h / 10, w - 3, h / 10);
	connect(editDBGain, SIGNAL(returnPressed()), this, SLOT(EqRegEventReturnPressed()));

	if (i < 2) {
		editCentFreq = new QLineEdit("-", this);
		editCentFreq->setAlignment(Qt::AlignCenter);
		editCentFreq->setGeometry(1, 4 * h / 5, w - 3, h / 10);
		connect(editCentFreq, SIGNAL(returnPressed()), this, SLOT(EqRegEventReturnPressed()));
	}
	else {
		editCentFreq = new QLineEdit(QString::number(centFreq), this);
		editCentFreq->setAlignment(Qt::AlignCenter);
		editCentFreq->setGeometry(1, 4 * h / 5, w - 3, h / 10);
		connect(editCentFreq, SIGNAL(returnPressed()), this, SLOT(EqRegEventReturnPressed()));
	}

	editBandwidth = new QLineEdit(QString::number(bandwidth), this);
	editBandwidth->setAlignment(Qt::AlignCenter);
	editBandwidth->setGeometry(1, 9 * h / 10, w - 3, h / 10);
	connect(editBandwidth, SIGNAL(returnPressed()), this, SLOT(EqRegEventReturnPressed()));
	adjustFilter(this->channel);
}

void EqRegulator::resizeNew(int w, int h)
{
	this->setGeometry(300 + channel * w, 0, w - 1, h);
	butNumberBand->setGeometry(0, 0, w - 1, h / 10);
	editDBGain->setGeometry(1, h / 10, w - 3, h / 10);
	editCentFreq->setGeometry(1, 4 * h / 5, w - 3, h / 10);
	editBandwidth->setGeometry(1, 9 * h / 10, w - 3, h / 10);
}

void EqRegulator::paintEvent(QPaintEvent *pe)
{
	QPainter pa(this);
	float perc = (dBGain - dBGainMin) / (dBGainMax - dBGainMin);
	if (perc < 0) perc = 0;
	if (perc > 1) perc = 1;
	pa.fillRect(1, (int)(3.0 * height() * (1 - perc) / 5) + height() / 5, width() - 2, (int)(perc * 3.0 * height() / 5 + 1),
		active ? QColor(0, 200, 255) : Qt::lightGray);
	pa.setPen(active ? Qt::black : Qt::gray);
	pa.setBrush(Qt::NoBrush);
	pa.drawRect(0, 0, width() - 1, height() - 1);
}

void EqRegulator::EqRegEventButActiveClicked() {
	active = !active;
	editDBGain->setEnabled(active);
	editCentFreq->setEnabled(active);
	editBandwidth->setEnabled(active);
	this->repaint();
	this->adjustFilter(this->channel);
}

void EqRegulator::wheelEvent(QWheelEvent *qwe) {
	if (active) {
		dBGain += qwe->angleDelta().y() > 0 ? 1 : -1;
		editDBGain->setText(QString::number(dBGain));
		readAll();
	}
}

void EqRegulator::mouseMoveEvent(QMouseEvent *qme) {
	if ((qme->y() > height() / 5) && (qme->y() < 4 * height() / 5) && active && ((Qt::LeftButton & qme->buttons()) > 0)) {
		int dBGainNew = -(int)((qme->y() - height() / 5) * (dBGainMax - dBGainMin) / (3 * height() / 5)) + dBGainMax;
		editDBGain->setText(QString::number(dBGainNew));
		readAll();
	}
}

void EqRegulator::setDBGain(float db) {
	this->dBGain = db;
	editDBGain->setText(QString::number(this->dBGain));
	repaint();
	adjustFilter(this->channel);
}


void EqRegulator::setCentFreq(int cent) {
	if (cent < 1) cent = 1;
	this->centFreq = cent;
	if (this->channel < 2) {
		editCentFreq->setText("-");
	}
	else {
		editCentFreq->setText(QString::number(this->centFreq));
	}
	
	repaint();
	adjustFilter(this->channel);
}

void EqRegulator::setBandwidth(float bw) {
	if (bw < 1) bw = 1;
	this->bandwidth = bw;
	editBandwidth->setText(QString::number(this->bandwidth));
	repaint();
	adjustFilter(this->channel);
}

void EqRegulator::setMasterVolume(int mv) {
	this->masterVolume = mv;
	setMasterVolume_alg_core(this->masterVolume);
}

void EqRegulator::setMasterVolume_alg_core(int mv) {
	mpsHsabAlgCore->HsabAlgCoreEqFs.fMasterVolume = (T_FLOAT)mv / (T_FLOAT) 100.0;
	mpsHsabAlgCore->HsabAlgCoreEqRs.fMasterVolume = (T_FLOAT)mv / (T_FLOAT) 100.0;
}

void EqRegulator::readAll() {
	bool recalc = false;
	bool ok;
	float ergDB = editDBGain->text().toFloat(&ok);
	if (ok) {
		if (ergDB > dBGainMax) ergDB = dBGainMax;
		if (ergDB < dBGainMin) ergDB = dBGainMin;
		this->dBGain = ergDB;
		editDBGain->setText(QString::number(this->dBGain));
		recalc = true;
	}
	int ergCentFreq = editCentFreq->text().toInt(&ok);
	if (ok) {
		if (ergCentFreq < 1) ergCentFreq = 1;
		this->centFreq = ergCentFreq;
		recalc = true;
	}
	float ergBandwidth = editBandwidth->text().toFloat(&ok);
	if (ok) {
		if (ergBandwidth < 1) ergBandwidth = 1;
		this->bandwidth = ergBandwidth;
		recalc = true;
	}
	if (recalc) {
		adjustFilter(this->channel);
		repaint();
	}
}

void EqRegulator::EqRegEventReturnPressed() {
	readAll();
}


void EqRegulator::adjustFilter(int Filter) {
	for (int i = 0;i < mpsHsabAlgCore->HsabAlgCoreEqFs.iNumChannels;i++) {
		if (Filter == 0) {
			mpsHsabAlgCore->HsabAlgCoreEqFs.pfLowShelvGaindB[i] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqFs.pfLowShelvBandwidthHz[i] = bandwidth;

			mpsHsabAlgCore->HsabAlgCoreEqRs.pfLowShelvGaindB[i] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqRs.pfLowShelvBandwidthHz[i] = bandwidth;
		}
		else if (Filter == 1) {
			mpsHsabAlgCore->HsabAlgCoreEqFs.pfHighShelvGaindB[i] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqFs.pfHighShelvBandwidthHz[i] = bandwidth;

			mpsHsabAlgCore->HsabAlgCoreEqRs.pfHighShelvGaindB[i] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqRs.pfHighShelvBandwidthHz[i] = bandwidth;
		}
		else {
			mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltGaindB[i][Filter-2] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltCenterFreqHz[i][Filter-2] = centFreq;
			mpsHsabAlgCore->HsabAlgCoreEqFs.ppfPeakFiltBandwidthHz[i][Filter-2] = bandwidth;

			mpsHsabAlgCore->HsabAlgCoreEqRs.ppfPeakFiltGaindB[i][Filter - 2] = dBGain;
			mpsHsabAlgCore->HsabAlgCoreEqRs.ppfPeakFiltCenterFreqHz[i][Filter - 2] = centFreq;
			mpsHsabAlgCore->HsabAlgCoreEqRs.ppfPeakFiltBandwidthHz[i][Filter - 2] = bandwidth;
		}
	}
}
