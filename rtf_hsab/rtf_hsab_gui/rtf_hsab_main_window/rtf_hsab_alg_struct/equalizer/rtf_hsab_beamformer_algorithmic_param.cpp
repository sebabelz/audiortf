/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtWidgets>
#include "types_hsab.h"
#include "defines_hsab.h"
#include "rtf_hsab_beamformer_algorithmic_param.h"


TabAlgoStructureBeamformerParamBeamformer::TabAlgoStructureBeamformerParamBeamformer(  tHsabAlgCoreMain           *psHsabAlgCore, 
												                                       tHsabAlgCoreParameters     *psParams,
                                                                                       TabAlgoStructureBeamformer *tabStructureBeamformer )

{
   T_INT   iLeftOffset          = 20;
   T_INT   iTopOffset           = 20;
   T_INT   iWidgetHeigth        = 22;

   QString Text;

}

/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructureBeamformerParamBeamformer::resizeEvent ( QResizeEvent * event )
{

   T_INT iButtonWidth         =  90;
   if (width() < 1000)
   {
      iButtonWidth = (T_INT) ((T_FLOAT) 90.0 * (width()-(T_FLOAT) 400.0) / (T_FLOAT) 600.0 );
   }

	T_INT   iLeftOffset          = 20;
   T_INT   iTopOffset           = 20;
   T_INT   iWidgetHeigth        = 22;

	T_INT iRightOffset         =  90;

   T_INT iWidth               =  width();
   if (iWidth > (T_INT) 1200)
   {
      iLeftOffset        += (iWidth - (T_INT) 1200) / 2;
      iRightOffset       += (iWidth - (T_INT) 1200) / 2;
      iWidth              = 1200;
   }

   T_INT iMinSizeForChangingText        = 1000;

 
}