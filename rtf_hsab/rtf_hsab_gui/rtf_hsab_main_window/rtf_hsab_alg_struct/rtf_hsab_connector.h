/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_CONNECTOR_H
#define RTF_HSAB_CONNECTOR_H

#include <QtWidgets>
#include "types_hsab.h"


class Connector: public QWidget
{
public:
   enum ConnectorHead
   {
      Up,
      Down,
      Left,
      Right,
      None
   };

   enum ConnectorDirection
   {
      Horizontal,
      Vertical
   };

   Connector(QWidget *parent, ConnectorDirection dir, ConnectorHead head = None)
      : QWidget(parent)
      , mHead(head)
      , mDirection(dir)
      , mIsActive(true)
   {
   }

   void paintEvent ( QPaintEvent* )
   {
      QPainter p(this);
      p.setRenderHint(QPainter::Antialiasing, true);
      
      if(mIsActive)
      {
         p.setPen(Qt::black);
         p.setBrush(Qt::black);
      }
      else
      {
         p.setPen(Qt::gray);  // QColor(255,0,0)
         p.setBrush(Qt::gray);
      }

      T_INT w = width();
      T_INT h = height();
      T_INT w2 = width()/2;
      T_INT h2 = height()/2;

      switch(mDirection)
      {
      case Vertical:
         p.drawLine(w2,0,w2,h);
         break;
      default:
         p.drawLine(0,h2,w,h2);
         break;
      }

      const T_INT ah = 10;
      const T_INT aw = 4;

      QPolygon poly;
      switch(mHead)
      {
      case Up:
         poly 
            << QPoint(w2, 0)
            << QPoint(w2-aw, ah)
            << QPoint(w2+aw, ah)
            ;
         break;
      case Down:
         poly 
            << QPoint(w2, h)
            << QPoint(w2-aw, h-ah)
            << QPoint(w2+aw, h-ah)
            ;
         break;
      case Left:
         poly 
            << QPoint(0, h2)
            << QPoint(ah, h2-aw)
            << QPoint(ah, h2+aw)
            ;
         break;
      case Right:
         poly 
            << QPoint(w, h2)
            << QPoint(w-ah, h2-aw)
            << QPoint(w-ah, h2+aw)
            ;
         break;
      default:
         break;
      }

      if(!poly.isEmpty())
      {
         p.drawPolygon(poly);
      }
   }

   bool setActive(bool isActive)
   {
      mIsActive=isActive;
      repaint();
   }

protected:
   ConnectorHead mHead;
   ConnectorDirection mDirection;
   bool mIsActive;
};


#endif