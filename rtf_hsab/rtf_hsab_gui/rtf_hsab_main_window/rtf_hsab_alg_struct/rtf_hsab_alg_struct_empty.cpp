/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#include "rtf_hsab_alg_struct_empty.h"

TabAlgoStructureEmptyTab::TabAlgoStructureEmptyTab ( QWidget   *parent )

     : QWidget ( parent )
{

}

  /*------- Resize tab ---------*/

void TabAlgoStructureEmptyTab::resizeEvent ( QResizeEvent* )
{
	
}
