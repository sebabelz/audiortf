/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SPLITTER_H
#define RTF_HSAB_SPLITTER_H

#include "types_hsab.h"

class Splitter: public QWidget
{
public:

   Splitter ( QWidget *parent )
      : QWidget(parent)
      , mIsActive(true)
   {
   }

   void paintEvent ( QPaintEvent* )
   {
      QPainter p(this);
      p.setRenderHint(QPainter::Antialiasing, true);

      T_INT w = width();
      T_INT h = height();
      T_INT s = 2;

      if(mIsActive)
      {
         p.setPen(Qt::black);
         p.setBrush(Qt::black);
      }
      else
      {
         p.setPen(Qt::gray);  
         p.setBrush(Qt::gray);
      }

      p.drawEllipse( QRectF( s, s, w-2*s, h-2*s) );

   }

   bool setActive(bool isActive)
   {
      mIsActive=isActive;
      repaint();
   }

protected:
   bool mIsActive;
};


#endif