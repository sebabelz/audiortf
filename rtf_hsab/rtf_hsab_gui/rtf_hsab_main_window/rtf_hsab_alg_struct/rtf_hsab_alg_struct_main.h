/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_ALG_STRUCT_MAIN_H
#define RTF_HSAB_ALG_STRUCT_MAIN_H

#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_main_window.h"

class Connector;
class Splitter;
class Adder;

class TabAlgoStructureMainSap : public QWidget
{
    Q_OBJECT

    public:
		TabAlgoStructureMainSap(tHsabAlgCoreMain *psHsabAlgCore, tSignalSelect *psSigSel, tRtf_hfSrc *psRtf_hfSrcUplink, tRtf_hfSrc *psRtf_hfSrcDownlink, tRtf_hfSrc *psRtf_hfSrcTts,
                                  tRtf_hfSrc *psRtf_hfSrcRef, tRtf_hfSrc *psRtf_hfSrcRecog, QWidget  *parent = 0 );

    protected:
		void resizeEvent ( QResizeEvent * event );

	 signals:
	   void switchToHandsFreeCore();
	   void switchToSigSelectionIn();
	   void switchToSigSelectionOut();

    public slots:
		 void activateSigSel1(bool);
		 void switchHandsFreeSys(bool bAct);
		 void activateSigSel2(bool);

		 void switchSrcUplink(bool bAct);
		 void switchSrcDownlink(bool bAct);
		 void switchSrcTts(bool bAct);
		 void switchSrcRef(bool bAct);
		 void switchSrcRecog(bool bAct);

    private:

		  tHsabAlgCoreMain *mHsabAlgCore;
		  tSignalSelect *mpsSigSel;
		  tRtf_hfSrc *mpsRtf_hfSrcUplink;			
		  tRtf_hfSrc *mpsRtf_hfSrcDownlink;
		  tRtf_hfSrc *mpsRtf_hfSrcTts;
		  tRtf_hfSrc *mpsRtf_hfSrcRef; 
		  tRtf_hfSrc *mpsRtf_hfSrcRecog;

		  T_BOOL       bShortTextActive;

		  QPushButton  *mPushButtonUplink;
		  QCheckBox    *mCheckBoxUplink;
		  Connector    *mConnectorUplinkHor1;
		  Connector    *mConnectorUplinkHor2;

          QPushButton  *mPushButtonSampleRateConvRec;
          Connector    *mConnectorSampleRateConvRec;
          QCheckBox    *mCheckBoxSampleRateConvRec;
		

		  QPushButton  *mPushButtonSrcBeforeHf1;
		  Connector    *mConnectorSrcBeforeHf1;
          QCheckBox    *mCheckBoxSrcBeforeHf1;

		  QPushButton  *mPushButtonSrcBeforeHf2;
		  Connector    *mConnectorSrcBeforeHf2;
          QCheckBox    *mCheckBoxSrcBeforeHf2;
		 
		  
          QPushButton  *mPushButtonSampleRateConvSend;
		  Connector    *mConnectorSampleRateConvSendHor1;
          QCheckBox    *mCheckBoxSampleRateConvSend;

		  QPushButton  *mPushButtonSampleRateConvLs;
		  Connector    *mConnectorSampleRateConvLs;        
		  QCheckBox    *mCheckBoxSampleRateConvLs;

		  QPushButton  *mPushButtonSampleRateConvMic;
		  Connector    *mConnectorSampleRateConvMicHor1;
          QCheckBox    *mCheckBoxSampleRateConvMic;

          QPushButton  *mPushButtonSampleRateConvRefIn;
          QCheckBox    *mCheckBoxSampleRateConvRefIn;
          Connector    *mConnectorSampleRateConvRefHor1;

		  QPushButton  *mPushButtonSigSel1;
	   	  QCheckBox    *mCheckBoxSigSel1;

		Connector    *mConnectorHfeIn0;
		Connector    *mConnectorHfeIn1;
		Connector    *mConnectorHfeIn2;
		Connector    *mConnectorHfeIn3;

		Connector    *mConnectorSigSelIn0;
		Connector    *mConnectorSigSelIn1;
		Connector    *mConnectorSigSelIn2;
		Connector    *mConnectorSigSelIn3;

		  QPushButton  *mPushButtonSigSel2;
		  QCheckBox    *mCheckBoxSigSel2;

		  QPushButton  *mPushButtonTtsOut;
		  Connector    *mConnectorTtsOutHor1;
		  QCheckBox    *mCheckBoxTtsOut;

          QPushButton  *mPushButtonHandsFreeSys;
		  Connector    *mConnectorHandsFreeSysHor1;
		  Connector    *mConnectorHandsFreeSysHor2;
		  Connector    *mConnectorHandsFreeSysHor3;
		  Connector    *mConnectorHandsFreeSysHor4;
          QCheckBox    *mCheckBoxHandsFreeSys;

        QPushButton  *mPushButtonHeadPhoneOutput;
        QCheckBox    *mCheckBoxHeadPhoneOutput;

		Connector    *mConnectorOut0;
		Connector    *mConnectorOut1;
		Connector    *mConnectorOut2;
		Connector    *mConnectorOut2ext;
		Connector    *mConnectorOut3ext;
		Connector    *mConnectorOut3;

		Adder		 *mAdderConnectOut3;

		QLabel		 *mLabelPhoneDownl;
		QLabel		 *mLabelMicSig;
		QLabel		 *mLabelRefSig;
		QLabel		 *mLabelTtsSig;
		QLabel		 *mLabelOutLs;
		QLabel		 *mLabelOutUpl;
		QLabel		 *mLabelOutRec;


};



#endif