/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_ALG_STRUCT_EMPTY_H
#define RTF_HSAB_ALG_STRUCT_EMPTY_H

#include <QtGui>
#include <QtWidgets>

class TabAlgoStructureEmptyTab : public QWidget

{
    Q_OBJECT

    public:
        TabAlgoStructureEmptyTab ( QWidget          *parent = 0 );

   protected:
		 void resizeEvent ( QResizeEvent * event );

   signals:
		

    public slots:

private:

};

#endif
