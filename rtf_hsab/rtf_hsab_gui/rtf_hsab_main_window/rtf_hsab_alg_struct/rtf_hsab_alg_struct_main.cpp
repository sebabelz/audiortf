/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include "rtf_hsab_alg_struct_main.h"
#include "rtf_hsab_connector.h"
#include "rtf_hsab_splitter.h"
#include "rtf_hsab_adder.h"

TabAlgoStructureMainSap::TabAlgoStructureMainSap( tHsabAlgCoreMain *psHsabAlgCore, tSignalSelect *psSigSel, tRtf_hfSrc *psRtf_hfSrcUplink, tRtf_hfSrc *psRtf_hfSrcDownlink,
	                                                tRtf_hfSrc *psRtf_hfSrcTts, tRtf_hfSrc *psRtf_hfSrcRef, tRtf_hfSrc *psRtf_hfSrcRecog,
												     QWidget   *parent )
     : QWidget ( parent )
	  , mHsabAlgCore(psHsabAlgCore)		  // entspricht:  mpsSapAlgCore = psSapAlgCore
	  , mpsSigSel(psSigSel)
	  , mpsRtf_hfSrcUplink(psRtf_hfSrcUplink)
	  , mpsRtf_hfSrcDownlink(psRtf_hfSrcDownlink)  
	  , mpsRtf_hfSrcTts(psRtf_hfSrcTts)
	  , mpsRtf_hfSrcRef(psRtf_hfSrcRef)
	  , mpsRtf_hfSrcRecog(psRtf_hfSrcRecog)
{
   

  /*---------------------------------------------------------------------------------------------*
   * Input and Output signal Labels
   *---------------------------------------------------------------------------------------------*/

   mLabelMicSig = new QLabel(tr("Mic. signals\n"),this);
   mLabelMicSig->setAlignment(Qt::AlignBottom);
   mLabelMicSig->setAlignment(Qt::AlignLeft);

   mLabelRefSig = new QLabel(tr("Ref. signals\n"),this);
   mLabelRefSig->setAlignment(Qt::AlignBottom);
   mLabelRefSig->setAlignment(Qt::AlignLeft);

   mLabelPhoneDownl = new QLabel(tr("Downl. signals\n"),this);
   mLabelPhoneDownl->setAlignment(Qt::AlignBottom);
   mLabelPhoneDownl->setAlignment(Qt::AlignLeft);

   mLabelTtsSig = new QLabel(tr("TTS signals\n"),this);
   mLabelTtsSig->setAlignment(Qt::AlignBottom);
   mLabelTtsSig->setAlignment(Qt::AlignLeft);

   mLabelOutUpl  = new QLabel(tr("Upl. signals\n"),this);
   mLabelOutUpl ->setAlignment(Qt::AlignBottom);
   mLabelOutUpl ->setAlignment(Qt::AlignRight);

   mLabelOutRec = new QLabel(tr("Recog. signals\n"),this);
   mLabelOutRec->setAlignment(Qt::AlignBottom);
   mLabelOutRec->setAlignment(Qt::AlignRight);

   mLabelOutLs = new QLabel(tr("Lsp. signals\n"),this);
   mLabelOutLs->setAlignment(Qt::AlignBottom);
   mLabelOutLs->setAlignment(Qt::AlignRight);


  /*---------------------------------------------------------------------------------------------*
   * Signal selection 
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSigSel1 = new QPushButton(tr("Signal\n selection\n"),this);
   connect(mPushButtonSigSel1, SIGNAL(clicked()), this, SIGNAL(switchToSigSelectionIn()));

  /*---------------------------------------------------------------------------------------------*
   * Connectors from signal selection to hands-free system
   *---------------------------------------------------------------------------------------------*/
   mConnectorSigSelIn0 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorSigSelIn1 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorSigSelIn2 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorSigSelIn3 = new Connector(this, Connector::Horizontal, Connector::Right);

  /*---------------------------------------------------------------------------------------------*
   * Connectors from signal selection to hands-free system
   *---------------------------------------------------------------------------------------------*/

   mConnectorHfeIn0 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHfeIn1 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHfeIn2 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHfeIn3 = new Connector(this, Connector::Horizontal, Connector::Right);
   mCheckBoxSigSel1 = new QCheckBox(tr("On"),this);
   //mCheckBoxSigSel1->setChecked(mpsSigSel->bActiveInput);
   connect(mCheckBoxSigSel1, SIGNAL(toggled(bool)), this, SLOT(activateSigSel1(bool)));
  /*---------------------------------------------------------------------------------------------*
   * Hands-free system  
   *---------------------------------------------------------------------------------------------*/

   mPushButtonHandsFreeSys = new QPushButton(tr("Speech\n signal\n enhancement\n"),this);
   connect(mPushButtonHandsFreeSys, SIGNAL(clicked()), this, SIGNAL(switchToHandsFreeCore()));
   
   mConnectorHandsFreeSysHor1 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHandsFreeSysHor2 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHandsFreeSysHor3 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorHandsFreeSysHor4 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxHandsFreeSys = new QCheckBox(tr("On"),this);
   connect(mCheckBoxHandsFreeSys, SIGNAL(toggled(bool)), this, SLOT(switchHandsFreeSys(bool)));

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion in receiving path
   *---------------------------------------------------------------------------------------------*/

   mPushButtonSampleRateConvRec = new QPushButton(tr("Sample rate\n conversion"),this);

   mConnectorSampleRateConvRec = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSampleRateConvRec = new QCheckBox(tr("On"),this);/*--MicIn Downsampling--*/                                                     
   mCheckBoxSampleRateConvRec->setChecked(mpsRtf_hfSrcUplink->bActiveSrc);
   connect(mCheckBoxSampleRateConvRec, SIGNAL(toggled(bool)), this, SLOT(switchSrcUplink(bool)));

   mPushButtonSrcBeforeHf1 = new QPushButton(tr("Sample rate\n conversion"),this);
   mConnectorSrcBeforeHf1 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSrcBeforeHf1 = new QCheckBox(tr("On"),this); /*--RefIn Downsampling--*/       

   mCheckBoxSrcBeforeHf1->setChecked(mpsRtf_hfSrcRef->bActiveSrc);
   connect(mCheckBoxSrcBeforeHf1, SIGNAL(toggled(bool)), this, SLOT(switchSrcRef(bool)));

   mPushButtonSrcBeforeHf2 = new QPushButton(tr("Sample rate\n conversion"),this);
   mConnectorSrcBeforeHf2 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSrcBeforeHf2 = new QCheckBox(tr("On"),this);/*--PhoneIn Downsampling--*/

   mCheckBoxSrcBeforeHf2->setChecked(mpsRtf_hfSrcDownlink->bActiveSrc);
   connect(mCheckBoxSrcBeforeHf2, SIGNAL(toggled(bool)), this, SLOT(switchSrcDownlink(bool)));

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion in sending path
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvSend = new QPushButton(tr("Sample rate\n conversion"),this);
   
   mConnectorSampleRateConvSendHor1 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSampleRateConvSend = new QCheckBox(tr("On"),this);	/*--Tts UpSampling--*/											
   
   mCheckBoxSampleRateConvSend->setChecked(mpsRtf_hfSrcTts->bActiveSrc);
   connect(mCheckBoxSampleRateConvSend, SIGNAL(toggled(bool)), this, SLOT(switchSrcTts(bool)));

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion after microphone output   
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvMic = new QPushButton(tr("Sample rate\n conversion"),this);
  
   mConnectorSampleRateConvMicHor1 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSampleRateConvMic = new QCheckBox(tr("On"),this); /*--Tts UpSampling--*/                                             
  
   mCheckBoxSampleRateConvMic->setChecked(mpsRtf_hfSrcTts->bActiveSrc);
   connect(mCheckBoxSampleRateConvMic, SIGNAL(toggled(bool)), this, SLOT(switchSrcTts(bool)));
  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion before loudspeaker output
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvLs = new QPushButton(tr("Sample rate\n conversion"),this);
 
   mConnectorSampleRateConvLs = new Connector(this, Connector::Horizontal, Connector::Right);
   mCheckBoxSampleRateConvLs = new QCheckBox(tr("On"),this);/*--PhoneOut UpSampling--*/	

   mCheckBoxSampleRateConvLs->setChecked(mpsRtf_hfSrcUplink->bActiveSrc);
   connect(mCheckBoxSampleRateConvLs, SIGNAL(toggled(bool)), this, SLOT(switchSrcUplink(bool)));

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion after reference input
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvRefIn = new QPushButton(tr("Sample rate\n conversion"),this);
  
   mConnectorSampleRateConvRefHor1 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxSampleRateConvRefIn = new QCheckBox(tr("On"),this); /*--Recog.Out UpSampling--*/                                                            
   mCheckBoxSampleRateConvRefIn->setChecked(mpsRtf_hfSrcRecog->bActiveSrc);
   connect(mCheckBoxSampleRateConvRefIn, SIGNAL(toggled(bool)), this, SLOT(switchSrcRecog(bool)));

  /*---------------------------------------------------------------------------------------------*
   * Headphone output 
   *---------------------------------------------------------------------------------------------*/
   mPushButtonTtsOut = new QPushButton(tr("Sample rate\n conversion"),this);
  
   mConnectorTtsOutHor1 = new Connector(this, Connector::Horizontal, Connector::Right);

   mCheckBoxTtsOut = new QCheckBox(tr("On"),this);/*--Lsp out UpSampling--*/																
   mCheckBoxTtsOut->setChecked(mpsRtf_hfSrcDownlink->bActiveSrc);
   connect(mCheckBoxTtsOut, SIGNAL(toggled(bool)), this, SLOT(switchSrcDownlink(bool)));
  /*---------------------------------------------------------------------------------------------*
   * Connectors at the output
   *---------------------------------------------------------------------------------------------*/
   mConnectorOut1 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorOut2 = new Connector(this, Connector::Horizontal, Connector::None);
   mConnectorOut2ext = new Connector(this, Connector::Vertical, Connector::Down);
   mConnectorOut3 = new Connector(this, Connector::Horizontal, Connector::Right);
   mConnectorOut3ext = new Connector(this, Connector::Horizontal, Connector::Right);
   mAdderConnectOut3 = new Adder(this);

  /*---------------------------------------------------------------------------------------------*
   * Signal selection 
   *---------------------------------------------------------------------------------------------*/	
   mConnectorOut0 = new Connector(this, Connector::Horizontal, Connector::Right);
   mPushButtonSigSel2 = new QPushButton(tr("Signal\n selection\n"),this);
   connect(mPushButtonSigSel2, SIGNAL(clicked()), this, SIGNAL(switchToSigSelectionOut()));
   mCheckBoxSigSel2 = new QCheckBox(tr("On"),this);
   //mCheckBoxSigSel2->setChecked(mpsSigSel->bActiveOutput);
   connect(mCheckBoxSigSel2, SIGNAL(toggled(bool)), this, SLOT(activateSigSel2(bool)));
  /*---------------------------------------------------------------------------------------------*
   * Amplifier simulation
   *---------------------------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------------------------*
   * Loudspeaker-enclosure-microphone system
   *---------------------------------------------------------------------------------------------*/

  /*---------------------------------------------------------------------------------------------*
   * Microphone input
   *---------------------------------------------------------------------------------------------*/	
	
}

/*------------------------------------------------------------------------------------------------*
 * Resize tab
 *------------------------------------------------------------------------------------------------*/
void TabAlgoStructureMainSap::resizeEvent ( QResizeEvent* )
{
   T_INT iButtonWidth            = 90; //BUTTONWIDTHLARGE;
   T_INT iMinSizeForChangingText =  1300; //MINSIZEFORCHANGINGTEXT;
   if (width() < iMinSizeForChangingText)
   {
      iButtonWidth = 80; // BUTTONWIDTHSMALL;
   }
T_INT iButtonHeight        =  38;
   T_INT iCheckBoxWidth       =  45;
   T_INT iCheckBoxHeight      =  18;
   T_INT iLeftOffset          =  (T_INT) ((T_FLOAT) 0.14 * (T_FLOAT) width());
   T_INT iRightOffset         =  iLeftOffset+iCheckBoxWidth;
   T_INT iWidth               =  width();

   T_INT iTopOffset           =  25;
   T_INT iHorSpaceBetwButtons =  (T_INT) ((T_FLOAT) (iWidth-iLeftOffset-iRightOffset-5*iButtonWidth) / (T_FLOAT) 4.0);
   T_INT iVerSpaceBetwButtons =  15;
   T_INT iAdderLength         =  22;
   T_INT iSplitterLength      =  10;

   T_INT ixpos[12], iypos[12];

   ixpos[0]  = iLeftOffset;
   ixpos[1]  = iLeftOffset + 1 * iButtonWidth;
   ixpos[2]  = iLeftOffset + 1 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[3]  = iLeftOffset + 2 * iButtonWidth + 1 * iHorSpaceBetwButtons;
   ixpos[4]  = iLeftOffset + 2 * iButtonWidth + 2 * iHorSpaceBetwButtons;
   ixpos[5]  = iLeftOffset + 3 * iButtonWidth + 2 * iHorSpaceBetwButtons;
   ixpos[6]  = iLeftOffset + 3 * iButtonWidth + 3 * iHorSpaceBetwButtons;
   ixpos[7]  = iLeftOffset + 4 * iButtonWidth + 3 * iHorSpaceBetwButtons;
   ixpos[8]  = iLeftOffset + 4 * iButtonWidth + 4 * iHorSpaceBetwButtons;
   ixpos[9]  = iLeftOffset + 5 * iButtonWidth + 4 * iHorSpaceBetwButtons;
   ixpos[10] = iLeftOffset + 5 * iButtonWidth + 5 * iHorSpaceBetwButtons;
   ixpos[11] = iLeftOffset + 6 * iButtonWidth + 5 * iHorSpaceBetwButtons;

   iypos[0]  = iTopOffset;
   iypos[1]  = iTopOffset + 1 * iButtonHeight;
   iypos[2]  = iTopOffset + 1 * iButtonHeight + 1 * iVerSpaceBetwButtons;
   iypos[3]  = iTopOffset + 2 * iButtonHeight + 1 * iVerSpaceBetwButtons;
   iypos[4]  = iTopOffset + 2 * iButtonHeight + 2 * iVerSpaceBetwButtons;
   iypos[5]  = iTopOffset + 3 * iButtonHeight + 2 * iVerSpaceBetwButtons;
   iypos[6]  = iTopOffset + 3 * iButtonHeight + 3 * iVerSpaceBetwButtons;
   iypos[7]  = iTopOffset + 4 * iButtonHeight + 3 * iVerSpaceBetwButtons;
   iypos[8]  = iTopOffset + 4 * iButtonHeight + 4 * iVerSpaceBetwButtons;
   iypos[9]  = iTopOffset + 5 * iButtonHeight + 4 * iVerSpaceBetwButtons;
   iypos[10] = iTopOffset + 5 * iButtonHeight + 5 * iVerSpaceBetwButtons;
   iypos[11] = iTopOffset + 6 * iButtonHeight + 5 * iVerSpaceBetwButtons;

  /*---------------------Check for text update neccessity -----------------------------*/
   bool bShortTextUpdate= (!bShortTextActive && iWidth < iMinSizeForChangingText);
   bool bLongTextUpdate = (bShortTextActive && iWidth >= iMinSizeForChangingText);
   bool bTextUpdate     = (!bShortTextActive && iWidth < iMinSizeForChangingText) || (bShortTextActive && iWidth >= iMinSizeForChangingText);
   bShortTextActive     = (!bTextUpdate && bShortTextActive) || (bTextUpdate && !bShortTextActive);

  /*---------------------------------------------------------------------------------------------*
   * Input and Output signals
   *---------------------------------------------------------------------------------------------*/
   mLabelMicSig->setGeometry(ixpos[0]-iLeftOffset+40, iypos[0]+3,2*iHorSpaceBetwButtons+20,iButtonHeight);
   mLabelRefSig->setGeometry(ixpos[0]-iLeftOffset+40, iypos[2]+3,2*iHorSpaceBetwButtons+20,iButtonHeight);
   mLabelPhoneDownl->setGeometry(ixpos[0]-iLeftOffset+40, iypos[4]+3,2*iHorSpaceBetwButtons+20,iButtonHeight);
   mLabelTtsSig->setGeometry(ixpos[0]-iLeftOffset+40, iypos[6]+3,2*iHorSpaceBetwButtons+20,iButtonHeight);
   mLabelOutUpl->setGeometry(ixpos[7]+iCheckBoxWidth, iypos[0]-2,iHorSpaceBetwButtons+iButtonWidth+iRightOffset-iCheckBoxWidth-10-35,iButtonHeight);
   mLabelOutRec->setGeometry(ixpos[7]+iCheckBoxWidth, iypos[2]-2,iHorSpaceBetwButtons+iButtonWidth+iRightOffset-iCheckBoxWidth-10-35,iButtonHeight);
   mLabelOutLs->setGeometry(ixpos[7]+iCheckBoxWidth, iypos[6]-2,iHorSpaceBetwButtons+iButtonWidth+iRightOffset-iCheckBoxWidth-10-35,iButtonHeight);

    if (bShortTextUpdate)
   {
      mLabelMicSig->setText(tr("Mic. signals"));
      mLabelRefSig->setText(tr("Ref. signals"));
      mLabelPhoneDownl->setText(tr("Downl. signals"));
      mLabelTtsSig->setText(tr("TTS signals"));
	  mLabelOutUpl->setText(tr("Upl. signals"));
      mLabelOutRec->setText(tr("Recog. signals"));
	  mLabelOutLs->setText(tr("Lsp. signals"));
   }
   else if(bLongTextUpdate)
   {
      mLabelMicSig->setText(tr("Microphone signals"));
      mLabelRefSig->setText(tr("Reference signals\n"));
      mLabelPhoneDownl->setText(tr("Downlink signals"));
      mLabelTtsSig->setText(tr("TTS signals"));
	  mLabelOutUpl->setText(tr("Uplink signals"));
      mLabelOutRec->setText(tr("Recognizer signals"));
      mLabelOutLs->setText(tr("Loudspeaker signals"));
   }


  /*---------------------------------------------------------------------------------------------*
   * Connectors from signal selection to hands-free system
   *---------------------------------------------------------------------------------------------*/
   mConnectorSigSelIn0->setGeometry(ixpos[0]-iLeftOffset+40, iypos[0], iLeftOffset-40, iButtonHeight);
   mConnectorSigSelIn1->setGeometry(ixpos[0]-iLeftOffset+40, iypos[2], iLeftOffset-40, iButtonHeight);
   mConnectorSigSelIn2->setGeometry(ixpos[0]-iLeftOffset+40, iypos[4], iLeftOffset-40, iButtonHeight);
   mConnectorSigSelIn3->setGeometry(ixpos[0]-iLeftOffset+40, iypos[6], iLeftOffset-40, iButtonHeight);

  /*---------------------------------------------------------------------------------------------*
   * Signal selection 
   *---------------------------------------------------------------------------------------------*/	
  mPushButtonSigSel1->setGeometry(ixpos[0], iypos[0], iButtonWidth, 4*iButtonHeight+3*iVerSpaceBetwButtons);

  /*---------------------------------------------------------------------------------------------*
   * Connectors from signal selection to hands-free system
   *---------------------------------------------------------------------------------------------*/
   mConnectorHfeIn0->setGeometry(ixpos[1], iypos[0], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHfeIn1->setGeometry(ixpos[1], iypos[2], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHfeIn2->setGeometry(ixpos[1], iypos[4], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHfeIn3->setGeometry(ixpos[1], iypos[6], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxSigSel1->setGeometry(ixpos[1]+5, iypos[0], iCheckBoxWidth, iCheckBoxHeight);

  /*---------------------------------------------------------------------------------------------*
   * Hands-free system  
   *---------------------------------------------------------------------------------------------*/
   mPushButtonHandsFreeSys->setGeometry(ixpos[4], iypos[0], iButtonWidth, 4*iButtonHeight+3*iVerSpaceBetwButtons); //
   mConnectorHandsFreeSysHor1->setGeometry(ixpos[5], iypos[0], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHandsFreeSysHor2->setGeometry(ixpos[5], iypos[2], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHandsFreeSysHor3->setGeometry(ixpos[5], iypos[6], iHorSpaceBetwButtons, iButtonHeight);
   mConnectorHandsFreeSysHor4->setGeometry(ixpos[5], iypos[4], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxHandsFreeSys->setGeometry(ixpos[5]+5, iypos[0], iCheckBoxWidth, iCheckBoxHeight);

 /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion in receiving path
   *---------------------------------------------------------------------------------------------*/
   mPushButtonSampleRateConvRec->setGeometry(ixpos[2], iypos[0], iButtonWidth, iButtonHeight);
   mConnectorSampleRateConvRec->setGeometry(ixpos[3], iypos[0], iHorSpaceBetwButtons, iButtonHeight);
   mPushButtonSrcBeforeHf1->setGeometry(ixpos[2], iypos[2], iButtonWidth, iButtonHeight);
   mConnectorSrcBeforeHf1->setGeometry(ixpos[3], iypos[2], iHorSpaceBetwButtons, iButtonHeight); //
   mPushButtonSrcBeforeHf2->setGeometry(ixpos[2], iypos[4], iButtonWidth, iButtonHeight);
   mConnectorSrcBeforeHf2->setGeometry(ixpos[3], iypos[4], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxSampleRateConvRec->setGeometry(ixpos[3]+5, iypos[0], iCheckBoxWidth, iCheckBoxHeight); 
   mCheckBoxSrcBeforeHf1->setGeometry(ixpos[3]+5, iypos[2], iCheckBoxWidth, iCheckBoxHeight);
   mCheckBoxSrcBeforeHf2->setGeometry(ixpos[3]+5, iypos[4], iCheckBoxWidth, iCheckBoxHeight);

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion in sending path
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvSend->setGeometry(ixpos[2], iypos[6], iButtonWidth, iButtonHeight);
   mConnectorSampleRateConvSendHor1->setGeometry(ixpos[3], iypos[6], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxSampleRateConvSend->setGeometry(ixpos[3]+5, iypos[6], iCheckBoxWidth, iCheckBoxHeight);

  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion after microphone output   
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvMic->setGeometry(ixpos[6], iypos[6], iButtonWidth, iButtonHeight);
   mConnectorSampleRateConvMicHor1->setGeometry(ixpos[7], iypos[6], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxSampleRateConvMic->setGeometry(ixpos[7]+5, iypos[6], iCheckBoxWidth, iCheckBoxHeight);
  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion before loudspeaker output
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvLs->setGeometry(ixpos[6], iypos[0], iButtonWidth, iButtonHeight);
   mConnectorSampleRateConvLs->setGeometry(ixpos[7], iypos[0], iHorSpaceBetwButtons, iButtonHeight);

   mCheckBoxSampleRateConvLs->setGeometry(ixpos[7]+5, iypos[0], iCheckBoxWidth, iCheckBoxHeight);
  /*---------------------------------------------------------------------------------------------*
   * Sample rate conversion after reference input
   *---------------------------------------------------------------------------------------------*/	
   mPushButtonSampleRateConvRefIn->setGeometry(ixpos[6], iypos[2], iButtonWidth, iButtonHeight);
   mConnectorSampleRateConvRefHor1->setGeometry(ixpos[7], iypos[2], iHorSpaceBetwButtons, iButtonHeight);
   mCheckBoxSampleRateConvRefIn->setGeometry(ixpos[7]+5, iypos[2], iCheckBoxWidth, iCheckBoxHeight);
  /*---------------------------------------------------------------------------------------------*
   * Signal selection 
   *---------------------------------------------------------------------------------------------*/	
    mPushButtonSigSel2->setGeometry(ixpos[8], iypos[0], iButtonWidth, 4*iButtonHeight+3*iVerSpaceBetwButtons);
	mCheckBoxSigSel2->setGeometry(ixpos[9]+5, iypos[0], iCheckBoxWidth, iCheckBoxHeight);
  /*---------------------------------------------------------------------------------------------*
   * Headphone output 
   *---------------------------------------------------------------------------------------------*/
   mPushButtonTtsOut->setGeometry(ixpos[6], iypos[4], iButtonWidth, iButtonHeight);
   mConnectorTtsOutHor1->setGeometry(ixpos[7], iypos[4], iHorSpaceBetwButtons, iButtonHeight);
   mCheckBoxTtsOut->setGeometry(ixpos[7]+5, iypos[4], iCheckBoxWidth, iCheckBoxHeight);
  /*---------------------------------------------------------------------------------------------*
   * Connectors at the output
   *---------------------------------------------------------------------------------------------*/
   mConnectorOut1->setGeometry(ixpos[9], iypos[0], iRightOffset-105, iButtonHeight);
   mConnectorOut0->setGeometry(ixpos[9], iypos[2], iRightOffset-105, iButtonHeight);
   mConnectorOut2->setGeometry(ixpos[9], iypos[4], iRightOffset-150, iButtonHeight);
   mConnectorOut2ext->setGeometry(ixpos[9], iypos[4]+iButtonHeight/2, (iRightOffset-150)*2, iVerSpaceBetwButtons+iButtonHeight-iAdderLength/2);
   mConnectorOut3->setGeometry(ixpos[9]+iAdderLength+iRightOffset-160, iypos[6], iRightOffset-105-(iAdderLength+iRightOffset-160), iButtonHeight);
   mConnectorOut3ext->setGeometry(ixpos[9], iypos[6], iRightOffset-160, iButtonHeight);
   mAdderConnectOut3->setGeometry(ixpos[9]+iRightOffset-160, iypos[6]+iButtonHeight/2-iAdderLength/2, iAdderLength, iAdderLength);

}





/*------Function to toggle Handsfree system-------*/
void TabAlgoStructureMainSap::switchHandsFreeSys(bool bAct)
{
	//mHsabAlgCore->bActive = (T_INT) bAct;
}

/*------Function to toggle signal selection (input)-------*/
void TabAlgoStructureMainSap::activateSigSel1(bool bAct)
{
	//mpsSigSel->bActiveInput = (T_INT) bAct;
}

/*------Function to toggle signal selection (output)-------*/
void TabAlgoStructureMainSap::activateSigSel2(bool bAct)
{
	mpsSigSel->bActiveOutput = (T_INT) bAct;
}

/*------Function to toggle Src Uplink-------*/
void TabAlgoStructureMainSap::switchSrcUplink(bool bAct)
{
	mpsRtf_hfSrcUplink->bActiveSrc = (T_INT) bAct;
	mCheckBoxSampleRateConvLs->setChecked(mpsRtf_hfSrcUplink->bActiveSrc);
	mCheckBoxSampleRateConvRec->setChecked(mpsRtf_hfSrcUplink->bActiveSrc);

}

/*------Function to toggle Src Downlink-------*/
void TabAlgoStructureMainSap::switchSrcDownlink(bool bAct)
{
	mpsRtf_hfSrcDownlink->bActiveSrc = (T_INT) bAct;
	mCheckBoxSrcBeforeHf2->setChecked(mpsRtf_hfSrcDownlink->bActiveSrc);
	mCheckBoxTtsOut->setChecked(mpsRtf_hfSrcDownlink->bActiveSrc);

}

/*------Function to toggle Src Tts-------*/
void TabAlgoStructureMainSap::switchSrcTts(bool bAct)
{
	mpsRtf_hfSrcTts->bActiveSrc = (T_INT) bAct;
	mCheckBoxSampleRateConvSend->setChecked(mpsRtf_hfSrcTts->bActiveSrc);
	mCheckBoxSampleRateConvMic->setChecked(mpsRtf_hfSrcTts->bActiveSrc);

}

/*------Function to toggle Src Reference signal-------*/
void TabAlgoStructureMainSap::switchSrcRef(bool bAct)
{
	mpsRtf_hfSrcRef->bActiveSrc = (T_INT) bAct;
	mCheckBoxSrcBeforeHf1->setChecked(mpsRtf_hfSrcRef->bActiveSrc);

}

/*------Function to toggle Src Recog. signal-------*/
void TabAlgoStructureMainSap::switchSrcRecog(bool bAct)
{
	mpsRtf_hfSrcRecog->bActiveSrc = (T_INT) bAct;
	mCheckBoxSampleRateConvRefIn->setChecked(mpsRtf_hfSrcRecog->bActiveSrc);

}







