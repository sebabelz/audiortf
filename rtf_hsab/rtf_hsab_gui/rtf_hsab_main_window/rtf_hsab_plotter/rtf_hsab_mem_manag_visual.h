/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_MEM_MANAG_VISUAL_H
#define RTF_HSAB_MEM_MANAG_VISUAL_H

#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_main_window.h"

class TabMemoryManagement : public QWidget
{
   Q_OBJECT

   public:
	   TabMemoryManagement(QWidget *parent = 0);
	  ~TabMemoryManagement();

   protected:

   signals:

   private:
	  QTreeWidget			 *tree;
      QHBoxLayout			 *mLayoutMemManagement;


};

#endif