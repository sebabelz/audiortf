/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <qwt_painter.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_scale_widget.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_math.h>
#include "rtf_hsab_time_plot.h"
#include <qwt_axis_id.h>

SignalTimeDomainPlot::SignalTimeDomainPlot(tPlotData* pPlotData,
	QWidget* parent)
	: QwtPlot(parent)
	, d_interval(0)
	, d_timerId(-1)
	, mpPlotData(pPlotData)
{
	T_INT k;

	engine = new ScaleEngine[4];

	mPlotCanvas = new QwtPlotCanvas;
	mPlotCanvas->setPaintAttribute(QwtPlotCanvas::BackingStore, false);

	/*-------------------------------------------------------------------------------*
	 * Legend
	 *-------------------------------------------------------------------------------*/
	QwtLegend* legend = new QwtLegend;
	legend->setFrameStyle(QFrame::Box | QFrame::Sunken);
	insertLegend(legend, QwtPlot::TopLegend);

	/*-------------------------------------------------------------------------------*
	 * Grid
	 *-------------------------------------------------------------------------------*/
	QwtPlotGrid* grid = new QwtPlotGrid;
	grid->enableXMin(true);
	grid->setMajorPen(QPen(Qt::gray, 0, Qt::DotLine));
	grid->setMinorPen(QPen(Qt::gray, 0, Qt::DotLine));
	grid->attach(this);

	/*-------------------------------------------------------------------------------*
	 * Insert new curves with labels
	 *-------------------------------------------------------------------------------*/
	QVector<QString> vTextLabels;

	for (k = 0; k < pPlotData->iNumberOfWidgets; k++)
	{
		vTextLabels.append(QString("Signal %1").arg(k));
	}

	for (k = 0; k < pPlotData->iNumberOfWidgets; k++)
	{
		if (k < vTextLabels.count())
		{
			pPlotCurveTime.append(new QwtPlotCurve(vTextLabels[k]));
		}
		else
		{
			pPlotCurveTime.append(new QwtPlotCurve("Name not set yet"));
		}
		pPlotCurveTime[k]->attach(this);
	}

	/*-------------------------------------------------------------------------------*
	 * Set curve styles
	 *-------------------------------------------------------------------------------*/
	QVector<QPen> vColor;
	vColor.append(QPen(Qt::blue));
	vColor.append(QPen(Qt::red));
	vColor.append(QPen(Qt::black));
	vColor.append(QPen(Qt::magenta));
	vColor.append(QPen(Qt::green));
	vColor.append(QPen(Qt::cyan));

	for (k = 0; k < pPlotData->iNumberOfWidgets; k++)
	{
		pPlotCurveTime[k]->setPen(vColor[k % vColor.count()]);
	}
	/*-------------------------------------------------------------------------------*
	 * Set axis
	 *-------------------------------------------------------------------------------*/
	QwtText XAxisLabel;
	XAxisLabel.setText("Time [s]");
	setAxisTitle(QwtPlot::xBottom, XAxisLabel);
	setAxisScale(QwtPlot::xBottom, 0, pPlotData->dSignalMemoryInSeconds);

	enableAxis(QwtPlot::yLeft + QwtPlot::yRight);
	setAxesCount(QwtPlot::yLeft, 2);
	setAxesCount(QwtPlot::yRight, 2);

	for (int i = 0; i < mpPlotData->iNumberOfWidgets; ++i)
	{
		if ( i < 2)
		{
			YAxisLabel.setText("Amplitude");
			setAxisTitle(QwtAxisId(QwtPlot::yLeft, i), YAxisLabel);
		}
		else 
		{
			YAxisLabel.setText("[dB]");
			setAxisTitle(QwtAxisId(QwtPlot::yRight, i - 2), YAxisLabel);
		}
	}

	for (int i = 0; i < mpPlotData->iNumberOfWidgets; ++i)
	{
		if (i < 2)
			axisWidget(QwtAxisId(QwtPlot::yLeft, i))->setPalette(axisColor(vColor.at(i).color()));
		else
			axisWidget(QwtAxisId(QwtPlot::yRight, i - 2))->setPalette(axisColor(vColor.at(i).color()));
	}

	setAxisScale(QwtAxisId(QwtPlot::yLeft, 0), mpPlotData->yLeftLimitTime[1], mpPlotData->yLeftLimitTime[0]);	// linear values
	setAxisScale(QwtAxisId(QwtPlot::yLeft, 1), mpPlotData->yLeftLimitTime[1], mpPlotData->yLeftLimitTime[0]);	// linear values
	setAxisScale(QwtAxisId(QwtPlot::yRight, 0), mpPlotData->yRightLimitTime[1], mpPlotData->yRightLimitTime[0]);	// dB values
	setAxisScale(QwtAxisId(QwtPlot::yRight, 1), mpPlotData->yRightLimitTime[1], mpPlotData->yRightLimitTime[0]);	// dB values

	alignScales();


	limitsDialog = new AxisLimitsDialog(mpPlotData->numOfLeftYAxes, mpPlotData->numOfRightYAxes,
	                                    mpPlotData->iNumberOfWidgets, "Signal", "Set Y-axes scale (time domain)", true, this);
	limitsDialog->setMaximumLimits(QVariant::fromValue(QPair<double, double>(200, -200)));
	limitsDialog->setLimits(QVariant::fromValue(QList<QPair<double, double>>({
		QPair<double, double>(mpPlotData->yLeftLimitTime[1], mpPlotData->yLeftLimitTime[0]),
		QPair<double, double>(mpPlotData->yLeftLimitTime[3], mpPlotData->yLeftLimitTime[2]),
		QPair<double, double>(mpPlotData->yRightLimitTime[1], mpPlotData->yRightLimitTime[0]),
		QPair<double, double>(mpPlotData->yRightLimitTime[3], mpPlotData->yRightLimitTime[2]),
	})));

	connect(limitsDialog, &AxisLimitsDialog::valueChanged, this, &SignalTimeDomainPlot::ApplyScales);
	connect(limitsDialog, &AxisLimitsDialog::scalerChanged, this, &SignalTimeDomainPlot::enableScaler);

	/*-------------------------------------------------------------------------------*
	 * Set timer interval (in ms)
	 *-------------------------------------------------------------------------------*/
	setTimerInterval(50);

}

void SignalTimeDomainPlot::mouseDoubleClickEvent(QMouseEvent *pmEvent)
{
	limitsDialog->show();
}

//
//  Set a plain canvas frame and align the scales to it
//
void SignalTimeDomainPlot::alignScales()
{
	// The code below shows how to align the scales to
	// the canvas frame, but is also a good example demonstrating
	// why the spreaded API needs polishing.

	for (int i = 0; i < axesCount(QwtPlot::yLeft); i++)
	{
		QwtScaleWidget *scaleWidget = axisWidget(QwtAxisId(QwtPlot::yLeft, i));
		if (scaleWidget)
			if (i == 0)
				scaleWidget->setMargin(0);
			else
				scaleWidget->setMargin(10);

		QwtScaleDraw *scaleDraw = axisScaleDraw(QwtAxisId(QwtPlot::yLeft, i));
		if (scaleDraw)
			scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
	}
	for (int i = 0; i < axesCount(QwtPlot::yRight); i++)
	{
		QwtScaleWidget *scaleWidget = axisWidget(QwtAxisId(QwtPlot::yRight, i));
		if (scaleWidget)
			if (i == 0)
				scaleWidget->setMargin(0);
			else
				scaleWidget->setMargin(10);

		QwtScaleDraw *scaleDraw = axisScaleDraw(QwtAxisId(QwtPlot::yRight, i));
		if (scaleDraw)
			scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
	}

		QwtScaleWidget *scaleWidget = axisWidget(xBottom);
		if (scaleWidget)
			scaleWidget->setMargin(0);
		QwtScaleDraw *scaleDraw = axisScaleDraw(xBottom);
		if (scaleDraw)
			scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
}

QPalette SignalTimeDomainPlot::axisColor(QColor c)
{
	QPalette p;
	p.setColor(QPalette::WindowText,c);
	p.setColor(QPalette::Text, c);
	return p;
}

void SignalTimeDomainPlot::startScaleEngine(int index)
{
	engine[index].startObserve();
	engine[index].setMargin(0.1);
	connect(&engine[index], &ScaleEngine::sendAxisScale, [=](double a, double b)
	{
		if (index < 2)
			setAxisScale(QwtAxisId(QwtPlot::yLeft, index), a, b);
		else
			setAxisScale(QwtAxisId(QwtPlot::yRight, index - 2), a, b);
	});
}

void SignalTimeDomainPlot::stopScaleEngine(int index)
{
	disconnect(&engine[index], &ScaleEngine::sendAxisScale, nullptr, nullptr);
	engine[index].stopObserve();
}

void SignalTimeDomainPlot::enableScaler(int index, int isEnabled)
{
	double max = 0, min = 0;
	if (isEnabled)
	{
		startScaleEngine(index);
	}
	else
	{
		limitsDialog->getLimits(index, max, min);
		updateScales(min, max, index);
		stopScaleEngine(index);
	}
}

void SignalTimeDomainPlot::setTimerInterval(double ms)
{
	d_interval = qRound(ms);

	if (d_timerId >= 0)  
	{
		killTimer(d_timerId);
		d_timerId = -1;
	}
	if (d_interval >= 0)
		d_timerId = startTimer(d_interval);
}

//  Generate new values 
void SignalTimeDomainPlot::timerEvent(QTimerEvent *)
{
	replot();
}

void SignalTimeDomainPlot::ApplyScales(QList<QPair<double, double>> list)
{
	mpPlotData->yLeftLimitTime[0] = list[0].first;
	mpPlotData->yLeftLimitTime[1] = list[0].second;
	mpPlotData->yLeftLimitTime[2] = list[1].first;
	mpPlotData->yLeftLimitTime[3] = list[1].second;
	mpPlotData->yRightLimitTime[0] = list[2].first;
	mpPlotData->yRightLimitTime[1] = list[2].second;
	mpPlotData->yRightLimitTime[2] = list[3].first;
	mpPlotData->yRightLimitTime[3] = list[3].second;

	/*engine[0].setActualScale(mpPlotData->yLeftLimitTime[1], mpPlotData->yLeftLimitTime[0]);
	engine[1].setActualScale(mpPlotData->yLeftLimitTime[3], mpPlotData->yLeftLimitTime[2]);
	engine[2].setActualScale(mpPlotData->yRightLimitTime[1], mpPlotData->yRightLimitTime[0]);
	engine[3].setActualScale(mpPlotData->yRightLimitTime[3], mpPlotData->yRightLimitTime[2]);*/

	setAxisScale(QwtAxisId(QwtPlot::yLeft, 0), mpPlotData->yLeftLimitTime[1], mpPlotData->yLeftLimitTime[0]);
	setAxisScale(QwtAxisId(QwtPlot::yLeft, 1), mpPlotData->yLeftLimitTime[3], mpPlotData->yLeftLimitTime[2]);
	setAxisScale(QwtAxisId(QwtPlot::yRight, 0), mpPlotData->yRightLimitTime[1], mpPlotData->yRightLimitTime[0]);
	setAxisScale(QwtAxisId(QwtPlot::yRight, 1), mpPlotData->yRightLimitTime[3], mpPlotData->yRightLimitTime[2]);
}

void SignalTimeDomainPlot::setAxesVisibility(int num, bool b)
{
	if (num < 2)
		setAxisVisible(QwtAxisId(QwtPlot::yLeft, num), b);
	else
		setAxisVisible(QwtAxisId(QwtPlot::yRight, num - 2), b);
}

void SignalTimeDomainPlot::updateAxisLabel(int num, QString s)
{
	if (num < 2)
		setAxisTitle(QwtAxisId(QwtPlot::yLeft, num), s);
	else
		setAxisTitle(QwtAxisId(QwtPlot::yRight, num - 2), s);
}

void SignalTimeDomainPlot::updateScales(double min , double max, int num)
{
	if (num < 2)
		setAxisScale(QwtAxisId(QwtPlot::yLeft, num), min, max);
	else
		setAxisScale(QwtAxisId(QwtPlot::yRight, num - 2), min, max);
}


