/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <qwt_painter.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_engine.h>
#include <qwt_plot_grid.h>
#include <qwt_scale_widget.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_math.h>
#include "rtf_hsab_freq_plot.h"

SignalFreqDomainPlot::SignalFreqDomainPlot( tPlotData *pPlotData,
                                            QWidget           *parent)
  : QwtPlot(parent)
  , d_interval(0)
  , d_timerId(-1)
  , mpPlotData(pPlotData)
{

	T_INT  k;

  /*-------------------------------------------------------------------------------*
   * We don't need the cache here
   *-------------------------------------------------------------------------------*/   
   mPlotCanvas = new QwtPlotCanvas;
   mPlotCanvas->setPaintAttribute(QwtPlotCanvas::BackingStore, false);

  /*-------------------------------------------------------------------------------*
   * Legend
   *-------------------------------------------------------------------------------*/   
   QwtLegend *legend = new QwtLegend;
   legend->setFrameStyle(QFrame::Box|QFrame::Sunken);
   insertLegend(legend, QwtPlot::TopLegend);

  /*-------------------------------------------------------------------------------*
   * Grid
   *-------------------------------------------------------------------------------*/  
   QwtPlotGrid *grid = new QwtPlotGrid;
   grid->enableXMin(true);
   grid->enableYMin(true);
   grid->setMajorPen(QPen(Qt::gray, 0, Qt::DotLine));
   grid->setMinorPen(QPen(Qt::gray, 0, Qt::DotLine));

   grid->attach(this);
   alignScales();
  /*-------------------------------------------------------------------------------*
   * Insert new curves with labels
   *-------------------------------------------------------------------------------*/    
   QVector<QString> vTextLabels;
   for (k=0; k<pPlotData->iNumberOfWidgets; k++)
   {
      vTextLabels.append(QString("Signal %1").arg(k));
   }
   
   for (k=0; k<pPlotData->iNumberOfWidgets; k++)
   {
      if (k < vTextLabels.count())
      {
         pPlotCurveFreq.append(new QwtPlotCurve(vTextLabels[k]));         
      }
      else
      {
         pPlotCurveFreq.append(new QwtPlotCurve("Name not defined yet"));
      }
      pPlotCurveFreq[k]->attach(this);
   }

  /*-------------------------------------------------------------------------------*
   * Set curve styles
   *-------------------------------------------------------------------------------*/ 
   QVector <QPen> vColor;
   vColor.append(QPen(Qt::blue));
   vColor.append(QPen(Qt::red));
   vColor.append(QPen(Qt::black));
   vColor.append(QPen(Qt::magenta));
   vColor.append(QPen(Qt::green));
   vColor.append(QPen(Qt::cyan));
   for (k=0; k<pPlotData->iNumberOfWidgets; k++)
   {
      pPlotCurveFreq[k]->setPen(vColor[k%vColor.count()]);   
   }
   QwtScaleDiv yDiv(0, pPlotData->dFreqMemoryInHertz * 0.001);


   QList<qreal> xMinorTicks = QList<qreal>() << 1 << 2 << 3 << 4 << 5 << 7 << 8 << 9 << 10 << 11 << 13 << 14 << 15 << 16 << 17 << 19 << 20 << 21 << 22 << 23;
   QList<qreal> xMajorTicks = QList<qreal>() << 0 << 6 << 12 << 18 << 24;

   yDiv.setTicks(QwtScaleDiv::MinorTick, xMinorTicks);
   yDiv.setTicks(QwtScaleDiv::MajorTick, xMajorTicks);

   this->setAxisScaleDiv(QwtAxis::xBottom, yDiv);
   // Axis 
   QwtText XAxisLabel;
   XAxisLabel.setText("Frequency [kHz]");
   setAxisTitle(QwtPlot::xBottom, XAxisLabel);
   //setAxisScale(QwtPlot::xBottom, 0, pPlotData->dFreqMemoryInHertz*0.001); // in kHz


   YAxisLabel.setText("[dB]");

   enableAxis(yLeft+yRight);
   setAxisTitle(QwtPlot::yLeft, YAxisLabel);
   YAxisLabel.setText("Amplitude");
   setAxisTitle(QwtPlot::yRight, YAxisLabel);

   setAxisScale(QwtPlot::yLeft, mpPlotData->yLeftLimitFreq[1] , mpPlotData->yLeftLimitFreq[0] );	// linear values
   setAxisScale(QwtPlot::yRight, mpPlotData->yRightLimitFreq[1] , mpPlotData->yRightLimitFreq[0] ); // dB values
   setTimerInterval(100.0); 

   limitsDialog = new AxisLimitsDialog(1, 2, 2,
	   QVariant::fromValue(QList<QString>({ "Left y-axis", "Right y-axis" })),
	   "Set Y - axes scale time domain", false, this);

   limitsDialog->setMaximumLimits(QVariant::fromValue(QPair<double, double>(100, -100)));
   limitsDialog->setLimits(QVariant::fromValue(QList<QPair<double, double>>({ QPair<double, double>(mpPlotData->yLeftLimitFreq[1], mpPlotData->yLeftLimitFreq[0]) ,
																		QPair<double, double>(mpPlotData->yRightLimitFreq[1],  mpPlotData->yRightLimitFreq[0]) })));

   connect(limitsDialog, &AxisLimitsDialog::valueChanged, this, &SignalFreqDomainPlot::ApplyScales);
}

void SignalFreqDomainPlot::mouseDoubleClickEvent ( QMouseEvent *pmEvent )
{
  limitsDialog->show();
}
//
//  Set a plain canvas frame and align the scales to it
//
void SignalFreqDomainPlot::alignScales()
{
    // The code below shows how to align the scales to
    // the canvas frame, but is also a good example demonstrating
    // why the spreaded API needs polishing.

	mPlotCanvas->setFrameStyle(QFrame::Box | QFrame::Plain );
	mPlotCanvas->setLineWidth(1);

    for ( int i = 0; i < QwtPlot::axisCnt; i++ )
    {
        QwtScaleWidget *scaleWidget = (QwtScaleWidget *)axisWidget(i);
        if ( scaleWidget )
            scaleWidget->setMargin(0);

        QwtScaleDraw *scaleDraw = (QwtScaleDraw *)axisScaleDraw(i);
        if ( scaleDraw )
            scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
    }
}

void SignalFreqDomainPlot::setTimerInterval(double ms)
{
    d_interval = qRound(ms);

    if ( d_timerId >= 0 )
    {
        killTimer(d_timerId);
        d_timerId = -1;
    }
    if (d_interval >= 0 )
        d_timerId = startTimer(d_interval);
}

//  Generate new values 
void SignalFreqDomainPlot::timerEvent(QTimerEvent *)
{
    replot();
}

void SignalFreqDomainPlot::ApplyScales(QList<QPair<double, double>> list)
{
	mpPlotData->yLeftLimitFreq[0] = list[0].first;
	mpPlotData->yLeftLimitFreq[1] = list[0].second;
	mpPlotData->yRightLimitFreq[0] = list[1].first;
	mpPlotData->yRightLimitFreq[1] = list[1].second;

   setAxisScale(QwtPlot::yLeft, mpPlotData->yLeftLimitFreq[1] , mpPlotData->yLeftLimitFreq[0] );	// linear values
   setAxisScale(QwtPlot::yRight, mpPlotData->yRightLimitFreq[1] , mpPlotData->yRightLimitFreq[0] );	// dB values
}