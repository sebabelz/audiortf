/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_mem_manag_visual.h"

TabMemoryManagement::TabMemoryManagement(QWidget *parent)
     : QWidget(parent)
{
	
   T_INT   iColumnWidth = 135;
   T_INT   iCurrentColumn, iStackAbs;
   T_FLOAT fRatioPercent;

   QHBoxLayout *LayoutMemManagementMain = new QHBoxLayout(this);

   QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));
  /*---------------------------------------------------------------------------------------------*
   * Basic layout
   *---------------------------------------------------------------------------------------------*/
    mLayoutMemManagement        = new QHBoxLayout();

    tree = new QTreeWidget(this);
	tree->setColumnCount(8);
	tree->setColumnWidth(0,2*iColumnWidth);
	tree->setColumnWidth(1,iColumnWidth);
	tree->setColumnWidth(2,iColumnWidth);
	tree->setColumnWidth(3,iColumnWidth);
	tree->setColumnWidth(4,iColumnWidth);
	tree->setColumnWidth(5,iColumnWidth);
	tree->setColumnWidth(6,iColumnWidth);
	tree->setColumnWidth(7,1);
	tree->setAlternatingRowColors(true);

   	QTreeWidgetItem* headerItem = new QTreeWidgetItem();
	headerItem->setText(0,QString("Modules"));
	headerItem->setText(1,QString("Total memory"));
	headerItem->setText(2,QString("Percentage"));
	headerItem->setText(3,QString("Local memory"));
    headerItem->setText(4,QString("Overhead"));
	headerItem->setText(5,QString("Max. total memory"));
	headerItem->setText(6,QString("Stack size at initialization"));
	headerItem->setText(7,QString(""));

	headerItem->setTextAlignment(1,Qt::AlignRight);
	headerItem->setTextAlignment(2,Qt::AlignRight);
    headerItem->setTextAlignment(3,Qt::AlignRight);
    headerItem->setTextAlignment(4,Qt::AlignRight);
	headerItem->setTextAlignment(5,Qt::AlignRight);
	headerItem->setTextAlignment(6,Qt::AlignRight);

	tree->setHeaderItem(headerItem);
  /*---------------------------------------------------------------------------------------------*
   * toDO
   *---------------------------------------------------------------------------------------------*/

  
}

TabMemoryManagement::~TabMemoryManagement()
{
}