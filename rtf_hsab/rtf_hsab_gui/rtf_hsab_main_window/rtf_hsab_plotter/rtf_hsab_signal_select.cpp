/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <qwt_painter.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_widget.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_math.h>
#include "rtf_hsab_signal_select.h"
#include "rtf_hsab_signal_select_channel.h"

SignalSelection::SignalSelection( tPlotData *pPlotData, QVector<QwtPlotCurve*>   pPlotCurveTime, QVector<QwtPlotCurve*>   pPlotCurveFreq, AudioIO *pAudioIO, tSession *ps, ScaleEngine *engine,
                                            QWidget           *parent)
: QDialog(parent) 
,  mpPlotData(pPlotData)

{
   T_INT    k;
   T_INT    iChannelNumber;
   QString  sChannelName;
   
  /*-----------------------------------------------------------------------------------------*
   * Main layout
   *-----------------------------------------------------------------------------------------*/
   QVBoxLayout *MainLayout = new QVBoxLayout(this);
   setMinimumWidth(220);

   mViewTimePlotCheckBox = new QCheckBox(tr("Time Plot"));
   mViewFreqPlotCheckBox = new QCheckBox(tr("Frequency Plot"));
   mViewSpecPlotCheckBox = new QCheckBox(tr("Spectrogram Plot"));

   mViewTimePlotCheckBox->setChecked(true);
   mViewFreqPlotCheckBox->setChecked(true);
   mViewSpecPlotCheckBox->setChecked(true);
   mpPlotData->bPlottingTime = true;
   mpPlotData->bPlottingFreq = true;
   mpPlotData->bPlottingSpec = true;

   MainLayout->addWidget(mViewTimePlotCheckBox);
   MainLayout->addWidget(mViewFreqPlotCheckBox);
   MainLayout->addWidget(mViewSpecPlotCheckBox);

   connect(mViewTimePlotCheckBox, SIGNAL(toggled(bool)), parent, SLOT(viewTimePlot(bool)));
   connect(mViewFreqPlotCheckBox, SIGNAL(toggled(bool)), parent, SLOT(viewFreqPlot(bool)));
   connect(mViewSpecPlotCheckBox, SIGNAL(toggled(bool)), parent, SLOT(viewSpecPlot(bool)));

   QVBoxLayout *SubLayoutCenter = new QVBoxLayout();
   MainLayout->addLayout(SubLayoutCenter);

   QScrollArea *ScrollArea      = new QScrollArea();
   MainLayout->addWidget(ScrollArea);

   QWidget     *Widget          = new QWidget(ScrollArea); 
   ScrollArea->setWidget(Widget);
   ScrollArea->setWidgetResizable(true);

   QVBoxLayout *SubLayoutScroll = new QVBoxLayout(Widget);
   Widget->setLayout(SubLayoutScroll);

   		
   iChannelNumber = (T_INT) 0;
   for ( k=0 ; k<mpPlotData->iNumberOfWidgets; k++ )
   {  
      sChannelName = QString("Signal %1").arg(k);
	  mSignalSelectChannel= new SignalSelectChannel(Widget, iChannelNumber, sChannelName, pPlotData, pPlotCurveTime, pPlotCurveFreq, pAudioIO, ps, engine);
	  mQVectorSignalSelectChannel.append(mSignalSelectChannel);
	  SubLayoutScroll->addWidget(mQVectorSignalSelectChannel[iChannelNumber]);
      iChannelNumber++;
   }
   pAudioIO->setPlotterModule2DVector(mSignalSelectChannel->mQVector2DPlotterSignal);
}