/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <qwt_painter.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_widget.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_math.h>
#include "rtf_hsab_signal_select_channel.h"


SignalSelectChannel::SignalSelectChannel(QWidget *parent, int channelNumber, QString string, tPlotData *pPlotData, QVector<QwtPlotCurve*>   pPlotCurveTime, QVector<QwtPlotCurve*>   pPlotCurveFreq, AudioIO *pAudioIO, tSession *ps,ScaleEngine *engine)
	: QGroupBox(string, parent)
	, mChannelNumber(channelNumber)
	, mpPlotData(pPlotData)
	, mpPlotCurveTime(pPlotCurveTime)
	, mpPlotCurveFreq(pPlotCurveFreq)
	, mps(ps)
	, mpAudioIO(pAudioIO)

{
	QString   Text;
	QGridLayout* ChannelLayout = new QGridLayout(this);
	setLayout(ChannelLayout);

	QLabel *LabelModule = new QLabel(tr("Select module"));
	ChannelLayout->addWidget(LabelModule, 0, 0);

	mComboBoxModuleSelect = new QComboBox(this);
	mComboBoxModuleSelect->addItem(tr("Main signals"));
	mComboBoxModuleSelect->addItem(tr("Beamformer signals"));

	mComboBoxModuleSelect->setFixedWidth(190);
	connect(mComboBoxModuleSelect, SIGNAL(activated(int)), this, SLOT(currentModuleIndex(int)));
	ChannelLayout->addWidget(mComboBoxModuleSelect, 1, 0);


	mCheckBoxSelectSignal = new QCheckBox(tr("Select signal"), this);
	mCheckBoxSelectSignal->setChecked(pPlotData->bActive);
	connect(mCheckBoxSelectSignal, SIGNAL(toggled(bool)), this, SLOT(visibleSignal(bool)));
	ChannelLayout->addWidget(mCheckBoxSelectSignal, 2, 0);

	mpComboBoxSignalSelect = new QComboBox(this);
	mpComboBoxSignalSelect->addItem(tr("Microphone-Input"));
	mpComboBoxSignalSelect->addItem(tr("Phone-Input"));
	mpComboBoxSignalSelect->addItem(tr("Phone-Output"));
	mpComboBoxSignalSelect->addItem(tr("Loudspeaker-Output"));
	mpComboBoxSignalSelect->addItem(tr("Mic-In Spect. Power [dB]"));
	mpComboBoxSignalSelect->addItem(tr("Phone-In Spect. Power [dB]"));
	mpComboBoxSignalSelect->addItem(tr("Phone-Out Spect. Power [dB]"));
	mpComboBoxSignalSelect->addItem(tr("Loudspeaker-Out Spect. Power [dB]"));
	mpComboBoxSignalSelect->addItem(("Mic-In RMS Level [dB]"));
	mpComboBoxSignalSelect->addItem(("Phone-In RMS Level [dB]"));
	mpComboBoxSignalSelect->addItem(("Phone-Out RMS Level [dB]"));
	mpComboBoxSignalSelect->addItem(("Loudspeaker-Out RMS Level [dB]"));

	mHLayout0 = new QHBoxLayout(this);
	mHLayout1 = new QHBoxLayout(this);
	mVLayout1 = new QVBoxLayout(this);
	mVLayout2 = new QVBoxLayout(this);
	mVLayout3 = new QVBoxLayout(this);
	mVLayout4 = new QVBoxLayout(this);

	mpComboBoxSignalSelect->setCurrentIndex(mChannelNumber);
	mpComboBoxSignalSelect->activated(mChannelNumber);
	connect(mpComboBoxSignalSelect, SIGNAL(activated(int)), this, SLOT(currentSignalIndex(int)));

	mpComboBoxSignalSelect->setFixedWidth(190);
	ChannelLayout->addWidget(mpComboBoxSignalSelect, 3, 0);
	LabelChannel = new QLabel();
	mVLayout3->addWidget(LabelChannel);
	mComboBoxChannelSelect = new QComboBox(this);

	mComboBoxChannelSelect->setFixedSize(40, 20);
	mComboBoxChannelSelect->setEditable(true);
	connect(mComboBoxChannelSelect, SIGNAL(activated(int)), this, SLOT(currentChannel0Index(int)));
	mVLayout3->addWidget(mComboBoxChannelSelect);


	LabelChannel1 = new QLabel(tr("Ch"));
	mVLayout4->addWidget(LabelChannel1);
	mComboBoxChannelSelect1 = new QComboBox();
	mComboBoxChannelSelect1->setEditable(true);
	mComboBoxChannelSelect1->setFixedSize(40, 20);
	mComboBoxChannelSelect1->setEnabled(false);
	connect(mComboBoxChannelSelect1, SIGNAL(activated(int)), this, SLOT(currentChannel1Index(int)));
	LabelChannel1->setEnabled(false);

	mVLayout4->addWidget(mComboBoxChannelSelect1);

	LabelFreq = new QLabel(tr("Freq [Hz]"));
	mVLayout1->addWidget(LabelFreq);

	QLabel *LabelOffset = new QLabel(tr("Offset"));
	mVLayout2->addWidget(LabelOffset);


	mSpinBoxFreq = new QDoubleSpinBox(this);
#ifdef BF_SYSTEM
	mSpinBoxFreq->setSingleStep((double)ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal / ps->HsabAlgCoreMain.HsabAlgCoreParams.iFFTLength);
	mSpinBoxFreq->setMaximum((double)ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal * (T_DOUBLE)0.5);
#else
	mSpinBoxFreq->setSingleStep((double)ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt / ps->HsabAlgCoreMain.HsabAlgCoreParams.iFftOrder);
	mSpinBoxFreq->setMaximum((double)ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt * (T_DOUBLE)0.5);
#endif 


	mSpinBoxFreq->setMinimum((double) 0.0);
	mSpinBoxFreq->setFixedSize(45, 22);
	mVLayout1->addWidget(mSpinBoxFreq);
	LabelFreq->setEnabled(false);
	mSpinBoxFreq->setEnabled(false);

	connect(mSpinBoxFreq, SIGNAL(valueChanged(double)), this, SLOT(selectFreqValue(double)));

	mSpinBoxOffset = new QDoubleSpinBox(this);
	mSpinBoxOffset->setSingleStep((double) 0.5);
	mSpinBoxOffset->setMinimum((double)-10.0);
	mSpinBoxOffset->setMaximum((double) 10.0);
	mSpinBoxOffset->setFixedSize(45, 22);

	connect(mSpinBoxOffset, SIGNAL(valueChanged(double)), this, SLOT(selectOffsetValue(double)));

	mVLayout2->addWidget(mSpinBoxOffset);

	mHLayout0->addLayout(mVLayout1);
	mHLayout0->addLayout(mVLayout2);
	mHLayout0->addStretch();
	mHLayout0->addLayout(mVLayout3);
	mHLayout0->addLayout(mVLayout4);

	ChannelLayout->addLayout(mHLayout0, 6, 0);


	uiData.pSpinBoxFreq = mSpinBoxFreq;
	uiData.pSpinBoxOffset = mSpinBoxOffset;
	uiData.pChannelNumber = mChannelNumber;
	uiData.pPlotData = pPlotData;
	uiData.pPlotCurveTime = pPlotCurveTime;
	uiData.pPlotCurveFreq = pPlotCurveFreq;
	uiData.pComboBoxSignalSelect = mpComboBoxSignalSelect;
	uiData.pComboBoxModuleSelect = mComboBoxModuleSelect;
	uiData.pComboBoxChannelSelect0 = mComboBoxChannelSelect;
	uiData.pComboBoxChannelSelect1 = mComboBoxChannelSelect1;
	uiData.pLabelChannel0 = LabelChannel;
	uiData.pLabelChannel1 = LabelChannel1;
	uiData.pLabelFreq = LabelFreq;
	uiData.ps = mps;
	uiData.engine = engine;

	/*------------------------------------------------------------------------------------------------*
	 * Visualization of signals in different modules
	 *------------------------------------------------------------------------------------------------*/

	 /*---------- Instantiate Main signals -------------*/

	mQVectorPlotterMainSignals.append(new MicIn(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneIn(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneOut(&uiData));

	mQVectorPlotterMainSignals.append(new LspOut(&uiData));

	mQVectorPlotterMainSignals.append(new MicInPsd(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneInPsd(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneOutPsd(&uiData));

	mQVectorPlotterMainSignals.append(new LspOutPsd(&uiData));

	mQVectorPlotterMainSignals.append(new MicInRms(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneInRms(&uiData));

	mQVectorPlotterMainSignals.append(new PhoneOutRms(&uiData));

	mQVectorPlotterMainSignals.append(new LspOutRms( &uiData));

	for (int i = 0; i < mQVectorPlotterMainSignals.count(); ++i)
	{
		connect(mQVectorPlotterMainSignals.at(i), &PlotterModule::sendLabel, this, &SignalSelectChannel::updateLabel);
		connect(mQVectorPlotterMainSignals.at(i), &PlotterModule::sendScales, this, &SignalSelectChannel::updateScale);
	}


	mQVector2DPlotterSignal.append(mQVectorPlotterMainSignals);
#ifdef BF_SYSTEM
	/*---------- Instantiate Beamformer signals -------------*/

	//ADAPTIVE BEAMFORMER

	mQVectorPlotterBeamformerSignals.append(new DCOutSpec(&uiData));

	mQVectorPlotterBeamformerSignals.append(new FbfOutSpec(&uiData));

	mQVectorPlotterBeamformerSignals.append(new BmOutSpec(&uiData));

	mQVectorPlotterBeamformerSignals.append(new IcOutSpec(&uiData));

	mQVectorPlotterBeamformerSignals.append(new SteerAngel(&uiData));

	mQVectorPlotterBeamformerSignals.append(new FilterCoeffIc(&uiData));

	//SPATIAL POSTFILTER

	mQVectorPlotterBeamformerSignals.append(new SmPowRatio(&uiData));

	mQVectorPlotterBeamformerSignals.append(new OptWienFilter(&uiData));

	mQVectorPlotterBeamformerSignals.append(new PowCorrPf(&uiData));

	mQVectorPlotterBeamformerSignals.append(new ShortTimePowAbf(&uiData));

	//STEP-SIZE CONTROL

	mQVectorPlotterBeamformerSignals.append(new MagRatio(&uiData));

	mQVectorPlotterBeamformerSignals.append(new ShortTimePowFbf(&uiData));

	mQVectorPlotterBeamformerSignals.append(new ShortTimePowBm(&uiData));

	mQVectorPlotterBeamformerSignals.append(new AverPowBm(&uiData));

	mQVectorPlotterBeamformerSignals.append(new PowCorrBm(&uiData));

	//ADAPTIVE BLOCKING MATRIX

	mQVectorPlotterBeamformerSignals.append(new AbmFiltOutSpec(&uiData));

	mQVectorPlotterBeamformerSignals.append(new FilterCoeffAbm(&uiData));

	mQVectorPlotterBeamformerSignals.append(new ThreshPowRatioAbm(&uiData));

	for (int i = 0; i < mQVectorPlotterBeamformerSignals.count(); ++i)
	{
		connect(mQVectorPlotterBeamformerSignals.at(i), &PlotterModule::sendLabel, this, &SignalSelectChannel::updateLabel);
		connect(mQVectorPlotterBeamformerSignals.at(i), &PlotterModule::sendScales, this, &SignalSelectChannel::updateScale);
	}

	mQVector2DPlotterSignal.append(mQVectorPlotterBeamformerSignals);
#endif
	/*------------------------------------------------------------------------------------------------*
	 *  Visualization at initialization
	 *------------------------------------------------------------------------------------------------*/
	mQVector2DPlotterSignal[0][mChannelNumber]->setPlotterSignal();
}

/*------------------------------------------------------------------------------------------------*
 * Set visualization parameters from GUI
 *------------------------------------------------------------------------------------------------*/

void SignalSelectChannel::currentModuleIndex(int idx)
{
   mQVector2DPlotterSignal[idx][0]->setPlotterModule();
}


void SignalSelectChannel::currentSignalIndex(int idx)
{
   mQVector2DPlotterSignal[mComboBoxModuleSelect->currentIndex()][idx]->setPlotterSignal();
   mpPlotData->piModuleIndex[mChannelNumber] = mComboBoxModuleSelect->currentIndex();
   mpPlotData->piSignalIndex[mChannelNumber] = idx;
}

void SignalSelectChannel::currentChannel0Index(int iCh0)
{
   mpPlotData->piChannel1[mChannelNumber] = iCh0;
}

void SignalSelectChannel::currentChannel1Index(int iCh1)
{
   mpPlotData->piChannel2[mChannelNumber] = iCh1;
}

void SignalSelectChannel::visibleSignal(bool bAct)
{
   mpPlotCurveTime[mChannelNumber]->setVisible(bAct);
   mpPlotCurveFreq[mChannelNumber]->setVisible(bAct);
   emit whichBox(mChannelNumber, bAct);
}

void SignalSelectChannel::selectOffsetValue(double dValue)
{
   mpAudioIO->setOffsetValueChannelSpec(dValue, mChannelNumber);
}

void SignalSelectChannel::selectFreqValue(double dValue)
{
   mpAudioIO->setFreqValueChannelSpec(dValue, mChannelNumber, mps);
}

void SignalSelectChannel::updateLabel(int num, QString s)
{
	emit sendLabel(num, s);
}

void SignalSelectChannel::updateScale(double min, double max, int num)
{
	emit sendScales(min, max, num);
}
