/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_PLOTTER_MODULE_H
#define RTF_HSAB_PLOTTER_MODULE_H

#include <qwt_plot_curve.h>
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_autoscaler.h"
#include "rtf_hsab_plotter_module_ui_data.h"

class PlotterModule : public QObject
{ 
	Q_OBJECT
  public:    
	  PlotterModule(UIData *data);

	  virtual void setPlotterModule();
	  virtual void setPlotterSignal();
	  virtual void plot_time(int, int);
	  virtual void plot_freq(int, int);

	signals:
		void sendLabel(int, QString);
		void sendScales(double, double, int);

  protected: 

	   QComboBox        		*mpComboBoxSignalSelect;
	   QComboBox				*mpComboBoxModuleSelect;

	   QDoubleSpinBox			*mpSpinBoxFreq;
	   QDoubleSpinBox			*mpSpinBoxOffset;

	   int                       mpChannelNumber; 

	   QComboBox				*mpComboBoxChannelSelect0;
	   QComboBox				*mpComboBoxChannelSelect1;

	   QLabel				    *mpLabelChannel0;
	   QLabel				    *mpLabelChannel1;
	   QLabel					*mpLabelFreq;


	   tPlotData                *mpPlotData;
	   tSession                 *mps;

	   QVector<QwtPlotCurve*>   mpPlotCurveTime;
	   QVector<QwtPlotCurve*>   mpPlotCurveFreq;

	   ScaleEngine *mEngine;

	   UIData *data;
};  

#endif