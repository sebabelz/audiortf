/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_plotter_module.h"
#include <qwt_plot_curve.h>

PlotterModule::PlotterModule(UIData *data) : data(data)
{ 

}

void PlotterModule::setPlotterModule()
{ 
   /* do nothing */	        // will be overridden
}

void PlotterModule::setPlotterSignal()
{ 
   /* do nothing */	        // will be overridden
}


void PlotterModule::plot_time( int iWidget, int iSamp)
{ 
   iWidget = 0; iSamp = 0;  // will be overridden
}

void PlotterModule::plot_freq( int iWidget, int iSbb)
{ 
   iWidget = 0; iSbb = 0;  // will be overridden
}
