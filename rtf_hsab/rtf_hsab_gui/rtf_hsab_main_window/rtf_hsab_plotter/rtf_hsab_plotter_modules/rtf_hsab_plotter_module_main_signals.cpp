/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_plotter_module_main_signals.h"
#include <qwt_plot.h>
#include <qwt_plot_curve.h>

Main_signals::Main_signals(UIData *data) : PlotterModule(data)
{
   iModuleId = (int) 0;
}
void Main_signals::setPlotterModule()
{ 
	int i;

	data->pComboBoxSignalSelect->clear();
	data->pComboBoxSignalSelect->addItem(("Microphone-Input"));
	data->pComboBoxSignalSelect->addItem(("Phone-Input"));
	data->pComboBoxSignalSelect->addItem(("Phone-Output"));
	data->pComboBoxSignalSelect->addItem(("Loudspeaker-Output"));
	data->pComboBoxSignalSelect->addItem(("Mic-In Spect. Power [dB]"));
	data->pComboBoxSignalSelect->addItem(("Phone-In Spect. Power [dB]"));
	data->pComboBoxSignalSelect->addItem(("Phone-Out Spect. Power [dB]"));
	data->pComboBoxSignalSelect->addItem(("Loudspeaker-Out Spect. Power [dB]"));
	data->pComboBoxSignalSelect->addItem(("Mic-In RMS Level [dB]"));
	data->pComboBoxSignalSelect->addItem(("Phone-In RMS Level [dB]"));
	data->pComboBoxSignalSelect->addItem(("Phone-Out RMS Level [dB]"));
	data->pComboBoxSignalSelect->addItem(("Loudspeaker-Out RMS Level [dB]"));


	data->pSpinBoxOffset->setSingleStep((double) 0.5);
	data->pSpinBoxOffset->setMinimum((double) -10.0);
	data->pSpinBoxOffset->setMaximum((double) 10.0);
    data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
    data->pLabelChannel0->setText(("Mic"));
    data->pLabelChannel0->setEnabled(true);
    data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
    data->pLabelChannel1->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT (0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT (0);
    data->pComboBoxChannelSelect0->setEnabled(true);
    data->pComboBoxChannelSelect1->setEnabled(false);
#ifdef BF_SYSTEM
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
#else
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
#endif // BF_SYSTEM
    {
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
    }
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdTimeAxis, data->pPlotData->pppdSignalMemory[iModuleId][data->pChannelNumber], data->pPlotData->iSignalMemoryLength);

	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[iModuleId][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);
	
	data->pPlotData->piModuleIndex[data->pChannelNumber] = data->pComboBoxModuleSelect->currentIndex();
    data->pPlotData->piSignalIndex[data->pChannelNumber] = (int) 0;

	emit sendLabel(data->pChannelNumber, "Amplitude");
	emit sendScales(-1.0, 1.0, data->pChannelNumber);
}

void Main_signals::setPlotterSignal()
{
  /* do nothing */	        // will be overridden
} 

void Main_signals::plot_time( int iWidget, int iSamp)
{
   iWidget = 0; iSamp = 0;  // will be overridden
}

void Main_signals::plot_freq( int iWidget, int iSbb)
{
   iWidget = 0; iSbb = 0;  // will be overridden
}


MicIn::MicIn(UIData *data) : Main_signals( data)
{

}

void MicIn::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
#ifdef BF_SYSTEM
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
#else
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
#endif

	
	{
       data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT (0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));

	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));

	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdTimeAxis,
															data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], 
															data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 0.5);
	data->pSpinBoxOffset->setMinimum((double) -10.0);
	data->pSpinBoxOffset->setMaximum((double) 10.0);

	emit sendLabel(data->pChannelNumber, "Amplitude");
	emit sendScales(-1.0, 1.0, data->pChannelNumber);
}

void MicIn::plot_time( int iWidget, int iSamp)
{
#ifdef BF_SYSTEM
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppfMicIn[data->pPlotData->piChannel1[iWidget]][iSamp] + data->pPlotData->pdOffset[iWidget];
#else
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE)data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppfMicInFrontSeat[data->pPlotData->piChannel1[iWidget]][iSamp] + data->pPlotData->pdOffset[iWidget];
#endif
	
	data->engine[iWidget].observeSignal(data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer]);
}

void MicIn::plot_freq( int iWidget, int iSbb)
{
#ifdef BF_SYSTEM
	psd = pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
#else
	psd = pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicInFrontSeat[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicInFrontSeat[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
#endif
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE) (psd ) + (T_DOUBLE) SYS_EPS );
}



PhoneIn::PhoneIn(UIData *data) : Main_signals(data)
{
	
}

void PhoneIn::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)0.5);
	data->pSpinBoxOffset->setMinimum((double)-10.0);
	data->pSpinBoxOffset->setMaximum((double)10.0);

	emit sendLabel(data->pChannelNumber, "Amplitude");
#endif // BF_SYSTEM
}

void PhoneIn::plot_time(  int iWidget, int iSamp)
{
}

void PhoneIn::plot_freq(  int iWidget, int iSbb)
{
}


PhoneOut::PhoneOut(UIData *data) : Main_signals(data)
{
	
}

void PhoneOut::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)0.5);
	data->pSpinBoxOffset->setMinimum((double)-10.0);
	data->pSpinBoxOffset->setMaximum((double)10.0);

	emit sendLabel(data->pChannelNumber, "Amplitude");
#endif // BF_SYSTEM

}


void PhoneOut::plot_time( int iWidget, int iSamp )
{
#ifdef BF_SYSTEM
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE)data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppfRemoteOut[data->pPlotData->piChannel1[iWidget]][iSamp] + data->pPlotData->pdOffset[iWidget];
	data->engine[iWidget].observeSignal(data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer]);
#endif // BF_SYSTEM

}

void PhoneOut::plot_freq( int iWidget, int iSbb )
{
#ifdef BF_SYSTEM
	psd = pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxSpecAfterBf[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxSpecAfterBf[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE)10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
#endif // BF_SYSTEM

}


LspOut::LspOut(UIData *data) : Main_signals(data)
{
	
}
void LspOut::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Lsp"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)0.5);
	data->pSpinBoxOffset->setMinimum((double)-10.0);
	data->pSpinBoxOffset->setMaximum((double)10.0);

	emit sendLabel(data->pChannelNumber, "Amplitude");
#endif // BF_SYSTEM

}

void LspOut::plot_time( int iWidget, int iSamp)
{
}

void LspOut::plot_freq( int iWidget, int iSbb)
{
}

// --------------------------psd 

MicInPsd::MicInPsd(UIData *data) : Main_signals(data)
																														{

}

void MicInPsd::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-80.0, 40.0, data->pChannelNumber);
#endif // BF_SYSTEM
}

void MicInPsd::plot_time( int iWidget, int iSamp)
{
#ifdef BF_SYSTEM
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE)10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
	data->engine[iWidget].observeSignal(data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer]);
#endif // BF_SYSTEM

}

void MicInPsd::plot_freq( int iWidget, int iSbb)
{
#ifdef BF_SYSTEM
	psd = pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppCmplxHsabSpecMicIn[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE)10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
#endif // BF_SYSTEM

}


PhoneInPsd::PhoneInPsd(UIData *data) : Main_signals(data)
{
	
}

void PhoneInPsd::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
#endif // BF_SYSTEM

}

void PhoneInPsd::plot_time(  int iWidget, int iSamp)
{
}

void PhoneInPsd::plot_freq(  int iWidget, int iSbb)
{
}


PhoneOutPsd::PhoneOutPsd(UIData *data) : Main_signals(data)
{
	
}

void PhoneOutPsd::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
#endif // BF_SYSTEM

}


void PhoneOutPsd::plot_time( int iWidget, int iSamp)
{
}

void PhoneOutPsd::plot_freq( int iWidget, int iSbb )
{
}



LspOutPsd::LspOutPsd(UIData *data) : Main_signals(data)
{
	
}

void LspOutPsd::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Lsp"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
#endif // BF_SYSTEM

}


void LspOutPsd::plot_time( int iWidget, int iSamp)
{
   iSamp = 0;
}

void LspOutPsd::plot_freq( int iWidget, int iSbb)
{
}



//----------------------------- rms

MicInRms::MicInRms(UIData *data) : Main_signals(data)
{

}

void MicInRms::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
#ifdef BF_SYSTEM
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
#else
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
{
#endif

       data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT (0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdTimeAxis, 
															data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], 
															data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double) -50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-80.0, 0, data->pChannelNumber);
} 
void MicInRms::plot_time( int iWidget, int iSamp)
{
   iSamp = 0;
   data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10(data->ps->pfMicInRms[data->pPlotData->piChannel1[iWidget]] + (T_DOUBLE) SYS_EPS ) + data->pPlotData->pdOffset[iWidget];
   data->engine[iWidget].observeSignal(data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer]);
}

void MicInRms::plot_freq( int iWidget, int iSbb)
{
   data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 0;
}



PhoneInRms::PhoneInRms(UIData *data) : Main_signals(data)
{
	
}

void PhoneInRms::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
#ifdef BF_SYSTEM
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
#else
	for ( i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
	{
#endif
       data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT (0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
														data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], 
															data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*) data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double) -50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
}


void PhoneInRms::plot_time(  int iWidget, int iSamp)
{
}

void PhoneInRms::plot_freq(  int iWidget, int iSbb)
{
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 0;
}


PhoneOutRms::PhoneOutRms(UIData *data) : Main_signals(data)
{
	
}

void PhoneOutRms::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Ch"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, -60, data->pChannelNumber);
#endif // BF_SYSTEM

}


void PhoneOutRms::plot_time( int iWidget, int iSamp )
{
#ifdef BF_SYSTEM
	iSamp = 0;
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE)20.0 * log10(data->ps->pfPhoneOutRms[data->pPlotData->piChannel1[iWidget]] + (T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
	data->engine[iWidget].observeSignal(data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer]);
#endif // BF_SYSTEM

}

void PhoneOutRms::plot_freq( int iWidget, int iSbb )
{
#ifdef BF_SYSTEM
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE)0;
#endif // BF_SYSTEM
}



LspOutRms::LspOutRms(UIData *data) : Main_signals(data)
{
	
}
void LspOutRms::setPlotterSignal()
{
#ifdef BF_SYSTEM
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	data->pComboBoxChannelSelect0->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setText(QCoreApplication::tr("Lsp"));
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double)50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
#endif // BF_SYSTEM

}


void LspOutRms::plot_time( int iWidget, int iSamp)
{
}

void LspOutRms::plot_freq( int iWidget, int iSbb)
{
   data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 0;
}


