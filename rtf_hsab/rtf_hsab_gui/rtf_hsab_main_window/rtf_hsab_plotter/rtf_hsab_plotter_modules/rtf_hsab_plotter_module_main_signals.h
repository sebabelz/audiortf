/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_PLOTTER_MODULE_MAIN_SIGNALS_H
#define RTF_HSAB_PLOTTER_MODULE_MAIN_SIGNALS_H   

#include "rtf_hsab_plotter_module.h"

class Main_signals: public PlotterModule
{ 
  public:
	 Main_signals(UIData *data);
  protected:
     void setPlotterModule();
     void setPlotterSignal();
	 void plot_time(int, int);
	 void plot_freq(int, int);

	 int iModuleId;
	 double psd;
};  

class MicIn: public Main_signals
{ 
  public:

	  MicIn (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 


class PhoneIn: public Main_signals
{ 
  public:

	  PhoneIn (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class PhoneOut: public Main_signals
{ 
  public:

	  PhoneOut (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class LspOut: public Main_signals
{ 
  public:

	  LspOut  (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 


class MicInPsd: public Main_signals
{ 
  public:

	  MicInPsd (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class PhoneInPsd: public Main_signals
{ 
  public:

	  PhoneInPsd (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class PhoneOutPsd: public Main_signals
{ 
  public:

	  PhoneOutPsd (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class LspOutPsd: public Main_signals
{ 
  public:

	  LspOutPsd  (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 


class MicInRms: public Main_signals
{ 
  public:

	  MicInRms (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class PhoneInRms: public Main_signals
{ 
  public:

	  PhoneInRms (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class PhoneOutRms: public Main_signals
{ 
  public:

	  PhoneOutRms (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
}; 

class LspOutRms: public Main_signals
{ 
  public:

	  LspOutRms  (UIData *data);
  protected:
	  void setPlotterSignal();
	  void plot_time(int, int);
	  void plot_freq(int, int);
};

#endif