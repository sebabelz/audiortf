#ifndef RTF_HSAB_PLOT_UI_DATA
#define RTF_HSAB_PLOT_UI_DATA

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMetaType>
#include <QPushButton>
#include <QPointer>
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_plot_limitbox.h"
#include <qwt_plot_curve.h>
#include <QComboBox>
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_autoscaler.h"


struct UIData
{
public:
	QDoubleSpinBox *pSpinBoxFreq; 
	QDoubleSpinBox *pSpinBoxOffset;
	int pChannelNumber;
	tPlotData *pPlotData;
	QVector<QwtPlotCurve*> pPlotCurveTime;
	QVector<QwtPlotCurve*> pPlotCurveFreq;
	QComboBox *pComboBoxSignalSelect;
	QComboBox *pComboBoxModuleSelect;
	QComboBox *pComboBoxChannelSelect0;
	QComboBox *pComboBoxChannelSelect1;
	QLabel	*pLabelChannel0;
	QLabel  *pLabelChannel1;
	QLabel	*pLabelFreq;
	tSession *ps;
	ScaleEngine *engine;
};

#endif // RTF_HSAB_PLOT_UI_DATA*/