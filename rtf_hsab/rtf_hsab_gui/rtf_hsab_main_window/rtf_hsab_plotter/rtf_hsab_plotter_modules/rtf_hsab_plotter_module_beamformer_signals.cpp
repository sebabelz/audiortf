/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/


/*
Informatik IV Projekt 2018
programmed by s160921, Christian Wagner
*/

#include "rtf_hsab_plotter_module_beamformer_signals.h"
#include <qwt_plot.h>


/*
Klasse f�r Anzeige Signale/Parameter des Beamformer (in Anlehnung an "Main_signals"-Class)
*/

Beamformer_signals::Beamformer_signals(UIData *data) :	PlotterModule(data)
{
	//ModuleId 0 = Main_signals
	//ModuleId 1 = Beamformer_signals
	iModuleId = 1;
}


void Beamformer_signals::setPlotterModule()
{
	int i;

	//Combobox "Select signal" mit Items f�llen
	data->pComboBoxSignalSelect->clear();
	data->pComboBoxSignalSelect->addItem(("AB-CmplxDelayCompOutSpec [dB]"));
	data->pComboBoxSignalSelect->addItem(("AB-CmplxFbfOutSpec [dB]"));
	data->pComboBoxSignalSelect->addItem(("AB-CmplxBmOutSpec [dB]"));
	data->pComboBoxSignalSelect->addItem(("AB-CmplxIcOutSpec [dB]"));
	data->pComboBoxSignalSelect->addItem(("AB-SteerAngle [degree]"));
	data->pComboBoxSignalSelect->addItem(("AB-CmplxFilterCoeffIc [dB]"));
	data->pComboBoxSignalSelect->addItem(("SP-SmPowRatio [dB]"));
	data->pComboBoxSignalSelect->addItem(("SP-OptWienFilt [dB]"));
	data->pComboBoxSignalSelect->addItem(("SP-PowCorrPf [dB]"));
	data->pComboBoxSignalSelect->addItem(("SP-ShortTimePowAbf [dB]"));
	data->pComboBoxSignalSelect->addItem(("SS-MagRatio [dB]"));
	data->pComboBoxSignalSelect->addItem(("SS-ShortTimePowFbf [dB]"));
	data->pComboBoxSignalSelect->addItem(("SS-ShortTimePowBm [dB]"));
	data->pComboBoxSignalSelect->addItem(("SS-AverPowBM [dB]"));
	data->pComboBoxSignalSelect->addItem(("SS-PowCorrBm [dB]"));
	data->pComboBoxSignalSelect->addItem(("ABM-CmplxAbmFiltOutSpec [dB]"));
	data->pComboBoxSignalSelect->addItem(("ABM-CmplxFilterCoeffAbm [dB]"));
	data->pComboBoxSignalSelect->addItem(("ABM-IntTreshPowRatioAbm [dB]"));


	//Spinbox Offset einstellen
	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	//Spinbox Freq einstellen
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);	
	
	
	//Combobox Mic einstellen
	data->pLabelChannel0->setText(("Mic"));
	data->pLabelChannel0->setEnabled(true);
	data->pComboBoxChannelSelect0->clear();
	//Mit Items f�llen (anhand Anzahl hinterlegter Microphone im alg. Kern)
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);

	//Combobox Ch einstellen
	data->pLabelChannel1->setEnabled(false);
	data->pComboBoxChannelSelect1->clear();
	data->pComboBoxChannelSelect1->setEnabled(false);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);

	//Einstellen Skalen f�r Time/Freq Plot
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis, data->pPlotData->pppdSignalMemory[iModuleId][data->pChannelNumber], data->pPlotData->iSignalMemoryLength);

	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[iModuleId][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	//�bergabe ModuleIndex und SignalIndex an Plotter
	data->pPlotData->piModuleIndex[data->pChannelNumber] = data->pComboBoxModuleSelect->currentIndex();
	data->pPlotData->piSignalIndex[data->pChannelNumber] = (int)0;
	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}

void Beamformer_signals::setPlotterSignal()
{
	/* do nothing */	        // will be overridden
}

void Beamformer_signals::plot_time(int iWidget, int iSamp)
{
	iWidget = 0; iSamp = 0;  // will be overridden
}

void Beamformer_signals::plot_freq(int iWidget, int iSbb)
{
	iWidget = 0; iSbb = 0;  // will be overridden
}


//ADAPTIVE BEAMFORMER


DCOutSpec::DCOutSpec(UIData *data) : Beamformer_signals(data)
{

}


//Alle anderen anzeigbaren Signale/Parameter haben die gleiche Struktur.
//Als Beispiel ist nur dieses Signal kommentiert
void DCOutSpec::setPlotterSignal()
{
	int i;	

	//Spinbox Freq einstellen	
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);	


	//Combobox Mic einstellen
	data->pLabelChannel0->setEnabled(true);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pComboBoxChannelSelect0->clear();
	//Mit Items f�llen (anhand Anzahl hinterlegter Microphone im alg. Kern)
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);


	//Combobox Ch einstellen
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect1->clear();
	data->pComboBoxChannelSelect1->setEnabled(false);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);


	//Einstellen Skalen f�r Time/Freq Plot
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);


	//Spinbox Offset einstellen
	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	//�bergabe ModuleIndex an Plotter
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}

//Plotfunktion f�r Zeitbereich
void DCOutSpec::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	//Berechnung Betrag aus komplexer Variable ppCmplxDelayCompOutSpec
	//Einstellbare Parameter durch GUI: Microphone, Frequenz, Offset
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxDelayCompOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxDelayCompOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	//Umrechnung f�r logarithmische Skala (dB) und �bergabe an Plotter
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

//Plotfunktion f�r Frequenzbereich
void DCOutSpec::plot_freq(int iWidget, int iSbb)
{	
	//Berechnung Betrag aus komplexer Variable ppCmplxDelayCompOutSpec
	//Einstellbare Parameter durch GUI Parameter: Microphone
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxDelayCompOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxDelayCompOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}





FbfOutSpec::FbfOutSpec(UIData *data) :	Beamformer_signals(data)
{

}

void FbfOutSpec::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumSeats; i++)
	{
		data->pComboBoxChannelSelect1->addItem(QCoreApplication::tr("%1").arg(i+1));
	}

	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);	
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(true);
	data->pLabelChannel1->setText(QCoreApplication::tr("Seats"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(true);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void FbfOutSpec::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxFbfOutSpec[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxFbfOutSpec[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void FbfOutSpec::plot_freq(int iWidget, int iSbb)
{
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxFbfOutSpec[data->pPlotData->piChannel2[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxFbfOutSpec[data->pPlotData->piChannel2[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}



BmOutSpec::BmOutSpec(UIData *data) :	Beamformer_signals( data)
{

}

void BmOutSpec::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(true);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void BmOutSpec::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxBmOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxBmOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];

}

void BmOutSpec::plot_freq(int iWidget, int iSbb)
{

	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxBmOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.ppCmplxBmOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}



IcOutSpec::IcOutSpec(UIData *data) : Beamformer_signals(data)
{

}

void IcOutSpec::setPlotterSignal()
{	
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();	
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[mpChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void IcOutSpec::plot_time(int iWidget, int iSamp)
{	
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.pCmplxIcOutSpec[data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.pCmplxIcOutSpec[data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];

}

void IcOutSpec::plot_freq(int iWidget, int iSbb)
{
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.pCmplxIcOutSpec[iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.pCmplxIcOutSpec[iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}





SteerAngel::SteerAngel(UIData *data) : Beamformer_signals(data)
{

}

void SteerAngel::setPlotterSignal()
{	
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double)-100);
	data->pSpinBoxOffset->setMaximum((double) 100);

	emit sendLabel(data->pChannelNumber, "degree");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void SteerAngel::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = data->ps->HsabAlgCoreMain.HsabBfData.fSteerAngle;
}

void SteerAngel::plot_freq(int iWidget, int iSbb)
{
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 0;
}



FilterCoeffIc::FilterCoeffIc(UIData *data) : Beamformer_signals(data)
{

}

void FilterCoeffIc::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabBfData.iFilterLengthIc; i++)
	{
		data->pComboBoxChannelSelect1->addItem(QCoreApplication::tr("%1").arg(i+1));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(true);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(true);
	data->pLabelChannel1->setText(QCoreApplication::tr("Lenght"));
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pComboBoxChannelSelect1->setEnabled(true);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void FilterCoeffIc::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.pppCmplxFilterCoeffIc[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.pppCmplxFilterCoeffIc[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void FilterCoeffIc::plot_freq(int iWidget, int iSbb)
{
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.pppCmplxFilterCoeffIc[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.pppCmplxFilterCoeffIc[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}


//SPATIAL POSTFILTER


SmPowRatio::SmPowRatio(UIData *data) : Beamformer_signals(data)
{

}

void SmPowRatio::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void SmPowRatio::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfSmPowRatio[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void SmPowRatio::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfSmPowRatio[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);

}



OptWienFilter::OptWienFilter(UIData *data) : Beamformer_signals(data)
{

}

void OptWienFilter::setPlotterSignal()
{	
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void OptWienFilter::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfOptWienFilt[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void OptWienFilter::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfOptWienFilt[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}



PowCorrPf::PowCorrPf(UIData *data) : Beamformer_signals(data)
{

}

void PowCorrPf::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void PowCorrPf::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfPowCorrPf[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void PowCorrPf::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfPowCorrPf[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}

ShortTimePowAbf::ShortTimePowAbf(UIData *data) : Beamformer_signals(data)
{

}

void ShortTimePowAbf::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void ShortTimePowAbf::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfShortTimePowAbf[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void ShortTimePowAbf::plot_freq(int iWidget, int iSbb)
{

	psd = data->ps->HsabAlgCoreMain.HsabBfData.sBfPfData.pfShortTimePowAbf[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}

//STEP-SIZE CONTROL

MagRatio::MagRatio(UIData *data) : Beamformer_signals(data)
{

}

void MagRatio::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void MagRatio::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;	
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10(data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.fMagRatio + (T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];

}

void MagRatio::plot_freq(int iWidget, int iSbb)
{
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE)0;
}


ShortTimePowFbf::ShortTimePowFbf(UIData *data) : Beamformer_signals(data)
{

}

void ShortTimePowFbf::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void ShortTimePowFbf::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfShortTimePowFbf[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void ShortTimePowFbf::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfShortTimePowFbf[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}

ShortTimePowBm::ShortTimePowBm(UIData *data) : Beamformer_signals(data)
{

}

void ShortTimePowBm::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void ShortTimePowBm::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfShortTimePowBm[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void ShortTimePowBm::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfShortTimePowBm[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}

AverPowBm::AverPowBm(UIData *data) : Beamformer_signals(data)
{

}

void AverPowBm::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);


	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void AverPowBm::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfAverPowBm[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];

}

void AverPowBm::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfAverPowBm[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);

}

PowCorrBm::PowCorrBm(UIData *data) : Beamformer_signals(data)
{

}

void PowCorrBm::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void PowCorrBm::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfPowCorrBm[data->pPlotData->piFreq[iWidget]];
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];

}

void PowCorrBm::plot_freq(int iWidget, int iSbb)
{
	psd = data->ps->HsabAlgCoreMain.HsabBfData.sStepSizeCtrl.pfPowCorrBm[iSbb];
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 10.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);

}


//ADAPTIVE BLOCKING MATRIX

AbmFiltOutSpec::AbmFiltOutSpec(UIData *data) : Beamformer_signals(data)
{

}

void AbmFiltOutSpec::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(true);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double)5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void AbmFiltOutSpec::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.ppCmplxAbmFiltOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.ppCmplxAbmFiltOutSpec[data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void AbmFiltOutSpec::plot_freq(int iWidget, int iSbb)
{
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.ppCmplxAbmFiltOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.ppCmplxAbmFiltOutSpec[data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}



FilterCoeffAbm::FilterCoeffAbm(UIData *data) :	Beamformer_signals(data)
{

}

void FilterCoeffAbm::setPlotterSignal()
{
	int i;
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		data->pComboBoxChannelSelect0->addItem(QCoreApplication::tr("%1").arg(i));
	}
	for (i = 0; i < data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.iFilterLengthAbm; i++)
	{
		data->pComboBoxChannelSelect1->addItem(QCoreApplication::tr("%1").arg(i + 1));
	}
	data->pLabelFreq->setEnabled(true);
	data->pSpinBoxFreq->setEnabled(true);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(true);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(true);
	data->pLabelChannel1->setText(QCoreApplication::tr("Lenght"));
	data->pComboBoxChannelSelect0->setEnabled(true);
	data->pComboBoxChannelSelect1->setEnabled(true);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void FilterCoeffAbm::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.pppCmplxFilterCoeffAbm[mpPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.pppCmplxFilterCoeffAbm[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][data->pPlotData->piFreq[iWidget]].im, 2);
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void FilterCoeffAbm::plot_freq(int iWidget, int iSbb)
{
	psd = pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.pppCmplxFilterCoeffAbm[data->pPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][iSbb].re, 2) + pow(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.pppCmplxFilterCoeffAbm[mpPlotData->piChannel2[iWidget]][data->pPlotData->piChannel1[iWidget]][iSbb].im, 2);
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE) 20.0 * log10((T_DOUBLE)(psd)+(T_DOUBLE)SYS_EPS);
}



ThreshPowRatioAbm::ThreshPowRatioAbm(UIData *data) : Beamformer_signals(data)
{

}

void ThreshPowRatioAbm::setPlotterSignal()
{
	data->pPlotData->piModuleIndex[data->pChannelNumber] = iModuleId;
	data->pComboBoxChannelSelect0->clear();
	data->pComboBoxChannelSelect1->clear();
	data->pLabelFreq->setEnabled(false);
	data->pSpinBoxFreq->setEnabled(false);
	data->pPlotData->piChannel1[data->pChannelNumber] = T_INT(0);
	data->pPlotData->piChannel2[data->pChannelNumber] = T_INT(0);
	data->pLabelChannel0->setEnabled(false);
	data->pLabelChannel0->setText(QCoreApplication::tr("Mic"));
	data->pLabelChannel1->setEnabled(false);
	data->pLabelChannel1->setText(QCoreApplication::tr("Ch"));
	data->pComboBoxChannelSelect0->setEnabled(false);
	data->pComboBoxChannelSelect1->setEnabled(false);
	if (data->pChannelNumber < 2)
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yLeft, data->pChannelNumber));
	else
		data->pPlotCurveTime[data->pChannelNumber]->setYAxis(QwtAxisId(QwtPlot::yRight, data->pChannelNumber - 2));
	data->pPlotCurveTime[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdTimeAxis,
		data->pPlotData->pppdSignalMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber],
		data->pPlotData->iSignalMemoryLength);
	data->pPlotCurveFreq[data->pChannelNumber]->setYAxis(QwtPlot::yLeft);
	data->pPlotCurveFreq[data->pChannelNumber]->setRawSamples((const T_DOUBLE*)data->pPlotData->pdFreqAxis, data->pPlotData->pppdFreqMemory[data->pComboBoxModuleSelect->currentIndex()][data->pChannelNumber], data->pPlotData->iFreqMemoryLength);

	data->pSpinBoxOffset->setSingleStep((double) 5);
	data->pSpinBoxOffset->setMinimum((double)-50.0);
	data->pSpinBoxOffset->setMaximum((double) 50.0);

	emit sendLabel(data->pChannelNumber, "[dB]");
	emit sendScales(-140, 80, data->pChannelNumber);
}
void ThreshPowRatioAbm::plot_time(int iWidget, int iSamp)
{
	iSamp = 0;
	data->pPlotData->pppdSignalMemory[iModuleId][iWidget][data->pPlotData->iWritePointer] = (T_DOUBLE) 10.0 * log10(data->ps->HsabAlgCoreMain.HsabBfData.sBfAbmData.fIntThreshPowRatioAbm + (T_DOUBLE)SYS_EPS) + data->pPlotData->pdOffset[iWidget];
}

void ThreshPowRatioAbm::plot_freq(int iWidget, int iSbb)
{
	data->pPlotData->pppdFreqMemory[iModuleId][iWidget][iSbb] = (T_DOUBLE)0;
}
