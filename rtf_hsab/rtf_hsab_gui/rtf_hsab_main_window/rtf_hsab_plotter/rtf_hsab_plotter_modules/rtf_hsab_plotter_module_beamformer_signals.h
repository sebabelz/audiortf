/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_PLOTTER_MODULE_Beamformer_SIGNALS_H
#define RTF_HSAB_PLOTTER_MODULE_Beamformer_SIGNALS_H   

#include "rtf_hsab_plotter_module.h"

class Beamformer_signals : public PlotterModule
{
public:
	Beamformer_signals(UIData *data);
protected:
	void setPlotterModule();
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);

	int iModuleId;
	double psd;
};

//ADAPTIVE BEAMFORMER

class DCOutSpec : public Beamformer_signals
{
public:

	DCOutSpec(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class FbfOutSpec : public Beamformer_signals
{
public:

	FbfOutSpec(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class BmOutSpec : public Beamformer_signals
{
public:

	BmOutSpec(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class IcOutSpec : public Beamformer_signals
{
public:

	IcOutSpec(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};



class SteerAngel : public Beamformer_signals
{
public:

	SteerAngel(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class FilterCoeffIc : public Beamformer_signals
{
public:

	FilterCoeffIc(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

//SPATIAL POSTFILTER

class SmPowRatio : public Beamformer_signals
{
public:

	SmPowRatio(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class OptWienFilter : public Beamformer_signals
{
public:

	OptWienFilter(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class PowCorrPf : public Beamformer_signals
{
public:

	PowCorrPf(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class ShortTimePowAbf : public Beamformer_signals
{
public:

	ShortTimePowAbf(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

//STEP-SIZE CONTROL

class MagRatio : public Beamformer_signals
{
public:

	MagRatio(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class ShortTimePowFbf : public Beamformer_signals
{
public:

	ShortTimePowFbf(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class ShortTimePowBm : public Beamformer_signals
{
public:

	ShortTimePowBm(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class AverPowBm : public Beamformer_signals
{
public:

	AverPowBm(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class PowCorrBm : public Beamformer_signals
{
public:

	PowCorrBm(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

//ADAPTIVE BLOCKING MATRIX

class AbmFiltOutSpec : public Beamformer_signals
{
public:

	AbmFiltOutSpec(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class FilterCoeffAbm : public Beamformer_signals
{
public:

	FilterCoeffAbm(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

class ThreshPowRatioAbm : public Beamformer_signals
{
public:

	ThreshPowRatioAbm(UIData *data);
protected:
	void setPlotterSignal();
	void plot_time(int, int);
	void plot_freq(int, int);
};

#endif