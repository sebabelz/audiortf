/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_PLOTS_VISUAL_H
#define RTF_HSAB_PLOTS_VISUAL_H

#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_plots_data.h"
#include <qwt_plot_curve.h>
#include "rtf_hsab_time_plot.h"


class SignalTimeDomainPlot;
class SignalFreqDomainPlot;
class SignalSpecDomainPlot;
class SignalSelection;
class AudioIO;
struct tSession;

class TabRealTimePlots : public QWidget
{
   Q_OBJECT

   public:
      TabRealTimePlots( tSession               *pSession, AudioIO *pAudioIO,
                        QWidget                *parent = 0 );
      ~TabRealTimePlots();
      tPlotData* getRealTimeDataPointer();

   protected:
      
   signals:

   public slots:
	  void viewParamsLayout(bool);
	  void viewTimePlot(bool);
	  void viewFreqPlot(bool);
	  void viewSpecPlot(bool);

   private:
      tPlotData      *mpPlotData;

      QHBoxLayout            *mLayoutVisPlotMain;  
      QSplitter              *mSplitterVisPlotMain;  
      QGraphicsView          *mGraphicsViewVisTime;
      QGraphicsView          *mGraphicsViewVisFreq;
	  QGraphicsView          *mGraphicsViewVisSpec;
	  QGraphicsView          *mGraphicsViewParam;

      SignalTimeDomainPlot   *mSignalTimeDomainPlot;
      SignalFreqDomainPlot   *mSignalFreqDomainPlot;
	  SignalSpecDomainPlot   *mSignalSpecDomainPlot;
	  SignalSelection	     *mSignalSelection;

};

#endif
