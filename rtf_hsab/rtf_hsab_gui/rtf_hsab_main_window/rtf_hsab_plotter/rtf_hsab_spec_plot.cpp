/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

/*
Studienarbeit "Implementierung Spektrogramm", 2019
programmed by s160921, Christian Wagner
*/


#include <stdlib.h>
#include <qwt_color_map.h>
#include <qwt_plot_spectrogram.h>
#include <qwt_matrix_raster_data.h>
#include "rtf_hsab_spec_plot.h"
#include <qwt_plot_renderer.h>


AxesDialogSpec::AxesDialogSpec(QWidget *parent, tPlotData *pPlotData, QwtPlot *plot, tSession *ps)
	: QDialog(parent)
	, mpPlotData(pPlotData)
	, mpPlot(plot)
	, mps(ps)
{
	/*-----------------------------------------------------------------------------------------*
	* Layouts
	*-----------------------------------------------------------------------------------------*/
	MainLayout = new QVBoxLayout(this);
	TopLayout = new QHBoxLayout(this);
	setWindowTitle(tr("Settings Spectrogram"));
	setFixedSize(320, 350);
	/*-----------------------------------------------------------------------------------------*
	* Left Axis
	*-----------------------------------------------------------------------------------------*/
	mgridLayoutZ = new QGridLayout(this);
	mGroupLayout = new QGroupBox();		
	
	mInterpolationLabel = new QLabel(tr(" Interpolation:"));
	mInterpolationLabel->setAlignment(Qt::AlignRight);
	//Interpolation 
	//0 = NearestNeighbour | 1 = Bilinear
	mComboBoxInterpolationSelect = new QComboBox(this);
	mComboBoxInterpolationSelect->addItem(tr("Nearest Neighbour"));
	mComboBoxInterpolationSelect->addItem(tr("Bilinear"));
	mComboBoxInterpolationSelect->setFixedSize(165, 22);
	connect(mComboBoxInterpolationSelect, SIGNAL(activated(int)), this, SLOT(setInterpolation(int)));
	connect(mComboBoxInterpolationSelect, SIGNAL(activated(int)), this, SIGNAL(setInterpolation()));
	
	mUpperLabelZ = new QLabel(tr("Z Upper Limit [dB]:"));
	mUpperLabelZ->setAlignment(Qt::AlignRight);
	mUpperLimitZ = new QSpinBox(this);
	mUpperLimitZ->setFixedSize(60, 22);
	mUpperLimitZ->setSingleStep((int) 10);
	mUpperLimitZ->setMaximum((int)500);
	mUpperLimitZ->setMinimum((int)-500);
	mUpperLimitZ->setAlignment(Qt::AlignRight);
	mUpperLimitZ->setValue(mpPlotData->zLimitSpec[0]);
	connect(mUpperLimitZ, SIGNAL(valueChanged(int)), this, SLOT(setZUpperLimit(int)));
	connect(mUpperLimitZ, SIGNAL(valueChanged(int)), this, SIGNAL(setZScale()));
	QPushButton *PushButtonUpperLimitZDefault = new QPushButton(tr("Default"));
	PushButtonUpperLimitZDefault->setFixedSize(60, 20);
	connect(PushButtonUpperLimitZDefault, SIGNAL(clicked()), this, SIGNAL(setZUpperLimitDefault()));

	mLowerLabelZ = new QLabel(tr("Z Lower Limit [dB]:"));
	mLowerLabelZ->setAlignment(Qt::AlignRight);
	mLowerLimitZ = new QSpinBox(this);
	mLowerLimitZ->setFixedSize(60, 22);
	mLowerLimitZ->setSingleStep((int) 10);
	mLowerLimitZ->setMaximum((int)500);
	mLowerLimitZ->setMinimum((int)-500);
	mLowerLimitZ->setAlignment(Qt::AlignRight);
	mLowerLimitZ->setValue(mpPlotData->zLimitSpec[1]);
	connect(mLowerLimitZ, SIGNAL(valueChanged(int)), this, SLOT(setZLowerLimit(int)));
	connect(mLowerLimitZ, SIGNAL(valueChanged(int)), this, SIGNAL(setZScale()));
	QPushButton *PushButtonLowerLimitZDefault = new QPushButton(tr("Default"));
	PushButtonLowerLimitZDefault->setFixedSize(60, 20);
	connect(PushButtonLowerLimitZDefault, SIGNAL(clicked()), this, SIGNAL(setZLowerLimitDefault()));

	mUpperLabelY = new QLabel(tr("Y Upper Limit [Hz]:"));
	mUpperLabelY->setAlignment(Qt::AlignRight);
	mUpperLimitY = new QSpinBox(this);
	mUpperLimitY->setFixedSize(60, 22);
	mUpperLimitY->setSingleStep((int) 100);
	mUpperLimitY->setMaximum((int)8000);
	mUpperLimitY->setMinimum((int)0);
	mUpperLimitY->setAlignment(Qt::AlignRight);
	mUpperLimitY->setValue(mpPlotData->yLeftLimitSpec[0]);
	connect(mUpperLimitY, SIGNAL(valueChanged(int)), this, SLOT(setYUpperLimit(int)));
	connect(mUpperLimitY, SIGNAL(valueChanged(int)), this, SIGNAL(setYScale()));
	QPushButton *PushButtonUpperLimitYDefault = new QPushButton(tr("Default"));
	PushButtonUpperLimitYDefault->setFixedSize(60, 20);
	connect(PushButtonUpperLimitYDefault, SIGNAL(clicked()), this, SIGNAL(setYUpperLimitDefault()));

	mLowerLabelY = new QLabel(tr("Y Lower Limit [Hz]:"));
	mLowerLabelY->setAlignment(Qt::AlignRight);
	mLowerLimitY = new QSpinBox(this);
	mLowerLimitY->setFixedSize(60, 22);
	mLowerLimitY->setSingleStep((int) 100);
	mLowerLimitY->setMaximum((int)8000);
	mLowerLimitY->setMinimum((int)0);
	mLowerLimitY->setAlignment(Qt::AlignRight);
	mLowerLimitY->setValue(mpPlotData->yLeftLimitSpec[1]);
	connect(mLowerLimitY, SIGNAL(valueChanged(int)), this, SLOT(setYLowerLimit(int)));
	connect(mLowerLimitY, SIGNAL(valueChanged(int)), this, SIGNAL(setYScale()));
	QPushButton *PushButtonLowerLimitYDefault = new QPushButton(tr("Default"));
	PushButtonLowerLimitYDefault->setFixedSize(60, 20);
	connect(PushButtonLowerLimitYDefault, SIGNAL(clicked()), this, SIGNAL(setYLowerLimitDefault()));


	mGroupLayout->setLayout(mgridLayoutZ);

	
	mSignalLabel = new QLabel(tr("Signal:"));
	mSignalLabel->setAlignment(Qt::AlignRight);
	mComboBoxSignalSelect = new QComboBox(this);
	
	for (int i = 0; i < mpPlotData->iNumberOfWidgets; i++) 
	{
		mComboBoxSignalSelect->addItem(QString::number(i));
	}
	mComboBoxSignalSelect->setFixedSize(165, 22);
	connect(mComboBoxSignalSelect, SIGNAL(activated(int)), this, SLOT(setSignal(int)));
	connect(mComboBoxSignalSelect, SIGNAL(activated(int)), this, SIGNAL(buildTitle()));
	

	mSecondsLabel = new QLabel(tr("Time to display [s]:"));
	mSecondsLabel->setAlignment(Qt::AlignRight);
	mSecondsSpinBox = new QSpinBox(this);
	mSecondsSpinBox->setFixedSize(60, 22);
	mSecondsSpinBox->setAlignment(Qt::AlignRight);
	mSecondsSpinBox->setSingleStep((T_INT) 1);
	mSecondsSpinBox->setMaximum((T_INT) 600);
	mSecondsSpinBox->setMinimum((T_INT) 1);
	mSecondsSpinBox->setValue((T_INT) mpPlotData->dSignalMemoryInSeconds);
	connect(mSecondsSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setSeconds(int)));
	connect(mSecondsSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(resetPlot()));
	QPushButton *PushButtonSecondsDefault = new QPushButton(tr("Default"));
	PushButtonSecondsDefault->setFixedSize(60, 20);
	connect(PushButtonSecondsDefault, SIGNAL(clicked()), this, SIGNAL(setSecondsDefault()));
	
	mSubSamplingFactorLabel = new QLabel(tr("Subsamplingfactor:"));
	mSubSamplingFactorLabel->setAlignment(Qt::AlignRight);
	mComboBoxSubSamplingFactorSelect = new QComboBox(this);
	mComboBoxSubSamplingFactorSelect->setFixedSize(165, 22);
	for (int i = 1; i <= 10; i++)
	{
#ifdef BF_SYSTEM
		mComboBoxSubSamplingFactorSelect->addItem("Factor " + QString::number(i) + " ( " + QString::number(qRound(10000 * (mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInternal / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal) / i)) + " Hz | " + QString::number(1000 * i * mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInternal / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal) + "ms )");
#else
		mComboBoxSubSamplingFactorSelect->addItem("Factor " + QString::number(i) + " ( " + QString::number(qRound(10000 * (mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInt / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt) / i)) + " Hz | " + QString::number(1000 * i * mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInt / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt) + "ms )");
#endif
		
	}
	connect(mComboBoxSubSamplingFactorSelect, SIGNAL(activated(int)), this, SLOT(setSubSamplingRate(int)));
	connect(mComboBoxSubSamplingFactorSelect, SIGNAL(activated(int)), this, SIGNAL(resetPlot()));

	mPlottingRateLabel = new QLabel(tr("Plottingrate [Hz]:"));
	mPlottingRateLabel->setAlignment(Qt::AlignRight);
	mPlottingRateSpinBox = new QSpinBox(this);
	mPlottingRateSpinBox->setFixedSize(60, 22);
	mPlottingRateSpinBox->setAlignment(Qt::AlignRight);
	mPlottingRateSpinBox->setSingleStep((T_INT)1);
	mPlottingRateSpinBox->setMaximum((T_INT)mpPlotData->iSignalSubSamplingRateSpec);
	mPlottingRateSpinBox->setMinimum((T_INT)1);
	mPlottingRateSpinBox->setValue((T_INT)mpPlotData->iSignalPlottingRateSpec);
	connect(mPlottingRateSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setPlottingRate(int)));
	connect(mPlottingRateSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(resetPlot()));
	QPushButton *PushButtonPlottingRateDefault = new QPushButton(tr("Default"));
	PushButtonPlottingRateDefault->setFixedSize(60, 20);
	connect(PushButtonPlottingRateDefault, SIGNAL(clicked()), this, SIGNAL(setPlottingRateDefault()));

	mgridLayoutZ->addWidget(mSignalLabel, 0, 0);
	mgridLayoutZ->addWidget(mComboBoxSignalSelect, 0, 1, 1, 2);
	mgridLayoutZ->addWidget(mInterpolationLabel, 1, 0);
	mgridLayoutZ->addWidget(mComboBoxInterpolationSelect, 1, 1, 1, 2);
	mgridLayoutZ->addWidget(mSubSamplingFactorLabel, 2, 0);
	mgridLayoutZ->addWidget(mComboBoxSubSamplingFactorSelect, 2, 1, 1, 2);
	mgridLayoutZ->addWidget(mPlottingRateLabel, 3, 0);
	mgridLayoutZ->addWidget(mPlottingRateSpinBox, 3, 1);
	mgridLayoutZ->addWidget(PushButtonPlottingRateDefault, 3, 2);
	mgridLayoutZ->addWidget(mSecondsLabel, 4, 0);
	mgridLayoutZ->addWidget(mSecondsSpinBox, 4, 1);
	mgridLayoutZ->addWidget(PushButtonSecondsDefault, 4, 2);		
	mgridLayoutZ->addWidget(mUpperLabelZ, 5, 0);
	mgridLayoutZ->addWidget(mUpperLimitZ, 5, 1);
	mgridLayoutZ->addWidget(PushButtonUpperLimitZDefault, 5, 2);
	mgridLayoutZ->addWidget(mLowerLabelZ, 6, 0);
	mgridLayoutZ->addWidget(mLowerLimitZ, 6, 1);
	mgridLayoutZ->addWidget(PushButtonLowerLimitZDefault, 6, 2);
	mgridLayoutZ->addWidget(mUpperLabelY, 7, 0);
	mgridLayoutZ->addWidget(mUpperLimitY, 7, 1);
	mgridLayoutZ->addWidget(PushButtonUpperLimitYDefault, 7, 2);
	mgridLayoutZ->addWidget(mLowerLabelY, 8, 0);
	mgridLayoutZ->addWidget(mLowerLimitY, 8, 1);
	mgridLayoutZ->addWidget(PushButtonLowerLimitYDefault, 8, 2);
	

	/*-----------------------------------------------------------------------------------------*
	* Export, Default All and Close buttons
	*-----------------------------------------------------------------------------------------*/
	QHBoxLayout *SubLayoutBottom = new QHBoxLayout(this);
	QPushButton *PushButtonExport = new QPushButton(tr("Export"));
	QPushButton *PushButtonClose = new QPushButton(tr("Close"));
	QPushButton *PushButtonDefaultAll = new QPushButton(tr("Default All"));	
	PushButtonDefaultAll->setFixedWidth(100);
	connect(PushButtonDefaultAll, SIGNAL(clicked()), this, SIGNAL(ApplyDefaultScales()));
	PushButtonClose->setFixedWidth(60);
	connect(PushButtonClose, SIGNAL(clicked()), this, SLOT(close()));
	PushButtonExport->setFixedWidth(60);
	connect(PushButtonExport, SIGNAL(clicked()), this, SLOT(exportDocument()));

	TopLayout->addWidget(mGroupLayout);
	SubLayoutBottom->addStretch();
	SubLayoutBottom->addWidget(PushButtonExport);
	SubLayoutBottom->addWidget(PushButtonDefaultAll);
	SubLayoutBottom->addWidget(PushButtonClose);	
	SubLayoutBottom->addStretch();
	MainLayout->addLayout(TopLayout);
	MainLayout->addLayout(SubLayoutBottom);
}
void AxesDialogSpec::exportDocument()
{
	QwtPlotRenderer renderer;
	renderer.exportTo(mpPlot, "Spectrogram.pdf");
}
void AxesDialogSpec::setZUpperLimit(int val)
{
	mpPlotData->zLimitSpec[0] = val;
	mUpperLimitZ->setValue(mpPlotData->zLimitSpec[0]);
}
void AxesDialogSpec::setZLowerLimit(int val)
{
	mpPlotData->zLimitSpec[1] = val;
	mLowerLimitZ->setValue(mpPlotData->zLimitSpec[1]);	
}
void AxesDialogSpec::setYUpperLimit(int val)
{
	mpPlotData->yLeftLimitSpec[0] = val;
	mUpperLimitY->setValue(mpPlotData->yLeftLimitSpec[0]);
}
void AxesDialogSpec::setYLowerLimit(int val)
{
	mpPlotData->yLeftLimitSpec[1] = val;
	mLowerLimitY->setValue(mpPlotData->yLeftLimitSpec[1]);
}
void AxesDialogSpec::setSignal(int val)
{
	mpPlotData->iSignalNumberForSpec = val;
}
void AxesDialogSpec::setInterpolation(int val)
{
	mpPlotData->iInterpolationModeSpec = val;
	mComboBoxInterpolationSelect->setCurrentIndex(val);
}
void AxesDialogSpec::setSeconds(int val)
{
	mpPlotData->dSignalMemoryInSeconds = (T_DOUBLE) val;
	mSecondsSpinBox->setValue(mpPlotData->dSignalMemoryInSeconds);

}
void AxesDialogSpec::setSubSamplingRate(int val)
{
#ifdef BF_SYSTEM
	mpPlotData->iSignalSubSamplingRateSpec = 10000 * (mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInternal / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal) / (val + 1);
#else
	mpPlotData->iSignalSubSamplingRateSpec = 10000 * (mps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftInt / mps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt) / (val + 1);
#endif // BF_SYSTEM
	mComboBoxSubSamplingFactorSelect->setCurrentIndex(val);
	mPlottingRateSpinBox->setMaximum((T_INT)mpPlotData->iSignalSubSamplingRateSpec);
}
void AxesDialogSpec::setPlottingRate(int val)
{
	mpPlotData->iSignalPlottingRateSpec = val;
	mPlottingRateSpinBox->setValue(mpPlotData->iSignalPlottingRateSpec);
}

class LinearColorMapRGB : public QwtLinearColorMap
{
public:
	LinearColorMapRGB() :
		QwtLinearColorMap(Qt::darkCyan, Qt::red, QwtColorMap::RGB)
	{
		addColorStop(0.3, Qt::cyan);
		addColorStop(0.5, Qt::green);
		addColorStop(0.7, Qt::yellow);
	}
};

SignalSpecDomainPlot::SignalSpecDomainPlot(tPlotData *pPlotData, tSession *ps, QWidget *parent)
	: QwtPlot(parent)
	, d_interval(0)
	, d_timerId(-1)
	, d_timerCount(-1)
	, iXSizeDataMatrix(mpPlotData->dSignalMemoryInSeconds * mpPlotData->iSignalSubSamplingRateSpec)
	, iYSizeDataMatrix(256)
	, iCountNumberForPlotting(qRound((double)(mpPlotData->iSignalSubSamplingRateSpec / mpPlotData->iSignalPlottingRateSpec)))
	, mpPlotData(pPlotData)
	, mps(ps)
{
	mAxesDialogSpec = new AxesDialogSpec(this, mpPlotData, this, mps);
	d_spectrogram = new QwtPlotSpectrogram();
	data = new QwtMatrixRasterData();	
	
	data->setInterval(Qt::ZAxis, QwtInterval(mpPlotData->zLimitSpec[1], mpPlotData->zLimitSpec[0]));
	data->setInterval(Qt::YAxis, QwtInterval(0, iYSizeDataMatrix));
	data->setInterval(Qt::XAxis, QwtInterval(0, iXSizeDataMatrix));
	//Interpolation
	data->setResampleMode(static_cast<QwtMatrixRasterData::ResampleMode>(mpPlotData->iInterpolationModeSpec));
	QVector<double> values;
	for (uint i = 0; i <= iXSizeDataMatrix * iYSizeDataMatrix; i++)
		values += 0;	
	data->setValueMatrix(values, iXSizeDataMatrix);
	data->setInterval(Qt::XAxis, QwtInterval(0, mpPlotData->dSignalMemoryInSeconds));
	data->setInterval(Qt::YAxis, QwtInterval(0, 8));
	
	//Use all available Threads for Rendering
	d_spectrogram->setRenderThreadCount(0);
	d_spectrogram->setData(data);
	d_spectrogram->attach(this);

	buildTitle();	

	/*-------------------------------------------------------------------------------*
	* Set axis
	*-------------------------------------------------------------------------------*/
	//XAxis
	QwtText XAxisLabel;
	XAxisLabel.setText("Time [s]");
	setAxisTitle(QwtPlot::xBottom, XAxisLabel);
	setAxisScale(QwtPlot::xBottom, 0, mpPlotData->dSignalMemoryInSeconds);

	//YAxis
	QwtText YAxisLabel;
	YAxisLabel.setText("Frequency [kHz]");
	setAxisTitle(QwtPlot::yLeft, YAxisLabel);
	setAxisScale(QwtPlot::yLeft, 0, 4);
	
	//ZScale
	QwtText ZScaleLabel;
	QwtScaleWidget *rightAxis = axisWidget(QwtPlot::yRight);
	ZScaleLabel.setText("[dB]");
	setAxisTitle(QwtPlot::yRight, ZScaleLabel);
	setAxisScale(QwtPlot::yRight, mpPlotData->zLimitSpec[1], mpPlotData->zLimitSpec[0]);
	rightAxis->setColorBarEnabled(true);
	
	enableAxis(yLeft + yRight);	

	setColorMap();
	
	/*-------------------------------------------------------------------------------*
	* Set timer interval (in ms)
	*-------------------------------------------------------------------------------*/
	setTimerInterval(1000 / mpPlotData->iSignalSubSamplingRateSpec);
}

void SignalSpecDomainPlot::setTimerInterval(double ms)
{
	d_timerCount = -1;
	d_interval = qRound(ms);

	if (d_timerId >= 0)
	{
		killTimer(d_timerId);
		d_timerId = -1;
	}
	if (d_interval >= 0)
		d_timerId = startTimer(d_interval);
}

//  Generate new values 
void SignalSpecDomainPlot::timerEvent(QTimerEvent *)
{		
	if (mpPlotData->bActive == true && mpPlotData->bPlottingSpec == true)
	{
		if (d_timerCount < iXSizeDataMatrix)
		{
			d_timerCount++;
		}
		else
		{
			d_timerCount = 0;
		}
		for (uint j = 0; j < iYSizeDataMatrix; j++)
		{
			data->setValue(j, d_timerCount, mpPlotData->pppdFreqMemory[mpPlotData->piModuleIndex[mpPlotData->iSignalNumberForSpec]][mpPlotData->iSignalNumberForSpec][j]);
		}
		if (d_timerCount % iCountNumberForPlotting == 0)
		{
			replot();
		}
	}
}

void SignalSpecDomainPlot::setColorMap()
{
	QwtScaleWidget *axis = axisWidget(QwtPlot::yRight);
	const QwtInterval zInterval = d_spectrogram->data()->interval(Qt::ZAxis);

	d_spectrogram->setColorMap(new LinearColorMapRGB());
	axis->setColorMap(zInterval, new LinearColorMapRGB());
}

void SignalSpecDomainPlot::ApplyDefaultScales()
{
	mAxesDialogSpec->setZLowerLimit(-90);
	mAxesDialogSpec->setZUpperLimit(10);
	mAxesDialogSpec->setSeconds(5);
	mAxesDialogSpec->setSubSamplingRate(0);
	mAxesDialogSpec->setPlottingRate(20);
	mAxesDialogSpec->setInterpolation(0);
	mAxesDialogSpec->setYUpperLimit(4000);
	mAxesDialogSpec->setYLowerLimit(0);
	setInterpolation();
	setZScale();
	setYScale();
	resetPlot();
	replot();
}
void SignalSpecDomainPlot::setSecondsDefault()
{
	mAxesDialogSpec->setSeconds(5);
	resetPlot();
	replot();
}
void SignalSpecDomainPlot::setPlottingRateDefault() 
{
	mAxesDialogSpec->setPlottingRate(20);
	iCountNumberForPlotting = qRound((double)(mpPlotData->iSignalSubSamplingRateSpec / mpPlotData->iSignalPlottingRateSpec));
	resetPlot();
	replot();
}
void SignalSpecDomainPlot::setZUpperLimitDefault() 
{
	mAxesDialogSpec->setZUpperLimit(10);
	setZScale();
	replot();
}
void SignalSpecDomainPlot::setZLowerLimitDefault() 
{
	mAxesDialogSpec->setZLowerLimit(-90);
	setZScale();
	replot();
}
void SignalSpecDomainPlot::setYUpperLimitDefault()
{
	mAxesDialogSpec->setYUpperLimit(4000);
	setYScale();
	replot();
}
void SignalSpecDomainPlot::setYLowerLimitDefault()
{
	mAxesDialogSpec->setYLowerLimit(0);
	setYScale();
	replot();
}

void SignalSpecDomainPlot::setInterpolation()
{
	data->setResampleMode(static_cast<QwtMatrixRasterData::ResampleMode>(mpPlotData->iInterpolationModeSpec));
}

void SignalSpecDomainPlot::setZScale()
{
	data->setInterval(Qt::ZAxis, QwtInterval(mpPlotData->zLimitSpec[1], mpPlotData->zLimitSpec[0]));
	setAxisScale(QwtPlot::yRight, mpPlotData->zLimitSpec[1], mpPlotData->zLimitSpec[0]);
	setColorMap();
}
void SignalSpecDomainPlot::setYScale()
{
	setAxisScale(QwtPlot::yLeft, (double) mpPlotData->yLeftLimitSpec[1]/1000, (double) mpPlotData->yLeftLimitSpec[0]/1000);
	replot();
}

void SignalSpecDomainPlot::buildTitle()
{
	QString str1 = "Signal ";
	QString str2 = QString::number(mpPlotData->iSignalNumberForSpec);
	QwtText title(str1 + str2);
	title.setFont(QFont("Helvetica", 12, 1));
	setTitle(title);
	replot();
}

//Open Settings Dialog
void SignalSpecDomainPlot::mouseDoubleClickEvent(QMouseEvent *pmEvent)
{	
	mAxesDialogSpec->show();
	connect(mAxesDialogSpec, SIGNAL(resetPlot()), this, SLOT(resetPlot()));
	connect(mAxesDialogSpec, SIGNAL(setZScale()), this, SLOT(setZScale()));
	connect(mAxesDialogSpec, SIGNAL(setYScale()), this, SLOT(setYScale()));
	connect(mAxesDialogSpec, SIGNAL(setInterpolation()), this, SLOT(setInterpolation()));
	connect(mAxesDialogSpec, SIGNAL(ApplyDefaultScales()), this, SLOT(ApplyDefaultScales()));
	connect(mAxesDialogSpec, SIGNAL(setSecondsDefault()), this, SLOT(setSecondsDefault()));
	connect(mAxesDialogSpec, SIGNAL(setSubSamplingRateDefault()), this, SLOT(setSubSamplingRateDefault()));
	connect(mAxesDialogSpec, SIGNAL(setPlottingRateDefault()), this, SLOT(setPlottingRateDefault()));
	connect(mAxesDialogSpec, SIGNAL(setZUpperLimitDefault()), this, SLOT(setZUpperLimitDefault()));
	connect(mAxesDialogSpec, SIGNAL(setZLowerLimitDefault()), this, SLOT(setZLowerLimitDefault()));
	connect(mAxesDialogSpec, SIGNAL(setYUpperLimitDefault()), this, SLOT(setYUpperLimitDefault()));
	connect(mAxesDialogSpec, SIGNAL(setYLowerLimitDefault()), this, SLOT(setYLowerLimitDefault()));
	connect(mAxesDialogSpec, SIGNAL(buildTitle()), this, SLOT(buildTitle()));
}

void SignalSpecDomainPlot::resetPlot()
{		
	//Building X Size of Data Matrix
	iXSizeDataMatrix = mpPlotData->dSignalMemoryInSeconds * mpPlotData->iSignalSubSamplingRateSpec;

	//Sets RasterMatrixData Matrix
	QVector<double> values;
	for (uint i = 0; i <= iXSizeDataMatrix * iYSizeDataMatrix; i++)
		values += 0;
	data->setInterval(Qt::XAxis, QwtInterval(0, mpPlotData->dSignalMemoryInSeconds));
	data->setInterval(Qt::YAxis, QwtInterval(0, 8));
	data->setInterval(Qt::ZAxis, QwtInterval(mpPlotData->zLimitSpec[1], mpPlotData->zLimitSpec[0]));
	data->setValueMatrix(values, iXSizeDataMatrix);

	//Set Plot Axis Scales
	setAxisScale(QwtPlot::xBottom, 0, mpPlotData->dSignalMemoryInSeconds);	
	setAxisScale(QwtPlot::yLeft, 0, 8);		

	//Restart Timer	
	setTimerInterval(1000 / mpPlotData->iSignalSubSamplingRateSpec);	
	iCountNumberForPlotting = qRound((double)(mpPlotData->iSignalSubSamplingRateSpec / mpPlotData->iSignalPlottingRateSpec));	
	if (iCountNumberForPlotting == 0)
	{
		iCountNumberForPlotting = 1;
	}
}