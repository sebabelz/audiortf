///*-----------------------------------------------------------------------------------------------*
//* Real-time framework for audio and speech signal processing
//* Aschaffenburg University of Applied Sciences
//* Mohammed.Krini@h-ab.de
//* (c) 2016-2017
//*-----------------------------------------------------------------------------------------------*/

/*
Studienarbeit "Implementierung Spektrogramm", 2019
programmed by s160921, Christian Wagner
*/

#ifndef RTF_HSAB_SPEC_PLOT_H
#define RTF_HSAB_SPEC_PLOT_H

#include <QtWidgets>
#include <qwt_plot.h>
#include "rtf_hsab_plots_data.h"
#include <qwt_color_map.h>
#include <qwt_plot_spectrogram.h>
#include <qwt_scale_widget.h>
#include <qwt_matrix_raster_data.h>
#include "rtf_hsab_plotter_module.h"

class AxesDialogSpec : public QDialog
{
	Q_OBJECT

public:
	AxesDialogSpec(QWidget * parent, tPlotData *pPlotData, QwtPlot *plot, tSession *ps);
public slots:
	void setZUpperLimit(int);
	void setZLowerLimit(int);
	void setYUpperLimit(int);
	void setYLowerLimit(int);
	void setInterpolation(int);
	void setSignal(int);
	void setSeconds(int);
	void setSubSamplingRate(int);
	void setPlottingRate(int);
	void exportDocument();
signals:
	void resetPlot();
	void setZScale();
	void setYScale();
	void setInterpolation();
	void ApplyDefaultScales();	
	void setSecondsDefault();
	void setPlottingRateDefault();
	void setZUpperLimitDefault();
	void setZLowerLimitDefault();
	void setYUpperLimitDefault();
	void setYLowerLimitDefault();
	void buildTitle();
private:
	tPlotData		*mpPlotData;
	QwtPlot			*mpPlot;
	tSession		*mps;
	QVBoxLayout		*MainLayout;
	QHBoxLayout     *TopLayout;
	QGroupBox		*mGroupLayout;
	QGridLayout		*mgridLayoutZ;
	QSpinBox		*mUpperLimitZ;
	QSpinBox		*mLowerLimitZ;
	QLabel		    *mUpperLabelZ;
	QLabel			*mLowerLabelZ;
	QSpinBox		*mUpperLimitY;
	QSpinBox		*mLowerLimitY;
	QLabel		    *mUpperLabelY;
	QLabel			*mLowerLabelY;
	QLabel			*mInterpolationLabel;
	QLabel			*mSignalLabel;
	QComboBox		*mComboBoxSignalSelect;
	QComboBox		*mComboBoxInterpolationSelect;
	QLabel			*mSecondsLabel;
	QSpinBox		*mSecondsSpinBox;
	QLabel			*mCoreSamplingRateLabel;
	QComboBox		*mComboBoxCoreSamplingRateSelect;
	QLabel			*mSubSamplingFactorLabel;
	QComboBox		*mComboBoxSubSamplingFactorSelect;
	QLabel			*mPlottingRateLabel;
	QSpinBox		*mPlottingRateSpinBox;
};



class SignalSpecDomainPlot : public QwtPlot
{
    Q_OBJECT

public:
    SignalSpecDomainPlot( tPlotData *pPlotData, tSession *ps, QWidget* = NULL);

	enum ColorMap
	{
		RGBMap,
		IndexMap
	};

public slots:
	void mouseDoubleClickEvent(QMouseEvent *pmEvent);
    void setTimerInterval(double interval);
	void setColorMap();
	void ApplyDefaultScales();
	void setSecondsDefault();
	void setPlottingRateDefault();
	void setZUpperLimitDefault();
	void setZLowerLimitDefault();
	void setYUpperLimitDefault();
	void setYLowerLimitDefault();
	void resetPlot();
	void setZScale();
	void setYScale();
	void setInterpolation();
	void buildTitle();
protected:
    virtual void timerEvent(QTimerEvent *e);

private:
	tPlotData	*mpPlotData;
	AxesDialogSpec	*mAxesDialogSpec;
	QwtPlotSpectrogram *d_spectrogram;
	QwtMatrixRasterData *data;
	tSession		*mps;

    T_INT d_interval; 
    T_INT d_timerId;
	T_INT d_timerCount;
	T_INT iYSizeDataMatrix;
	T_INT iXSizeDataMatrix;
	T_INT iCountNumberForPlotting;	
};

#endif
