#include "rtf_hsab_plot_axislimitsialog.h"

AxisLimitsDialog::AxisLimitsDialog(int rows, int cols, int limitCount, QVariant var, QWidget *parent) :
	AxisLimitsDialog(rows, cols, limitCount, var, "", parent)
{
}

AxisLimitsDialog::AxisLimitsDialog(int rows, int cols, int limitCount,QVariant var, QString dialogTitle, bool scaler,
                                   QWidget *parent) : QDialog(parent)
{
	setWindowTitle(tr(qPrintable(dialogTitle)));
	layout = new QVBoxLayout(this);
	signalGrid = new QGridLayout();
	
	this->rows = rows;
	this->cols = cols;
	this->limitCount = limitCount;

	layout->addLayout(signalGrid);

	for (int i = 0; i < limitCount; ++i)
	{
		if (var.canConvert<QString>())
		{
			limitWidgets.append(new LimitBox(var.toString() + " " + QString::number(i), scaler));
		}
		else if (var.canConvert<QList<QString>>())
		{
			limitWidgets.append(new LimitBox(var.value<QList<QString>>().at(i), scaler));
		}

		limits.append(QPair<double, double>(0.0, 0.0));

		connect(limitWidgets[i], &LimitBox::limitChanged, [this, i](double a, double b)
		{
			limits[i].first = a;
			limits[i].second = b;
		});

		if(scaler)
		{
			connect(limitWidgets[i], &LimitBox::scalerStateChanged, [this, i](int a)
			{
				emit scalerChanged(i, a);
			});
		}
	}
	if (limitCount <= cols * rows)
	{
		QList<LimitBox *>::iterator iter = limitWidgets.begin();

		for (auto i = 0, k = 0; i < rows; ++i)
		{
			for (auto j = 0; j < cols; ++j, ++k, ++iter)
			{
				signalGrid->addWidget(limitWidgets[k], i, j);
			}
		}
	}

	buttonLayout = new QHBoxLayout(this);

	applyButton = new QPushButton(tr("Apply"));
	closeButton = new QPushButton(tr("Cancel"));

	applyButton->setFixedWidth(50);
	closeButton->setFixedWidth(50);

	buttonLayout->setAlignment(Qt::AlignCenter);
	buttonLayout->addWidget(applyButton);
	buttonLayout->addWidget(closeButton);
	layout->addLayout(buttonLayout);

	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

	connect(applyButton, &QPushButton::clicked, this, &AxisLimitsDialog::sendValues);
	connect(closeButton, &QPushButton::clicked, this, &AxisLimitsDialog::close);
	setFixedSize(QDialog::sizeHint());
}

AxisLimitsDialog::~AxisLimitsDialog()
{
}

void AxisLimitsDialog::setLimits(QVariant var)
{
	if (var.canConvert<QList<QPair<double, double>>>())
	{
		for (int i = 0; i < limitWidgets.count(); ++i)
		{
			if (var.value<QList<QPair<double, double>>>().count() <= limitWidgets.count())
			{
				limitWidgets.at(i)->setLimits(var.value<QList<QPair<double, double>>>().at(i).second,
										      var.value<QList<QPair<double, double>>>().at(i).first);
			}
		}
	}
}

void AxisLimitsDialog::setMaximumLimits(QVariant var)
{
	if (var.canConvert<QPair<double, double>>())
	{
		for (auto limit : limitWidgets)
		{
			limit->setMaximumLimits(var.value<QPair<double, double>>().second,
			                        var.value<QPair<double, double>>().first);
		}
	}
}

void AxisLimitsDialog::getLimits(int i, double &max, double &min)
{
	limitWidgets[i]->getLimits(max, min);
}

void AxisLimitsDialog::close()
{
	for (auto limit : limitWidgets)
	{
		limit->resetToLastLimits();
	}

	QDialog::close();
}

void AxisLimitsDialog::sendValues()
{
	for (auto limit: limitWidgets)
	{
		limit->setApplyLimits();
	}

	emit valueChanged(limits);
	accept();
}
