#include "rtf_hsab_plot_limitbox.h"

LimitBox::LimitBox(QString boxName, bool scaler, QWidget *parent) : QWidget(parent)
{
	upperLimit.first.setText("Upper Limit");
	lowerLimit.first.setText("Lower Limit");
	
	groupBox = new QGroupBox(tr(qPrintable(boxName)));
	layout = new QVBoxLayout(this);
	grid = new QGridLayout();

	grid->addWidget(&upperLimit.first, 0, 0);
	grid->addWidget(&upperLimit.second, 0, 1);
	grid->addWidget(&lowerLimit.first, 1, 0);
	grid->addWidget(&lowerLimit.second, 1, 1);

	upperLimit.second.setSingleStep(0.5);
	lowerLimit.second.setSingleStep(0.5);

	groupBox->setLayout(grid);
	layout->addWidget(groupBox);

	connect(&upperLimit.second, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LimitBox::sendUpperLimit);
	connect(&lowerLimit.second, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LimitBox::sendLowerLimit);

	if(scaler)
	{
		grid->addWidget(&autoScale.first, 2, 0);
		grid->addWidget(&autoScale.second, 2, 1);

		autoScale.first.setText("Autoscaler");
		autoScale.second.setText("enabled");

		connect(&autoScale.second, &QCheckBox::stateChanged, this, &LimitBox::autoScaledEnabled);
	}
}

void LimitBox::setLimits(double upper, double lower)
{
	upperLimit.second.setValue(upper);
	lowerLimit.second.setValue(lower);
}

void LimitBox::setMaximumLimits(double max, double min)
{
	upperLimit.second.setRange(max, min);
	lowerLimit.second.setRange(max, min);
}

void LimitBox::getLimits(double &max, double &min)
{
	max = upperLimit.second.value();
	min = lowerLimit.second.value();
}

void LimitBox::resetToLastLimits()
{
	upperLimit.second.setValue(lastUpperLimit);
	lowerLimit.second.setValue(lastLowerLimit);
}

void LimitBox::setApplyLimits()
{
	lastUpperLimit = upperLimit.second.value();
	lastLowerLimit = lowerLimit.second.value();
}

void LimitBox::sendUpperLimit(double i)
{
	emit limitChanged(i, lowerLimit.second.value());
}

void LimitBox::sendLowerLimit(double i)
{
	emit limitChanged(upperLimit.second.value(), i);
}

void LimitBox::autoScaledEnabled(int)
{
	if(autoScale.second.isChecked())
	{
		upperLimit.second.setEnabled(false);
		lowerLimit.second.setEnabled(false);
	}
	else
	{
		upperLimit.second.setEnabled(true);
		lowerLimit.second.setEnabled(true);
	}

	emit scalerStateChanged(static_cast<int>(autoScale.second.isChecked()));
}
