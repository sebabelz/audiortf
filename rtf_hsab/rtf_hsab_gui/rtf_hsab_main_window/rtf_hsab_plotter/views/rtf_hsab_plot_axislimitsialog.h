#ifndef RTF_HSAB_PLOT_VIEWS_AXISLIMITSDIALOG_H
#define RTF_HSAB_PLOT_VIEWS_AXISLIMITSDIALOG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMetaType>
#include <QPushButton>
#include <QPointer>
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_plot_limitbox.h"

class AxisLimitsDialog : public QDialog
{
	Q_OBJECT
public:
	AxisLimitsDialog(int rows, int cols, int limitCount, QVariant var, QWidget *parent = nullptr);
	AxisLimitsDialog(int rows, int cols, int limitCount, QVariant var, QString dialogTitle, bool scaler, QWidget *parent = nullptr);
	~AxisLimitsDialog();

	void setLimits(QVariant);
	void setMaximumLimits(QVariant);
	void getLimits(int ,double &, double &);

	void close();

public slots:


private slots:
	void sendValues();

signals:
	void valueChanged(QList<QPair<double, double>>);
	void scalerChanged(int, int);

private:
	int limitCount = 0;
	int rows = 0;
	int cols = 0;
	QString boxesName;
	QList<QPair<double, double>> limits;
	QPointer<QVBoxLayout> layout;
	QPointer<QHBoxLayout> buttonLayout;
	QPointer<QGridLayout> signalGrid;
	QList<LimitBox *> limitWidgets;
	QPointer<QPushButton> applyButton;
	QPointer<QPushButton> closeButton;
};

#endif // RTF_HSAB_PLOT_VIEWS_AXISLIMITSDIALOG_H*/