#ifndef RTF_HSAB_PLOT_VIEWS_LIMITBOX_H
#define  RTF_HSAB_PLOT_VIEWS_LIMITBOX_H

#include <QWidget>
#include <QtDebug>
#include <QGroupBox>
#include <QLabel>
#include <QSpinBox>
#include <QPair>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPointer>
#include <QCheckBox>

class LimitBox : public QWidget
{
	Q_OBJECT
public:
	LimitBox(QString boxName, bool scaler, QWidget *parent = nullptr);
	void setLimits(double, double);
	void setMaximumLimits(double, double);
	void getLimits(double &, double &);

public slots:
	void resetToLastLimits();
	void setApplyLimits();

private slots:
	void sendUpperLimit(double);
	void sendLowerLimit(double);
	void autoScaledEnabled(int);

signals:
	void limitChanged(double, double);
	void scalerStateChanged(int);

private:
	double lastLowerLimit = 0.0;
	double lastUpperLimit = 0.0;

	QPointer<QVBoxLayout> layout;
	QPointer<QGroupBox> groupBox;

	QPointer<QGridLayout> grid;
	QPair<QLabel, QDoubleSpinBox> upperLimit;
	QPair<QLabel, QDoubleSpinBox> lowerLimit;
	QPair<QLabel, QCheckBox> autoScale;

};

#endif //  RTF_HSAB_PLOT_VIEWS_LIMITBOX_H
