/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include <qwt_counter.h>
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_plots_visual.h"
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_time_plot.h"
#include "rtf_hsab_freq_plot.h"
#include "rtf_hsab_spec_plot.h"
#include "rtf_hsab_signal_select.h"
#include "types_hsab.h"
#include "rtf_hsab_signal_select_channel.h"
#include "rtf_hsab_audio_io.h"
#include <QPair>
#include <QMap>
#include <qwt_axis_id.h>

TabRealTimePlots::TabRealTimePlots( tSession               *pSession, AudioIO *pAudioIO,
                                    QWidget                *parent )
     : QWidget(parent)
{
   T_INT    i,k;
   T_DOUBLE dLocInvSampleRate = (T_DOUBLE) 1.0 / (T_DOUBLE) pSession->iSampleRateExt;

  /*------------------------------------------------------------------------------------------------*
   * Initialization of the data struct for the real-time plots
   *------------------------------------------------------------------------------------------------*/
   mpPlotData = (tPlotData*) calloc(1, sizeof(tPlotData));
   mpPlotData->bActive = false;
   mpPlotData->bPlottingTime = false;
   mpPlotData->bPlottingFreq = false;
   mpPlotData->bPlottingSpec = false;

   mpPlotData->dSignalMemoryInSeconds        = (T_DOUBLE) 5.0;
   mpPlotData->iSignalPlotSubSampling        = (T_INT)    16;
   mpPlotData->iSignalPlotSubSamplingCounter = (T_INT)    0;
   mpPlotData->iNumberOfModules				 = (T_INT)    4;
   mpPlotData->iNumberOfWidgets              = (T_INT)    4;
   mpPlotData->iNumEchoCompSignals			 = (T_INT)   17;

   mpPlotData->iInterpolationModeSpec		= (T_INT)	0;
   mpPlotData->iSignalNumberForSpec			= (T_INT)	0;
   mpPlotData->iModuleNumberForSpec			= (T_INT)	0;
   mpPlotData->iSignalSubSamplingRateSpec	= (T_INT)	100;
   mpPlotData->iSignalPlottingRateSpec		= (T_INT)	20;

   mpPlotData->iWritePointer                 = (T_INT)    0;
   mpPlotData->iSignalMemoryLength           = (T_INT) (mpPlotData->dSignalMemoryInSeconds 
															   * (T_FLOAT) pSession->iSampleRateExt
                                                               / (T_FLOAT) mpPlotData->iSignalPlotSubSampling );

#ifdef BF_SYSTEM
   mpPlotData->dFreqMemoryInHertz = (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal * (T_DOUBLE)0.5;
   mpPlotData->iFreqMemoryLength = (T_INT)pSession->HsabAlgCoreMain.HsabAlgCoreParams.iFFTLength / 2 + 1;
#else
   mpPlotData->dFreqMemoryInHertz = (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt * (T_DOUBLE)0.5;
   mpPlotData->iFreqMemoryLength = (T_INT)pSession->HsabAlgCoreMain.HsabAlgCoreParams.iFftOrder / 2 + 1;
#endif // BF_SYSTEM

   mpPlotData->pdTimeAxis = (T_DOUBLE*) calloc( mpPlotData->iSignalMemoryLength, 
                                                        sizeof(T_DOUBLE) );

   for (i=0; i<mpPlotData->iSignalMemoryLength; i++)
   {
      mpPlotData->pdTimeAxis[i] = (T_DOUBLE) i 
                                          * (T_DOUBLE) mpPlotData->iSignalPlotSubSampling
                                          * dLocInvSampleRate;
   }

   mpPlotData->pdFreqAxis = (T_DOUBLE*) calloc( mpPlotData->iFreqMemoryLength, 
                                                        sizeof(T_DOUBLE) );
   for (i=0; i<mpPlotData->iFreqMemoryLength; i++)
   {
#ifdef BF_SYSTEM
	   mpPlotData->pdFreqAxis[i] = (T_DOUBLE)i
		   / (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.iFFTLength
		   * (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal * 0.001; // in kHz  
#else
	   mpPlotData->pdFreqAxis[i] = (T_DOUBLE)i
		   / (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.iFftOrder
		   * (T_DOUBLE)pSession->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt * 0.001; // in kHz

#endif // BF_SYSTEM

   }

   mpPlotData->piModuleIndex = (T_INT*) calloc( mpPlotData->iNumberOfModules,   
                                                        sizeof(T_INT) );

   mpPlotData->piSignalIndex = (T_INT*) calloc( mpPlotData->iNumberOfModules,
                                                        sizeof(T_INT) );

   /* View main signals at initialization */
      mpPlotData->piModuleIndex[0] = 0;
	  mpPlotData->piModuleIndex[1] = 0;
	  mpPlotData->piModuleIndex[2] = 0;
	  mpPlotData->piModuleIndex[3] = 0;

	  mpPlotData->piSignalIndex[0] = 0;
	  mpPlotData->piSignalIndex[1] = 1;
	  mpPlotData->piSignalIndex[2] = 2;
	  mpPlotData->piSignalIndex[3] = 3;

   mpPlotData->piChannel1 = (T_INT*) calloc( mpPlotData->iNumberOfWidgets, 
                                                        sizeof(T_INT) );

   mpPlotData->piChannel2 = (T_INT*) calloc( mpPlotData->iNumberOfWidgets, 
                                                        sizeof(T_INT) );
  /*------------------------------------------------------------------------------------------------*
   * Set y-scales (time domain)
   *------------------------------------------------------------------------------------------------*/
   mpPlotData->yLeftLimitTime = (T_DOUBLE*) calloc((T_DOUBLE) 4, sizeof(T_DOUBLE) );

   mpPlotData->yRightLimitTime = (T_DOUBLE*) calloc((T_DOUBLE) 4, sizeof(T_DOUBLE) );

   mpPlotData->yLeftLimitTime[0]  = (T_DOUBLE)   1;
   mpPlotData->yLeftLimitTime[1]  = (T_DOUBLE)  -1;
   mpPlotData->yLeftLimitTime[2] = (T_DOUBLE)	  1;
   mpPlotData->yLeftLimitTime[3] = (T_DOUBLE)	 -1;

   mpPlotData->yRightLimitTime[0] = (T_DOUBLE)  20;
   mpPlotData->yRightLimitTime[1] = (T_DOUBLE) -90;
   mpPlotData->yRightLimitTime[2] = (T_DOUBLE)	 20;
   mpPlotData->yRightLimitTime[3] = (T_DOUBLE)	-90;

  /*------------------------------------------------------------------------------------------------*
   * Set y-scales (freq domain)
   *------------------------------------------------------------------------------------------------*/
   mpPlotData->yLeftLimitFreq = (T_DOUBLE*) calloc((T_DOUBLE) 2, sizeof(T_DOUBLE) );

   mpPlotData->yRightLimitFreq = (T_DOUBLE*) calloc((T_DOUBLE) 2, sizeof(T_DOUBLE) );

   mpPlotData->yLeftLimitFreq[0]  = (T_DOUBLE)  10;
   mpPlotData->yLeftLimitFreq[1]  = (T_DOUBLE) -90;
   mpPlotData->yRightLimitFreq[0] = (T_DOUBLE)   2;
   mpPlotData->yRightLimitFreq[1] = (T_DOUBLE)  -1;

   /*------------------------------------------------------------------------------------------------*
   * Set z-scales (spec domain)
   *------------------------------------------------------------------------------------------------*/
   mpPlotData->zLimitSpec = (T_INT*)calloc((T_INT)2, sizeof(T_INT));

   mpPlotData->yLeftLimitSpec = (T_INT*)calloc((T_INT)2, sizeof(T_INT));

   mpPlotData->zLimitSpec[0] = (T_INT)10;
   mpPlotData->zLimitSpec[1] = (T_INT)-90;

   mpPlotData->yLeftLimitSpec[0] = (T_INT)8000;
   mpPlotData->yLeftLimitSpec[1] = (T_INT)0;

  /*------------------------------------------------------------------------------------------------*
   * Plotting buffer memory allocation (time domain)
   *------------------------------------------------------------------------------------------------*/
   mpPlotData->pppdSignalMemory = (T_DOUBLE***) calloc( mpPlotData->iNumberOfModules, 
                                                              sizeof(T_DOUBLE**) );
   for (k=0; k<mpPlotData->iNumberOfModules; k++)
   {
	  mpPlotData->pppdSignalMemory[k] = (T_DOUBLE**) calloc( mpPlotData->iNumberOfWidgets, 
                                                                   sizeof(T_DOUBLE*) );
	  for (i=0; i<mpPlotData->iNumberOfWidgets; i++)
	  {
		 mpPlotData->pppdSignalMemory[k][i] = (T_DOUBLE*) calloc( mpPlotData->iSignalMemoryLength, 
                                                                   sizeof(T_DOUBLE) );      
	  }
   }

  /*------------------------------------------------------------------------------------------------*
   * Plotting buffer memory allocation (frequency domain)
   *------------------------------------------------------------------------------------------------*/
   mpPlotData->pppdFreqMemory = (T_DOUBLE***) calloc( mpPlotData->iNumberOfModules, 
                                                              sizeof(T_DOUBLE**) );
   for (k=0; k<mpPlotData->iNumberOfModules; k++)
   {
	  mpPlotData->pppdFreqMemory[k] = (T_DOUBLE**) calloc( mpPlotData->iNumberOfWidgets, 
                                                                   sizeof(T_DOUBLE*) );
	  for (i=0; i<mpPlotData->iNumberOfWidgets; i++)
	  {
	     mpPlotData->pppdFreqMemory[k][i] = (T_DOUBLE*) calloc( mpPlotData->iFreqMemoryLength, 
                                                                   sizeof(T_DOUBLE) );      
	  }

   }
   mpPlotData->pdOffset = (T_DOUBLE*) calloc( mpPlotData->iNumberOfWidgets,
                                                        sizeof(T_DOUBLE) );
   mpPlotData->piFreq = (T_INT*) calloc( mpPlotData->iNumberOfWidgets, 
                                                        sizeof(T_INT) );

   mpPlotData->numOfLeftYAxes = 2;
   mpPlotData->numOfRightYAxes = 2;

   mLayoutVisPlotMain    = new QHBoxLayout(this);
   mGraphicsViewParam    = new QGraphicsView(this);
   mGraphicsViewVisTime  = new QGraphicsView(this);
   mGraphicsViewVisFreq  = new QGraphicsView(this);
   mGraphicsViewVisSpec = new QGraphicsView(this);
   mSplitterVisPlotMain  = new QSplitter(Qt::Horizontal, this);
   
   mGraphicsViewParam->setMinimumWidth(287);
   mGraphicsViewParam->setMaximumWidth(287);
   mGraphicsViewVisTime->setMinimumWidth(350);
   mGraphicsViewVisFreq->setMinimumWidth(290);
   mGraphicsViewVisSpec->setMinimumWidth(500);

   mSplitterVisPlotMain->addWidget(mGraphicsViewParam); 
   mSplitterVisPlotMain->addWidget(mGraphicsViewVisTime);
   mSplitterVisPlotMain->addWidget(mGraphicsViewVisFreq);
   mSplitterVisPlotMain->addWidget(mGraphicsViewVisSpec);
      
   mLayoutVisPlotMain->addWidget(mSplitterVisPlotMain);

   QGridLayout *LayoutParam   = new QGridLayout(this);
   QGridLayout *LayoutVisTime = new QGridLayout(this);
   QGridLayout *LayoutVisFreq = new QGridLayout(this);
   QGridLayout *LayoutVisSpec = new QGridLayout(this);

   mGraphicsViewParam->setLayout(LayoutParam);
   mGraphicsViewParam->setVisible(false);
   mGraphicsViewVisTime->setLayout(LayoutVisTime);
   mGraphicsViewVisTime->setVisible(true);
   mGraphicsViewVisFreq->setLayout(LayoutVisFreq);
   mGraphicsViewVisFreq->setVisible(true);
   mGraphicsViewVisSpec->setLayout(LayoutVisSpec);
   mGraphicsViewVisSpec->setVisible(true);

   mSignalTimeDomainPlot = new SignalTimeDomainPlot(mpPlotData, mGraphicsViewVisTime);
   mSignalFreqDomainPlot = new SignalFreqDomainPlot(mpPlotData, mGraphicsViewVisFreq);
   mSignalSpecDomainPlot = new SignalSpecDomainPlot(mpPlotData, pSession, mGraphicsViewVisSpec);
	

   mSignalSelection      = new SignalSelection(mpPlotData, mSignalTimeDomainPlot->pPlotCurveTime, mSignalFreqDomainPlot->pPlotCurveFreq, pAudioIO, pSession, mSignalTimeDomainPlot->engine, this);

	for (int i = 0; i < mpPlotData->iNumberOfWidgets; ++i)
	{
		connect(mSignalSelection->mQVectorSignalSelectChannel[i], &SignalSelectChannel::whichBox, mSignalTimeDomainPlot, &SignalTimeDomainPlot::setAxesVisibility);
		connect(mSignalSelection->mQVectorSignalSelectChannel[i], &SignalSelectChannel::sendLabel, mSignalTimeDomainPlot, &SignalTimeDomainPlot::updateAxisLabel);
		connect(mSignalSelection->mQVectorSignalSelectChannel[i], &SignalSelectChannel::sendScales, mSignalTimeDomainPlot, &SignalTimeDomainPlot::updateScales);
	}
  
   LayoutParam->addWidget(mSignalSelection);
   LayoutVisTime->addWidget(mSignalTimeDomainPlot);
   LayoutVisFreq->addWidget(mSignalFreqDomainPlot);
   LayoutVisSpec->addWidget(mSignalSpecDomainPlot);
}

TabRealTimePlots::~TabRealTimePlots()
{
  /*------------------------------------------------------------------------------------------------*
   * Plotting buffers memory deallocation (time and frequency buffers)
   *------------------------------------------------------------------------------------------------*/
   T_INT i,k;
   for (k=0; k<mpPlotData->iNumberOfModules; k++)
   {
	  for (i=0; i<mpPlotData->iNumberOfWidgets; i++)
      {
		 free(mpPlotData->pppdSignalMemory[k][i]); 
		 free(mpPlotData->pppdFreqMemory[k][i]);
	  }
	  free(mpPlotData->pppdSignalMemory[k]);
	  free(mpPlotData->pppdFreqMemory[k]);
   }

   free(mpPlotData->pppdSignalMemory);
   free(mpPlotData->pppdFreqMemory);

   free(mpPlotData->pdTimeAxis);
   free(mpPlotData->pdFreqAxis);

   free(mpPlotData->pdOffset);
   free(mpPlotData->piFreq);

   free(mpPlotData->piModuleIndex);
   free(mpPlotData->piSignalIndex);
   free(mpPlotData->piChannel1);
   free(mpPlotData->piChannel2);
   free(mpPlotData->yLeftLimitTime);
   free(mpPlotData->yRightLimitTime);
   free(mpPlotData->yLeftLimitFreq);
   free(mpPlotData->yRightLimitFreq);
   free(mpPlotData->yLeftLimitSpec);
   free(mpPlotData->zLimitSpec);
}

tPlotData* TabRealTimePlots::getRealTimeDataPointer()
{
   return mpPlotData;
}

void TabRealTimePlots::viewParamsLayout(bool bView)
{
   mGraphicsViewParam->setVisible(bView);
}

void TabRealTimePlots::viewTimePlot(bool bView)
{
	mGraphicsViewVisTime->setVisible(bView);
	mpPlotData->bPlottingTime = bView;

}

void TabRealTimePlots::viewFreqPlot(bool bView)
{
	mGraphicsViewVisFreq->setVisible(bView);
	mpPlotData->bPlottingFreq = bView;
}

void TabRealTimePlots::viewSpecPlot(bool bView)
{
	mGraphicsViewVisSpec->setVisible(bView);
	mpPlotData->bPlottingSpec = bView;
}