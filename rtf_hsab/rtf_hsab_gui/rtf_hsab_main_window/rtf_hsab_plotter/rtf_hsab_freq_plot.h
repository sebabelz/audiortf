/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_FREQ_PLOT_H
#define RTF_HSAB_FREQ_PLOT_H

#include <QtGui>
#include <QtWidgets>
#include <QString>
#include <qwt_plot.h>
#include "rtf_hsab_plots_data.h"
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_text.h>
#include "rtf_hsab_plot_axislimitsialog.h"

class SignalFreqDomainPlot : public QwtPlot
{
	Q_OBJECT

public:
    SignalFreqDomainPlot( tPlotData *pPlotData,
                          QWidget* = NULL );
	QVector<QwtPlotCurve*>   pPlotCurveFreq;
public slots:
    void setTimerInterval(double interval);
	void mouseDoubleClickEvent ( QMouseEvent *pmEvent );
	void ApplyScales(QList<QPair<double, double>>);
protected:
    virtual void timerEvent(QTimerEvent *e);

private:
    void alignScales();
	tPlotData	*mpPlotData;
	QwtText YAxisLabel;
    T_INT d_interval; // timer in ms
    T_INT d_timerId;
	QPointer<AxisLimitsDialog> limitsDialog;
	QwtPlotCanvas* mPlotCanvas;
};

#endif
