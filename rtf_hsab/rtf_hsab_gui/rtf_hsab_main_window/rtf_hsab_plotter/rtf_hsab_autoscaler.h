/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SCALE_ENGINE_H
#define RTF_HSAB_SCALE_ENGINE_H

#include <QtGui>
#include <QtWidgets>
#include <QString>
#include <QtMath>
#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_text.h>
#include <qwt_axis_id.h>
#include <qwt_scale_engine.h>
#include "rtf_hsab_plots_data.h"

class ScaleEngine : public QObject
{
	Q_OBJECT

public:
	ScaleEngine(QObject *parent = nullptr);
	virtual ~ScaleEngine();

	void checkScale();
	void observeSignal(double);
	void startObserve();
	void stopObserve();
	void setActualScale(double, double);
	void setMargin(double);
	void setMargin(double, double);
	void scaleDown();
signals:
	void sendAxisScale(double, double);

private:
	int emitState;
	double upperMargin;
	double lowerMargin;
	double min;
	double max;
	double actualMinScale;
	double actualMaxScale;
	QTimer *timer;
	QTimer *downScaleTimer;
};

#endif 
