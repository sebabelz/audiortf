/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SIGNAL_SELECT_H
#define RTF_HSAB_SIGNAL_SELECT_H

#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include "rtf_hsab_plots_data.h"
#include "types_hsab.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_autoscaler.h"
   

class SignalSelectChannel;

class SignalSelection : public QDialog
{
    Q_OBJECT

public:
    SignalSelection( tPlotData *pPlotData, QVector<QwtPlotCurve*>   pPlotCurveTime, QVector<QwtPlotCurve*>   pPlotCurveFreq, AudioIO *pAudioIO, tSession *ps, ScaleEngine *engine,
                          QWidget *parent = 0 );
	QVector<SignalSelectChannel*>   mQVectorSignalSelectChannel;

protected:

private:
	   tPlotData      *mpPlotData;

	   T_INT           mChannelNumber;
	   QDoubleSpinBox *mSpinBoxGainFileInputChannelSpec;
	   QDoubleSpinBox *mSpinBoxGainSoundcardInputChannelSpec;

	   QCheckBox		*mViewTimePlotCheckBox;
	   QCheckBox		 *mViewFreqPlotCheckBox;
	   QCheckBox		*mViewSpecPlotCheckBox;

	   QComboBox     *mComboBoxSoundcardInputChannelSpec;
	   QComboBox     *mComboBoxFileInputChannelSpec;
	   QPushButton   *mPushButtonFileInputChannel;
	   
	  SignalSelectChannel *mSignalSelectChannel;
};

#endif
