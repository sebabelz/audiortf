/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_TIME_PLOT_H
#define RTF_HSAB_TIME_PLOT_H

#include <QtGui>
#include <QtWidgets>
#include <QString>
#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_text.h>
#include <qwt_axis_id.h>
#include <qwt_scale_engine.h>
#include "types_hsab.h"
#include "rtf_hsab_plot_limitbox.h"
#include "rtf_hsab_plot_axislimitsialog.h"
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_autoscaler.h"


const T_INT PLOT_SIZE = 1001; // 0 to 200

class SignalTimeDomainPlot : public QwtPlot
{
Q_OBJECT

public:
	SignalTimeDomainPlot(tPlotData* pPlotData,
	                     QWidget* = NULL);
	QVector<QwtPlotCurve*> pPlotCurveTime;
	ScaleEngine *engine;

	void enableScaler(int a, int b);

	
public slots:
	void setTimerInterval(T_DOUBLE interval);
	void mouseDoubleClickEvent(QMouseEvent* pmEvent);
	void ApplyScales(QList<QPair<double, double>>);
	void setAxesVisibility(int, bool);
	void updateAxisLabel(int, QString);
	void updateScales(double, double, int num);

protected:
	virtual void timerEvent(QTimerEvent* e);

private:
	void startScaleEngine(int index);
	void stopScaleEngine(int index);
	void alignScales();
	tPlotData* mpPlotData;
	T_INT d_interval; // timer in ms
	T_INT d_timerId;
	QwtPlotCanvas* mPlotCanvas;
	QPointer<AxisLimitsDialog> limitsDialog;
	QwtText YAxisLabel;
	QPalette axisColor(QColor c);
};

#endif
