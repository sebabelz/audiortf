/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_autoscaler.h"

ScaleEngine::ScaleEngine(QObject *parent) : QObject(parent)
{
	timer = new QTimer(this);
	connect(timer, &QTimer::timeout, this, &ScaleEngine::checkScale);
	downScaleTimer = new QTimer(this);
	downScaleTimer->setSingleShot(true);
	connect(downScaleTimer, &QTimer::timeout, this, &ScaleEngine::scaleDown);

	min = 0;
	max = 0;
	actualMinScale = 0;
	actualMaxScale = 0;
	upperMargin = 0;
	lowerMargin = 0;
	emitState = -1;
}

ScaleEngine::~ScaleEngine()
{
	delete timer;
}

void ScaleEngine::checkScale()
{
	if (actualMinScale * (1 - lowerMargin) < min && actualMaxScale * (1 - upperMargin) > max)
	{
		emitState = 0;
	}
	if (actualMaxScale * (1 - upperMargin) < max)
	{
		emitState = 1;	
	}
	if (actualMinScale * (1 - lowerMargin) > min)
	{
		emitState = 2;
	}
	if (actualMinScale * (1 - lowerMargin) > min && actualMaxScale * (1 - upperMargin) < max)
	{
		emitState = 3;
	}
	qDebug() << "max: " << max << " min: " << min;

	switch (emitState)
	{
	case 0:
		if (!downScaleTimer->isActive())
		{
			downScaleTimer->start(5000);
		}
		qDebug() << "state 0";
		break;
	case 1:
		actualMaxScale = max * (1 + upperMargin);
		qDebug() << "emit max";
		emit sendAxisScale(actualMinScale, actualMaxScale);
		max = 0;
		downScaleTimer->stop();
		break;
	case 2:
		actualMinScale = min * (1 + lowerMargin);
		qDebug() << "emit min";
		emit sendAxisScale(actualMinScale, actualMaxScale);
		min = 0;
		downScaleTimer->stop();
		break;
	case 3:
		actualMinScale = min * (1 + lowerMargin);
		actualMaxScale = max * (1 + upperMargin);
		qDebug() << "emit both";
		emit sendAxisScale(actualMinScale, actualMaxScale);
		max = 0;
		min = 0;
		downScaleTimer->stop();
		break;
	default:
		break;
	}
	emitState = -1;
}

void ScaleEngine::observeSignal(double y)
{
	auto distance = (actualMaxScale - actualMinScale) / 2;
	
	if (y > max)
		max = y;
	if (y < min)
		min = y;
}

void ScaleEngine::startObserve()
{
	timer->start(5000);
}

void ScaleEngine::stopObserve()
{
	timer->stop();
}

void ScaleEngine::setActualScale(double min, double max)
{
	this->actualMaxScale = max;
	this->actualMinScale = min;
}

void ScaleEngine::setMargin(double margin)
{
	lowerMargin = margin;
	upperMargin = margin;
}

void ScaleEngine::setMargin(double lower, double upper)
{
	lowerMargin = lower;
	upperMargin = upper;
}

void ScaleEngine::scaleDown()
{
	actualMaxScale -= actualMaxScale * 0.1;
	actualMinScale += abs(actualMinScale) * 0.1;
	emit sendAxisScale(actualMinScale, actualMaxScale);
	qDebug() << "Scale Down";
}