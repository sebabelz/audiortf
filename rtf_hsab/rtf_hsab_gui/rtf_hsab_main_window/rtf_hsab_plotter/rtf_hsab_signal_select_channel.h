/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SIGNAL_SELECT_CHANNEL_H
#define RTF_HSAB_SIGNAL_SELECT_CHANNEL_H

#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include "rtf_hsab_plots_data.h"
#include "types_hsab.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>
#include "rtf_hsab_signal_select.h"
#include "rtf_hsab_plotter_modules/rtf_hsab_plotter_module_main_signals.h"
#include "rtf_hsab_plotter_modules/rtf_hsab_plotter_module_beamformer_signals.h"
#include "rtf_hsab_plotter_module.h"
#include "rtf_hsab_autoscaler.h"


class SignalSelectChannel: public QGroupBox
{
	Q_OBJECT
	
   public:
	   SignalSelectChannel(QWidget *parent, int channelNumber, QString string, tPlotData *pPlotData, QVector<QwtPlotCurve*>   pPlotCurveTime, QVector<QwtPlotCurve*>   pPlotCurveFreq, /*QVector<QComboBox*>  mpComboBoxSignalSelect,*/ AudioIO *pAudioIO, tSession *ps, ScaleEngine *engine);
	   QVector<QVector<PlotterModule*>>   mQVector2DPlotterSignal;

   signals:
	   void whichBox(int, bool);
	   void sendLabel(int, QString);
	   void sendScales(double, double, int);

   public slots:
	   void currentChannel0Index(int);
	   void currentChannel1Index(int);
	   void currentSignalIndex(int);
	   void currentModuleIndex(int idx);
	   void visibleSignal(bool);
	   void selectOffsetValue(double);
	   void selectFreqValue(double);
	   void updateLabel(int, QString);
	   void updateScale(double, double, int);

   private:
	   UIData						uiData;
	   tPlotData					*mpPlotData;
	   AudioIO						*mpAudioIO;
	   tSession						*mps;

	   T_INT						mChannelNumber;
	   QComboBox			        *mpComboBoxSignalSelect;
	   QComboBox					*mComboBoxModuleSelect;

	   QDoubleSpinBox				*mSpinBoxFreq;
	   QDoubleSpinBox				*mSpinBoxOffset;
	   QCheckBox					*mCheckBoxSelectSignal;

	   QComboBox					*mComboBoxChannelSelect;
	   QComboBox					*mComboBoxChannelSelect1;

	   QVector<QwtPlotCurve*>		mpPlotCurveTime;
	   QVector<QwtPlotCurve*>		mpPlotCurveFreq;

	   QLabel						*LabelChannel;
	   QLabel						*LabelChannel1;
	   QLabel						*LabelFreq;
	   QHBoxLayout					*mHLayout0;
	   QHBoxLayout					*mHLayout1;

	   QVBoxLayout					*mVLayout1;
	   QVBoxLayout					*mVLayout2;
	   QVBoxLayout					*mVLayout3;
	   QVBoxLayout					*mVLayout4;

	   QVector<PlotterModule*>		mQVectorPlotterMainSignals;
	   QVector<PlotterModule*>		mQVectorPlotterBeamformerSignals;
	   QVector<PlotterModule*>		mQVectorPlotterCommomEstimationSignals;
	   QVector<PlotterModule*>		mQVectorPlotterEchoCancellerSignals;
	   QVector<PlotterModule*>		mQVectorPlotterCombinerPostfilterSignals;

};

#endif
