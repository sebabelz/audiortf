/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_PLOTS_DATA_H
#define RTF_HSAB_PLOTS_DATA_H

#include "types_hsab.h"

#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

/*---------------------------------------------------------------------------------*
 * Parameters and variables for the real-time plots
 *---------------------------------------------------------------------------------*/
typedef struct 
{   
	/*-------------------------------------------------------------------------------*
	* Data for time and freq domain plots                                           
	*-------------------------------------------------------------------------------*/
	T_BOOL			bActive;
	T_BOOL			bPlottingTime;
	T_BOOL			bPlottingFreq;
	T_BOOL			bPlottingSpec;

	T_INT           *piModuleIndex;
	T_INT           *piSignalIndex;

	T_DOUBLE			dSignalMemoryInSeconds;
	T_DOUBLE			dFreqMemoryInHertz;
	T_INT			iSignalMemoryLength;
	T_INT			iFreqMemoryLength;
	T_INT			iNumberOfModules;
	T_INT			iNumberOfWidgets;
	T_INT            iNumEchoCompSignals;
	T_INT			iWritePointer;
	T_INT			iSignalPlotSubSampling;
	T_INT			iSignalPlotSubSamplingCounter; 
	T_INT		  iInterpolationModeSpec;	//0=NearestNeighbour | 1=bilinear
	T_INT		  iModuleNumberForSpec;
	T_INT		  iSignalNumberForSpec;
	T_INT		  iSignalSubSamplingRateSpec; //Hz
	T_INT		  iSignalPlottingRateSpec;	//Hz
	T_DOUBLE		 ***pppdSignalMemory;
	T_DOUBLE		 ***pppdFreqMemory;
	T_DOUBLE		   *pdTimeAxis;
	T_DOUBLE		   *pdFreqAxis;

	T_DOUBLE		   *pdOffset;
	T_INT           *piFreq;
	T_INT		   *piChannel1;
	T_INT		   *piChannel2;

	T_DOUBLE          *yLeftLimitTime;
	T_DOUBLE          *yRightLimitTime;

	T_DOUBLE          *yLeftLimitFreq;
	T_DOUBLE          *yRightLimitFreq;

	T_INT          *yLeftLimitSpec;
	T_INT          *zLimitSpec;

	T_INT		  numOfLeftYAxes;
	T_INT		  numOfRightYAxes;

}tPlotData;

#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */