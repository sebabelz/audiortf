/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_MAIN_WINDOW_H
#define RTF_HSAB_MAIN_WINDOW_H

#include <QMainWindow>
#include <QtGui>
#include "rtf_hsab_audio_io.h"
#include <QtWidgets>




/* Speech and Audio Processing */
#include "rtf_hsab_interface_messages.h"
#include "alg_core_hsab_defines.h"

class TabAlgoStructureMainSap; 
class TabAlgoStructureSigSelectInput;
class TabAlgoStructureSigSelectOutput;
class TabAlgoStructureEmptyTab;
class TabRealTimePlots;
class MeasureDebugDetailsDialog;
class AudioDetailsDialog;
class MeasureRecordDetailsDialog;
class TabMemoryManagement;
class TabAlgoStructureBeamformer;


#ifdef BF_SYSTEM
class TabAlgoStructurePostfilter;
#else
#include "rtf_hsab_src.h"
#include "rtf_hsab_eq.h"
#endif


/*--------------------------------------------------------------------------------*
* Sample rate conversion main struct
*--------------------------------------------------------------------------------*/
struct tRtf_hfSrc
{

  /*-------------------------------------------------------------------------------*                              
   * Activation control for sample rate conversion
   *-------------------------------------------------------------------------------*/ 

   T_BOOL   bActiveSrc;
 
  /*-------------------------------------------------------------------------------*                              
   * Main parameters used for sample rate conversion
   *-------------------------------------------------------------------------------*/ 
   T_INT   iNumChannelsUpSamp;
   T_INT   iNumChannelsDownSamp;
   T_INT   iNumBiQuadFilter;
   T_INT   iUpDownSamplingFactor;
   T_INT   iNumCoeff;
   T_INT   iDenCoeff;
   T_INT   iSrcRatio;
  /*-------------------------------------------------------------------------------*                              
   * Filter coefficients for anti-aliasing and anti-imaging filter 
   *-------------------------------------------------------------------------------*/ 
 	T_FLOAT **ppfFilterCoeffNumerator;
	T_FLOAT **ppfFilterCoeffDenominator;

  /*-------------------------------------------------------------------------------*                              
   * Signals
   *-------------------------------------------------------------------------------*/ 
   T_FLOAT ***pppfLastInputDown;
   T_FLOAT ***pppfLastOutputDown;

   T_FLOAT ***pppfLastInputUp;
   T_FLOAT ***pppfLastOutputUp;
  

   T_FLOAT   *pfTempSignal1;
   T_FLOAT   *pfTempSignal2;
};	

struct tSignalSelect {
	T_BOOL  bActiveInput;
	T_BOOL* pbActiveHfInMic;
	T_BOOL* pbActiveHfInPhone;

	T_BOOL  bActiveOutput;
	T_BOOL* pbActiveHfOutLsp;
	T_BOOL* pbActiveHfOutPhone;
};

struct tRecordSelect {
     /*---------------------------------------------------------------------------------*
	  * File handles
	  *---------------------------------------------------------------------------------*/
	  FILE**     fHandleIn;
	  T_CHAR**   ppcfileNameIn;

	  FILE**     fHandleOut;
	  T_CHAR**   ppcfileNameOut;

#ifndef BF_SYSTEM
	  FILE** fHandleCompRtfIn;
	  FILE** fHandleCompRtfOut;
	  FILE** fHandleCompSrcIn;
	  FILE** fHandleCompSrcOut;
#endif

	  T_SHORT**  ppsWriteBufferIn; 
	  T_SHORT**  ppsWriteBufferOut;
	  T_INT      iLocNumAllIn;
	  T_INT      iLocNumAllOut;
	  T_INT      iNumChars;
	 /*---------------------------------------------------------------------------------*
	  * Flags to set recording
	  *---------------------------------------------------------------------------------*/
	  T_BOOL     bRecordStatus;
	  T_BOOL     bRecordAllInputs;
	  T_BOOL*    pbRecordInputDetails;
	  T_BOOL     bRecordAllOutputs;
	  T_BOOL*    pbRecordOutputDetails;
};

struct tSession {
	/* Global parameters */
	struct tRtf_hfSrc sRtf_hfSrcSend;	
	struct tRtf_hfSrc sRtf_hfSrcTts;
	struct tRtf_hfSrc sRtf_hfSrcReceive;	
	struct tRtf_hfSrc sRtf_hfSrcReferenceIn;	
	struct tRtf_hfSrc sRtf_hfSrcRecognizerIn;
	struct tRtf_hfSrc sRtf_hfSrcSendUpSamp;
	struct tRtf_hfSrc sRtf_hfSrcReceiveUpSamp;

	struct	tSignalSelect *psSigSel;
	struct  tRecordSelect *psRecord;
	/* Main signals */
	T_FLOAT **ppfMicInSelect;
	T_FLOAT **ppfPhoneOutSelect;
	T_FLOAT *pfMicInRms;
	T_FLOAT *pfPhoneOutRms;
	T_FLOAT fIntThreshPowRatioAbm;
	
	/* External parameters */
	T_INT iFrameshiftExt;
	T_INT iSampleRateExt;

	T_INT iNumAllIn;  
	T_INT iNumAllOut;
	T_INT iNumExcitationIn;
	T_INT iNumSubbandsShadowFilters;

	

	/*-------------------------------------------------------------------------------*
	* Algorithmic core
	*-------------------------------------------------------------------------------*/
	tHsabAlgCoreMain                 HsabAlgCoreMain;

#ifndef BF_SYSTEM
	//Samplerate conversion struct
	tHsabRtfSrc                  HsabRtfSrcFs;
	tHsabRtfSrc                  HsabRtfSrcRs;
#endif 



};


class AudioIO;

class RtfMainWindow : public QMainWindow
{
   Q_OBJECT

   public:
      RtfMainWindow();
      ~RtfMainWindow();

	  T_INT RtfCalloc1DArray(void** ppPtr, const T_INT iDim1, const T_INT iSize);
	  void  RtfDeCalloc1DArray(void** ppPtr);
	  static int const REBOOT_CODE;
   protected:
	   
   signals:

   public slots:
	   
      void startAudioProcessing();
      void stopAudioProcessing();
	  void setPauseButtonState();
	  void setContinueButtonState();
	  void selectSoundcardInput(bool); 
	  void switchToHandsFreeCore();
	  void switchToSigSelectionIn();
	  void switchToSigSelectionOut();
	  void restartApp();

   private:
     /*-------------------------------------------------------------------------------*
      * Widgets used within main window		
      *-------------------------------------------------------------------------------*/
      QPushButton                      *mPushButtonAudioStart;	 
      QPushButton                      *mPushButtonAudioStop;

	  QPushButton					   *mPushButtonAudioPause;
	  QPushButton					   *mPushButtonAudioContinue;

	  MeasureDebugDetailsDialog		   *mMeasureDebugDetailsDialog;

	  QPushButton                      *mPushButtonInputs;
	  QPushButton                      *mPushButtonOutputs;
	  QPushButton					   *PushButtonDebug;

	  MeasureRecordDetailsDialog       *mMeasureRecordDetailsDialog;

      QGroupBox                        *mGroupBoxAlgStructures;

      QTabWidget                       *mTabWidgetAlgStructures;
	  QTabWidget					   *mchildTabWidgetSigSel;
	  QTabWidget					   *mchildTabWidgetPostProc;
	  QTabWidget					   *mchildTabWidgetPostProcLsp;
	  QTabWidget					   *mchildTabWidgetPostProcPhone;
	  QTabWidget					   *mchildTabWidgetPostProcPhoneLimiter;
	  QTabWidget					   *mchildTabWidgetPostProcLspLimiter;
	  QTabWidget                       *mchildTabWidgetEcho;
	  QTabWidget                       *mchildTabWidgetEcControl;
	  QTabWidget                       *mchildTabWidgetMse;
	  QTabWidget					   *mchildTabWidgetMseCombiner;
	  QTabWidget					   *mchildTabWidgetMseMixer;
	  QTabWidget                       *mchildTabWidgetMsePostfilter;

	  
	  QHBoxLayout					   *MseCombinerLayout;
	  QHBoxLayout					   *MsePostfilterLayout;

	 /*-------------------------------------------------------------------------------*
      * Tabs		
      *-------------------------------------------------------------------------------*/

      TabAlgoStructureMainSap							  *mMainSapTab;
	  TabAlgoStructureEmptyTab							  *mSigSelectionTab;
	  TabAlgoStructureSigSelectInput                      *mSigSelectionHfInput;
	  TabAlgoStructureSigSelectOutput                     *mSigSelectionHfOutput;
	  
#ifdef BF_SYSTEM
	  TabAlgoStructureEmptyTab                            *mSynFbPostProTab;
#else
	  TabEqualizerTab									  *mSynFbPostProTab;
#endif // BF_SYSTEM

	  TabAlgoStructureEmptyTab							  *mSynFbPostProLspTab;
	  TabAlgoStructureEmptyTab							  *mSynFbPostProPhoneTab;
	  TabAlgoStructureBeamformer						  *mBeamformerTab;
#ifdef BF_SYSTEM
	  TabAlgoStructurePostfilter* mPostfilterTab;
#endif 




	  TabRealTimePlots									  *mRealTimePlotsTab;
	  QCheckBox											  *mViewParamsCheckBox;
	  QGroupBox										      *mGroupBoxVisualizations;
	  QTabWidget										  *mTabWidgetVisualizations;

	  TabMemoryManagement								  *mMemManagementTab;

     /*-------------------------------------------------------------------------------*
      * Algorithmic core		
      *-------------------------------------------------------------------------------*/
	  tSession						   session;
	  tSession						  *ps;
	  T_INT						       miIndexBeamPost;

	 /*-------------------------------------------------------------------------------*
	  * AudioIO
	  *-------------------------------------------------------------------------------*/
	  AudioIO                         *mAudio;
};

#endif 
