/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "types_hsab.h"
#include "rtf_hsab_interface_messages.h"
#include "rtf_hsab_dialog_debug.h"

class DebugMessageInterface sDebugMessageInterface;

/* Send debug message -------------------------------------------------------------*/
void SendDebugMessage(const char *message, tMessageType type)
{
   sDebugMessageInterface.addMessage(message, type );
}
