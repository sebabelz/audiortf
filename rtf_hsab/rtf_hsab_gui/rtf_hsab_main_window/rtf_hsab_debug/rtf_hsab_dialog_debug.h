/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_DIALOG_DEBUG_H
#define RTF_HSAB_DIALOG_DEBUG_H

#include <QDialog> 
#include <QtGui>
#include "types_hsab.h"
#include <QtWidgets>

class DebugMessage
{
   public:
      DebugMessage(QString qsDate, QString qsTime, QString qsMessage, tMessageType mtType);
      ~DebugMessage();

   private:
      tMessageType mtType;
	  T_INT iColumn;
	  QTreeWidgetItem* debugItem;
};


class DebugMessageInterface
{
   public:
      DebugMessageInterface();
	  void addMessage(const char *message, tMessageType type);

   private:

};

class MeasureDebugDetailsDialog: public QDialog
{
   Q_OBJECT

   public:
	    MeasureDebugDetailsDialog( QWidget *parent );
		~MeasureDebugDetailsDialog();
   public slots:
	   void toggleWarning(bool);
	   void toggleDebug(bool);
	   void toggleInfo(bool);
	   void toggleError(bool);

   private:
	   QTreeWidget				*tree;
	   QVBoxLayout				*MainLayout;

	   QCheckBox				*mcheckWarning;
	   QCheckBox				*mcheckInfo;
	   QCheckBox				*mcheckDebug;
	   QCheckBox				*mcheckError;

	   QList<QTreeWidgetItem*>  mQListWarningItems;
	   QList<QTreeWidgetItem*>  mQListInfoItems;
	   QList<QTreeWidgetItem*>  mQListDebugItems;
	   QList<QTreeWidgetItem*>  mQListErrorItems;
};
#endif