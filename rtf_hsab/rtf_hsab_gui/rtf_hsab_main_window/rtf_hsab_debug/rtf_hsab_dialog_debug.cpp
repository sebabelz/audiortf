/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_dialog_debug.h"
#include "types_hsab.h"
#include <QString>
#include <QTime>
#include <QDate>

QList<QTreeWidgetItem*>  msQListDebugItems;
DebugMessage::DebugMessage(QString qsDate, QString qsTime, QString qsMessage, tMessageType mtType)
{
   iColumn = (T_INT) 0;
   debugItem = new QTreeWidgetItem();
   debugItem->setText(iColumn, qsDate);
   iColumn++;
   debugItem->setText(iColumn, qsTime);
   iColumn++;
   switch(mtType)
   {
      case MessageTypeDebug:
         debugItem->setText(iColumn, QString("Debug"));
      break;

      case MessageTypeInfo:
         debugItem->setText(iColumn, QString("Info"));
      break;

      case MessageTypeWarning:
         debugItem->setText(iColumn, QString("Warning"));
      break;
         
      case MessageTypeError:
		    debugItem->setTextColor(iColumn, QColor("red"));
            debugItem->setText(iColumn, QString("Error"));
      break;

      default:;
   }
   iColumn++;
   if(mtType == MessageTypeError)
   debugItem->setTextColor(iColumn, QColor("red"));
   debugItem->setText(iColumn, qsMessage);

   msQListDebugItems.append(debugItem);
}

DebugMessage::~DebugMessage()
{
}

DebugMessageInterface::DebugMessageInterface()
{
}

void DebugMessageInterface::addMessage(const char *message, tMessageType type)
{
   QDate       date;
   QTime       time;

   /* Get current date and time ------------------------------------------------------*/
   date = date.currentDate();
   time = time.currentTime();

   /* Send data to store in list -----------------------------------------------------*/
   DebugMessage *psMessage;
   psMessage = new DebugMessage(date.toString("dd.MM.yyyy"), time.toString("hh:mm:ss,zzz"), message, type);

}


MeasureDebugDetailsDialog::MeasureDebugDetailsDialog( QWidget *parent )
: QDialog(parent) 
{

  /*-----------------------------------------------------------------------------------------*
   * Main layout
   *-----------------------------------------------------------------------------------------*/
   T_INT   iColumnWidth = (T_INT) 120;
   QVBoxLayout *MainLayout = new QVBoxLayout(this);
   setWindowTitle(tr("Messages"));
   setMinimumHeight(350);
   setFixedWidth(800);

  /*-----------------------------------------------------------------------------------------*
   * Tree widget
   *-----------------------------------------------------------------------------------------*/
    tree = new QTreeWidget(this);
	tree->setColumnCount(4);
	tree->setColumnWidth(0,iColumnWidth);
	tree->setColumnWidth(1,iColumnWidth);
	tree->setColumnWidth(2,iColumnWidth);
	tree->setColumnWidth(3,iColumnWidth);
	tree->setAlternatingRowColors(true);
   	QTreeWidgetItem* headerItem = new QTreeWidgetItem();

	headerItem->setText(0,QString("Date"));
	headerItem->setText(1,QString("Time"));
	headerItem->setText(2,QString("Type"));
	headerItem->setText(3,QString("Message"));

	tree->setHeaderItem(headerItem);
  /*-----------------------------------------------------------------------------------------*
   * Add all items to tree
   *-----------------------------------------------------------------------------------------*/
    tree->addTopLevelItems(msQListDebugItems);

  /*-----------------------------------------------------------------------------------------*
   * Sort items to toggle visibility
   *-----------------------------------------------------------------------------------------*/
    mQListInfoItems    = tree->findItems("Info", Qt::MatchExactly, 2);
    mQListWarningItems = tree->findItems("Warning", Qt::MatchExactly, 2);
    mQListDebugItems   = tree->findItems("Debug", Qt::MatchExactly, 2);
    mQListErrorItems   = tree->findItems("Error", Qt::MatchExactly, 2);

  /*-----------------------------------------------------------------------------------------*
   * CheckBoxes and Close button
   *-----------------------------------------------------------------------------------------*/
    QHBoxLayout *SubLayoutBottom = new QHBoxLayout(this);
    QPushButton *PushButtonClose = new QPushButton(tr("&Close"), this);

    mcheckWarning = new QCheckBox("Warning");
    connect(mcheckWarning, SIGNAL(toggled(bool)), this, SLOT(toggleWarning(bool)));
    mcheckWarning->setChecked(true);

    QCheckBox *mcheckInfo = new QCheckBox("Info");
    connect(mcheckInfo, SIGNAL(toggled(bool)), this, SLOT(toggleInfo(bool)));
    mcheckInfo->setChecked(true);

    QCheckBox *mcheckDebug = new QCheckBox("Debug");
    connect(mcheckDebug, SIGNAL(toggled(bool)), this, SLOT(toggleDebug(bool)));
    mcheckDebug->setChecked(true);

    QCheckBox *mcheckError = new QCheckBox("Error");
    connect(mcheckError, SIGNAL(toggled(bool)), this, SLOT(toggleError(bool)));
    mcheckError->setChecked(true);

    PushButtonClose->setMinimumWidth(100);

  /*-----------------------------------------------------------------------------------------*
   * Add child widgets/layouts to the main layout
   *-----------------------------------------------------------------------------------------*/
    connect(PushButtonClose, SIGNAL(clicked()), this, SLOT(close()));
    SubLayoutBottom->addWidget(mcheckInfo);
    SubLayoutBottom->addWidget(mcheckWarning);
    SubLayoutBottom->addWidget(mcheckDebug);
    SubLayoutBottom->addWidget(mcheckError);
    SubLayoutBottom->addStretch();	
    SubLayoutBottom->addWidget(PushButtonClose);
 
    MainLayout->addWidget(tree);
    MainLayout->addLayout(SubLayoutBottom);

}

void MeasureDebugDetailsDialog::toggleInfo(bool bAct)
{
   for(int i = 0; i<mQListInfoItems.count(); i++)
   {
	  mQListInfoItems[i]->setHidden(!bAct);
   }
}
void MeasureDebugDetailsDialog::toggleWarning(bool bAct)
{
   for(int i = 0; i<mQListWarningItems.count(); i++)
   {
      mQListWarningItems[i]->setHidden(!bAct);
   }
}

void MeasureDebugDetailsDialog::toggleDebug(bool bAct)
{
   for(int i = 0; i<mQListDebugItems.count(); i++)
   {
      mQListDebugItems[i]->setHidden(!bAct);
   }
}
void MeasureDebugDetailsDialog::toggleError(bool bAct)
{
   for(int i = 0; i<mQListErrorItems.count(); i++)
   {
      mQListErrorItems[i]->setHidden(!bAct);
   }
}

MeasureDebugDetailsDialog::~MeasureDebugDetailsDialog()
{
   msQListDebugItems.clear();
}