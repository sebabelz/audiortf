/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_INTERFACE_MESSAGES_H
#define RTF_HSAB_INTERFACE_MESSAGES_H

#include "types_hsab.h"
#ifdef __cplusplus

extern "C"
{
#endif /*---- C++ -----*/

void SendDebugMessage(const char *message, tMessageType type);

#ifdef __cplusplus
}
#endif /*---- C++ -----*/

#endif 
