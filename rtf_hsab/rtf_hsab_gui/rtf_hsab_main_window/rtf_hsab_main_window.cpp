/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QtGui>
#include <QTabWidget>
#include <QSizePolicy>
#include <QtWidgets>
#include <QPixmap>
#include <QIcon>
#include <QtSvg>
#include <QStyleFactory>

#include "rtf_hsab_main_window.h"
#include "rtf_hsab_audio_select.h"
#include "rtf_hsab_audio_measure_record.h"
#include "rtf_hsab_dialog_debug.h"
#include "rtf_hsab_alg_struct_main.h"
#include "rtf_hsab_alg_struct_empty.h"
#include "rtf_hsab_alg_struct_signal_select_input.h"
#include "rtf_hsab_alg_struct_signal_select_output.h"
#include "rtf_hsab_mem_manag_visual.h"
#include "rtf_hsab_plots_visual.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
/* ------ Algcore includes ------*/
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_main.h"
#include "alg_core_hsab_memory_allocation.h"

#ifdef BF_SYSTEM
#include "rtf_hsab_beamformer_algorithmic_main.h"
#include "rtf_hsab_postfilter_algorithmic_main.h"
#else
#include "rtf_hsab_eq.h"
#endif 


int const RtfMainWindow::REBOOT_CODE = -234506789;

RtfMainWindow::RtfMainWindow()
{
	SendDebugMessage("Starting TH-AB - Real Time Framework...", MessageTypeInfo);
	int iMainButtonWidth = 100 - 30;
	int iMainButtonHeight = 26;
	int iMaxHeight, iError = 0, i;

	qputenv("QT_AUTO_SCREEN_SCALE_FACTOR", "1");

	/*---------------------------------------------------------------------------------------------*
	 * Algorithmic core
	 *---------------------------------------------------------------------------------------------*/
	ps = &session;

	/*---------------------------------------------------------------------------------------------*
	* Algorithmic core
	*---------------------------------------------------------------------------------------------*/
	HsabAlgCoreInit(&(ps->HsabAlgCoreMain));

#ifdef BF_SYSTEM
	/*----- External parameters -----*/
	ps->iFrameshiftExt = 160 * 3;
	ps->iSampleRateExt = 48000;

	/*----- Signal Selection Hf-Input activation-----*/
	ps->psSigSel = (tSignalSelect*)calloc(1, sizeof(struct tSignalSelect));

	ps->psSigSel->bActiveInput = HSAB_TRUE;
	ps->psSigSel->pbActiveHfInMic = (T_BOOL*)calloc(ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn, sizeof (T_BOOL));
	for (i = 0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
	{
		ps->psSigSel->pbActiveHfInMic[i] = HSAB_TRUE;
	}

	ps->psSigSel->pbActiveHfInPhone = (T_BOOL*)calloc(ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn, sizeof (T_BOOL));
	for (i = 0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn; i++)
	{
		ps->psSigSel->pbActiveHfInPhone[i] = HSAB_TRUE;
	}

	/*----- Signal Selection Hf-Output activation-----*/
	ps->psSigSel->bActiveOutput = HSAB_TRUE;
	ps->psSigSel->pbActiveHfOutLsp = (T_BOOL*)calloc(ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl, sizeof (T_BOOL));
	for (i = 0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
	{
		ps->psSigSel->pbActiveHfOutLsp[i] = HSAB_TRUE;
	}

	ps->psSigSel->pbActiveHfOutPhone = (T_BOOL*)calloc(ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut, sizeof (T_BOOL));
	for (i = 0; i < ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; i++)
	{
		ps->psSigSel->pbActiveHfOutPhone[i] = HSAB_TRUE;
	}

  /*---------------------------------------------------------------------------------------------*
   * Recording
   *---------------------------------------------------------------------------------------------*/
   ps->psRecord = (tRecordSelect*) calloc(1,sizeof(struct tRecordSelect));

   ps->psRecord->bRecordStatus               =  HSAB_FALSE;
   ps->psRecord->iNumChars                   =  CONFIG_KEY_LENGTH;
   ps->psRecord->iLocNumAllIn = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn
	   + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRefInFromAmp;
   ps->psRecord->iLocNumAllOut = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut;

   ps->psRecord->fHandleIn  = (FILE**) calloc (ps->psRecord->iLocNumAllIn, sizeof (FILE*));
   ps->psRecord->fHandleOut = (FILE**) calloc (ps->psRecord->iLocNumAllOut, sizeof (FILE*));

   ps->psRecord->pbRecordInputDetails = (T_BOOL*) calloc (ps->psRecord->iLocNumAllIn, sizeof (T_BOOL));	
   for (i=0; i<ps->psRecord->iLocNumAllIn; i++ )
   {
	  ps->psRecord->pbRecordInputDetails[i]  = HSAB_FALSE;
   }

   ps->psRecord->pbRecordOutputDetails = (T_BOOL*) calloc (ps->psRecord->iLocNumAllOut, sizeof (T_BOOL));
   for (i=0; i<ps->psRecord->iLocNumAllOut; i++ )
   {
	  ps->psRecord->pbRecordOutputDetails[i] = HSAB_FALSE;
   }

   ps->psRecord->bRecordAllInputs            =  HSAB_TRUE;
   ps->psRecord->bRecordAllOutputs           =  HSAB_TRUE;

  /*---------------------------------------------------------------------------------------------*
   * Activation parameters used for sample rate conversion 
   *---------------------------------------------------------------------------------------------*/

	  ps->sRtf_hfSrcSend.bActiveSrc          = HSAB_TRUE;
	  ps->sRtf_hfSrcReceive.bActiveSrc       = HSAB_TRUE;
	  ps->sRtf_hfSrcTts.bActiveSrc           = HSAB_TRUE;
	  ps->sRtf_hfSrcReferenceIn.bActiveSrc   = HSAB_TRUE;
	  ps->sRtf_hfSrcRecognizerIn.bActiveSrc  = HSAB_TRUE;
#else
	//Init of the samplerate conversion
	HsabRtfSrcUpDownInit(&(ps->HsabRtfSrcFs),
		&(ps->HsabAlgCoreMain.HsabAlgCoreParams),
		&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement),
		ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat,
		ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumLspOutFrontSeat);

	ps->psRecord = (tRecordSelect*)calloc(1, sizeof(struct tRecordSelect));
	ps->psRecord->fHandleCompRtfIn = (FILE * *)calloc(1, sizeof(FILE*));
	ps->psRecord->fHandleCompRtfOut = (FILE * *)calloc(1, sizeof(FILE*));
	ps->psRecord->fHandleCompSrcIn = (FILE * *)calloc(1, sizeof(FILE*));
	ps->psRecord->fHandleCompSrcOut = (FILE * *)calloc(1, sizeof(FILE*));

	/*----- External parameters -----*/
	ps->iFrameshiftExt = ps->HsabAlgCoreMain.HsabAlgCoreParams.iFrameshiftExt;
	ps->iSampleRateExt = ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateExt;
#endif // BF_SYSTEM
/*--------------------------------------------------------------*/
   SendDebugMessage("Initialization of audio buffers...", MessageTypeDebug);
   if(!iError)
   {
#ifdef BF_SYSTEM
	  RtfCalloc1DArray((void **)&(ps->ppfMicInSelect),
		  ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn,
		  sizeof(T_INT));

	  RtfCalloc1DArray((void **)&(ps->ppfPhoneOutSelect),
		  ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut,
		  sizeof(T_INT));

	  HsabAlgCoreCalloc2DArray((void ***)&(ps->psRecord->ppsWriteBufferIn),
		  ps->psRecord->iLocNumAllIn,
		  ps->iFrameshiftExt,
		  sizeof(T_SHORT),
		  sizeof(T_SHORT*),
		  "RtfMainWindow",
		  "ppsWriteBufferIn",
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytesSignals));


	  HsabAlgCoreCalloc2DArray((void ***)&(ps->psRecord->ppsWriteBufferOut),
		  ps->psRecord->iLocNumAllOut,
		  ps->iFrameshiftExt,
		  sizeof(T_SHORT),
		  sizeof(T_SHORT*),
		  "RtfMainWindow",
		  "ppsWriteBufferOut",
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytesSignals));


	  HsabAlgCoreCalloc2DArray((void ***)&(ps->psRecord->ppcfileNameIn),
		  ps->psRecord->iLocNumAllIn,
		  ps->psRecord->iNumChars,
		  sizeof(T_CHAR),
		  sizeof(T_CHAR*),
		  "RtfMainWindow",
		  "ppcfileNameIn",
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytesSignals));


	  HsabAlgCoreCalloc2DArray((void ***)&(ps->psRecord->ppcfileNameOut),
		  ps->psRecord->iLocNumAllOut,
		  ps->psRecord->iNumChars,
		  sizeof(T_CHAR),
		  sizeof(T_CHAR*),
		  "RtfMainWindow",
		  "ppcfileNameOut",
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytes),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals),
		  &(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iMaxDynMemInBytesSignals));


	  RtfCalloc1DArray((void **)&(ps->pfMicInRms),
		  ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn,
		  sizeof(T_FLOAT));


	  RtfCalloc1DArray((void **)&(ps->pfPhoneOutRms),
		  ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut,
		  sizeof(T_FLOAT));
#endif

	if(!iError)
	SendDebugMessage("Initialization of audio buffers successful", MessageTypeDebug);
	else
	SendDebugMessage("Initialization of audio buffers failed!", MessageTypeError);
   }
#ifdef BF_SYSTEM
   ps->iNumAllIn = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn
	   + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumTtsIn
	   + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRefInFromAmp
	   + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn;

   ps->iNumAllOut = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut
	   + ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl;
#else
   ps->iNumAllIn = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumAllIn;
   ps->iNumAllOut = ps->HsabAlgCoreMain.HsabAlgCoreParams.iNumAllOut;
#endif



 
  /*---------------------------------------------------------------------------------------------*
   * AudioIO
   *---------------------------------------------------------------------------------------------*/
   mAudio = new AudioIO( ps );

  /*---------------------------------------------------------------------------------------------*
   * Main window
   *---------------------------------------------------------------------------------------------*/
   QWidget *widget = new QWidget;
   setCentralWidget(widget);   

  /*---------------------------------------------------------------------------------------------*
   * Main layout
   *---------------------------------------------------------------------------------------------*/
   QVBoxLayout *MainLayout = new QVBoxLayout;

  /*---------------------------------------------------------------------------------------------*
   * Top layout
   *---------------------------------------------------------------------------------------------*/
   {
      QHBoxLayout *TopLayout = new QHBoxLayout;
      iMaxHeight = 88;

      TopLayout->addStretch();
   }

  /*---------------------------------------------------------------------------------------------*
   * Second layout
   *---------------------------------------------------------------------------------------------*/
   SendDebugMessage("Initialization of algorithmic structures...", MessageTypeDebug); 
   {
		QHBoxLayout *SecondLayout = new QHBoxLayout;
       
		QVBoxLayout* logoLayout = new QVBoxLayout;

		/*------------------------------------------------------------------------------------------*
		* Logo
		*------------------------------------------------------------------------------------------*/
		QSvgWidget* svgWidget = new QSvgWidget("th_ab.svg");
		//svgWidget->setFixedSize(140, 60);
	
		
		/*-----------------------------------------------------------------------------------------*
		* Main control
		*-----------------------------------------------------------------------------------------*/
		QVBoxLayout *MainControlLayout = new QVBoxLayout;
		QGroupBox* mGroupBoxMainControl = new QGroupBox(tr("Main control"));
		svgWidget->setFixedSize(160, 120);
		
		logoLayout->addWidget(svgWidget, 0, Qt::AlignCenter);
		
		mGroupBoxMainControl->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		mGroupBoxMainControl->setMinimumHeight(350);
		mGroupBoxMainControl->setMaximumHeight(350);
		mGroupBoxMainControl->setMinimumWidth(200);
		mGroupBoxMainControl->setMaximumWidth(200);


		/*------------------------------------------------------------------------------------------*
		* Top layout - AudioIO  [start / stop and pause / continue]
		*------------------------------------------------------------------------------------------*/
		QGridLayout *TopLayoutAudio = new QGridLayout;
		QGroupBox   *GroupBoxAudioControl = new QGroupBox(tr("Audio control"));


		GroupBoxAudioControl->setMaximumHeight(iMaxHeight);
        mPushButtonAudioStart = new QPushButton(tr("&Start"));
		mPushButtonAudioStop = new QPushButton(tr("&Stop"));
		mPushButtonAudioPause = new QPushButton(tr("&Pause"));
		mPushButtonAudioContinue = new QPushButton(tr("&Continue"));

		connect(mPushButtonAudioPause, SIGNAL(clicked()), this, SLOT(stopAudioProcessing()));
		connect(mPushButtonAudioPause, SIGNAL(clicked()), this, SLOT(setPauseButtonState()));
		connect(mPushButtonAudioContinue, SIGNAL(clicked()), this, SLOT(startAudioProcessing()));
		connect(mPushButtonAudioContinue, SIGNAL(clicked()), this, SLOT(setContinueButtonState()));

		mPushButtonAudioStart->setFixedWidth(iMainButtonWidth);
		mPushButtonAudioStart->setFixedHeight(iMainButtonHeight);
		mPushButtonAudioStop->setFixedWidth(iMainButtonWidth);
		mPushButtonAudioStop->setFixedHeight(iMainButtonHeight);
		mPushButtonAudioPause->setFixedWidth(iMainButtonWidth);
		mPushButtonAudioPause->setFixedHeight(iMainButtonHeight);
		mPushButtonAudioContinue->setFixedWidth(iMainButtonWidth);
		mPushButtonAudioContinue->setFixedHeight(iMainButtonHeight);

		mPushButtonAudioStop->setDisabled(true);
		mPushButtonAudioPause->setDisabled(true);
		mPushButtonAudioContinue->setDisabled(true);

		connect(mPushButtonAudioStart, SIGNAL(clicked()), this, SLOT(startAudioProcessing()));
		connect(mPushButtonAudioStop, SIGNAL(clicked()), this, SLOT(stopAudioProcessing()));

		TopLayoutAudio->addWidget(mPushButtonAudioStart, 0, 0);
		TopLayoutAudio->addWidget(mPushButtonAudioStop, 0, 1);

		GroupBoxAudioControl->setLayout(TopLayoutAudio);

		MainControlLayout->addWidget(GroupBoxAudioControl);

		/*------------------------------------------------------------------------------------------*
		* Top layout - AudioIO selection (soundcard vs file i/o)
		*------------------------------------------------------------------------------------------*/
		QGridLayout *TopLayoutAudioSelection = new QGridLayout;
		QGroupBox   *GroupBoxAudioSelection = new QGroupBox(tr("Audio I/O"));
		GroupBoxAudioSelection->setMaximumHeight(iMaxHeight);

		mPushButtonInputs = new QPushButton(tr("&Inputs"));
		mPushButtonOutputs = new QPushButton(tr("&Outputs"));

		TopLayoutAudioSelection->addWidget(mPushButtonInputs, 0, 0);
		TopLayoutAudioSelection->addWidget(mPushButtonOutputs, 0, 1);
		mPushButtonInputs->setFixedWidth(iMainButtonWidth);
		mPushButtonInputs->setFixedHeight(iMainButtonHeight);
		mPushButtonOutputs->setFixedWidth(iMainButtonWidth);
		mPushButtonOutputs->setFixedHeight(iMainButtonHeight);

		mAudio->mAudioDetailsDialog = new AudioDetailsDialog(mAudio, &(ps->HsabAlgCoreMain.HsabAlgCoreParams), this);
		connect(mPushButtonInputs, SIGNAL(clicked()), mAudio->mAudioDetailsDialog, SLOT(showInputsTab()));
		connect(mPushButtonOutputs, SIGNAL(clicked()), mAudio->mAudioDetailsDialog, SLOT(showOutputsTab()));
		GroupBoxAudioSelection->setLayout(TopLayoutAudioSelection);

		MainControlLayout->addWidget(GroupBoxAudioSelection);

		/*------------------------------------------------------------------------------------------*
		* Top layout - Recordings/Measurements/Messages (Tools)
		*------------------------------------------------------------------------------------------*/
		QGridLayout *TopLayoutMeasurementRecording = new QGridLayout;
		QGroupBox   *GroupBoxMeasurementRecording = new QGroupBox(tr("Tools"));
		GroupBoxMeasurementRecording->setMaximumHeight(iMaxHeight);

		QPalette p = GroupBoxMeasurementRecording->palette();
		p.setColor(QPalette::ButtonText, Qt::blue);
		GroupBoxMeasurementRecording->setPalette(p);

		QPushButton *PushButtonRecording = new QPushButton(tr("Record"));
		PushButtonDebug = new QPushButton(tr("Messages"));
		PushButtonDebug->setFixedWidth(iMainButtonWidth);
		PushButtonDebug->setFixedHeight(iMainButtonHeight);

		mMeasureRecordDetailsDialog = new MeasureRecordDetailsDialog(mAudio, this, ps);
		connect(PushButtonRecording, SIGNAL(clicked()), mMeasureRecordDetailsDialog, SLOT(showRecordingTab()));

		PushButtonRecording->setFixedWidth(iMainButtonWidth);
		PushButtonRecording->setFixedHeight(iMainButtonHeight);

		TopLayoutMeasurementRecording->addWidget(PushButtonRecording, 0, 0);
		TopLayoutMeasurementRecording->addWidget(PushButtonDebug, 0, 1);

		GroupBoxMeasurementRecording->setLayout(TopLayoutMeasurementRecording);
		MainControlLayout->addWidget(GroupBoxMeasurementRecording);

		/*------------------------------------------------------------------------------------------*
		* Top layout - GUI control
		*------------------------------------------------------------------------------------------*/
		QGridLayout *TopLayoutApplicationControl = new QGridLayout;
		QGroupBox   *GroupBoxApplicationControl = new QGroupBox(tr("GUI control"));

		GroupBoxApplicationControl->setMaximumHeight(iMaxHeight);
		QPushButton *PushButtonExit = new QPushButton(tr("Exit"));
		QPushButton *PushButtonRestart = new QPushButton(tr("Restart"));

		PushButtonExit->setFixedWidth(iMainButtonWidth);
		PushButtonExit->setFixedHeight(iMainButtonHeight);

		PushButtonRestart->setFixedWidth(iMainButtonWidth);
		PushButtonRestart->setFixedHeight(iMainButtonHeight);

		connect(PushButtonExit, SIGNAL(clicked()), this, SLOT(stopAudioProcessing()));
		connect(PushButtonExit, SIGNAL(clicked()), this, SLOT(close()));
		connect(PushButtonRestart, SIGNAL(clicked()), this, SLOT(restartApp()));

		TopLayoutApplicationControl->addWidget(PushButtonExit, 0, 0);
		TopLayoutApplicationControl->addWidget(PushButtonRestart, 0, 1);

		GroupBoxApplicationControl->setLayout(TopLayoutApplicationControl);
		MainControlLayout->addWidget(GroupBoxApplicationControl);

		/*-----------------------------------------------------------------------------------------*
		* Algorithmic structure layout
		*-----------------------------------------------------------------------------------------*/
		QHBoxLayout *AlgStrutureLayout      = new QHBoxLayout;

		mGroupBoxAlgStructures = new QGroupBox(tr("Algorithmic structures"));
		mGroupBoxAlgStructures->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		mGroupBoxAlgStructures->setMinimumHeight(350);
		mGroupBoxAlgStructures->setMaximumHeight(350);

		mTabWidgetAlgStructures     = new QTabWidget();
		mTabWidgetAlgStructures->setMinimumHeight(300);
		mTabWidgetAlgStructures->setMaximumHeight(300);

		mchildTabWidgetSigSel       = new QTabWidget();
		mchildTabWidgetSigSel ->setMinimumHeight(260);
		mchildTabWidgetSigSel ->setMaximumHeight(260);
		mchildTabWidgetSigSel ->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetPostProc     = new QTabWidget();
		mchildTabWidgetPostProc->setMinimumHeight(260);
		mchildTabWidgetPostProc->setMaximumHeight(260);
		mchildTabWidgetPostProc->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetPostProcLsp     = new QTabWidget();
		mchildTabWidgetPostProcLsp->setMinimumHeight(220);
		mchildTabWidgetPostProcLsp->setMaximumHeight(220);
		mchildTabWidgetPostProcLsp->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetPostProcLspLimiter    = new QTabWidget();
		mchildTabWidgetPostProcLspLimiter->setMinimumHeight(153);
		mchildTabWidgetPostProcLspLimiter->setMaximumHeight(153);
		mchildTabWidgetPostProcLspLimiter->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetPostProcPhone    = new QTabWidget();
		mchildTabWidgetPostProcPhone->setMinimumHeight(220);
		mchildTabWidgetPostProcPhone->setMaximumHeight(220);
		mchildTabWidgetPostProcPhone->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetPostProcPhoneLimiter    = new QTabWidget();
		mchildTabWidgetPostProcPhoneLimiter->setMinimumHeight(153);
		mchildTabWidgetPostProcPhoneLimiter->setMaximumHeight(153);
		mchildTabWidgetPostProcPhoneLimiter->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetEcho       = new QTabWidget();
		mchildTabWidgetEcho->setMinimumHeight(260);
		mchildTabWidgetEcho->setMaximumHeight(260);
		mchildTabWidgetEcho->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetEcControl      = new QTabWidget();
		mchildTabWidgetEcControl ->setMinimumHeight(220);
		mchildTabWidgetEcControl ->setMaximumHeight(220);
		mchildTabWidgetEcControl ->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetMse       = new QTabWidget();
		mchildTabWidgetMse->setMinimumHeight(260);
		mchildTabWidgetMse->setMaximumHeight(260);
		mchildTabWidgetMse->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetMseCombiner =  new QTabWidget();
		mchildTabWidgetMseCombiner ->setMinimumHeight(220);
		mchildTabWidgetMseCombiner ->setMaximumHeight(220);
		mchildTabWidgetMseCombiner->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetMseMixer =  new QTabWidget();
		mchildTabWidgetMseMixer ->setMinimumHeight(220);
		mchildTabWidgetMseMixer ->setMaximumHeight(220);
		mchildTabWidgetMseMixer->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		mchildTabWidgetMsePostfilter =  new QTabWidget();
		mchildTabWidgetMsePostfilter ->setMinimumHeight(180);
		mchildTabWidgetMsePostfilter ->setMaximumHeight(180);
		mchildTabWidgetMsePostfilter ->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

		connect(mGroupBoxAlgStructures, SIGNAL(toggled(bool)), mTabWidgetAlgStructures, SLOT(setVisible(bool)));

		/*---------------------------------------------------------------------------------------*
		* Add individual tabs
		*---------------------------------------------------------------------------------------*/
		mMainSapTab = new TabAlgoStructureMainSap(&(ps->HsabAlgCoreMain), ps->psSigSel, &(ps->sRtf_hfSrcSend), &(ps->sRtf_hfSrcReceive), &(ps->sRtf_hfSrcTts), &(ps->sRtf_hfSrcReferenceIn), &(ps->sRtf_hfSrcRecognizerIn));
		mTabWidgetAlgStructures->addTab(mMainSapTab, tr("Main"));   
		connect(mMainSapTab, SIGNAL(switchToHandsFreeCore()), this, SLOT(switchToHandsFreeCore()));
		connect(mMainSapTab, SIGNAL(switchToSigSelectionIn()), this, SLOT(switchToSigSelectionIn()));
		connect(mMainSapTab, SIGNAL(switchToSigSelectionOut()), this, SLOT(switchToSigSelectionOut()));

        mSigSelectionTab = new TabAlgoStructureEmptyTab();
		mTabWidgetAlgStructures->addTab( mSigSelectionTab,"Signal selection");
#ifdef BF_SYSTEM
		mBeamformerTab = new TabAlgoStructureBeamformer(&(ps->HsabAlgCoreMain), 0);
		mTabWidgetAlgStructures->addTab(mBeamformerTab, tr("Beamformer"));
		mTabWidgetAlgStructures->setCurrentWidget(mMainSapTab);

		mPostfilterTab = new TabAlgoStructurePostfilter(&(ps->HsabAlgCoreMain), 0);
		mTabWidgetAlgStructures->addTab(mPostfilterTab, tr("Postfilter"));
		mTabWidgetAlgStructures->setCurrentWidget(mMainSapTab);

		mSynFbPostProTab = new TabAlgoStructureEmptyTab();
		mTabWidgetAlgStructures->addTab(mSynFbPostProTab, tr("Syn. filterbank and postproc."));
#else

        mSynFbPostProTab = new TabEqualizerTab(&(ps->HsabAlgCoreMain), 0);

        mTabWidgetAlgStructures->addTab(mSynFbPostProTab, tr("Equalizer"));
        mTabWidgetAlgStructures->setCurrentWidget(mMainSapTab);
#endif
		   
		mSigSelectionHfInput = new TabAlgoStructureSigSelectInput(mAudio, ps);
		mchildTabWidgetSigSel->addTab( mSigSelectionHfInput,"Input");

		mSigSelectionHfOutput = new TabAlgoStructureSigSelectOutput(mAudio, ps);
		mchildTabWidgetSigSel->addTab( mSigSelectionHfOutput,"Output");
		



		/*-----------------------------------------------------------------------------------------*
		* Post processing sub-tabs
		*-----------------------------------------------------------------------------------------*/
		mSynFbPostProLspTab  = new TabAlgoStructureEmptyTab();
		mchildTabWidgetPostProc->addTab(mSynFbPostProLspTab,"Post processing (lsp.)");

		mSynFbPostProPhoneTab  = new TabAlgoStructureEmptyTab();
		mchildTabWidgetPostProc->addTab(mSynFbPostProPhoneTab,"Post processing (phone)");

		/*-----------------------------------------------------------------------------------------*
		* Signal selection layout
		*-----------------------------------------------------------------------------------------*/
		QHBoxLayout *SigSelLayout = new QHBoxLayout;
		mSigSelectionTab->setLayout(SigSelLayout );
		SigSelLayout->addWidget(mchildTabWidgetSigSel);

		/*------------------------------------------------------------------------------------------*
		* Put all widgets of layer together
		*------------------------------------------------------------------------------------------*/
		AlgStrutureLayout->addWidget(mTabWidgetAlgStructures);
		mGroupBoxAlgStructures->setLayout(AlgStrutureLayout);

		QHBoxLayout *TopLayout = new QHBoxLayout;
		iMaxHeight = 88;

		SecondLayout->addWidget(mGroupBoxAlgStructures);
		logoLayout->addLayout(MainControlLayout);
		SecondLayout->addLayout(logoLayout);
		MainLayout->addLayout(SecondLayout);  
   }

  /*---------------------------------------------------------------------------------------------*
   * Third layout
   *---------------------------------------------------------------------------------------------*/
   SendDebugMessage("Initialization of real time visualizations...", MessageTypeDebug); 
   {
       QHBoxLayout *ThirdLayout = new QHBoxLayout;
       mViewParamsCheckBox		= new QCheckBox(tr("Set visualization parameters"));
      /*-----------------------------------------------------------------------------------------*
       * Algorithmic structure layout
       *-----------------------------------------------------------------------------------------*/ 
       QVBoxLayout *VisualizationsLayout    = new QVBoxLayout;
       mGroupBoxVisualizations              = new QGroupBox(tr("Real-Time Visualizations"));
       mTabWidgetVisualizations             = new QTabWidget();
       mGroupBoxVisualizations->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	   
       connect(mGroupBoxVisualizations, SIGNAL(toggled(bool)), mTabWidgetVisualizations, SLOT(setVisible(bool)));
	   connect(mGroupBoxVisualizations, SIGNAL(toggled(bool)), mViewParamsCheckBox, SLOT(setVisible(bool)));
      /*------------------------------------------------------------------------------------------*
       * Add individual tabs
       *------------------------------------------------------------------------------------------*/
	   mRealTimePlotsTab = new TabRealTimePlots( ps, mAudio );
       mTabWidgetVisualizations->addTab(mRealTimePlotsTab, tr("Plotter"));  
      // connect(this, SIGNAL(updatePlotSignalTimeDomain()), mRealTimePlotsTab, SIGNAL(updatePlotSignalTimeDomain()) );	
	   connect(mViewParamsCheckBox, SIGNAL(toggled(bool)), mRealTimePlotsTab, SLOT(viewParamsLayout(bool)));

	   mMemManagementTab = new TabMemoryManagement();
       mTabWidgetVisualizations->addTab(mMemManagementTab, tr("Memory management"));  

      /*------------------------------------------------------------------------------------------*
       * Put all widgets of layer together
       *------------------------------------------------------------------------------------------*/
	   VisualizationsLayout->addWidget(mViewParamsCheckBox);
	   VisualizationsLayout->addWidget(mTabWidgetVisualizations);
       mGroupBoxVisualizations->setLayout(VisualizationsLayout);
       ThirdLayout->addWidget(mGroupBoxVisualizations,0,0);
       MainLayout->addLayout(ThirdLayout);
   }


  /*----------------------------------------------------------------------------------------------*
   * Connect real-time processing and real-time plotting
   *----------------------------------------------------------------------------------------------*/
   mAudio->connectPlotData( mRealTimePlotsTab->getRealTimeDataPointer() );

   QVBoxLayout *layout = new QVBoxLayout;
   layout->setMargin(5);

   MainLayout->addLayout(layout);
   widget->setLayout(MainLayout);

   setWindowTitle(tr("TH Aschaffenburg - Acoustic Signal Processing"));
   setWindowIcon(QIcon("th_ab.svg"));
   
  /*----------------------------------------------------------------------------------------------*
   * Set minimum window size
   *----------------------------------------------------------------------------------------------*/
   setMinimumSize(1130, 800);
   SendDebugMessage("Starting GUI...", MessageTypeInfo); 
   mMeasureDebugDetailsDialog  = new MeasureDebugDetailsDialog(this); 
   connect(PushButtonDebug, SIGNAL(clicked()), mMeasureDebugDetailsDialog, SLOT(show()));
 }


 RtfMainWindow::~RtfMainWindow()
 {
 
  /*---------------------------------------------------------------------------------------------*
   * Algorithmic core
   *---------------------------------------------------------------------------------------------*/
	T_INT iError = 0;

	/* close audio thread (if running) before exiting the app */
	if (!mPushButtonAudioStart->isEnabled())
		mAudio->stopAudioProcessing();
#ifdef BF_SYSTEM

	if (ps)
	{
		HsabAlgCoreFree2DArray((void ***)&(ps->psRecord->ppsWriteBufferIn),
			ps->psRecord->iLocNumAllIn,
			ps->iFrameshiftExt,
			sizeof(T_SHORT),
			sizeof(T_SHORT*),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals));

		HsabAlgCoreFree2DArray((void ***)&(ps->psRecord->ppsWriteBufferOut),
			ps->psRecord->iLocNumAllOut,
			ps->iFrameshiftExt,
			sizeof(T_SHORT),
			sizeof(T_SHORT*),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals));

		HsabAlgCoreFree2DArray((void ***)&(ps->psRecord->ppcfileNameIn),
			ps->psRecord->iLocNumAllIn,
			ps->psRecord->iNumChars,
			sizeof(T_CHAR),
			sizeof(T_CHAR*),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals));

		HsabAlgCoreFree2DArray((void ***)&(ps->psRecord->ppcfileNameOut),
			ps->psRecord->iLocNumAllOut,
			ps->psRecord->iNumChars,
			sizeof(T_CHAR),
			sizeof(T_CHAR*),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytes),
			&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement.iCurrentDynMemInBytesSignals));

		RtfDeCalloc1DArray((void **)& ps->pfMicInRms);

		RtfDeCalloc1DArray((void **)&(ps->pfMicInRms));

		RtfDeCalloc1DArray((void **)&(ps->pfPhoneOutRms));

		free(ps->psSigSel->pbActiveHfInMic);
		ps->psSigSel->pbActiveHfInMic = NULL;

		free(ps->psSigSel->pbActiveHfInPhone);
		ps->psSigSel->pbActiveHfInPhone = NULL;

		free(ps->psSigSel->pbActiveHfOutLsp);
		ps->psSigSel->pbActiveHfOutLsp = NULL;

		free(ps->psSigSel->pbActiveHfOutPhone);
		ps->psSigSel->pbActiveHfOutPhone = NULL;

		free(ps->psRecord->pbRecordInputDetails);
		ps->psRecord->pbRecordInputDetails = NULL;

		free(ps->psRecord->pbRecordOutputDetails);
		ps->psRecord->pbRecordOutputDetails = NULL;

		/*---------------------------------------------------------------------------------------------*
			* Free file handles
			*---------------------------------------------------------------------------------------------*/
		free(ps->psRecord->fHandleIn);
		free(ps->psRecord->fHandleOut);
	}

	free(ps->psSigSel);
#else
	//Deinitialization of the samplerate conversion
	HsabRtfSrcUpDownDeInit(&(ps->HsabRtfSrcFs),
		&(ps->HsabAlgCoreMain.HsabAlgCoreParams),
		&(ps->HsabAlgCoreMain.HsabAlgCoreMemManagement));

	//free(ps->psSigSel);
	HsabAlgCoreDeInit(&(ps->HsabAlgCoreMain));

	free(ps->psRecord->fHandleCompRtfIn);
	free(ps->psRecord->fHandleCompRtfOut);
	free(ps->psRecord->fHandleCompSrcIn);
	free(ps->psRecord->fHandleCompSrcOut);
#endif
}
 
T_INT RtfMainWindow::RtfCalloc1DArray(void** ppPtr, const T_INT iDim1, const T_INT iSize)
{
	/*-----------------------------------------------------------------------------------------------*
	* Memory allocation for vectors
	*-----------------------------------------------------------------------------------------------*/
	T_INT iError = (T_INT)0;

	/* (Try to) allocate memory and set elements to zero -----------------------------------------*/
	*ppPtr = calloc(iDim1, iSize);

	if (!*ppPtr)
	{
		iError = 1;
	}

	/* Return error value ------------------------------------------------------------------------*/
	return iError;
}

void RtfMainWindow::RtfDeCalloc1DArray(void** ppPtr)
{
	/* Don't do anything if null pointer ---------------------------------------------------------*/
	if (!ppPtr || !*ppPtr) return;

	/* Free memory and set pointer to null -------------------------------------------------------*/
	*ppPtr = 0;
	free(*ppPtr);
}

void RtfMainWindow::startAudioProcessing()
{
	mAudio->startAudioProcessing(&session, &(ps->HsabAlgCoreMain));

  /*---------------------------------------------------------------------------------------------*
   * Change audioIO buttons
   *---------------------------------------------------------------------------------------------*/
   printf ("%p\n", mPushButtonAudioStart);
   mPushButtonAudioStart->setDisabled(true);
   mPushButtonAudioStop->setEnabled(true);
   mPushButtonAudioPause->setEnabled(true);
   mAudio->setStartButtonStatus(mPushButtonAudioStart->isEnabled());
   mAudio->mAudioDetailsDialog->updateAudioDetailsInputProperties(mPushButtonAudioStart->isEnabled());
}

void RtfMainWindow::stopAudioProcessing()
{
   if(!mPushButtonAudioStart->isEnabled())
   mAudio->stopAudioProcessing();

  /*---------------------------------------------------------------------------------------------*
   * Change audioIO buttons
   *---------------------------------------------------------------------------------------------*/
   mPushButtonAudioStart->setEnabled(true);
   mPushButtonAudioStop->setDisabled(true);
   mPushButtonAudioPause->setEnabled(false);
   mAudio->mAudioDetailsDialog->updateAudioDetailsInputProperties(mPushButtonAudioStart->isEnabled());
}

void RtfMainWindow::setPauseButtonState()
{
  mPushButtonAudioPause->setEnabled(false);
  mAudio->setPauseButtonStatus(mPushButtonAudioPause->isEnabled());
  /*---------------------------------------------------------------------------------------------*
   * Change audioIO buttons
   *---------------------------------------------------------------------------------------------*/
  mPushButtonAudioContinue->setEnabled(true);
  mPushButtonAudioStart->setEnabled(false);
  mPushButtonAudioStop->setEnabled(false);
}

void RtfMainWindow::setContinueButtonState()
{
  mPushButtonAudioContinue->setEnabled(false);

  /*---------------------------------------------------------------------------------------------*
   * Change audioIO buttons
   *---------------------------------------------------------------------------------------------*/
  mPushButtonAudioPause->setEnabled(true);
  mAudio->setPauseButtonStatus(mPushButtonAudioPause->isEnabled());
  mPushButtonAudioStart->setEnabled(false);
  mPushButtonAudioStop->setEnabled(true);
}
/*------------------------------------------------------------------------------------------------*
 * Set activation status of sound card input in general
 *------------------------------------------------------------------------------------------------*/
void RtfMainWindow::selectSoundcardInput( bool bAct )
{
   mAudio->setSoundcardInActivationGeneral( bAct );
}

/*------------------------------------------------------------------------------------------------*
 * Switches main overview tab to Handsfree system tab
 *------------------------------------------------------------------------------------------------*/
void RtfMainWindow::switchToHandsFreeCore()
{
}

/*------------------------------------------------------------------------------------------------*
 * Switches main overview tab to Signal selection Tab (input)
 *------------------------------------------------------------------------------------------------*/
void RtfMainWindow::switchToSigSelectionIn()
{
   mTabWidgetAlgStructures->setCurrentWidget(mSigSelectionTab);
   mchildTabWidgetSigSel->setCurrentWidget(mSigSelectionHfInput);
}

/*------------------------------------------------------------------------------------------------*
 * Switches main overview tab to Signal selection Tab (output)
 *------------------------------------------------------------------------------------------------*/
void RtfMainWindow::switchToSigSelectionOut()
{
   mTabWidgetAlgStructures->setCurrentWidget(mSigSelectionTab);
   mchildTabWidgetSigSel->setCurrentWidget(mSigSelectionHfOutput);
}



void RtfMainWindow::restartApp()
{
   stopAudioProcessing();
   qApp->exit(RtfMainWindow::REBOOT_CODE);
}
