/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include "rtf_hsab_main_window.h"


int main(int argc, char* argv[])
{
   int iResult = 0;
  /*---------------------------------------------------------------------------------------------*
   * This function generates a new instance of QApplication
   *  with two parameters: argc and argv
   *---------------------------------------------------------------------------------------------*/
   do {
       QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

       QApplication app( argc, argv );

       QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath()+"/imageformats");

      /*---------------------------------------------------------------------------------------------*
       * A new instance of RtfMainWindow with no parent widget
       *---------------------------------------------------------------------------------------------*/
       RtfMainWindow RtfMainWin;

      /*---------------------------------------------------------------------------------------------*aa
       * Widget and its child widgets are shown
       *---------------------------------------------------------------------------------------------*/
       RtfMainWin.show();

      /*---------------------------------------------------------------------------------------------*
       * Waits until exit() is called
       *---------------------------------------------------------------------------------------------*/
       iResult = app.exec();
   }while (iResult == RtfMainWindow::REBOOT_CODE);
   return iResult;
}
