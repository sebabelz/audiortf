/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "types_hsab.h"
#include "rtf_hsab_src.h"
#include "alg_core_hsab_memory_allocation.h"
#include <memory.h>

/*---------------------------------------------------------------------------------*
* Init function of the up and downsampling routines
*---------------------------------------------------------------------------------*/
void HsabRtfSrcUpDownInit(tHsabRtfSrc           *psHsabRtfSrc,
	tHsabAlgCoreParameters    *psParams,
	tHsabAlgCoreMemManagement *psMemManagement,
	T_INT                     iNumChannelsDown,
	T_INT                     iNumChannelsUp)
{
	/*------------------------------------------------------------------------------*
	* Basic parameters
	*------------------------------------------------------------------------------*/
	psHsabRtfSrc->iMainControl = (T_INT)HSAB_OFF;
	psHsabRtfSrc->iNumChannelsDown = iNumChannelsDown;
	psHsabRtfSrc->iNumChannelsUp = iNumChannelsUp;
	psHsabRtfSrc->iNumBiQuads = (T_INT)3;
	psHsabRtfSrc->iUpDownSampleFactor = psParams->iUpDownSamplFac;

	/*------------------------------------------------------------------------------*
	* Buffers to save previous inputs/outputs for downsampling
	*------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc3DArray((void ****)&(psHsabRtfSrc->pppfInputPrevDown),
		psHsabRtfSrc->iNumChannelsDown,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"pppfInputPrevDown",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	HsabAlgCoreCalloc3DArray((void ****)&(psHsabRtfSrc->pppfOutputPrevDown),
		psHsabRtfSrc->iNumChannelsDown,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"pppfOutputPrevDown",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Buffers to save previous inputs/outputs for upsampling
	*------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc3DArray((void ****)&(psHsabRtfSrc->pppfInputPrevUp),
		psHsabRtfSrc->iNumChannelsUp,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"pppfInputPrevUp",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	HsabAlgCoreCalloc3DArray((void ****)&(psHsabRtfSrc->pppfOutputPrevUp),
		psHsabRtfSrc->iNumChannelsUp,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"pppfOutputPrevUp",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Buffers for temporal buffers
	*------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc1DArray((void **)&(psHsabRtfSrc->pfTempSignal1),
		psParams->iFrameshiftExt,
		sizeof(T_FLOAT),
		"psHsabRtfSrc",
		"pfTempSignal1",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	HsabAlgCoreCalloc1DArray((void **)&(psHsabRtfSrc->pfTempSignal2),
		psParams->iFrameshiftExt,
		sizeof(T_FLOAT),
		"psHsabRtfSrc",
		"pfTempSignal2",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Coefficients for the anti-aliasing and anti-imaging filters
	*------------------------------------------------------------------------------*/
	HsabAlgCoreCalloc2DArray((void ***)&(psHsabRtfSrc->ppfFilterNumerator),
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)3,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"ppfFilterNumerator",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	HsabAlgCoreCalloc2DArray((void ***)&(psHsabRtfSrc->ppfFilterDenominator),
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		"psHsabRtfSrc",
		"ppfFilterDenominator",
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iMaxDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc),
		&(psMemManagement->iMaxDynMemInBytesSrc));

	if (psHsabRtfSrc->iUpDownSampleFactor == (T_INT)2)
	{
		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 2)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[0][0] = (T_FLOAT)  0.31220742550767;
		psHsabRtfSrc->ppfFilterNumerator[0][1] = (T_FLOAT)  0.55778154384362;
		psHsabRtfSrc->ppfFilterNumerator[0][2] = (T_FLOAT)  0.31220742550767;

		psHsabRtfSrc->ppfFilterDenominator[0][0] = (T_FLOAT)  0.97787366147367;
		psHsabRtfSrc->ppfFilterDenominator[0][1] = (T_FLOAT)-0.37978887123256;

		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 2)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[1][0] = (T_FLOAT)  0.31220742550767;
		psHsabRtfSrc->ppfFilterNumerator[1][1] = (T_FLOAT)  0.29810569020862;
		psHsabRtfSrc->ppfFilterNumerator[1][2] = (T_FLOAT)  0.31220742550767;

		psHsabRtfSrc->ppfFilterDenominator[1][0] = (T_FLOAT)  0.39034450272129;
		psHsabRtfSrc->ppfFilterDenominator[1][1] = (T_FLOAT)-0.69999812187527;

		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 2)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[2][0] = (T_FLOAT)  0.31220742550767;
		psHsabRtfSrc->ppfFilterNumerator[2][1] = (T_FLOAT)  0.17437970546341;
		psHsabRtfSrc->ppfFilterNumerator[2][2] = (T_FLOAT)  0.31220742550767;

		psHsabRtfSrc->ppfFilterDenominator[2][0] = (T_FLOAT)  0.06411928636129;
		psHsabRtfSrc->ppfFilterDenominator[2][1] = (T_FLOAT)-0.92111064741003;
	}

	if (psHsabRtfSrc->iUpDownSampleFactor == (T_INT)3)
	{
		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 3)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[0][0] = (T_FLOAT)  0.159489198953952;
		psHsabRtfSrc->ppfFilterNumerator[0][1] = (T_FLOAT)  0.242653135356086;
		psHsabRtfSrc->ppfFilterNumerator[0][2] = (T_FLOAT)  0.159489198953951;

		psHsabRtfSrc->ppfFilterDenominator[0][0] = (T_FLOAT)  1.460537016562101;
		psHsabRtfSrc->ppfFilterDenominator[0][1] = (T_FLOAT)-0.592059246566115;

		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 3)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[1][0] = (T_FLOAT)  0.159489198953952;
		psHsabRtfSrc->ppfFilterNumerator[1][1] = (T_FLOAT)  0.016574448180888;
		psHsabRtfSrc->ppfFilterNumerator[1][2] = (T_FLOAT)  0.159489198953952;

		psHsabRtfSrc->ppfFilterDenominator[1][0] = (T_FLOAT)  1.250856137090370;
		psHsabRtfSrc->ppfFilterDenominator[1][1] = (T_FLOAT)-0.758365452110685;

		/*---------------------------------------------------------------------------*
		* Coefficients for the anti-aliasing and anti-imaging filters (factor 3)
		*---------------------------------------------------------------------------*/
		psHsabRtfSrc->ppfFilterNumerator[2][0] = (T_FLOAT)  0.159489198953952;
		psHsabRtfSrc->ppfFilterNumerator[2][1] = (T_FLOAT)-0.064267559006829;
		psHsabRtfSrc->ppfFilterNumerator[2][2] = (T_FLOAT)  0.159489198953952;

		psHsabRtfSrc->ppfFilterDenominator[2][0] = (T_FLOAT)  1.119131775023546;
		psHsabRtfSrc->ppfFilterDenominator[2][1] = (T_FLOAT)-0.926026088590815;
	}
}

/*---------------------------------------------------------------------------------*
* Deinit function of the up and downsampling routines
*---------------------------------------------------------------------------------*/
void HsabRtfSrcUpDownDeInit(tHsabRtfSrc           *psHsabRtfSrc,
	tHsabAlgCoreParameters    *psParams,
	tHsabAlgCoreMemManagement *psMemManagement)
{

	/*------------------------------------------------------------------------------*
	* Coefficients for the anti-aliasing and anti-imaging filters
	*------------------------------------------------------------------------------*/
	HsabAlgCoreFree2DArray((void ***)&(psHsabRtfSrc->ppfFilterNumerator),
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)3,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	HsabAlgCoreFree2DArray((void ***)&(psHsabRtfSrc->ppfFilterDenominator),
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Buffers to save previous inputs/outputs for downsampling
	*------------------------------------------------------------------------------*/
	HsabAlgCoreFree3DArray((void ****)&(psHsabRtfSrc->pppfInputPrevDown),
		psHsabRtfSrc->iNumChannelsDown,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	HsabAlgCoreFree3DArray((void ****)&(psHsabRtfSrc->pppfOutputPrevDown),
		psHsabRtfSrc->iNumChannelsDown,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Buffers to save previous inputs/outputs for upsampling
	*------------------------------------------------------------------------------*/
	HsabAlgCoreFree3DArray((void ****)&(psHsabRtfSrc->pppfInputPrevUp),
		psHsabRtfSrc->iNumChannelsUp,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	HsabAlgCoreFree3DArray((void ****)&(psHsabRtfSrc->pppfOutputPrevUp),
		psHsabRtfSrc->iNumChannelsUp,
		psHsabRtfSrc->iNumBiQuads,
		(T_INT)2,
		sizeof(T_FLOAT),
		sizeof(T_FLOAT*),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	/*------------------------------------------------------------------------------*
	* Buffers for temporal buffers
	*------------------------------------------------------------------------------*/
	HsabAlgCoreFree1DArray((void **)&(psHsabRtfSrc->pfTempSignal1),
		psParams->iFrameshiftExt,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

	HsabAlgCoreFree1DArray((void **)&(psHsabRtfSrc->pfTempSignal2),
		psParams->iFrameshiftExt,
		sizeof(T_FLOAT),
		&(psMemManagement->iCurrentDynMemInBytes),
		&(psMemManagement->iCurrentDynMemInBytesSrc));

}



/*---------------------------------------------------------------------------------*
* Downsampling process function
*---------------------------------------------------------------------------------*/
void HsabRtfSrcDownProcess(T_FLOAT               **ppfInput,
	T_FLOAT               **ppfOutput,
	tHsabRtfSrc        *psHsabRtfSrc,
	tHsabAlgCoreParameters *psParams)
{
	T_INT   i, m, n;
	T_INT   iLocUpDownSampleFactor = psHsabRtfSrc->iUpDownSampleFactor;
	T_FLOAT fLocDenCoeff1;
	T_FLOAT fLocDenCoeff2;
	T_FLOAT fLocNumCoeff0;
	T_FLOAT fLocNumCoeff1;
	T_FLOAT fLocNumCoeff2;
	T_FLOAT *pfLocPointer1;
	T_FLOAT *pfLocPointer2;
	T_FLOAT *pfLocPointerTmp;

	/*-------------------------------------------------------------------------------*
	* Pointer initalization
	*-------------------------------------------------------------------------------*/
	pfLocPointer1 = psHsabRtfSrc->pfTempSignal1;
	pfLocPointer2 = psHsabRtfSrc->pfTempSignal2;

	/*-------------------------------------------------------------------------------*
	* Loop over all channels
	*-------------------------------------------------------------------------------*/
	for (i = 0; i < psHsabRtfSrc->iNumChannelsDown; i++)
	{
		/*----------------------------------------------------------------------------*
		* Copy input signal into temporal signal vector
		*----------------------------------------------------------------------------*/
		memcpy(pfLocPointer2,
			ppfInput[i],
			psParams->iFrameshiftExt * sizeof(T_FLOAT));

		/*----------------------------------------------------------------------------*
		* Anti-aliasing filtering
		*----------------------------------------------------------------------------*/
		for (m = 0; m < psHsabRtfSrc->iNumBiQuads; m++)
		{
			/*-------------------------------------------------------------------------*
			* Change temporal input and output vector
			*-------------------------------------------------------------------------*/
			pfLocPointerTmp = pfLocPointer1;
			pfLocPointer1 = pfLocPointer2;
			pfLocPointer2 = pfLocPointerTmp;

			/*-------------------------------------------------------------------------*
			* Put filter coefficients into local variables
			*-------------------------------------------------------------------------*/
			fLocNumCoeff0 = psHsabRtfSrc->ppfFilterNumerator[m][0];
			fLocNumCoeff1 = psHsabRtfSrc->ppfFilterNumerator[m][1];
			fLocNumCoeff2 = psHsabRtfSrc->ppfFilterNumerator[m][2];
			fLocDenCoeff1 = psHsabRtfSrc->ppfFilterDenominator[m][0];
			fLocDenCoeff2 = psHsabRtfSrc->ppfFilterDenominator[m][1];

			/*-------------------------------------------------------------------------*
			* Compute first output sample
			*-------------------------------------------------------------------------*/
			pfLocPointer2[0] =
				pfLocPointer1[0] * fLocNumCoeff0 +
				psHsabRtfSrc->pppfInputPrevDown[i][m][0] * fLocNumCoeff1 +
				psHsabRtfSrc->pppfInputPrevDown[i][m][1] * fLocNumCoeff2 +
				psHsabRtfSrc->pppfOutputPrevDown[i][m][0] * fLocDenCoeff1 +
				psHsabRtfSrc->pppfOutputPrevDown[i][m][1] * fLocDenCoeff2;

			/*-------------------------------------------------------------------------*
			* Compute second output sample
			*-------------------------------------------------------------------------*/
			pfLocPointer2[1] =
				pfLocPointer1[1] * fLocNumCoeff0 +
				pfLocPointer1[0] * fLocNumCoeff1 +
				psHsabRtfSrc->pppfInputPrevDown[i][m][0] * fLocNumCoeff2 +
				pfLocPointer2[0] * fLocDenCoeff1 +
				psHsabRtfSrc->pppfOutputPrevDown[i][m][0] * fLocDenCoeff2;

			for (n = 2; n < psParams->iFrameshiftExt; n++)
			{
				/*----------------------------------------------------------------------*
				* Compute current output signal
				*----------------------------------------------------------------------*/
				pfLocPointer2[n] =
					pfLocPointer1[n] * fLocNumCoeff0 +
					pfLocPointer1[n - 1] * fLocNumCoeff1 +
					pfLocPointer1[n - 2] * fLocNumCoeff2 +
					pfLocPointer2[n - 1] * fLocDenCoeff1 +
					pfLocPointer2[n - 2] * fLocDenCoeff2;
			}

			/*-------------------------------------------------------------------------*
			* Store last two input and output signals
			*-------------------------------------------------------------------------*/
			psHsabRtfSrc->pppfOutputPrevDown[i][m][0] = pfLocPointer2[psParams->iFrameshiftExt - 1];
			psHsabRtfSrc->pppfOutputPrevDown[i][m][1] = pfLocPointer2[psParams->iFrameshiftExt - 2];
			psHsabRtfSrc->pppfInputPrevDown[i][m][0] = pfLocPointer1[psParams->iFrameshiftExt - 1];
			psHsabRtfSrc->pppfInputPrevDown[i][m][1] = pfLocPointer1[psParams->iFrameshiftExt - 2];
		}

		/*----------------------------------------------------------------------------*
		* Subsampling
		*----------------------------------------------------------------------------*/
		for (n = 0, m = 0; n < psParams->iFrameshiftInt; n++, m = m + iLocUpDownSampleFactor)
		{
			ppfOutput[i][n] = pfLocPointer2[m];
		}
	}
}

/*---------------------------------------------------------------------------------*
* Upsampling process function
*---------------------------------------------------------------------------------*/
void HsabRtfSrcUpProcess(T_FLOAT               **ppfInput,
	T_FLOAT               **ppfOutput,
	tHsabRtfSrc        *psHsabRtfSrc,
	tHsabAlgCoreParameters *psParams)
{
	T_INT   i, k, m, n;
	T_INT   iLocUpDownSampleFactor = psHsabRtfSrc->iUpDownSampleFactor;
	T_FLOAT fLocDenCoeff1;
	T_FLOAT fLocDenCoeff2;
	T_FLOAT fLocNumCoeff0;
	T_FLOAT fLocNumCoeff1;
	T_FLOAT fLocNumCoeff2;
	T_FLOAT *pfLocPointer1;
	T_FLOAT *pfLocPointer2;
	T_FLOAT *pfLocPointerTmp;

	/*-------------------------------------------------------------------------------*
	* Pointer initalization
	*-------------------------------------------------------------------------------*/
	pfLocPointer1 = psHsabRtfSrc->pfTempSignal1;
	pfLocPointer2 = psHsabRtfSrc->pfTempSignal2;

	/*-------------------------------------------------------------------------------*
	* Loop over all channels
	*-------------------------------------------------------------------------------*/
	for (i = 0; i < psHsabRtfSrc->iNumChannelsUp; i++)
	{
		/*----------------------------------------------------------------------------*
		* Upsample with hold mechanism
		*----------------------------------------------------------------------------*/
		for (n = 0, m = 0; n < psParams->iFrameshiftInt; n++, m = m + iLocUpDownSampleFactor)
		{
			pfLocPointer2[m] = ppfInput[i][n];
			for (k = 1; k<iLocUpDownSampleFactor; k++)
			{
				pfLocPointer2[m + k] = pfLocPointer2[m];
			}
		}

		/*----------------------------------------------------------------------------*
		* Anti-aliasing filtering
		*----------------------------------------------------------------------------*/
		for (m = 0; m < psHsabRtfSrc->iNumBiQuads; m++)
		{
			/*-------------------------------------------------------------------------*
			* Change temporal input and output vector
			*-------------------------------------------------------------------------*/
			pfLocPointerTmp = pfLocPointer1;
			pfLocPointer1 = pfLocPointer2;
			pfLocPointer2 = pfLocPointerTmp;

			/*-------------------------------------------------------------------------*
			* Put filter coefficients into local variables
			*-------------------------------------------------------------------------*/
			fLocNumCoeff0 = psHsabRtfSrc->ppfFilterNumerator[m][0];
			fLocNumCoeff1 = psHsabRtfSrc->ppfFilterNumerator[m][1];
			fLocNumCoeff2 = psHsabRtfSrc->ppfFilterNumerator[m][2];
			fLocDenCoeff1 = psHsabRtfSrc->ppfFilterDenominator[m][0];
			fLocDenCoeff2 = psHsabRtfSrc->ppfFilterDenominator[m][1];

			/*-------------------------------------------------------------------------*
			* Compute first output sample
			*-------------------------------------------------------------------------*/
			pfLocPointer2[0] =
				pfLocPointer1[0] * fLocNumCoeff0 +
				psHsabRtfSrc->pppfInputPrevUp[i][m][0] * fLocNumCoeff1 +
				psHsabRtfSrc->pppfInputPrevUp[i][m][1] * fLocNumCoeff2 +
				psHsabRtfSrc->pppfOutputPrevUp[i][m][0] * fLocDenCoeff1 +
				psHsabRtfSrc->pppfOutputPrevUp[i][m][1] * fLocDenCoeff2;

			/*-------------------------------------------------------------------------*
			* Compute second output sample
			*-------------------------------------------------------------------------*/
			pfLocPointer2[1] =
				pfLocPointer1[1] * fLocNumCoeff0 +
				pfLocPointer1[0] * fLocNumCoeff1 +
				psHsabRtfSrc->pppfInputPrevUp[i][m][0] * fLocNumCoeff2 +
				pfLocPointer2[0] * fLocDenCoeff1 +
				psHsabRtfSrc->pppfOutputPrevUp[i][m][0] * fLocDenCoeff2;

			for (n = 2; n < psParams->iFrameshiftExt; n++)
			{
				/*----------------------------------------------------------------------*
				* Compute current output signal
				*----------------------------------------------------------------------*/
				pfLocPointer2[n] =
					pfLocPointer1[n] * fLocNumCoeff0 +
					pfLocPointer1[n - 1] * fLocNumCoeff1 +
					pfLocPointer1[n - 2] * fLocNumCoeff2 +
					pfLocPointer2[n - 1] * fLocDenCoeff1 +
					pfLocPointer2[n - 2] * fLocDenCoeff2;
			}

			/*-------------------------------------------------------------------------*
			* Store last two input and output signals
			*-------------------------------------------------------------------------*/
			psHsabRtfSrc->pppfOutputPrevUp[i][m][0] = pfLocPointer2[psParams->iFrameshiftExt - 1];
			psHsabRtfSrc->pppfOutputPrevUp[i][m][1] = pfLocPointer2[psParams->iFrameshiftExt - 2];
			psHsabRtfSrc->pppfInputPrevUp[i][m][0] = pfLocPointer1[psParams->iFrameshiftExt - 1];
			psHsabRtfSrc->pppfInputPrevUp[i][m][1] = pfLocPointer1[psParams->iFrameshiftExt - 2];
		}

		/*----------------------------------------------------------------------------*
		* Copy result to output buffer
		*----------------------------------------------------------------------------*/
		memcpy(ppfOutput[i],
			pfLocPointer2,
			psParams->iFrameshiftExt * sizeof(T_FLOAT));
	}
}


