/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SRC_H
#define RTF_HSAB_SRC_H

/*#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "rtf_hsab_src_defines.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_memory_allocation.h"

	/*---------------------------------------------------------------------------------*
	* Sample rate conversion
	*---------------------------------------------------------------------------------*/
void HsabRtfSrcUpDownInit(tHsabRtfSrc           *psHsabRtfSrc,
		tHsabAlgCoreParameters    *psParams,
		tHsabAlgCoreMemManagement *psMemManagement,
		T_INT                     iNumChannelsDown,
		T_INT                     iNumChannelsUp);

	void HsabRtfSrcDownProcess(T_FLOAT               **ppfInput,
		T_FLOAT               **ppfOutput,
		tHsabRtfSrc        *psHsabRtfSrc,
		tHsabAlgCoreParameters *psParams);

	void HsabRtfSrcUpProcess(T_FLOAT               **ppfInput,
		T_FLOAT               **ppfOutput,
		tHsabRtfSrc        *psHsabRtfSrc,
		tHsabAlgCoreParameters *psParams);

	void HsabRtfSrcUpDownDeInit(tHsabRtfSrc           *psHsabRtfSrc,
		tHsabAlgCoreParameters    *psParams,
		tHsabAlgCoreMemManagement *psMemManagement);

/*#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
