/*-----------------------------------------------------------------------------------------------*
* Audio and Speech Signal Enhancement
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* edited by Tobias Vogel
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_SRC_DEFINES_H
#define RTF_HSAB_SRC_DEFINES_H

/*#ifdef __cplusplus
extern "C"
{
#endif /* C++ */

#include "types_hsab.h"

	/*---------------------------------------------------------------------------------*
	* Parameters and variables of the sample rate conversion
	*---------------------------------------------------------------------------------*/
	typedef struct
	{
		/*-------------------------------------------------------------------------------*
		* Main parameters
		*-------------------------------------------------------------------------------*/
		T_INT   iMainControl;
		T_INT   iNumChannelsUp;
		T_INT   iNumChannelsDown;
		T_INT   iNumBiQuads;
		T_INT   iUpDownSampleFactor;

		/*-------------------------------------------------------------------------------*
		* Anti-aliasing and anti-imigin filter coefficients
		*-------------------------------------------------------------------------------*/
		T_FLOAT **ppfFilterNumerator;
		T_FLOAT **ppfFilterDenominator;

		/*-------------------------------------------------------------------------------*
		* Signals
		*-------------------------------------------------------------------------------*/
		T_FLOAT ***pppfInputPrevDown;
		T_FLOAT ***pppfOutputPrevDown;

		T_FLOAT ***pppfInputPrevUp;
		T_FLOAT ***pppfOutputPrevUp;

		T_FLOAT   *pfTempSignal1;
		T_FLOAT   *pfTempSignal2;

	} tHsabRtfSrc;

/*#ifdef __cplusplus
}
#endif	/* C++ */

#endif	/* Header-Guard */
