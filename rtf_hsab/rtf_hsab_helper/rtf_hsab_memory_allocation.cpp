/*-----------------------------------------------------------------------------------------------*
* Memory allocation for real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Manuel Diener
* (c) 2019
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_memory_allocation.h"
#include <stdlib.h>

//Allocation

//1D Array
void HsabRtfAlloc1DArray(float **ptr, const int Dim) {
	*ptr = new float[Dim];
    if (*ptr == nullptr)
    {
        exit(1);
    }
}

//2D Array
void HsabRtfAlloc2DArray(float ***ptr, const int Dim1, const int Dim2) {
	*ptr = new float*[Dim1];
    if (*ptr == nullptr) {
		exit(1);
	}
	for (int i = 0; i < Dim1; i++) {
		((*ptr)[i]) = new float[Dim2];
        if ((*ptr)[i] == nullptr) {
            exit(1);
        }
	}
}

void HsabRtfAlloc2DArray(EqRegulator ***ptr, const int Dim1, const int Dim2) {
	*ptr = new EqRegulator*[Dim1];
	if (*ptr == NULL) exit(1);
	for (int i = 0; i < Dim1; i++) {
		(*ptr)[i] = new EqRegulator[Dim2];
		if ((*ptr)[i] == NULL) exit(1);
	}
}



//Deallocation

//1D Array
void HsabRtfFree1DArray(float **ptr) {
	if (*ptr != NULL) {
		delete[] *ptr;
	}
}

//2D Array
void HsabRtfFree2DArray(float ***ptr, const int Dim1, const int Dim2) {
	for (int i = 0; i < Dim1; i++) {
	if ((*ptr)[i] != NULL && Dim2 != 0) {
			delete[] (*ptr)[i];
		}
	}
	if (*ptr != NULL) {
		delete[] *ptr;
	}
}

void HsabRtfFree2DArray(EqRegulator ***ptr, const int Dim1, const int Dim2) {
	for (int i = 0; i < Dim1; i++) {
		if ((*ptr)[i] != NULL && Dim2 != 0) {
			delete[](*ptr)[i];
		}
	}
	if (*ptr != NULL) {
		delete[] *ptr;
	}
}
