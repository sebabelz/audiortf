/*-----------------------------------------------------------------------------------------------*
* Memory allocation for real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Manuel Diener
* (c) 2019
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_MEMORY_ALLOCATION_H
#define RTF_HSAB_MEMORY_ALLOCATION_H

#include "types_hsab.h"
#include "rtf_hsab_eq.h"

//Allocation

//1D Array
void HsabRtfAlloc1DArray(float **ptr, int Dim);

//2DArray
void HsabRtfAlloc2DArray(float ***ptr, const int Dim1, const int Dim2);
void HsabRtfAlloc2DArray(EqRegulator ***ptr, const int Dim1, const int Dim2);



//Deallocation

//1D Array
void HsabRtfFree1DArray(float **ptr);

//2D Array
void HsabRtfFree2DArray(float ***ptr, const int Dim1, const int Dim2);
void HsabRtfFree2DArray(EqRegulator ***ptr, const int Dim1, const int Dim2);

#endif
