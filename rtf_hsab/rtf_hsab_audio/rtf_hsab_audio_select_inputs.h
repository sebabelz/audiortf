/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_SELECT_INPUTS_H
#define RTF_HSAB_AUDIO_SELECT_INPUTS_H

#include "rtf_hsab_audio_io.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>
#include "alg_core_hsab_defines.h"
#include "alg_core_hsab_main.h"

class AudioIO;

class AudioDetailsChannel: public QGroupBox
{
   Q_OBJECT

   public:
	   AudioDetailsChannel(QWidget *parent, int channelNumber, QString string, AudioIO *audioIO, int iNumAllIn, QString  TextFileInput);
	   
	   QComboBox     *mComboBoxSoundcardInputChannelSpec;
	   QComboBox     *mComboBoxFileInputChannelSpec;
	   QPushButton   *mPushButtonFileInputChannel;
	   QCheckBox     *mFileInputCheckBox;
	   QCheckBox     *mSoundCardInputCheckBox;

   private slots:
	   void selectSoundcardInputChannel(bool bActive);
	   void selectFileInputChannel(bool bActive);
	   void selectGainSoundcardInputChannel(double dValue);
	   void selectGainFileInputChannel(double dValue);
	   void browse();
	   void getNewFile(const QString);

   private:
	   T_INT           mChannelNumber;
	   QDoubleSpinBox *mSpinBoxGainFileInputChannelSpec;
	   QDoubleSpinBox *mSpinBoxGainSoundcardInputChannelSpec;
	   AudioIO        *mAudio;
};


class TabMicSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabMicSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsChannel*>   mQVectorAudioDetailsChannel;
		
    protected:

	signals:


    public slots:


    private:
	
};


class TabRefSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabRefSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsChannel*>   mQVectorAudioDetailsChannel;
    protected:


	signals:


    public slots:

    private:


};


class TabDownlSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabDownlSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsChannel*>   mQVectorAudioDetailsChannel;
    protected:


	signals:


    public slots:

    private:

};

class TabTtsSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabTtsSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsChannel*>   mQVectorAudioDetailsChannel;
    protected:


	signals:


    public slots:

    private:

};


class AudioDetailsInputs : public QWidget
{
	Q_OBJECT
	
   public:
	   AudioDetailsInputs(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget *parent);

   signals:
		
   public slots:
		void setGlobalFileInput( bool bActive );
		void setGlobalSoundcardInput( bool bActive );
		void setFileNameInputComboBoxAndBrowseButtonStatus(bool);

   private:
	   tHsabAlgCoreParameters *mParams;

		QCheckBox  *mCheckBoxAudioFileInputGlobal;
		QCheckBox  *mCheckBoxAudioSoundcardInputGlobal;

		QGroupBox  *mGroupBoxMic0;
		QTabWidget *mTabWidgetMic0;
		AudioIO    *mAudio;
		T_INT       iNumAllIn;
		TabMicSelectSap   *mMicSapTab;
		TabRefSelectSap   *mRefSapTab;
		TabDownlSelectSap *mDownlSapTab;
		TabTtsSelectSap   *mTtsSapTab;
		QTabWidget        *mTabWidgetAudioSelect;

};

#endif 