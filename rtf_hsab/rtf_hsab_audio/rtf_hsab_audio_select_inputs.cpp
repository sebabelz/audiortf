/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#include <QtGui>
#include "rtf_hsab_audio_select_inputs.h"
#include "types_hsab.h"

AudioDetailsChannel::AudioDetailsChannel(QWidget *parent, int channelNumber, QString string, AudioIO *audioIO, int iNumAllIn, QString  TextFileInput)
		   : QGroupBox(string, parent)
		   , mChannelNumber(channelNumber) 
		   , mAudio(audioIO)
{
	QString      Text;
	QGridLayout* ChannelLayout = new QGridLayout(this);
	setLayout(ChannelLayout);
	T_INT k;
        
	mSoundCardInputCheckBox = new QCheckBox( tr("Soundcard"), this );
	mSoundCardInputCheckBox->setChecked(audioIO->getSoundcardInActivationChannelSpec(channelNumber));
	connect(mSoundCardInputCheckBox, SIGNAL(toggled(bool)), this, SLOT(selectSoundcardInputChannel(bool)));
	ChannelLayout->addWidget(mSoundCardInputCheckBox,0,0);

	mFileInputCheckBox = new QCheckBox(tr("File input"), this);
	mFileInputCheckBox->setChecked(audioIO->getFileInActivationChannelSpec(channelNumber));
	connect(mFileInputCheckBox, SIGNAL(toggled(bool)), this, SLOT(selectFileInputChannel(bool)));
	ChannelLayout->addWidget(mFileInputCheckBox,1,0);
   		
	mSpinBoxGainSoundcardInputChannelSpec = new QDoubleSpinBox( this );
	ChannelLayout->addWidget(mSpinBoxGainSoundcardInputChannelSpec,0,4);  
	mSpinBoxGainSoundcardInputChannelSpec->setSingleStep((double) 1.0);
	mSpinBoxGainSoundcardInputChannelSpec->setMinimum((double) -100.0);
	mSpinBoxGainSoundcardInputChannelSpec->setMaximum((double) 40.0);
	mSpinBoxGainSoundcardInputChannelSpec->setValue((double) mAudio->getGainSoundcardInChannelSpec(channelNumber));
	connect(mSpinBoxGainSoundcardInputChannelSpec, SIGNAL(valueChanged(double)), this, SLOT(selectGainSoundcardInputChannel(double)));

	mSpinBoxGainFileInputChannelSpec = new QDoubleSpinBox(this);
   		
	ChannelLayout->addWidget(mSpinBoxGainFileInputChannelSpec,1,4);
   		
	mSpinBoxGainFileInputChannelSpec->setSingleStep((double) 1.0);
	mSpinBoxGainFileInputChannelSpec->setMinimum((double) -100.0);
	mSpinBoxGainFileInputChannelSpec->setMaximum((double) 40.0);

	mSpinBoxGainFileInputChannelSpec->setValue((double) mAudio->getGainFileInChannelSpec(channelNumber));
	connect(mSpinBoxGainFileInputChannelSpec, SIGNAL(valueChanged(double)), this, SLOT(selectGainFileInputChannel(double)));

	QLabel *LabelGainSoundcardInputChannelSpec = new QLabel(tr("Gain in dB"));
	ChannelLayout->addWidget(LabelGainSoundcardInputChannelSpec,0,5);

	mComboBoxSoundcardInputChannelSpec = new QComboBox(this);
	mComboBoxSoundcardInputChannelSpec->setEditable(true);

	for ( k=0; k<iNumAllIn; k++ )
	{
		Text = QString("Soundcard input channel %1").arg(k);
		mComboBoxSoundcardInputChannelSpec->addItem(Text);
	}
	Text = QString("Soundcard input channel %1").arg(mChannelNumber);
	mComboBoxSoundcardInputChannelSpec->setEditText(Text);

	mComboBoxSoundcardInputChannelSpec->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	ChannelLayout->addWidget(mComboBoxSoundcardInputChannelSpec,0,7);

	QLabel *LabelGainFileInputChannelSpec = new QLabel(tr("Gain in dB"));
	ChannelLayout->addWidget(LabelGainFileInputChannelSpec,1,5);

	mComboBoxFileInputChannelSpec = new QComboBox(this);
	mComboBoxFileInputChannelSpec->setEditable(true);
	     
	mComboBoxFileInputChannelSpec->addItem(TextFileInput);
	mComboBoxFileInputChannelSpec->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	ChannelLayout->addWidget(mComboBoxFileInputChannelSpec,1,7);

	mPushButtonFileInputChannel = new QPushButton(tr("Browse"), this);
	connect(mPushButtonFileInputChannel, SIGNAL(clicked()), this, SLOT(browse()));

	connect(mComboBoxFileInputChannelSpec, SIGNAL(editTextChanged(const QString)), this, SLOT(getNewFile(const QString)));

	ChannelLayout->addWidget(mPushButtonFileInputChannel,1,8);	
}

TabMicSelectSap::TabMicSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn)
{
   T_INT    k;
   QString  sChannelName;
   QString  TextFileInput;

   QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);

   QScrollArea *ScrollArea      = new QScrollArea(this);
   SubLayoutCenter->addWidget(ScrollArea);

   QWidget     *Widget          = new QWidget(ScrollArea); 
   ScrollArea->setWidget(Widget);
   ScrollArea->setWidgetResizable(true);

   QVBoxLayout *SubLayoutScroll = new QVBoxLayout(Widget);
   Widget->setLayout(SubLayoutScroll);
#ifdef BF_SYSTEM
   for ( k=0; k<Params->iNumMicIn; k++ )
   {  

      sChannelName = QString("Microphone input %1").arg(k);
#else
   for (k = 0; k < Params->iNumMicInFrontSeat; k++)
   {
	   sChannelName = QString("Microphone front seat %1").arg(k);
#endif
	  TextFileInput = QString("signals\\mic_%1.pcm").arg(k);
	  mQVectorAudioDetailsChannel.append(new AudioDetailsChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllIn, TextFileInput) );
	  SubLayoutScroll->addWidget(mQVectorAudioDetailsChannel[iChannelNumber]);
      iChannelNumber++;
	}
    SubLayoutScroll->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding));

}


TabRefSelectSap::TabRefSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn)
{
	T_INT    k;
	QString  sChannelName;
	QString  TextFileInput;

	QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);

	QScrollArea *ScrollArea      = new QScrollArea(this);
	SubLayoutCenter->addWidget(ScrollArea);

	QWidget     *Widget          = new QWidget(ScrollArea); 
	ScrollArea->setWidget(Widget);
	ScrollArea->setWidgetResizable(true);

	QVBoxLayout *SubLayoutScroll = new QVBoxLayout(Widget);
	Widget->setLayout(SubLayoutScroll);

#ifdef BF_SYSTEM
	for ( k=0; k<Params->iNumRefInFromAmp; k++ )
	{  
		sChannelName = QString("Reference input %1").arg(k);
		TextFileInput = QString("signals\\sounds_cau\\phone_%1_44kHz.pcm").arg(k);
#else
	for (k = 0; k < Params->iNumMicInRearSeat; k++)
	{
		sChannelName = QString("Microphone rear seat %1").arg(k);
		TextFileInput = QString("signals\\phone_%1_44kHz.pcm").arg(k);
#endif // BF_SYSTEM

		mQVectorAudioDetailsChannel.append(new AudioDetailsChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllIn, TextFileInput) );
		SubLayoutScroll->addWidget(mQVectorAudioDetailsChannel[k]);
		iChannelNumber++;
	}
	SubLayoutScroll->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding));
}

TabDownlSelectSap::TabDownlSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn)
{
	T_INT    k;
	QString  sChannelName;
	QString  TextFileInput;
#ifdef BF_SYSTEM
	QVBoxLayout* SubLayoutCenter = new QVBoxLayout(this);

	QScrollArea* ScrollArea = new QScrollArea(this);
	SubLayoutCenter->addWidget(ScrollArea);

	QWidget* Widget = new QWidget(ScrollArea);
	ScrollArea->setWidget(Widget);
	ScrollArea->setWidgetResizable(true);

	QVBoxLayout* SubLayoutScroll = new QVBoxLayout(Widget);
	Widget->setLayout(SubLayoutScroll);

	for (k = 0; k < Params->iNumRemoteIn; k++)
	{
		sChannelName = QString("Phone input %1").arg(k);
		TextFileInput = QString("signals\\ref_in.pcm").arg(k);
		mQVectorAudioDetailsChannel.append(new AudioDetailsChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllIn, TextFileInput));
		SubLayoutScroll->addWidget(mQVectorAudioDetailsChannel[k]);
		iChannelNumber++;
	}
	SubLayoutScroll->addItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Expanding));
#endif
}

TabTtsSelectSap::TabTtsSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn)
{
#ifdef BF_SYSTEM
	T_INT    k;
	QString  sChannelName;
	QString  TextFileInput;

	QVBoxLayout* SubLayoutCenter = new QVBoxLayout(this);

	QScrollArea* ScrollArea = new QScrollArea(this);
	SubLayoutCenter->addWidget(ScrollArea);

	QWidget* Widget = new QWidget(ScrollArea);
	ScrollArea->setWidget(Widget);
	ScrollArea->setWidgetResizable(true);

	QVBoxLayout* SubLayoutScroll = new QVBoxLayout(Widget);
	Widget->setLayout(SubLayoutScroll);

	for (k = 0; k < Params->iNumTtsIn; k++)
	{
		sChannelName = QString("TTS input %1").arg(k);
		TextFileInput = QString("signals\\sounds_cau\\tts_%1_44kHz.pcm").arg(k);
		mQVectorAudioDetailsChannel.append(new AudioDetailsChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllIn, TextFileInput));
		SubLayoutScroll->addWidget(mQVectorAudioDetailsChannel[k]);
		iChannelNumber++;
	}
	SubLayoutScroll->addItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Expanding));
#endif	
}

AudioDetailsInputs::AudioDetailsInputs(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget * parent)
: QWidget ( parent )
, mAudio(audioIO)
, mParams(Params)
{
   T_INT    iChannelNumber;
   QString  sChannelName;
   QVBoxLayout *MainLayout = new QVBoxLayout(this);

  /*-----------------------------------------------------------------------------------------*
   * Top groupbox
   *-----------------------------------------------------------------------------------------*/
   QVBoxLayout *SubLayoutTop = new QVBoxLayout(this);
   QGroupBox   *TopGroupBox  = new QGroupBox(tr("Audio input details"));
   QGridLayout *TopLayout    = new QGridLayout(this);

   mCheckBoxAudioFileInputGlobal = new QCheckBox(tr("All file inputs"), this);
   mCheckBoxAudioFileInputGlobal->setChecked(false);
   connect(mCheckBoxAudioFileInputGlobal, SIGNAL(toggled(bool)), this, SLOT(setGlobalFileInput(bool)));
   	
   mCheckBoxAudioSoundcardInputGlobal = new QCheckBox(tr("All soundcard inputs"), this);
   mCheckBoxAudioSoundcardInputGlobal->setChecked(true);
   connect(mCheckBoxAudioSoundcardInputGlobal, SIGNAL(toggled(bool)), this, SLOT(setGlobalSoundcardInput(bool)));   
   
   TopLayout->addWidget(mCheckBoxAudioSoundcardInputGlobal,0,0);
   TopLayout->addWidget(mCheckBoxAudioFileInputGlobal,1,0);

   TopGroupBox->setLayout(TopLayout);
   SubLayoutTop->addWidget(TopGroupBox);

  /*-----------------------------------------------------------------------------------------*
   * Channel specific settings
   *-----------------------------------------------------------------------------------------*/
   QVBoxLayout *AudioSelectLayout = new QVBoxLayout(this);
  
   mTabWidgetAudioSelect = new QTabWidget(this);
  
   iChannelNumber = (T_INT) 0;

#ifdef BF_SYSTEM
   iNumAllIn = Params->iNumMicIn + Params->iNumRefInFromAmp + Params->iNumRemoteIn;
   mMicSapTab = new TabMicSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mMicSapTab, tr("Mic. signals"));

   iChannelNumber = Params->iNumMicIn;
   mRefSapTab = new TabRefSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mRefSapTab, tr("Ref. signals"));

   iChannelNumber = Params->iNumMicIn + Params->iNumRefInFromAmp;
   mDownlSapTab = new TabDownlSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mDownlSapTab, tr("Downlink signals"));

   iChannelNumber = Params->iNumMicIn + Params->iNumRefInFromAmp + Params->iNumRemoteIn;
   mTtsSapTab = new TabTtsSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mTtsSapTab, tr("TTS signals"));
#else
   iNumAllIn = Params->iNumAllIn;
   mMicSapTab = new TabMicSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mMicSapTab, tr("Mic input front seat signals"));

   iChannelNumber = Params->iNumMicInFrontSeat;
   mRefSapTab = new TabRefSelectSap(Params, audioIO, iChannelNumber, iNumAllIn);
   mTabWidgetAudioSelect->addTab(mRefSapTab, tr("Mic input rear seat signals"));
#endif


   AudioSelectLayout->addWidget(mTabWidgetAudioSelect);
   MainLayout->addLayout(SubLayoutTop);
   MainLayout->addLayout(AudioSelectLayout);

}

void AudioDetailsInputs::setGlobalFileInput( bool bActive )
{
#ifdef BF_SYSTEM
	for (int i = 0; i < mParams->iNumMicIn; i++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(bActive);
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumRemoteIn; i++)
	{
		mDownlSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(bActive);
		mDownlSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumRefInFromAmp; i++)
	{
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(bActive);
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(!bActive);
	}
#else
	for (int i = 0; i < mParams->iNumMicInFrontSeat; i++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(bActive);
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumMicInRearSeat; i++)
	{
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(bActive);
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(!bActive);
	}
#endif
	mCheckBoxAudioSoundcardInputGlobal->setChecked(!bActive);
	mAudio->setFileInActivationGeneral( bActive );
}

void AudioDetailsInputs::setGlobalSoundcardInput( bool bActive )
{
#ifdef BF_SYSTEM
	for (int i = 0; i < mParams->iNumMicIn; i++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(bActive);
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumRemoteIn; i++)
	{
		mDownlSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(bActive);
		mDownlSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumRefInFromAmp; i++)
	{
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(bActive);
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(!bActive);
	}
#else
	for (int i = 0; i < mParams->iNumMicInFrontSeat; i++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(bActive);
		mMicSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(!bActive);
	}
	for (int i = 0; i < mParams->iNumMicInRearSeat; i++)
	{
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mSoundCardInputCheckBox->setChecked(bActive);
		mRefSapTab->mQVectorAudioDetailsChannel[i]->mFileInputCheckBox->setChecked(!bActive);
	}
#endif 

	
	mCheckBoxAudioFileInputGlobal->setChecked(!bActive);
	mAudio->setSoundcardInActivationGeneral( bActive );
}

void AudioDetailsChannel::selectSoundcardInputChannel(bool bActive)
{
   mAudio->setSoundcardInActivationChannelSpec(bActive, mChannelNumber);
}

void AudioDetailsChannel::selectFileInputChannel(bool bActive)
{
   mAudio->setFileInputActivationChannelSpec(bActive, mChannelNumber);
}

void AudioDetailsChannel::selectGainSoundcardInputChannel(double dValue)
{
   mAudio->setGainSoundcardInputChannel(dValue, mChannelNumber);
}

void AudioDetailsChannel::selectGainFileInputChannel(double dValue)
{
   mAudio->setGainFileInputChannel(dValue, mChannelNumber);
}

void AudioDetailsChannel::browse()
{
  	QString directory = QFileDialog::getOpenFileName(this,
		tr("Open Audio File"), QString(), tr("AudioFiles (*.wav *.pcm)"));

  if (!directory.isEmpty()) 
  {
      if (mComboBoxFileInputChannelSpec->findText(directory) == -1)
          mComboBoxFileInputChannelSpec->addItem(directory);

      mComboBoxFileInputChannelSpec->setCurrentIndex(mComboBoxFileInputChannelSpec->findText(directory));
  }
}

void AudioDetailsChannel::getNewFile(const QString fileName)
{		
	QByteArray fileNameData = fileName.toLocal8Bit();

	const char *x = fileNameData.constData();

	mAudio->getNewFile(x, mChannelNumber);
}

void AudioDetailsInputs::setFileNameInputComboBoxAndBrowseButtonStatus(bool bAct)
{
	int k;
#ifdef BF_SYSTEM
	for (k = 0; k < mParams->iNumMicIn; k++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[k]->mComboBoxFileInputChannelSpec->setEnabled(bAct);
		mMicSapTab->mQVectorAudioDetailsChannel[k]->mPushButtonFileInputChannel->setEnabled(bAct);
	}

	for (k = 0; k < mParams->iNumRemoteIn; k++)
	{
		mDownlSapTab->mQVectorAudioDetailsChannel[k]->mComboBoxFileInputChannelSpec->setEnabled(bAct);
		mDownlSapTab->mQVectorAudioDetailsChannel[k]->mPushButtonFileInputChannel->setEnabled(bAct);
	}
#else
	for (k = 0; k < mParams->iNumMicInFrontSeat; k++)
	{
		mMicSapTab->mQVectorAudioDetailsChannel[k]->mComboBoxFileInputChannelSpec->setEnabled(bAct);
		mMicSapTab->mQVectorAudioDetailsChannel[k]->mPushButtonFileInputChannel->setEnabled(bAct);
	}

	for (k = 0; k < mParams->iNumMicInRearSeat; k++)
	{
		mRefSapTab->mQVectorAudioDetailsChannel[k]->mComboBoxFileInputChannelSpec->setEnabled(bAct);
		mRefSapTab->mQVectorAudioDetailsChannel[k]->mPushButtonFileInputChannel->setEnabled(bAct);
	}
#endif 
}
