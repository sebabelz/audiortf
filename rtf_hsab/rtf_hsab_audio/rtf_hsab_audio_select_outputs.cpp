/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/
#include "rtf_hsab_audio_select_outputs.h"
#include "types_hsab.h"

AudioDetailsOutChannel::AudioDetailsOutChannel(QWidget *parent, int channelNumber, QString string, AudioIO *audioIO, int iNumAllOut)
		   : QGroupBox(string, parent)
		   , mChannelNumber(channelNumber) 
		   , mAudio(audioIO)
{
	QString      Text;
	QGridLayout* ChannelLayout = new QGridLayout(this);
	setLayout(ChannelLayout);
	T_INT k;
        
	mSoundCardOutputCheckBox = new QCheckBox( tr("Soundcard"), this );
	mSoundCardOutputCheckBox->setChecked(audioIO->getSoundcardOutActivationChannelSpec(channelNumber));
	connect(mSoundCardOutputCheckBox, SIGNAL(toggled(bool)), this, SLOT(selectSoundcardOutputChannel(bool)));
	ChannelLayout->addWidget(mSoundCardOutputCheckBox,0,0);
   		
	mSpinBoxGainSoundcardOutputChannelSpec = new QDoubleSpinBox( this );
	ChannelLayout->addWidget(mSpinBoxGainSoundcardOutputChannelSpec,0,4);  
	mSpinBoxGainSoundcardOutputChannelSpec->setSingleStep((double) 1.0);
	mSpinBoxGainSoundcardOutputChannelSpec->setMinimum((double) -100.0);
	mSpinBoxGainSoundcardOutputChannelSpec->setMaximum((double) 40.0);
	mSpinBoxGainSoundcardOutputChannelSpec->setValue((double) mAudio->getGainSoundcardOutChannelSpec(channelNumber));
	connect(mSpinBoxGainSoundcardOutputChannelSpec, SIGNAL(valueChanged(double)), this, SLOT(selectGainSoundcardOutputChannel(double)));

	QLabel *LabelGainSoundcardOutputChannelSpec = new QLabel(tr("Gain in dB"));
	ChannelLayout->addWidget(LabelGainSoundcardOutputChannelSpec,0,5);

	mComboBoxSoundcardOutputChannelSpec = new QComboBox(this);
	mComboBoxSoundcardOutputChannelSpec->setEditable(true);

	for ( k=0; k < iNumAllOut; k++ )
	{
		Text = QString("Soundcard output channel %1").arg(k);
		mComboBoxSoundcardOutputChannelSpec->addItem(Text);
	}
	Text = QString("Soundcard output channel %1").arg(mChannelNumber);
	mComboBoxSoundcardOutputChannelSpec->setEditText(Text);
	connect(mComboBoxSoundcardOutputChannelSpec, SIGNAL(activated(int)), this, SLOT(setSoundCardOutputChannel(int)));
	mComboBoxSoundcardOutputChannelSpec->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	ChannelLayout->addWidget(mComboBoxSoundcardOutputChannelSpec,0,7);
}

void AudioDetailsOutChannel::setSoundCardOutputChannel(int idx)
{
   mAudio->setSoundCardOutputChannel(idx, mChannelNumber);
}

void AudioDetailsOutChannel::selectSoundcardOutputChannel(bool bActive)
{
   mAudio->setSoundcardOutActivationChannelSpec(bActive, mChannelNumber);
}

void AudioDetailsOutChannel::selectGainSoundcardOutputChannel(double dValue)
{
   mAudio->setGainSoundcardOutputChannel(dValue, mChannelNumber);
}

TabPhoneOutSelectSap::TabPhoneOutSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllOut)
{
#ifdef BF_SYSTEM
	T_INT    k;
	QString  sChannelName;

	QVBoxLayout* SubLayoutCenter = new QVBoxLayout(this);

	QScrollArea* ScrollArea = new QScrollArea(this);
	SubLayoutCenter->addWidget(ScrollArea);

	QWidget* Widget = new QWidget(ScrollArea);
	ScrollArea->setWidget(Widget);
	ScrollArea->setWidgetResizable(true);

	QVBoxLayout* SubLayoutScroll = new QVBoxLayout(Widget);
	Widget->setLayout(SubLayoutScroll);

	for (k = 0; k < Params->iNumRemoteOut; k++)
	{
		sChannelName = QString("Phone output %1").arg(k);
		mQVectorAudioDetailsOutChannel.append(new AudioDetailsOutChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllOut));
		SubLayoutScroll->addWidget(mQVectorAudioDetailsOutChannel[k]);
		iChannelNumber++;
	}
	SubLayoutScroll->addItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Expanding));
#endif // BF_SYSTEM

}

TabLspOutSelectSap::TabLspOutSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllOut)
{
#ifdef BF_SYSTEM
	T_INT    k;
	QString  sChannelName;

	QVBoxLayout* SubLayoutCenter = new QVBoxLayout(this);

	QScrollArea* ScrollArea = new QScrollArea(this);
	SubLayoutCenter->addWidget(ScrollArea);

	QWidget* Widget = new QWidget(ScrollArea);
	ScrollArea->setWidget(Widget);
	ScrollArea->setWidgetResizable(true);

	QVBoxLayout* SubLayoutScroll = new QVBoxLayout(Widget);
	Widget->setLayout(SubLayoutScroll);

	for (k = 0; k < Params->iNumOutToAmpl; k++)
	{
		sChannelName = QString("Lsp output %1").arg(k);
		mQVectorAudioDetailsOutChannel.append(new AudioDetailsOutChannel(Widget, iChannelNumber, sChannelName, audioIO, iNumAllOut));
		SubLayoutScroll->addWidget(mQVectorAudioDetailsOutChannel[iChannelNumber]);
		iChannelNumber++;
	}
	SubLayoutScroll->addItem(new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Expanding));
#endif // BF_SYSTEM

}

AudioDetailsOutputs::AudioDetailsOutputs(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget * parent)
: QWidget ( parent )
, mAudio(audioIO)
, mParams(Params)
{
#ifdef BF_SYSTEM
	T_INT    iChannelNumber;
	QString  sChannelName;
	QVBoxLayout* MainLayout = new QVBoxLayout(this);

	/*-----------------------------------------------------------------------------------------*
	 * Top groupbox
	 *-----------------------------------------------------------------------------------------*/
	QVBoxLayout* SubLayoutTop = new QVBoxLayout(this);
	QGroupBox* TopGroupBox = new QGroupBox(tr("Audio output details"));
	QGridLayout* TopLayout = new QGridLayout(this);


	mCheckBoxAudioSoundcardOutputGlobal = new QCheckBox(tr("All soundcard outputs"), this);
	mCheckBoxAudioSoundcardOutputGlobal->setChecked(true);
	connect(mCheckBoxAudioSoundcardOutputGlobal, SIGNAL(toggled(bool)), this, SLOT(setGlobalSoundcardOutput(bool)));


	TopLayout->addWidget(mCheckBoxAudioSoundcardOutputGlobal, 0, 0);

	TopGroupBox->setLayout(TopLayout);
	SubLayoutTop->addWidget(TopGroupBox);

	/*-----------------------------------------------------------------------------------------*
	 * Channel specific settings
	 *-----------------------------------------------------------------------------------------*/
	QVBoxLayout* AudioSelectLayout = new QVBoxLayout(this);

	mTabWidgetAudioSelect = new QTabWidget(this);

	iChannelNumber = (T_INT)0;
	iNumAllOut = Params->iNumRemoteOut + Params->iNumOutToAmpl;
	mLspOutSelectTap = new TabLspOutSelectSap(Params, audioIO, iChannelNumber, iNumAllOut);
	mTabWidgetAudioSelect->addTab(mLspOutSelectTap, tr("Lsp signals"));

	iChannelNumber = Params->iNumOutToAmpl;
	mPhoneOutSelectTap = new TabPhoneOutSelectSap(Params, audioIO, iChannelNumber, iNumAllOut);
	mTabWidgetAudioSelect->addTab(mPhoneOutSelectTap, tr("Uplink signals"));

	AudioSelectLayout->addWidget(mTabWidgetAudioSelect);
	MainLayout->addLayout(SubLayoutTop);
	MainLayout->addLayout(AudioSelectLayout);
#endif // BF_SYSTEM


}

void AudioDetailsOutputs::setGlobalSoundcardOutput( bool bActive )
{
#ifdef BF_SYSTEM
	for (int i = 0; i < mParams->iNumRemoteOut; i++)
	{
		mPhoneOutSelectTap->mQVectorAudioDetailsOutChannel[i]->mSoundCardOutputCheckBox->setChecked(bActive);
	}
	for (int i = 0; i < mParams->iNumOutToAmpl; i++)
	{
		mLspOutSelectTap->mQVectorAudioDetailsOutChannel[i]->mSoundCardOutputCheckBox->setChecked(bActive);
	}

	mAudio->setSoundcardOutActivationGeneral(bActive);
#endif // BF_SYSTEM

}
