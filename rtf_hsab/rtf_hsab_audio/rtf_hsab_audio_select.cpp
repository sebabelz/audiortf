/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_audio_select.h"
#include "types_hsab.h"

AudioDetailsDialog::AudioDetailsDialog(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget * parent)
: QDialog(parent) 
{
	/*-----------------------------------------------------------------------------------------*
   * Main layout
   *-----------------------------------------------------------------------------------------*/
   QVBoxLayout *MainLayout = new QVBoxLayout(this);
   setWindowTitle(tr("Audio I/O"));
   setMinimumWidth(800);
   mTabWidgetInputsSelect = new QTabWidget(this);

  /*-----------------------------------------------------------------------------------------*
   * Inputs
   *-----------------------------------------------------------------------------------------*/
   mAudioDetailsInputs = new AudioDetailsInputs(audioIO, Params, this);
   mTabWidgetInputsSelect->addTab(mAudioDetailsInputs, tr("Inputs"));

  /*-----------------------------------------------------------------------------------------*
   * Outputs
   *-----------------------------------------------------------------------------------------*/
   mAudioDetailsOutputs = new AudioDetailsOutputs(audioIO, Params, this);
   mTabWidgetInputsSelect->addTab(mAudioDetailsOutputs, tr("Outputs"));

  /*-----------------------------------------------------------------------------------------*
   * Close button
   *-----------------------------------------------------------------------------------------*/
   QHBoxLayout *SubLayoutBottom = new QHBoxLayout(this);
   QPushButton *PushButtonClose = new QPushButton(tr("&Close"), this);

   PushButtonClose->setMinimumWidth(100);
   connect(PushButtonClose, SIGNAL(clicked()), this, SLOT(close()));
   SubLayoutBottom->addStretch();	
   SubLayoutBottom->addWidget(PushButtonClose);

   MainLayout->addWidget(mTabWidgetInputsSelect);
   MainLayout->addLayout(SubLayoutBottom);
}


void AudioDetailsDialog::showInputsTab()
{
	show();
	mTabWidgetInputsSelect->setCurrentWidget(mAudioDetailsInputs);
}

void AudioDetailsDialog::showOutputsTab()
{
	show();
	mTabWidgetInputsSelect->setCurrentWidget(mAudioDetailsOutputs);
}

void AudioDetailsDialog::updateAudioDetailsInputProperties(bool bAct)
{
	mAudioDetailsInputs->setFileNameInputComboBoxAndBrowseButtonStatus(bAct);
}


