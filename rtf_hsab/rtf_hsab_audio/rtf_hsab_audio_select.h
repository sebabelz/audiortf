/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_SELECT_H
#define RTF_HSAB_AUDIO_SELECT_H

#include "rtf_hsab_audio_io.h"
#include "rtf_hsab_audio_select_inputs.h"
#include "rtf_hsab_audio_select_outputs.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>

class AudioIO;
class AudioDetailsInputs;
class AudioDetailsOutputs;

class AudioDetailsDialog : public QDialog
{
	Q_OBJECT
	
   public:
	   AudioDetailsDialog(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget *parent);

   signals:
		
   public slots:
		void showInputsTab();
		void showOutputsTab();
		void updateAudioDetailsInputProperties(bool);

   private:
	   QTabWidget				*mTabWidgetInputsSelect;
	   QTabWidget				*mTabWidgetOutputsSelect;

	   AudioDetailsOutputs		*mAudioDetailsOutputs;
	   AudioDetailsInputs		*mAudioDetailsInputs;


};


#endif 