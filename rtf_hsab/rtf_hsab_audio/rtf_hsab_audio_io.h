/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_IO_H
#define RTF_HSAB_AUDIO_IO_H

#include "rtf_hsab_main_window.h"
#include "portaudio.h"
#include "pa_asio.h"
#include "rtf_hsab_audio_channel.h"
#include "rtf_hsab_plots_data.h"
#include "rtf_hsab_audio_select.h"

#include <QtGui>
#include <QObject>
#include <stdio.h>
#include <stdlib.h>

#include "alg_core_hsab_main.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_defines.h"
#include "types_hsab.h"
#include "defines_hsab.h"

struct tRtf_hfSrc;
struct tSession;
class PlotterModule;

/*---------------------------------------------------------------------------------*
 * Possible soundcard types
 *---------------------------------------------------------------------------------*/
 typedef enum SOUNDCARDTYPE
 {
	SAP_SOUNDCARDTYPE_WMM = 0,
    SAP_SOUNDCARDTYPE_ASIO,
 } SoundcardType;

class AudioIO : public QObject
{
   Q_OBJECT

   public:
      AudioIO( tSession *pSession );	
      ~AudioIO(  ); 										
	  void    Rtf_hfSrcDownProcess( T_FLOAT  **ppfInput, T_FLOAT   **ppfOutput, tRtf_hfSrc *psRtf_hfSrc,  T_INT iFrameshiftExt, T_INT iFrameshift);
	  void    Rtf_hfSrcUpProcess( T_FLOAT  **ppfInput, T_FLOAT  **ppfOutput, tRtf_hfSrc *psRtf_hfSrc, T_INT iFrameshiftExt, T_INT iFrameshift);
      
	  bool    getFileInputActivationGeneral( );
	  bool    getSoundcardActivationGeneral( );
	  bool    getSoundcardInActivationChannelSpec( int iChannel );
	  bool    getSoundcardOutActivationChannelSpec( int iChannel );
	  bool    getFileInActivationChannelSpec( int iChannel );
	  void    setFileInActivationGeneral( bool bAct );
	  void    setSoundcardInActivationGeneral( bool bAct );
	  void    setSoundcardOutActivationGeneral( bool bAct );
	  void    setStartButtonStatus(bool bAct);
	  void    setPauseButtonStatus( bool bAct );
	  void    setSoundCardOutputChannel(int idx, int iChannelNumber);
	  float   getGainFileInChannelSpec( int iChannel );
	  float   getGainSoundcardInChannelSpec( int iChannel );
	  float   getGainSoundcardOutChannelSpec( int iChannel );
	  void    freqDomainPlotting( );
	  void    timeDomainPlotting( );

	  void    setPlotterModule2DVector(QVector<QVector<PlotterModule*>>);
	  AudioDetailsDialog *mAudioDetailsDialog;
   signals:

   protected:

   public slots:
	  void startAudioProcessing(tSession * pSession, tHsabAlgCoreMain *HsabAlgCoreMain);
      void stopAudioProcessing();
	  void startRecording(tSession *ps);
	  void stopRecording(tSession *ps);
	  void AllSoundcardInputsFlag(bool bActive, tSession *ps);
	  void AllSoundcardOutputsFlag(bool bActive, tSession *ps);
	  void activateInputChannel(bool bActive, int iChannel, tSession *ps);
	  void activateOutputChannel(bool bActive, int iChannel, tSession *ps);
	  void setSoundcardInActivationChannelSpec( bool bActive, int iChannel );
	  void setSoundcardOutActivationChannelSpec( bool bActive, int iChannel );
      void setFileInputActivationChannelSpec( bool bActive, int iChannel );
	  void setGainSoundcardInputChannel( double dValue, int iChannel );
	  void setGainSoundcardOutputChannel( double dValue, int iChannel );
	  void setGainFileInputChannel( double dValue, int iChannel );
	  void mapMicInSignalChannelSpec(int index, int iChannel, tSession *ps );
	  void mapPhoneInSignalChannelSpec(int index, int iChannel, tSession *ps );
	  void mapLspOutSignalChannelSpec(int index, int iChannel, tSession *ps );
	  void mapPhoneOutSignalChannelSpec(int index, int iChannel, tSession *ps );
	  void setMicInSignalSelActivationChannelSpec(bool bActive, int iChannel, tSession *ps );
	  void setPhoneInSignalSelActivationChannelSpec(bool bActive, int iChannel, tSession *ps );
	  void setLspOutSignalSelActivationChannelSpec(bool bActive, int iChannel, tSession *ps );
	  void setPhoneOutSignalSelActivationChannelSpec(bool bActive, int iChannel, tSession *ps );
	  void getNewFile( const char *x, int iChannel );
      void connectPlotData( tPlotData *pPlotData );
	  void updateTimeChannel();

	  void setOffsetValueChannelSpec(double dValue, int mChannelNumber);
	  void setFreqValueChannelSpec(double dValue, int mChannelNumber, tSession *ps);
	  
   private:
     /*-------------------------------------------------------------------------------*
      * Type of soundcard (WMM or ASIO)
      *-------------------------------------------------------------------------------*/
      int miSoundCardType;

	  /*-------------------------------------------------------------------------------*
      * Noise generator
      *-------------------------------------------------------------------------------*/
      unsigned int muiNoiseGenShiftRegister;
      float        mfNoiseGenGain;
	  tSession	  *mSession;
     /*-------------------------------------------------------------------------------*
      * Port audio
      *-------------------------------------------------------------------------------*/
      PaStream *mpAudioHandle;
	  
      static int cbPortAudioStub( const void *inputBuffer, 
                              void *outputBuffer,
                              unsigned long framesPerBuffer,
                              const PaStreamCallbackTimeInfo* timeInfo,
                              PaStreamCallbackFlags statusFlags,
                              void *userData );
	  
	   int cbPortAudio( const void *inputBuffer, 
                              void *outputBuffer,
                              unsigned long framesPerBuffer,
                              const PaStreamCallbackTimeInfo* timeInfo,
                              PaStreamCallbackFlags statusFlags );
							  
		tsAudioDataEntire *psAudioDataInEntire;
		tsAudioDataEntire *psAudioDataOutEntire;

		tsAudioChannel    *psAudioChannel;

		int miIndexSoundcardInputChannel;

		tHsabAlgCoreMain  *mHsabAlgCoreMain;

	  /*--------------------------------------------------------------------------------*
      * Main initialization of audio data
      *--------------------------------------------------------------------------------*/
		void initAudioData( tSession *pSession );

	  /*--------------------------------------------------------------------------------*
      * Init of file input data
      *--------------------------------------------------------------------------------*/
		void initAudioFileInput( int iNumAllIn );

	  /*--------------------------------------------------------------------------------*
      * Get input file
      *--------------------------------------------------------------------------------*/
		bool getInputFile( tsAudioChannel *psAudioChannel );

	  /*--------------------------------------------------------------------------------*
      * Add file signal
      *--------------------------------------------------------------------------------*/
		void addFileSignal( float* fAuxInBuffer, T_INT iFrameshift, tsAudioChannel *psAudioChannel );

	 /*--------------------------------------------------------------------------------*
      * Fill a buffer with zeros
      *--------------------------------------------------------------------------------*/
		void fillFloatArrayWithZeros(T_FLOAT *pfBuffer, T_INT iSize);

     /*--------------------------------------------------------------------------------*
      * Fill a buffer with low power noise
      *--------------------------------------------------------------------------------*/
	  void fillFloatBufferWithLowPowerNoise( float*, int );

	 /*--------------------------------------------------------------------------------*
      * Compute rms energy of signal
      *--------------------------------------------------------------------------------*/
		void  ComputeRmsLevel(const T_FLOAT*, T_FLOAT*, T_INT);

	  /*--------------------------------------------------------------------------------*
      * Serial TDM audio stream access functions
      *--------------------------------------------------------------------------------*/
	  void copyChannelFromSerialStream (float* , const float* , int , int );
	  void copyChannelToSerialStream (const float* , float* , int , int );
	  void fillChannelInSerialStreamWithZeros (float* , int , int );

	  /*--------------------------------------------------------------------------------*
      * Data for real-time plots
      *--------------------------------------------------------------------------------*/
      tPlotData *pPlotData;

	  /*---------------------------------------------------------------------------------*
	  * Flag used for demonstrator settings
	  *---------------------------------------------------------------------------------*/
	  T_BOOL mbDemonstratorSettings;

	 /*---------------------------------------------------------------------------------*
	  * Flag for Start Button (audio processing)
	  *---------------------------------------------------------------------------------*/
	  T_BOOL bActiveStartButton;

	 /*---------------------------------------------------------------------------------*
	  * Flag for Pause Button (audio processing)
	  *---------------------------------------------------------------------------------*/
	  T_BOOL bActivePauseButton;

	 /*---------------------------------------------------------------------------------*
	  * Set/get functionality
	  *---------------------------------------------------------------------------------*/
	  T_CHAR pcBuffer[MESSAGE_LENGTH];
	  int iError; 

	 /*---------------------------------------------------------------------------------*
	  * Set pointer to plotter class
	  *---------------------------------------------------------------------------------*/
	  QVector<QVector<PlotterModule*>>   mQVector2DPlotterSignal;

};

#endif 