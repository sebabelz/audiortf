/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_MEASURE_RECORD_H
#define RTF_HSAB_AUDIO_MEASURE_RECORD_H

#include "rtf_hsab_audio_io.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>



class InputCheckBoxWidget : public QCheckBox
{
    Q_OBJECT
    public:
        InputCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO,tSession *ps);
    
    signals:
  
    private slots:

        void activateInputChannel(bool bAct);

    private:
        T_INT       mChannelNumber;
	    AudioIO    *mpAudioIO;
	    tSession   *mps;


};

class OutputCheckBoxWidget : public QCheckBox
{
    Q_OBJECT
    public:
        OutputCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent,AudioIO *pAudioIO, tSession *ps);
    
    signals:
  
    private slots:

        void activateOutputChannel(bool bAct);

    private:
        T_INT        mChannelNumber;
	    AudioIO     *mpAudioIO;
	    tSession    *mps;

};

class TabRecordSap : public QWidget
{
    Q_OBJECT

    public:
        TabRecordSap ( AudioIO *audioIO, QWidget * parent, tSession *ps );

    protected:


	signals:

    public slots:
		void startRecording();
	    void stopRecording();
		void showInputDetails(bool);
		void showOutputDetails(bool);
		void AllSoundcardInputsFlag(bool);
		void AllSoundcardOutputsFlag(bool);

    private:
	    AudioIO        *mAudio;
		tSession       *mps;

		QWidget          *mWidget;
        QScrollArea      *mScrollArea;

		QPushButton       *mPushButtonRecord;
	    QPushButton       *mPushButtonStop;

		T_INT iLocNumAllIn;
		T_INT iLocNumAllOut;

		InputCheckBoxWidget *mInputCheckBoxWidget;
		QString sChannelNameInput;
		QString sChannelNameOutput;
		T_INT iChannelNumberInput;
		T_INT iChannelNumberOutput;

		QGroupBox   *InputGroupBox;

		QGridLayout *InputLayout;
		QGridLayout *InputLayout2;

		QGridLayout *OutputLayout;
		QGridLayout *OutputLayout2;

		QHBoxLayout *TopLayout;

		QGroupBox  *mGroupBoxInputDetails;
		QCheckBox  *mCheckBoxAudioSoundcardInputGlobal;

		OutputCheckBoxWidget  *mOutputCheckBoxWidget;
	    QGroupBox  *mGroupBoxOutputDetails;
		QCheckBox  *mCheckBoxAudioSoundcardOutputGlobal;
};


class TabMeasureSap : public QWidget
{
    Q_OBJECT

    public:
        TabMeasureSap( AudioIO *audioIO, QWidget * parent);

    protected:

    signals:

    public slots:

    private:
		AudioIO     *mAudio;

};


class MeasureRecordDetailsDialog: public QDialog
{
   Q_OBJECT

   public:
	    MeasureRecordDetailsDialog(AudioIO *audioIO, QWidget * parent, tSession *ps );

   public slots:
	   void showRecordingTab();
	   void showMeasuringTab();

   private:
	   AudioIO           *mAudio;
	   tSession          *mps;
	   QVBoxLayout       *MainLayout;

	   TabRecordSap      *mTabRecordSap;
	   TabMeasureSap     *mTabMeasureSap;



	   QTabWidget        *mTabWidgetMeasureRecordSelect;
};

#endif