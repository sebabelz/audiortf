/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_CHANNEL_H
#define RTF_HSAB_AUDIO_CHANNEL_H

typedef struct
{
   float           fGainFileIn;
   float           fGainFileIndB;
   float           fGainSoundcardIn;
   float           fGainSoundcardIndB;
   float           fGainSoundcardOut;
   float           fGainSoundcardOutdB;
   int             iChannelOut;
   bool            bError;
   short*          psFileData;
   char*           pcFileName;   
   int             iPositionIdx;
   unsigned int    uiLength;
   unsigned int    uiSampleRate;
   bool            bSelectFileInput;
   bool            bSelectSoundcardInput;
   bool            bSelectSoundcardOutput;

}tsAudioChannel;

typedef struct
{
	bool             bActivateSoundcardInput;
	bool             bActivateFileInput;
	bool             bActivateSoundcardOutput;
    tsAudioChannel*  psAudioChannel;

	int*			 piActiveOutChannel;
	int				 iChDownlink;
	int				 iChUplink;

}tsAudioDataEntire;

#endif 