/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include <math.h>
#include "rtf_hsab_audio_io.h"
#include "rtf_hsab_main_window.h"
#include "rtf_hsab_signal_select_channel.h"
#include "rtf_hsab_plotter_module.h"

#include "alg_core_hsab_main.h"
#include "alg_core_hsab_parameters.h"
#include "alg_core_hsab_defines.h"
#include "types_hsab.h"

#ifndef BF_SYSTEM
#include "rtf_hsab_src.h"
#include "alg_pal_hsab_fft_data.h"
#endif


AudioIO::AudioIO( tSession *ps)  
{
	mSession = ps;

  /*---------------------------------------------------------------------------------------------*
   * Soundcard type (ASIO = SAP_SOUNDCARDTYPE_ASIO = 1 or WMM = SAP_SOUNDCARDTYPE_WMM = 0)
   *---------------------------------------------------------------------------------------------*/
   miSoundCardType = (T_INT)SAP_SOUNDCARDTYPE_WMM;
   switch(miSoundCardType)
   {
	  case 0:
         SendDebugMessage("Soundcard type: WMM", MessageTypeInfo);
         break;
         
      case 1:
         SendDebugMessage("Soundcard type: ASIO", MessageTypeInfo);
         break;

      default:;
   }
  /*---------------------------------------------------------------------------------------------*
   * Set demonstrator settings
   *---------------------------------------------------------------------------------------------*/
   mbDemonstratorSettings = FALSE;//TRUE;

  /*---------------------------------------------------------------------------------------------*
   * Noise generator (for avoiding permanent zeros in the input signals)
   *---------------------------------------------------------------------------------------------*/
   muiNoiseGenShiftRegister = (T_UINT32) 1;
   mfNoiseGenGain           = (T_FLOAT)  1e-10;
   bActiveStartButton       =  HSAB_TRUE;
   bActivePauseButton       =  HSAB_TRUE;

  /*---------------------------------------------------------------------------------------------*
   * Port audio
   *---------------------------------------------------------------------------------------------*/
   SendDebugMessage("Initializing port audio...", MessageTypeDebug);
   Pa_Initialize();

  /*---------------------------------------------------------------------------------------------*
   * Main initialization of audio data
   *---------------------------------------------------------------------------------------------*/
   initAudioData( ps );

 }


AudioIO::~AudioIO()
{
   int i;

  /*---------------------------------------------------------------------------------------------*
   * AudioIO using port audio
   *---------------------------------------------------------------------------------------------*/
   Pa_Terminate();

	/*---------------------------------------------------------------------------------------------*
	* Deallocation of dynamic memory
	*---------------------------------------------------------------------------------------------*/
	for( i=0; i< mSession->iNumAllIn; i++ )
	{
		free(psAudioDataInEntire->psAudioChannel[i].pcFileName);
		psAudioDataInEntire->psAudioChannel[i].pcFileName = 0;
		if( psAudioDataInEntire->psAudioChannel[i].bSelectFileInput )
		{
			free(psAudioDataInEntire->psAudioChannel[i].psFileData);
			psAudioDataInEntire->psAudioChannel[i].psFileData = 0;			
		}
	}

	/*---------------------------------------------------------------------------------------------*
	* Free of allocated structure memory (audio selection)
	*---------------------------------------------------------------------------------------------*/
	free(psAudioDataInEntire->psAudioChannel);
	free(psAudioDataOutEntire->piActiveOutChannel);
	free(psAudioDataInEntire);
	free(psAudioDataOutEntire);
}

void AudioIO::startAudioProcessing(tSession * pSession, tHsabAlgCoreMain *HsabAlgCoreMain)
{
   PaError ePaError = 0;
   const char* ePaErrorMsg = 0;
   mSession = pSession;
   mHsabAlgCoreMain = HsabAlgCoreMain;

   //For Activation of Plotting
   pPlotData->bActive = true;

   T_INT i;

#ifdef BF_SYSTEM
   for (i = 0; i < HsabAlgCoreMain->HsabAlgCoreParams.iNumMicIn; i++)
   {
	   mSession->ppfMicInSelect[i] = HsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i];
   }

   for (i = 0; i < HsabAlgCoreMain->HsabAlgCoreParams.iNumRemoteOut; i++)
   {
	   mSession->ppfPhoneOutSelect[i] = HsabAlgCoreMain->HsabAlgCoreSignals.ppfRemoteOut[i];
   }

   /*---------------------------------------------------------------------------------------------*
   * Open input and output files for writing (record)
   *---------------------------------------------------------------------------------------------*/
   for (i = 0; i < mSession->psRecord->iLocNumAllIn; i++)
   {
	   sprintf(mSession->psRecord->ppcfileNameIn[i], ".\\recordings\\SoundCardInput_%d.pcm", i);
	   mSession->psRecord->fHandleIn[i] = fopen(mSession->psRecord->ppcfileNameIn[i], "wb"); /*to append, use "ab" */
   }

   for (i = 0; i < mSession->psRecord->iLocNumAllOut; i++)
   {
	   sprintf(mSession->psRecord->ppcfileNameOut[i], ".\\recordings\\SoundCardOutput_%d.pcm", i);
	   mSession->psRecord->fHandleOut[i] = fopen(mSession->psRecord->ppcfileNameOut[i], "wb"); /*to append, use "ab" */
   }
#else
   mSession->psRecord->fHandleCompRtfIn[0] = fopen("raw_Testaufnahmen/Aufnahme_vor_bearbeitung.raw", "wb");
   mSession->psRecord->fHandleCompRtfOut[0] = fopen("raw_Testaufnahmen/Aufnahme_nach_bearbeitung.raw", "wb");
   mSession->psRecord->fHandleCompSrcIn[0] = fopen("raw_Testaufnahmen/Src_nach_Downsampling.raw", "wb");
   mSession->psRecord->fHandleCompSrcOut[0] = fopen("raw_Testaufnahmen/Src_vor_Upsampling.raw", "wb");
#endif

  /*---------------------------------------------------------------------------------------------*
   * Initilisation file input data
   *---------------------------------------------------------------------------------------------*/
   if (bActivePauseButton)
   {
      initAudioFileInput(pSession->iNumAllIn);
   }

  /*---------------------------------------------------------------------------------------------*
   * Open audio stream
   *---------------------------------------------------------------------------------------------*/
   PaStreamParameters inputParameters;
   PaStreamParameters outputParameters;

   PaHostApiTypeId devType = paMME;
   T_INT iInputChannels  = 2;
   T_INT iOutputChannels = 2;
   
   if (SAP_SOUNDCARDTYPE_ASIO == miSoundCardType)
   {
      devType         = paASIO;
	  iInputChannels  = pSession->iNumAllIn;
	  iOutputChannels = pSession->iNumAllOut;
   }

   if (mbDemonstratorSettings)
   {
	  devType         = paASIO;
	  iInputChannels  = 8;
	  iOutputChannels = 8;
   }
  
   inputParameters.device =						Pa_GetHostApiInfo( Pa_HostApiTypeIdToHostApiIndex( devType ) )->defaultInputDevice;
   inputParameters.channelCount               = iInputChannels;
   inputParameters.sampleFormat               = paFloat32; /* 32 bit floating point output */
   inputParameters.suggestedLatency           = Pa_GetDeviceInfo( inputParameters.device )->defaultLowInputLatency;
   inputParameters.hostApiSpecificStreamInfo  = NULL;	

   outputParameters.device                    = Pa_GetHostApiInfo( Pa_HostApiTypeIdToHostApiIndex( devType ) )->defaultOutputDevice;
   outputParameters.channelCount              = iOutputChannels;
   outputParameters.sampleFormat              = paFloat32; /* 32 bit floating point output */
   outputParameters.suggestedLatency          = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
   outputParameters.hostApiSpecificStreamInfo = NULL;

   

   ePaError = Pa_OpenStream( &mpAudioHandle,
                             &inputParameters,
								&outputParameters,
							 pSession->iSampleRateExt,
							 pSession->iFrameshiftExt,
                             paClipOff,      /* we won't output out of range samples so don't bother clipping them */
                             &AudioIO::cbPortAudioStub,
                             (void *) this );

  /*---------------------------------------------------------------------------------------------*
   * Reset function of the algorithmic core
   *---------------------------------------------------------------------------------------------*/
   //T_INT iError = SapAlgCoreSpeechenhHandsfreeReset (mHandsfree);

  /*---------------------------------------------------------------------------------------------*
   * Start audio
   *---------------------------------------------------------------------------------------------*/
   if(ePaError == 0)
   {
     ePaError = Pa_StartStream( mpAudioHandle );
   }

   else
   {
     ePaErrorMsg = Pa_GetErrorText( ePaError );
   }
}

void AudioIO::stopAudioProcessing()
{
	//For Deactivating of Plotting
	pPlotData->bActive = false;

  /*---------------------------------------------------------------------------------------------*
   * Stop audio
   *---------------------------------------------------------------------------------------------*/
   Pa_StopStream( mpAudioHandle );

  /*---------------------------------------------------------------------------------------------*
   * Close all files
   *---------------------------------------------------------------------------------------------*/
for (int i=0; i< mSession->psRecord->iLocNumAllIn; i++)
    {
	  fclose(mSession->psRecord->fHandleIn[i]);	
    }

    for (int i=0; i< mSession->psRecord->iLocNumAllOut; i++)
    {
	  fclose(mSession->psRecord->fHandleOut[i]);	
    }

  /*---------------------------------------------------------------------------------------------*
   * Close audio stream
   *---------------------------------------------------------------------------------------------*/
   Pa_CloseStream( mpAudioHandle );

}


int AudioIO::cbPortAudioStub( const void *inputBuffer, 
                             void *outputBuffer,
                             unsigned long framesPerBuffer,
                             const PaStreamCallbackTimeInfo *timeInfo,
                             PaStreamCallbackFlags statusFlags,
                             void *userData )
{
   AudioIO *audioObject = reinterpret_cast<AudioIO*>(userData);  /* cast operator */
   return   audioObject->cbPortAudio(inputBuffer, outputBuffer, framesPerBuffer, timeInfo, statusFlags);
}

int AudioIO::cbPortAudio( const void *inputBuffer, 
                             void *outputBuffer,
                             unsigned long framesPerBuffer,
                             const PaStreamCallbackTimeInfo *timeInfo,
                             PaStreamCallbackFlags statusFlags )
{
   T_FLOAT      fScaleFactor;
   T_FLOAT      fTemp;
   T_INT        i, j, k, m;
   T_INT        iChannelNumber;
   T_INT        iLocNumAllIn;
   T_INT        iLocNumAllOut;
   const T_FLOAT *in_ptr_array   = (const T_FLOAT *) inputBuffer;
   T_FLOAT       *out_ptr_array  = (T_FLOAT *)       outputBuffer;
   T_INT        iFrameShiftExt = (T_INT) framesPerBuffer;

#ifndef BF_SYSTEM
   T_INT    n, o, p, q;
   T_FLOAT* pfLocPointer;
#endif 


  /*------------------------------------------------------------------------------------------------*
   * Set number of soundcard input and output channels
   *------------------------------------------------------------------------------------------------*/
   iLocNumAllIn  = mSession->iNumAllIn;
   iLocNumAllOut = mSession->iNumAllOut;									   
																						   
   if (SAP_SOUNDCARDTYPE_WMM == miSoundCardType)
   {
      if (iLocNumAllIn > 2)
      {
         iLocNumAllIn = 2;
      }
      if (iLocNumAllOut > 2)
      {
         iLocNumAllOut = 2;
      }
   }

   if (mbDemonstratorSettings)
   {
	  iLocNumAllIn  = 8;
	  iLocNumAllOut = 8;
   }

#ifdef BF_SYSTEM
   /*------------------------------------------------------------------------------------------------*
	* Fill all input buffers with low power noise
	*------------------------------------------------------------------------------------------------*/
   for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicIn; i++)
   {
	   fillFloatBufferWithLowPowerNoise(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i], iFrameShiftExt);
   }


   /*------------------------------------------------------------------------------------------------*
   * Init channel counter
   *------------------------------------------------------------------------------------------------*/
   iChannelNumber = (T_INT)0;

   /*------------------------------------------------------------------------------------------------*
   * Add soundcard and file input signals for microphone input
   *------------------------------------------------------------------------------------------------*/
   for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicIn; i++)
   {
	   if ((iChannelNumber < iLocNumAllIn) && (psAudioDataInEntire->bActivateSoundcardInput))
	   {
		   /*------------------------------------------------------------------------------------------*
		   * Copy soundcard signal
		   *------------------------------------------------------------------------------------------*/
		   copyChannelFromSerialStream(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i],
			   (const T_FLOAT*)in_ptr_array + iChannelNumber, iLocNumAllIn,
			   iFrameShiftExt);


		   /*------------------------------------------------------------------------------------------*
		   * Scale soundcard signal
		   *------------------------------------------------------------------------------------------*/
		   fScaleFactor = (T_FLOAT)psAudioDataInEntire->psAudioChannel[iChannelNumber].bSelectSoundcardInput
			   * psAudioDataInEntire->psAudioChannel[iChannelNumber].fGainSoundcardIn;

		   for (k = 0; k < iFrameShiftExt; k++)
		   {
			   mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i][k] *= fScaleFactor;
		   }
	   }

	   /*---------------------------------------------------------------------------------------------*
	   * Add file IO signal
	   *---------------------------------------------------------------------------------------------*/
	   if (psAudioDataInEntire->bActivateFileInput)
	   {
		   addFileSignal(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i],
			   iFrameShiftExt,
			   &(psAudioDataInEntire->psAudioChannel[iChannelNumber]));
	   }

	   /*---------------------------------------------------------------------------------------------*
	   * Increment channel counter
	   *---------------------------------------------------------------------------------------------*/
	   iChannelNumber++;
   }


   /*-----------------------------------------------------*
	* Compute RMS energy of MicIn
	*-----------------------------------------------------*/
   for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicIn; i++)
   {
	   ComputeRmsLevel(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[i], &mSession->pfMicInRms[i], mSession->iFrameshiftExt);
   }


   /*------------------------------------------------------------------------------------------------*
	* Main process function of algorithmic core
	*------------------------------------------------------------------------------------------------*/
   HsabAlgCoreProcess(mHsabAlgCoreMain);

   /*------------------------------------------------------------------------------------------------*
   * Init channel counter
   *------------------------------------------------------------------------------------------------*/
   iChannelNumber = (T_INT)0;

   /*------------------------------------------------------------------------------------------------*
   * Send rear output signals (front loudspeaker signals) to soundcard
   *------------------------------------------------------------------------------------------------*/
   for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumRemoteOut; i++)
   {
	   if (iChannelNumber < iLocNumAllOut)
	   {

		   /*------------------------------------------------------------------------------------------*
		   * Scale soundcard signal
		   *------------------------------------------------------------------------------------------*/
		   fScaleFactor = (T_FLOAT)psAudioDataOutEntire->psAudioChannel[iChannelNumber].bSelectSoundcardOutput
			   * psAudioDataOutEntire->psAudioChannel[iChannelNumber].fGainSoundcardOut;

		   for (k = 0; k < iFrameShiftExt; k++)
		   {
			   mSession->ppfPhoneOutSelect[i][k] *= fScaleFactor;
		   }

		   /*-----------------------------------------------------*
		   * Compute RMS energy of PhoneOut
		   *-----------------------------------------------------*/
		   ComputeRmsLevel(mSession->ppfPhoneOutSelect[i], &mSession->pfPhoneOutRms[i], mSession->iFrameshiftExt);

		   /*------------------------------------------------------------------------------------------*
		   * Copy soundcard signal
		   *------------------------------------------------------------------------------------------*/
		   if (psAudioDataOutEntire->piActiveOutChannel[psAudioDataOutEntire->iChDownlink] == psAudioDataOutEntire->piActiveOutChannel[psAudioDataOutEntire->iChUplink])
		   {

			   fillChannelInSerialStreamWithZeros((T_FLOAT*)out_ptr_array + iChannelNumber, 2, iFrameShiftExt);

			   copyChannelToSerialStream(mSession->ppfPhoneOutSelect[i],
				   (T_FLOAT*)out_ptr_array + psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut, 2,
				   iFrameShiftExt);
			   /* Stero-like (left and right channel same signal) */
			   fillChannelInSerialStreamWithZeros((T_FLOAT*)out_ptr_array + 1, 2, iFrameShiftExt);
			   copyChannelToSerialStream(mSession->ppfPhoneOutSelect[i],
				   (T_FLOAT*)out_ptr_array + 1 + psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut, 2,
				   iFrameShiftExt);
		   }
		   else
		   {
			   copyChannelToSerialStream(mSession->ppfPhoneOutSelect[i],
				   (T_FLOAT*)out_ptr_array + psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut, iLocNumAllOut,
				   iFrameShiftExt);
		   }

	   }
	   /*---------------------------------------------------------------------------------------------*
	   * Increment channel counter
	   *---------------------------------------------------------------------------------------------*/
	   iChannelNumber++;
   }
	/*------------------------------------------------------------------------------------------------*
	* Record soundcard inputs
	*------------------------------------------------------------------------------------------------*/

	if (mSession->psRecord->bRecordStatus)//&& psAudioDataInEntire->bActivateSoundcardInput)
	{
		T_INT iChannelNumber = (T_INT)0;

		for (j = 0; j < mSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; j++)
		{
			if (mSession->psRecord->pbRecordInputDetails[iChannelNumber] || mSession->psRecord->bRecordAllInputs)
			{
				for (k = 0; k < mSession->iFrameshiftExt; k++)
				{
					mSession->psRecord->ppsWriteBufferIn[iChannelNumber][k] = (T_SHORT)(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicIn[j][k] * (T_FLOAT)32767.0);
				}

				if (mSession->psRecord->fHandleIn[iChannelNumber] != 0) 	/* error handling */
				{
					fwrite((const T_SHORT*)mSession->psRecord->ppsWriteBufferIn[iChannelNumber], sizeof(T_SHORT), iFrameShiftExt, mSession->psRecord->fHandleIn[iChannelNumber]);
				}
			}
			iChannelNumber++;
		}

	/*------------------------------------------------------------------------------------------------*
	* Record soundcard outputs
	*------------------------------------------------------------------------------------------------*/
		iChannelNumber = (T_INT)0;

		for (j = 0; j < mSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; j++)
		{
			if (mSession->psRecord->pbRecordOutputDetails[iChannelNumber] || mSession->psRecord->bRecordAllOutputs)
			{
				for (k = 0; k < mSession->iFrameshiftExt; k++)
				{
					mSession->psRecord->ppsWriteBufferOut[iChannelNumber][k] = (T_SHORT)(mSession->HsabAlgCoreMain.HsabAlgCoreSignals.ppfRemoteOut[j][k] * (T_FLOAT)32767.0);
				}

				if (mSession->psRecord->fHandleOut[iChannelNumber] != 0) 	 /* error handling */
				{
					fwrite((const T_SHORT*)mSession->psRecord->ppsWriteBufferOut[iChannelNumber], sizeof(T_SHORT), iFrameShiftExt, mSession->psRecord->fHandleOut[iChannelNumber]);
				}
			}
			iChannelNumber++;
		}
	}
#else
	/*------------------------------------------------------------------------------------------------*
	  * Fill all input buffers with low power noise
	  *------------------------------------------------------------------------------------------------*/
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInFrontSeat; i++)
	{
		fillFloatBufferWithLowPowerNoise(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat[i], iFrameShiftExt);
	}
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInRearSeat; i++)
	{
		fillFloatBufferWithLowPowerNoise(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[i], iFrameShiftExt);
	}

	/*------------------------------------------------------------------------------------------------*
	* Init channel counter
	*------------------------------------------------------------------------------------------------*/
	iChannelNumber = (T_INT)0;

	/*------------------------------------------------------------------------------------------------*
	* Add soundcard and file input signals for microphone input
	*------------------------------------------------------------------------------------------------*/
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInFrontSeat; i++)
	{
		if ((iChannelNumber < iLocNumAllIn) && (psAudioDataInEntire->bActivateSoundcardInput))
		{
			/*------------------------------------------------------------------------------------------*
			* Copy soundcard signal
			*------------------------------------------------------------------------------------------*/
			copyChannelFromSerialStream(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat[i],
				(const T_FLOAT*)in_ptr_array + iChannelNumber, iLocNumAllIn,
				iFrameShiftExt);


			/*------------------------------------------------------------------------------------------*
			* Scale soundcard signal
			*------------------------------------------------------------------------------------------*/
			fScaleFactor = (T_FLOAT)psAudioDataInEntire->psAudioChannel[iChannelNumber].bSelectSoundcardInput
				* psAudioDataInEntire->psAudioChannel[iChannelNumber].fGainSoundcardIn;

			for (k = 0; k < iFrameShiftExt; k++)
			{
				mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat[i][k] *= fScaleFactor;
			}
		}

		/*---------------------------------------------------------------------------------------------*
		* Add file IO signal
		*---------------------------------------------------------------------------------------------*/
		if (psAudioDataInEntire->bActivateFileInput)
		{
			addFileSignal(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat[i],
				iFrameShiftExt,
				&(psAudioDataInEntire->psAudioChannel[iChannelNumber]));
		}

		/*---------------------------------------------------------------------------------------------*
		* Increment channel counter
		*---------------------------------------------------------------------------------------------*/
		iChannelNumber++;
	}



	/*------------------------------------------------------------------------------------------------*
	* Add soundcard and file input signals for rear microphone input
	*------------------------------------------------------------------------------------------------*/
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInRearSeat; i++)
	{
		if ((iChannelNumber < iLocNumAllIn) && (psAudioDataInEntire->bActivateSoundcardInput))
		{
			/*------------------------------------------------------------------------------------------*
			* Copy soundcard signal
			*------------------------------------------------------------------------------------------*/
			copyChannelFromSerialStream(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[i],
				(const T_FLOAT*)in_ptr_array + iChannelNumber, iLocNumAllIn,
				iFrameShiftExt);


			/*------------------------------------------------------------------------------------------*
			* Scale soundcard signal
			*------------------------------------------------------------------------------------------*/
			fScaleFactor = (T_FLOAT)psAudioDataInEntire->psAudioChannel[iChannelNumber].bSelectSoundcardInput
				* psAudioDataInEntire->psAudioChannel[iChannelNumber].fGainSoundcardIn;

			for (k = 0; k < iFrameShiftExt; k++)
			{
				mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[i][k] *= fScaleFactor;
			}
		}

		/*---------------------------------------------------------------------------------------------*
		* Add file IO signal
		*---------------------------------------------------------------------------------------------*/
		if (psAudioDataInEntire->bActivateFileInput)
		{
			addFileSignal(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[i],
				iFrameShiftExt,
				&(psAudioDataInEntire->psAudioChannel[iChannelNumber]));
		}

		/*---------------------------------------------------------------------------------------------*
		* Increment channel counter
		*---------------------------------------------------------------------------------------------*/
		iChannelNumber++;
	}

	/*-------------------------------------------------------------------------------*
	* Downsampling front seats
	*-------------------------------------------------------------------------------*/
	if ((T_INT)HSAB_ON == mSession->HsabRtfSrcFs.iMainControl)
	{

		/*-------------------------------------------------------------------------------*
		* Samplerate conversion down (with anti-aliasing filter) front seats
		*-------------------------------------------------------------------------------*/
		HsabRtfSrcDownProcess(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat,
			mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInAlgCoreFrontSeat,
			&(mSession->HsabRtfSrcFs),
			&(mHsabAlgCoreMain->HsabAlgCoreParams));
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Simplified downsampling (without anti-aliasing filter)
		*----------------------------------------------------------------------------*/
		for (n = 0; n < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInFrontSeat; n++)
		{
			for (k = 0, q = 0; k < mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt; k++, q = q + mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac)
			{
				mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInAlgCoreFrontSeat[n][k] = mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInFrontSeat[n][q];
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Downsampling rear seats
	*-------------------------------------------------------------------------------*/
	if ((T_INT)HSAB_ON == mSession->HsabRtfSrcRs.iMainControl)
	{

		/*-------------------------------------------------------------------------------*
		* Samplerate conversion down rear seats
		*-------------------------------------------------------------------------------*/
		HsabRtfSrcDownProcess(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat,
			mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInAlgCoreRearSeat,
			&(mSession->HsabRtfSrcRs),
			&(mHsabAlgCoreMain->HsabAlgCoreParams));
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Simplified downsampling (without anti-aliasing filter)
		*----------------------------------------------------------------------------*/
		for (n = 0; n < mHsabAlgCoreMain->HsabAlgCoreParams.iNumMicInRearSeat; n++)
		{
			for (k = 0, q = 0; k < mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt; k++, q = q + mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac)
			{
				mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInAlgCoreRearSeat[n][k] = mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[n][q];
			}
		}
	}

	/*------------------------------------------------------------------------------------------------*
	 * Main process function of algorithmic core
	 *------------------------------------------------------------------------------------------------*/
	//fwrite((const T_FLOAT*)mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInRearSeat[0], sizeof(T_FLOAT), iFrameShiftExt, mSession->psRecord->fHandleCompRtfIn[0]);

	//schreibe ppfMicInAlgCoreFrontSeat in .raw Datei
	//fwrite((const T_FLOAT*)mHsabAlgCoreMain->HsabAlgCoreSignals.ppfMicInAlgCoreRearSeat[0], sizeof(T_FLOAT), mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt, mSession->psRecord->fHandleCompSrcIn[0]);

	HsabAlgCoreProcess(mHsabAlgCoreMain);

	//OutputDebugStringW(L"My output string");

	//schreibe ppfLspOutAlgCoreFrontSeat in .raw Datei
	//fwrite((const T_FLOAT*)mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutAlgCoreRearSeat[0], sizeof(T_FLOAT), mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt, mSession->psRecord->fHandleCompSrcOut[0]);

	/*-------------------------------------------------------------------------------*
	* Samplerate conversion up front seats
	*-------------------------------------------------------------------------------*/
	if ((T_INT)HSAB_ON == mSession->HsabRtfSrcFs.iMainControl)
	{

		/*-------------------------------------------------------------------------------*
		* Samplerate conversion up front seats
		*-------------------------------------------------------------------------------*/
		HsabRtfSrcUpProcess(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutAlgCoreFrontSeat,
			mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutFrontSeat,
			&(mSession->HsabRtfSrcFs),
			&(mHsabAlgCoreMain->HsabAlgCoreParams));
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Simplified upsampling (without anti-aliasing filter, just hold the value)
		*----------------------------------------------------------------------------*/
		for (n = 0; n < mHsabAlgCoreMain->HsabAlgCoreParams.iNumLspOutFrontSeat; n++)
		{
			for (k = 0, p = 0; p < mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt; k = k + mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac, p++)
			{
				for (q = 0; q < mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac; q++)
				{
					mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutFrontSeat[n][k + q] = mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutAlgCoreFrontSeat[n][p];
				}
			}
		}
	}

	/*-------------------------------------------------------------------------------*
	* Samplerate conversion up rear seats
	*-------------------------------------------------------------------------------*/
	if ((T_INT)HSAB_ON == mSession->HsabRtfSrcRs.iMainControl)
	{

		/*-------------------------------------------------------------------------------*
		* Samplerate conversion up rear seats
		*-------------------------------------------------------------------------------*/
		HsabRtfSrcUpProcess(mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutAlgCoreRearSeat,
			mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutRearSeat,
			&(mSession->HsabRtfSrcRs),
			&(mHsabAlgCoreMain->HsabAlgCoreParams));
	}
	else
	{
		/*----------------------------------------------------------------------------*
		* Simplified upsampling (without anti-aliasing filter, just hold the value)
		*----------------------------------------------------------------------------*/
		for (n = 0; n < mHsabAlgCoreMain->HsabAlgCoreParams.iNumLspOutRearSeat; n++)
		{
			for (k = 0, p = 0; p < mHsabAlgCoreMain->HsabAlgCoreParams.iFrameshiftInt; k = k + mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac, p++)
			{
				for (q = 0; q < mHsabAlgCoreMain->HsabAlgCoreParams.iUpDownSamplFac; q++)
				{
					mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutRearSeat[n][k + q] = mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutAlgCoreRearSeat[n][p];
				}
			}
		}
	}

	//schreibe ppfLspOutFrontSeat in .raw Datei
	//fwrite((const T_FLOAT*)mHsabAlgCoreMain->HsabAlgCoreSignals.ppfLspOutRearSeat[0], sizeof(T_FLOAT), iFrameShiftExt, mSession->psRecord->fHandleCompRtfOut[0]);

	/*------------------------------------------------------------------------------------------------*
	* Init channel counter
	*------------------------------------------------------------------------------------------------*/
	iChannelNumber = (T_INT)0;

	/*------------------------------------------------------------------------------------------------*
	* Send rear output signals (front loudspeaker signals) to soundcard
	*------------------------------------------------------------------------------------------------*/
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumLspOutFrontSeat; i++)
	{
		if (iChannelNumber < iLocNumAllOut)
		{

			/*------------------------------------------------------------------------------------------*
			* Scale soundcard signal
			*------------------------------------------------------------------------------------------*/
			fScaleFactor = (T_FLOAT)psAudioDataOutEntire->psAudioChannel[iChannelNumber].bSelectSoundcardOutput
				* psAudioDataOutEntire->psAudioChannel[iChannelNumber].fGainSoundcardOut;

			for (k = 0; k < iFrameShiftExt; k++)
			{
				mSession->HsabAlgCoreMain.HsabAlgCoreSignals.ppfLspOutFrontSeat[i][k] *= fScaleFactor;
			}

			copyChannelToSerialStream(mSession->HsabAlgCoreMain.HsabAlgCoreSignals.ppfLspOutFrontSeat[i],
				(T_FLOAT*)out_ptr_array + psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut, iLocNumAllOut,
				iFrameShiftExt);
		}
		/*---------------------------------------------------------------------------------------------*
		* Increment channel counter
		*---------------------------------------------------------------------------------------------*/
		iChannelNumber++;
	}

	/*------------------------------------------------------------------------------------------------*
	* Send front output signals (rear loudspeaker signals) to soundcard
	*------------------------------------------------------------------------------------------------*/
	for (i = 0; i < mHsabAlgCoreMain->HsabAlgCoreParams.iNumLspOutRearSeat; i++)
	{
		if (iChannelNumber < iLocNumAllOut)
		{

			/*------------------------------------------------------------------------------------------*
			* Scale soundcard signal
			*------------------------------------------------------------------------------------------*/
			fScaleFactor = (T_FLOAT)psAudioDataOutEntire->psAudioChannel[iChannelNumber].bSelectSoundcardOutput
				* psAudioDataOutEntire->psAudioChannel[iChannelNumber].fGainSoundcardOut;

			for (k = 0; k < iFrameShiftExt; k++)
			{
				mSession->HsabAlgCoreMain.HsabAlgCoreSignals.ppfLspOutRearSeat[i][k] *= fScaleFactor;
			}

			copyChannelToSerialStream(mSession->HsabAlgCoreMain.HsabAlgCoreSignals.ppfLspOutRearSeat[i],
				(T_FLOAT*)out_ptr_array + psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut, iLocNumAllOut,
				iFrameShiftExt);
		}
		/*---------------------------------------------------------------------------------------------*
		* Increment channel counter
		*---------------------------------------------------------------------------------------------*/
		iChannelNumber++;
	}
#endif // BF_SYSTEM

	/*------------------------------------------------------------------------------------------------*
	* Update time-domain real-time plots
	*------------------------------------------------------------------------------------------------*/
	timeDomainPlotting();

	/*------------------------------------------------------------------------------------------------*
	* Update frequency-domain real-time plots
	*------------------------------------------------------------------------------------------------*/
	freqDomainPlotting();

 return 0;
}

/*------------------------------------------------------------------------------------------------*
 * Update frequency-domain real-time plots
 *------------------------------------------------------------------------------------------------*/
void AudioIO::freqDomainPlotting()
{
    T_INT  iSbb, iWidget;

	for ( iSbb=0; iSbb < pPlotData->iFreqMemoryLength; iSbb++ )
	{  	 
	   for ( iWidget=0; iWidget < pPlotData->iNumberOfWidgets; iWidget++ )  
       {
	      mQVector2DPlotterSignal[pPlotData->piModuleIndex[iWidget]][pPlotData->piSignalIndex[iWidget]]->plot_freq(iWidget, iSbb);
	   }
	}
}

/*------------------------------------------------------------------------------------------------*
 * Update time-domain real-time plots
 *------------------------------------------------------------------------------------------------*/
void AudioIO::timeDomainPlotting()
{
	T_INT  iSamp, iWidget;

	for ( iSamp=0; iSamp < mSession->iFrameshiftExt; iSamp++ )
	{
       pPlotData->iSignalPlotSubSamplingCounter++;
	   if ( pPlotData->iSignalPlotSubSamplingCounter >= pPlotData->iSignalPlotSubSampling )
	   { 	   	 
	      pPlotData->iSignalPlotSubSamplingCounter = (T_INT) 0;
           
		  for ( iWidget=0; iWidget < pPlotData->iNumberOfWidgets; iWidget++ )  
          {
			 mQVector2DPlotterSignal[pPlotData->piModuleIndex[iWidget]][pPlotData->piSignalIndex[iWidget]]->plot_time(iWidget, iSamp);
		  }

		  pPlotData->iWritePointer++;
		  if (pPlotData->iWritePointer >= pPlotData->iSignalMemoryLength)
		  {
		     pPlotData->iWritePointer = (T_INT) 0;
		  }
	   }
	}
}

/*------------------------------------------------------------------------------------------------*
 * Initilisation file input data
 *------------------------------------------------------------------------------------------------*/
void AudioIO::initAudioFileInput(int iNumAllIn)
{
   int i;

   for ( i=0; i<iNumAllIn; i++ )
   {
      psAudioDataInEntire->psAudioChannel[i].iPositionIdx = 0;
   }

}

/*------------------------------------------------------------------------------------------------*
 * Get input data file
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getInputFile(tsAudioChannel *psAudioChannel)
{

   FILE* fileHandle = 0;
   psAudioChannel->iPositionIdx = 0;

   if ((psAudioChannel == NULL) || (psAudioChannel->pcFileName == NULL))
   {
      return true;
   }

   psAudioChannel->bError = false;
    
   /* open the file in binary read only mode */
   fileHandle = fopen(psAudioChannel->pcFileName, "rb");

   if( fileHandle == 0 )
   {
      /* error case */
      psAudioChannel->bError = true;
   }
   else
   {
      /* Get the length of the input signal */
      fseek(fileHandle, 0L, SEEK_END);
      psAudioChannel->uiLength  = ftell(fileHandle);
      psAudioChannel->uiLength /= sizeof(short);
      fseek(fileHandle, 0L, SEEK_SET);

      if( psAudioChannel->uiLength == 0U )
      {
         psAudioChannel->bError = true;
      }
      else
      {
         /* allocate or maybe reallocate memory */
         psAudioChannel->psFileData = (T_SHORT*)realloc(psAudioChannel->psFileData, sizeof(T_SHORT) * psAudioChannel->uiLength);

         /* error handling if allocation of memory fails */
         if( psAudioChannel->psFileData == 0 )
         {
            psAudioChannel->bError = true;
         }
         else
         {
            if( fread(psAudioChannel->psFileData, sizeof(T_SHORT), psAudioChannel->uiLength, fileHandle) != psAudioChannel->uiLength )
            {
               psAudioChannel->bError = true;
            }
         }
      }

      fclose(fileHandle);
   }

   return psAudioChannel->bError;
}

/*------------------------------------------------------------------------------------------------*
 * Main initialization of audio data
 *------------------------------------------------------------------------------------------------*/
void AudioIO::initAudioData( tSession *pSession )
{
   
   T_INT      i, iNumAllIn, iNumAllOut, iChannel;
   QString    Text;
   QByteArray ByteArray;
   SendDebugMessage("Initializing audio data...", MessageTypeDebug);
  /*------------------------------------------------------------------------------------------------*
   * Initialization of input file data structures
   *------------------------------------------------------------------------------------------------*/
   iNumAllIn = pSession->iNumAllIn;
   psAudioDataInEntire                 = (tsAudioDataEntire*) calloc(1, sizeof(tsAudioDataEntire));
   psAudioDataInEntire->psAudioChannel = (tsAudioChannel*) calloc(iNumAllIn, sizeof(tsAudioChannel));
   
   psAudioDataInEntire->bActivateSoundcardInput = true;
   psAudioDataInEntire->bActivateFileInput      = false;
#ifdef BF_SYSTEM
   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn; i++)
   {
	   Text = QString("signals\\mic_%1.pcm").arg(i);
#else
   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat; i++)
   {
	   Text = QString("signals\\mic_%1.pcm").arg(i);
#endif 

      psAudioDataInEntire->psAudioChannel[i].bSelectFileInput      = false;
      psAudioDataInEntire->psAudioChannel[i].bSelectSoundcardInput = true;
	  psAudioDataInEntire->psAudioChannel[i].uiSampleRate          = (unsigned int) pSession->iSampleRateExt;
      psAudioDataInEntire->psAudioChannel[i].uiLength              = 0;
      psAudioDataInEntire->psAudioChannel[i].pcFileName            = (char*) malloc(Text.toLatin1().size()); // Null-Byte checken und free checken
      strcpy(psAudioDataInEntire->psAudioChannel[i].pcFileName,Text.toLatin1().data());
      psAudioDataInEntire->psAudioChannel[i].psFileData            = 0; 
      psAudioDataInEntire->psAudioChannel[i].bError                = false;
      psAudioDataInEntire->psAudioChannel[i].fGainFileIndB         = (T_FLOAT) 0.0;
      psAudioDataInEntire->psAudioChannel[i].fGainFileIn           = (T_FLOAT) pow( 10, psAudioDataInEntire->psAudioChannel[i].fGainFileIndB / 20.0f );
      psAudioDataInEntire->psAudioChannel[i].iPositionIdx          = 0;
      psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIndB      = (T_FLOAT) -3.0;
      psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIn        = (T_FLOAT) pow( 10, psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIndB / 20.0f );
   }
#ifdef BF_SYSTEM 
   iChannel = pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicIn;

   for (i = 0; i<pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumRefInFromAmp; i++)
   {      
      Text      = QString("signals\\sounds_cau\\phone_%1_44kHz.pcm").arg(i);
#else
   iChannel = pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInFrontSeat;

   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumMicInRearSeat; i++)
   {
	   Text = QString("signals\\mic_%1.pcm").arg(i);
#endif
 

      psAudioDataInEntire->psAudioChannel[iChannel].bSelectFileInput      = false;
      psAudioDataInEntire->psAudioChannel[iChannel].bSelectSoundcardInput = true;
	  psAudioDataInEntire->psAudioChannel[iChannel].uiSampleRate          = (unsigned int) pSession->iSampleRateExt;
      psAudioDataInEntire->psAudioChannel[iChannel].uiLength              = 0;
      psAudioDataInEntire->psAudioChannel[iChannel].pcFileName            = (char*) malloc(Text.toLatin1().size()); // Null-Byte checken und free checken
      strcpy(psAudioDataInEntire->psAudioChannel[iChannel].pcFileName,Text.toLatin1().data());
      psAudioDataInEntire->psAudioChannel[iChannel].psFileData            = 0; 
      psAudioDataInEntire->psAudioChannel[iChannel].bError                = false;
      psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIndB         = (T_FLOAT) 0.0;
      psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIn           = (T_FLOAT) pow( 10, psAudioDataInEntire->psAudioChannel[i].fGainFileIndB / 20.0f );
      psAudioDataInEntire->psAudioChannel[iChannel].iPositionIdx          = 0;
      psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIndB      = (T_FLOAT) -3.0;
      psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIn        = (T_FLOAT) pow( 10, psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIndB / 20.0f );

	  iChannel++;
   }

#ifdef BF_SYSTEM
   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteIn; i++)
   {

	   Text = QString("signals\\ref_in.pcm").arg(i);

	   psAudioDataInEntire->psAudioChannel[iChannel].bSelectFileInput = false;
	   psAudioDataInEntire->psAudioChannel[iChannel].bSelectSoundcardInput = true;
	   psAudioDataInEntire->psAudioChannel[iChannel].uiSampleRate = (unsigned int)pSession->iSampleRateExt;
	   psAudioDataInEntire->psAudioChannel[iChannel].uiLength = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].pcFileName = (char*)malloc(Text.toLatin1().size()); // Null-Byte checken und free checken
	   strcpy(psAudioDataInEntire->psAudioChannel[iChannel].pcFileName, Text.toLatin1().data());
	   psAudioDataInEntire->psAudioChannel[iChannel].psFileData = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].bError = false;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIndB = (T_FLOAT)0.0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIn = (T_FLOAT)pow(10, psAudioDataInEntire->psAudioChannel[i].fGainFileIndB / 20.0f);
	   psAudioDataInEntire->psAudioChannel[iChannel].iPositionIdx = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIndB = (T_FLOAT)-3.0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIn = (T_FLOAT)pow(10, psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIndB / 20.0f);

	   iChannel++;
   }

   for (i = 0; pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumTtsIn; i++)
   {

	   Text = QString("signals\\sounds_cau\\tts_%1_44kHz.pcm").arg(i);

	   psAudioDataInEntire->psAudioChannel[iChannel].bSelectFileInput = true;
	   psAudioDataInEntire->psAudioChannel[iChannel].bSelectSoundcardInput = false;
	   psAudioDataInEntire->psAudioChannel[iChannel].uiSampleRate = (unsigned int)pSession->iSampleRateExt;
	   psAudioDataInEntire->psAudioChannel[iChannel].uiLength = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].pcFileName = (char*)malloc(Text.toLatin1().size()); // Null-Byte checken und free checken
	   strcpy(psAudioDataInEntire->psAudioChannel[iChannel].pcFileName, Text.toLatin1().data());
	   psAudioDataInEntire->psAudioChannel[iChannel].psFileData = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].bError = false;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIndB = (T_FLOAT)0.0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIn = (T_FLOAT)pow(10, psAudioDataInEntire->psAudioChannel[i].fGainFileIndB / 20.0f);
	   psAudioDataInEntire->psAudioChannel[iChannel].iPositionIdx = 0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIndB = (T_FLOAT)-20.0;
	   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIn = (T_FLOAT)pow(10, psAudioDataInEntire->psAudioChannel[i].fGainSoundcardIndB / 20.0f);
   }
#endif 

   

  /*---------------------------------------------------------------------------------------------*
   * Get default input files
   *---------------------------------------------------------------------------------------------*/
   for( i=0; i<iNumAllIn; i++ )
   {
      getInputFile(&(psAudioDataInEntire->psAudioChannel[i]));
   }

  /*------------------------------------------------------------------------------------------------*
   * Initialization of output file data structures
   *------------------------------------------------------------------------------------------------*/
   iNumAllOut = pSession->iNumAllOut;
   psAudioDataOutEntire                 = (tsAudioDataEntire*) calloc(1, sizeof(tsAudioDataEntire));
   psAudioDataOutEntire->psAudioChannel = (tsAudioChannel*) calloc(iNumAllOut, sizeof(tsAudioChannel));
   psAudioDataOutEntire->piActiveOutChannel = (T_INT*) calloc (iNumAllOut, sizeof(T_INT));
   
   psAudioDataOutEntire->bActivateSoundcardOutput = true;
#ifdef BF_SYSTEM
   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl; i++)
   {
	   psAudioDataOutEntire->iChDownlink = i;
#else
   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumLspOutFrontSeat; i++)
   {
#endif

      psAudioDataOutEntire->psAudioChannel[i].bSelectSoundcardOutput = true;
	  psAudioDataOutEntire->psAudioChannel[i].uiSampleRate           = (unsigned int) pSession->iSampleRateExt;
      psAudioDataOutEntire->psAudioChannel[i].uiLength               = 0;
	  psAudioDataOutEntire->psAudioChannel[i].iChannelOut            = i;
      psAudioDataOutEntire->psAudioChannel[i].bError                 = false;
      psAudioDataOutEntire->psAudioChannel[i].fGainSoundcardOutdB    = (T_FLOAT) 0.0;
      psAudioDataOutEntire->psAudioChannel[i].fGainSoundcardOut      = (T_FLOAT) pow( 10, psAudioDataOutEntire->psAudioChannel[i].fGainSoundcardOutdB / 20.0f );
	  psAudioDataOutEntire->piActiveOutChannel[i]                    = i;
   }

#ifdef BF_SYSTEM
   iChannel = pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl;

   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumRemoteOut; i++, iChannel++)
   {
	   psAudioDataOutEntire->iChUplink = iChannel;
#else
   iChannel = pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumLspOutFrontSeat;

   for (i = 0; i < pSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumLspOutRearSeat; i++)
   {
#endif
      psAudioDataOutEntire->psAudioChannel[iChannel].bSelectSoundcardOutput = true;
	  psAudioDataOutEntire->psAudioChannel[iChannel].uiSampleRate           = (unsigned int) pSession->iSampleRateExt;
      psAudioDataOutEntire->psAudioChannel[iChannel].uiLength               = 0;
	  psAudioDataOutEntire->psAudioChannel[iChannel].iChannelOut            = iChannel;
      psAudioDataOutEntire->psAudioChannel[iChannel].bError                 = false;
      psAudioDataOutEntire->psAudioChannel[iChannel].fGainSoundcardOutdB    = (T_FLOAT) 0.0;
      psAudioDataOutEntire->psAudioChannel[iChannel].fGainSoundcardOut      = (T_FLOAT) pow( 10, psAudioDataOutEntire->psAudioChannel[i].fGainSoundcardOutdB / 20.0f );
	  psAudioDataOutEntire->piActiveOutChannel[iChannel]                    = iChannel;
	 
   }
}

/*------------------------------------------------------------------------------------------------*
 * Add file signal to InBuffer signal
 *------------------------------------------------------------------------------------------------*/
void AudioIO::addFileSignal( float* fInBuffer, T_INT iFrameshift, tsAudioChannel *psAudioChannel )
{
   T_INT   i;
   T_FLOAT fPcmScaleInv    = (T_FLOAT) 1.0 / (T_FLOAT) 32767.0;
   T_FLOAT fGainFileInTemp = psAudioChannel->fGainFileIn * (T_FLOAT) psAudioChannel->bSelectFileInput * fPcmScaleInv;

   if( psAudioChannel->iPositionIdx + iFrameshift > (T_INT) psAudioChannel->uiLength )
   {
      for( i=0; psAudioChannel->iPositionIdx < (T_INT) psAudioChannel->uiLength; i++,psAudioChannel->iPositionIdx++ )
      {
         fInBuffer[i] += psAudioChannel->psFileData[psAudioChannel->iPositionIdx] 
                             * fGainFileInTemp;
      }
      
      psAudioChannel->iPositionIdx = 0;
      
      for( ; i<iFrameshift; i++,psAudioChannel->iPositionIdx++ )
      {
         fInBuffer[i] += psAudioChannel->psFileData[psAudioChannel->iPositionIdx] * fGainFileInTemp;
      }
   }
   else    
   {
      for( i=0; i<iFrameshift; i++,psAudioChannel->iPositionIdx++ )
      {
         fInBuffer[i] += psAudioChannel->psFileData[psAudioChannel->iPositionIdx] * fGainFileInTemp;
      }
   }
}


/*------------------------------------------------------------------------------------------------*
 * Set pointer to plotter module class
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setPlotterModule2DVector( QVector<QVector<PlotterModule*>>   pQVector2DPlotterSignal)
{
   mQVector2DPlotterSignal = pQVector2DPlotterSignal; 
}
/*------------------------------------------------------------------------------------------------*
 * Fill a buffer with zeros
 *------------------------------------------------------------------------------------------------*/
void AudioIO::fillFloatArrayWithZeros(T_FLOAT *pfBuffer, T_INT iSize)
{
   for ( int i = 0; i<iSize; i++)
   {
      pfBuffer[i] = 0.0f;
   }
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of file input in general
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getFileInputActivationGeneral( )
{
   return psAudioDataInEntire->bActivateFileInput; 
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of file input (channel specific)
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getFileInActivationChannelSpec( T_INT iChannel )
{
   return psAudioDataInEntire->psAudioChannel[iChannel].bSelectFileInput;
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of sound card input in general
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getSoundcardActivationGeneral( )
{
   return psAudioDataInEntire->bActivateSoundcardInput;
}
/*------------------------------------------------------------------------------------------------*
 * Get activation status of soundcard input (channel specific)
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getSoundcardInActivationChannelSpec( T_INT iChannel )
{
   return psAudioDataInEntire->psAudioChannel[iChannel].bSelectSoundcardInput;
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of soundcard output (channel specific)
 *------------------------------------------------------------------------------------------------*/
bool AudioIO::getSoundcardOutActivationChannelSpec( T_INT iChannel )
{
   return psAudioDataOutEntire->psAudioChannel[iChannel].bSelectSoundcardOutput;
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of file input gain (channel specific)
 *------------------------------------------------------------------------------------------------*/
float AudioIO::getGainFileInChannelSpec( T_INT iChannel )
{
   return psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIndB;
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of soundcard input gain (channel specific)
 *------------------------------------------------------------------------------------------------*/
float AudioIO::getGainSoundcardInChannelSpec( T_INT iChannel )
{
   return psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIndB;
}

/*------------------------------------------------------------------------------------------------*
 * Get activation status of soundcard output gain (channel specific)
 *------------------------------------------------------------------------------------------------*/
float AudioIO::getGainSoundcardOutChannelSpec( T_INT iChannel )
{
   return psAudioDataOutEntire->psAudioChannel[iChannel].fGainSoundcardOutdB;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of file input in general and all input channels
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setFileInActivationGeneral( bool bAct )
{
   psAudioDataInEntire->bActivateFileInput = bAct;
   psAudioDataInEntire->bActivateSoundcardInput = !bAct;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of sound card input in general all input channels
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setSoundcardInActivationGeneral( bool bAct )
{
   psAudioDataInEntire->bActivateSoundcardInput = bAct;
   psAudioDataInEntire->bActivateFileInput = !bAct;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of sound card output in general all output channels
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setSoundcardOutActivationGeneral( bool bAct )
{
   psAudioDataInEntire->bActivateSoundcardOutput = bAct;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of sound card input (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setSoundcardInActivationChannelSpec( bool bActive, T_INT iChannel )
{
   psAudioDataInEntire->psAudioChannel[iChannel].bSelectSoundcardInput = bActive;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of sound card output (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setSoundcardOutActivationChannelSpec( bool bActive, T_INT iChannel )
{
   psAudioDataOutEntire->psAudioChannel[iChannel].bSelectSoundcardOutput = bActive;
}

/*------------------------------------------------------------------------------------------------*
 * Set sound card output channel (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setSoundCardOutputChannel( T_INT idx, T_INT iChannelNumber )
{
#ifdef BF_SYSTEM
	psAudioDataOutEntire->psAudioChannel[iChannelNumber].iChannelOut = idx;
	psAudioDataOutEntire->piActiveOutChannel[iChannelNumber] = idx;
	if (iChannelNumber < mSession->HsabAlgCoreMain.HsabAlgCoreParams.iNumOutToAmpl)
		psAudioDataOutEntire->iChDownlink = iChannelNumber;
	else
		psAudioDataOutEntire->iChUplink = iChannelNumber;
#endif
}

/*------------------------------------------------------------------------------------------------*
 * Set record status of sound card inputs and outputs
 *------------------------------------------------------------------------------------------------*/
void AudioIO::startRecording(tSession *ps)
{
	ps->psRecord->bRecordStatus = HSAB_TRUE;																							
}

/*------------------------------------------------------------------------------------------------*
 * Set global record status of sound card inputs and outputs
 *------------------------------------------------------------------------------------------------*/
void AudioIO::stopRecording(tSession *ps)
{
   ps->psRecord->bRecordStatus = HSAB_FALSE;																							 
}

/*------------------------------------------------------------------------------------------------*
 * Set record status of all sound card inputs
 *------------------------------------------------------------------------------------------------*/
void AudioIO::AllSoundcardInputsFlag(bool bActive, tSession *ps)
{
   ps->psRecord->bRecordAllInputs = bActive;																						 
}
/*------------------------------------------------------------------------------------------------*
 * Set record status of all sound card outputs
 *------------------------------------------------------------------------------------------------*/
void AudioIO::AllSoundcardOutputsFlag(bool bActive, tSession *ps)
{
   ps->psRecord->bRecordAllOutputs = bActive;																							
}

/*------------------------------------------------------------------------------------------------*
 * Set input channel status for recording (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::activateInputChannel(bool bActive, T_INT iChannel, tSession *ps)
{
	ps->psRecord->pbRecordInputDetails[iChannel] = bActive;																				
}

/*------------------------------------------------------------------------------------------------*
 * Set output channel status for recording (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::activateOutputChannel(bool bActive, T_INT iChannel, tSession *ps)
{
   ps->psRecord->pbRecordOutputDetails[iChannel] = bActive;																	        
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of file input (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setFileInputActivationChannelSpec( bool bActive, T_INT iChannel )
{
   psAudioDataInEntire->psAudioChannel[iChannel].bSelectFileInput = bActive;
}

/*------------------------------------------------------------------------------------------------*
 * Set frequency value for visualization (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setFreqValueChannelSpec( T_DOUBLE dValue, T_INT iChannel, tSession *ps )																	
{
#ifdef BF_SYSTEM
	pPlotData->piFreq[iChannel] = (dValue * ps->HsabAlgCoreMain.HsabAlgCoreParams.iFFTLength) / ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInternal;
#else
	pPlotData->piFreq[iChannel] = (dValue * ps->HsabAlgCoreMain.HsabAlgCoreParams.iFftOrder) / ps->HsabAlgCoreMain.HsabAlgCoreParams.fSampleRateInt;
#endif // BF_SYSTEM
}

/*------------------------------------------------------------------------------------------------*
 * Set offset value for visualization (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setOffsetValueChannelSpec( T_DOUBLE dValue, T_INT iChannel )																	
{
   pPlotData->pdOffset[iChannel] = dValue;
}

 /*------------------------------------------------------------------------------------------------*
 * Set activation status of start button 
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setStartButtonStatus(bool bActive)
{
   bActiveStartButton = bActive;
}

 /*------------------------------------------------------------------------------------------------*
 * Set activation status of pause button 
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setPauseButtonStatus(bool bActive)
{
   bActivePauseButton = bActive;
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of microphone input signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setMicInSignalSelActivationChannelSpec(bool bActive, T_INT iChannel, tSession *ps)
{
   ps->psSigSel->pbActiveHfInMic[iChannel] = bActive;
}

 /*------------------------------------------------------------------------------------------------*
 * Map microphone input signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::mapMicInSignalChannelSpec( T_INT index, T_INT iChannel, tSession *ps )														      		
{
#ifdef BF_SYSTEM
	if ((ps->psSigSel->pbActiveHfInMic[iChannel]) && !bActiveStartButton)
	{
		mSession->ppfMicInSelect[iChannel] = ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppfMicIn[index];
	}
#endif
}
	
/*------------------------------------------------------------------------------------------------*
 * Set activation status of phone input signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setPhoneInSignalSelActivationChannelSpec(bool bActive, T_INT iChannel, tSession *ps)
{
   ps->psSigSel->pbActiveHfInPhone[iChannel] = bActive;
}

 /*------------------------------------------------------------------------------------------------*
 * Map phone input signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::mapPhoneInSignalChannelSpec( T_INT index, T_INT iChannel, tSession *ps )																											
{
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of loudspeaker output signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setLspOutSignalSelActivationChannelSpec(bool bActive, T_INT iChannel, tSession *ps)
{
   ps->psSigSel->pbActiveHfOutLsp[iChannel] = bActive;
}

 /*------------------------------------------------------------------------------------------------*
 * Map loudspeaker output signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::mapLspOutSignalChannelSpec( T_INT index, T_INT iChannel, tSession *ps )																												
{
}

/*------------------------------------------------------------------------------------------------*
 * Set activation status of phone output signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setPhoneOutSignalSelActivationChannelSpec(bool bActive, T_INT iChannel, tSession *ps)
{
   ps->psSigSel->pbActiveHfOutPhone[iChannel] = bActive;
}

 /*------------------------------------------------------------------------------------------------*
 * Map phone output signals (channel specific)
 *------------------------------------------------------------------------------------------------*/
void AudioIO::mapPhoneOutSignalChannelSpec( T_INT index, T_INT iChannel, tSession *ps )																													
{
#ifdef BF_SYSTEM
   if ((ps->psSigSel->pbActiveHfOutPhone[iChannel]) && !bActiveStartButton)
   {
	   mSession->ppfPhoneOutSelect[iChannel] = ps->HsabAlgCoreMain.HsabAlgCoreSignals.ppfRemoteOut[index];
   }
#endif
}
	
/*------------------------------------------------------------------------------------------------*
 * Set gain of the sound card input
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setGainSoundcardInputChannel( T_DOUBLE dValue, T_INT iChannel )
{
   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIndB = dValue;
   psAudioDataInEntire->psAudioChannel[iChannel].fGainSoundcardIn   = (T_FLOAT) pow( (T_DOUBLE) 10.0, dValue / (T_DOUBLE) 20.0 );
}

/*------------------------------------------------------------------------------------------------*
 * Set gain of the sound card output
 *------------------------------------------------------------------------------------------------*/
void AudioIO::setGainSoundcardOutputChannel( T_DOUBLE dValue, T_INT iChannel )
{
   psAudioDataOutEntire->psAudioChannel[iChannel].fGainSoundcardOutdB = dValue;
   psAudioDataOutEntire->psAudioChannel[iChannel].fGainSoundcardOut   = (T_FLOAT) pow( (T_DOUBLE) 10.0, dValue / (T_DOUBLE) 20.0 );
}

/*------------------------------------------------------------------------------------------------*
 * Set gain of the file input
 *------------------------------------------------------------------------------------------------*/

void AudioIO::setGainFileInputChannel( T_DOUBLE dValue, T_INT iChannel )
{
   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIndB = dValue;
   psAudioDataInEntire->psAudioChannel[iChannel].fGainFileIn   = (T_FLOAT) pow( (T_DOUBLE) 10.0, dValue / (T_DOUBLE) 20.0 );
}


void AudioIO::getNewFile( const T_CHAR *pcFileName, T_INT iChannel )
{
   psAudioDataInEntire->psAudioChannel[iChannel].pcFileName = (T_CHAR*) pcFileName;
   getInputFile(&(psAudioDataInEntire->psAudioChannel[iChannel]));
}

void AudioIO::connectPlotData( tPlotData *pDataStruct )
{
   pPlotData = pDataStruct;
}

/*------------------------------------------------------------------------------------------------*
 * Set input channel (time domain)
 *------------------------------------------------------------------------------------------------*/

void AudioIO::updateTimeChannel()
{


}

/*--------------------------------------------------------------------------------*
 * Fill a buffer with low power noise
 *--------------------------------------------------------------------------------*/
void AudioIO::fillFloatBufferWithLowPowerNoise( float* pfBuffer, int iSize )
{
	T_INT m;
	T_FLOAT   fLocNoiseGenSignal;
    for (m=0; m < iSize; m++)
    {
        muiNoiseGenShiftRegister = (T_UINT32) 2147001325UL * muiNoiseGenShiftRegister
                                     + (T_UINT32) 715136305UL;
        fLocNoiseGenSignal       = (T_FLOAT)  muiNoiseGenShiftRegister 
                                     * (T_FLOAT)  4.65661287308e-010 
                                     - (T_FLOAT)  1.0;

		pfBuffer[m] = fLocNoiseGenSignal * mfNoiseGenGain; 
    }
}

/*--------------------------------------------------------------------------------*
 * Serial TDM audio stream access functions
 *--------------------------------------------------------------------------------*/
void AudioIO::fillChannelInSerialStreamWithZeros(float* pfBuffer, int iStep, int iSize)
{
   	T_INT i, idx;
    for(i = 0; i < iSize; i++)
    {
		idx = iStep*i;
        pfBuffer[idx] = 0.0f;
    }
}
void AudioIO::copyChannelFromSerialStream (float* pfBuffer, const float* pfInputStream, int iStep, int iSize)
{
	T_INT i, idx;
    for(i = 0; i < iSize; i++)
    {
		idx = iStep*i;
        pfBuffer[i] = pfInputStream[idx];
    }
}

void AudioIO::copyChannelToSerialStream (const float* pfBuffer, float* pfOutputStream, int iStep, int iSize)
{
	T_INT i, idx;
    for(i = 0; i < iSize; i++)
    {
		idx = iStep*i;
		pfOutputStream[idx] = pfBuffer[i];
    }
}

/*------------------------------------------------------------------------------------------------*
 *						 RMS energy of time domain signals
 *------------------------------------------------------------------------------------------------*/
void AudioIO::ComputeRmsLevel(const T_FLOAT* pfInput, T_FLOAT *pfOutput, T_INT iSize)
{
	int k;
	float fSum = 0;
	for (k = 0; k < iSize; k++)
	{
		fSum += pow(pfInput[k],2);
	}
	fSum/=iSize;
	*pfOutput = sqrt(fSum);
}


/*------------------------------------------------------------------------------------------------*
 *						  Downsampling Process function
 *------------------------------------------------------------------------------------------------*/

void AudioIO::Rtf_hfSrcDownProcess( T_FLOAT  **ppfInput, T_FLOAT   **ppfOutput, tRtf_hfSrc *psRtf_hfSrc, T_INT iFrameshiftExt, T_INT iFrameshift)

{
   T_INT   i,m,n;
   T_INT   iLocUpDownSampleFactor = psRtf_hfSrc->iUpDownSamplingFactor;
   T_FLOAT fLocDenCoeff1; 
   T_FLOAT fLocDenCoeff2;
   T_FLOAT fLocNumCoeff0;
   T_FLOAT fLocNumCoeff1; 
   T_FLOAT fLocNumCoeff2; 
   T_FLOAT *pfLocPointer1;
   T_FLOAT *pfLocPointer2; 
   T_FLOAT *pfLocPointerTmp;
   
  /*--------Pointer initalization---------------*/
  
   pfLocPointer1 = psRtf_hfSrc->pfTempSignal1;
   pfLocPointer2 = psRtf_hfSrc->pfTempSignal2;

  /*-------Loop over all channels------------------*/
   for(i = 0; i < psRtf_hfSrc->iNumChannelsDownSamp; i++)       
   {

     /*-----Copy input signal into temporal signal vector--------------------*/
      memcpy( pfLocPointer2, 
              ppfInput[i], 
			  iFrameshiftExt * sizeof(T_FLOAT) );

     /*--------Anti-aliasing filtering-----------------*/
      for (m=0; m < psRtf_hfSrc->iNumBiQuadFilter; m++)     
      {
        /*--------------Change temporal input and output vector -----------------*/                 
     
         pfLocPointerTmp = pfLocPointer1;
         pfLocPointer1   = pfLocPointer2;
         pfLocPointer2   = pfLocPointerTmp;

        /*-------Put filter coefficients into local variables   -----------*/
         fLocNumCoeff0 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][0];
         fLocNumCoeff1 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][1];
         fLocNumCoeff2 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][2];
         fLocDenCoeff1 = psRtf_hfSrc->ppfFilterCoeffDenominator[m][0];
         fLocDenCoeff2 = psRtf_hfSrc->ppfFilterCoeffDenominator[m][1];

        /*-------- Compute first output sample ------------------*/
         pfLocPointer2[0] = 
            pfLocPointer1[0]                               * fLocNumCoeff0 +
            psRtf_hfSrc->pppfLastInputDown[i][m][0]   * fLocNumCoeff1 +
            psRtf_hfSrc->pppfLastInputDown[i][m][1]   * fLocNumCoeff2 +
            psRtf_hfSrc->pppfLastOutputDown[i][m][0]  * fLocDenCoeff1 +
            psRtf_hfSrc->pppfLastOutputDown[i][m][1]  * fLocDenCoeff2;

        /*----------Compute second output sample-------------*/
         pfLocPointer2[1] = 
            pfLocPointer1[1]                               * fLocNumCoeff0 +
            pfLocPointer1[0]                               * fLocNumCoeff1 +
            psRtf_hfSrc->pppfLastInputDown[i][m][0]   * fLocNumCoeff2 +
            pfLocPointer2[0]                               * fLocDenCoeff1 +
            psRtf_hfSrc->pppfLastOutputDown[i][m][0]  * fLocDenCoeff2;

		 for(n = 2; n <iFrameshiftExt; n++)
         {
          /*---------------Compute current output signal ---------------*/
            pfLocPointer2[n] = 
               pfLocPointer1[n]   * fLocNumCoeff0 +
               pfLocPointer1[n-1] * fLocNumCoeff1 +
               pfLocPointer1[n-2] * fLocNumCoeff2 +
               pfLocPointer2[n-1] * fLocDenCoeff1 +
               pfLocPointer2[n-2] * fLocDenCoeff2;
         }

        /*--------------------Store last two input and output signals -----------------------*/
 
		 psRtf_hfSrc->pppfLastOutputDown[i][m][0] = pfLocPointer2[iFrameshiftExt-1];
         psRtf_hfSrc->pppfLastOutputDown[i][m][1] = pfLocPointer2[iFrameshiftExt-2];
         psRtf_hfSrc->pppfLastInputDown[i][m][0]  = pfLocPointer1[iFrameshiftExt-1];
         psRtf_hfSrc->pppfLastInputDown[i][m][1]  = pfLocPointer1[iFrameshiftExt-2];
      }

     /*-------------------Subsampling ----------------------*/
	  for(n=0, m=0; n < iFrameshift; n++, m=m+iLocUpDownSampleFactor)
      {
         ppfOutput[i][n] = pfLocPointer2[m];
      }
   }
}



/*------------------------------------------------------------------------------------------------*
 *								 Upsampling Process function
 *------------------------------------------------------------------------------------------------*/

void AudioIO::Rtf_hfSrcUpProcess( T_FLOAT  **ppfInput, T_FLOAT  **ppfOutput, tRtf_hfSrc *psRtf_hfSrc, T_INT iFrameshiftExt, T_INT iFrameshift)
{
   T_INT   i,k,m,n;
   T_INT   iLocUpDownSampleFactor = psRtf_hfSrc->iUpDownSamplingFactor;
   T_FLOAT fLocDenCoeff1; 
   T_FLOAT fLocDenCoeff2;
   T_FLOAT fLocNumCoeff0;
   T_FLOAT fLocNumCoeff1;
   T_FLOAT fLocNumCoeff2; 
   T_FLOAT *pfLocPointer1;
   T_FLOAT *pfLocPointer2; 
   T_FLOAT *pfLocPointerTmp;
   
  /*--------------------Pointer initalization----------------------*/  
  
   pfLocPointer1 = psRtf_hfSrc->pfTempSignal1;
   pfLocPointer2 = psRtf_hfSrc->pfTempSignal2;
   
  /*-------------Loop over all channels------------------------------*/

   for(i=0; i < psRtf_hfSrc->iNumChannelsUpSamp; i++)       
   {
     /*------Upsample with hold mechanism------------------------------*/

  
	   for(n=0, m=0; n < iFrameshift; n++, m=m+iLocUpDownSampleFactor)
       {
         pfLocPointer2[m] = ppfInput[i][n];
         for (k=1; k<iLocUpDownSampleFactor; k++)
         {
            pfLocPointer2[m+k] = pfLocPointer2[m];
         }
      }

     /*---------------Anti-aliasing filtering----------------------------------------*/
 
      for (m=0; m < psRtf_hfSrc->iNumBiQuadFilter; m++)     
      {
        /*-------------------------------------------------------------------------*
         * Change temporal input and output vector                    
         *-------------------------------------------------------------------------*/
         pfLocPointerTmp = pfLocPointer1;
         pfLocPointer1   = pfLocPointer2;
         pfLocPointer2   = pfLocPointerTmp;

        /*-------------------------------------------------------------------------*
         * Put filter coefficients into local variables                    
         *-------------------------------------------------------------------------*/
         fLocNumCoeff0 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][0];
         fLocNumCoeff1 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][1];
         fLocNumCoeff2 = psRtf_hfSrc->ppfFilterCoeffNumerator[m][2];
         fLocDenCoeff1 = psRtf_hfSrc->ppfFilterCoeffDenominator[m][0];
         fLocDenCoeff2 = psRtf_hfSrc->ppfFilterCoeffDenominator[m][1];

        /*-------------------------------------------------------------------------*
         * Compute first output sample 
         *-------------------------------------------------------------------------*/
         pfLocPointer2[0] = 
            pfLocPointer1[0]                            * fLocNumCoeff0 +
            psRtf_hfSrc->pppfLastInputUp[i][m][0]  * fLocNumCoeff1 +
            psRtf_hfSrc->pppfLastInputUp[i][m][1]  * fLocNumCoeff2 +
            psRtf_hfSrc->pppfLastOutputUp[i][m][0] * fLocDenCoeff1 +
            psRtf_hfSrc->pppfLastOutputUp[i][m][1] * fLocDenCoeff2;
		
        /*-------------------------------------------------------------------------*
         * Compute second output sample 
         *-------------------------------------------------------------------------*/
         pfLocPointer2[1] = 
            pfLocPointer1[1]                            * fLocNumCoeff0 +
            pfLocPointer1[0]                            * fLocNumCoeff1 +
            psRtf_hfSrc->pppfLastInputUp[i][m][0]  * fLocNumCoeff2 +
            pfLocPointer2[0]                            * fLocDenCoeff1 +
            psRtf_hfSrc->pppfLastOutputUp[i][m][0] * fLocDenCoeff2;

         for(n = 2; n < iFrameshiftExt; n++)
         {
           /*----------------------------------------------------------------------*
            * Compute current output signal 
            *----------------------------------------------------------------------*/
            pfLocPointer2[n] = 
               pfLocPointer1[n]   * fLocNumCoeff0 +
               pfLocPointer1[n-1] * fLocNumCoeff1 +
               pfLocPointer1[n-2] * fLocNumCoeff2 +
               pfLocPointer2[n-1] * fLocDenCoeff1 +
               pfLocPointer2[n-2] * fLocDenCoeff2;
         }

        /*-------------------------------------------------------------------------*
         * Store last two input and output signals  
         *-------------------------------------------------------------------------*/
         psRtf_hfSrc->pppfLastOutputUp[i][m][0] = pfLocPointer2[iFrameshiftExt-1];
         psRtf_hfSrc->pppfLastOutputUp[i][m][1] = pfLocPointer2[iFrameshiftExt-2];
         psRtf_hfSrc->pppfLastInputUp[i][m][0]  = pfLocPointer1[iFrameshiftExt-1];
         psRtf_hfSrc->pppfLastInputUp[i][m][1]  = pfLocPointer1[iFrameshiftExt-2];
      }

     /*----------------------------------------------------------------------------*
      * Copy result to output buffer                                                             
      *----------------------------------------------------------------------------*/
      memcpy( ppfOutput[i],
              pfLocPointer2,                
              iFrameshiftExt * sizeof(T_FLOAT) );
   }
}