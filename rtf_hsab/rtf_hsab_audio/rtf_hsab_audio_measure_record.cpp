/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#include "rtf_hsab_audio_measure_record.h"
#include "types_hsab.h"
#include "QIcon"

InputCheckBoxWidget::InputCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
	    , mpAudioIO(pAudioIO)
		, mps(ps)

{
   setText(sChannelName);
   setChecked(false);
   connect(this, SIGNAL(toggled(bool)), this, SLOT(activateInputChannel(bool)));
}

OutputCheckBoxWidget::OutputCheckBoxWidget(QString sChannelName, int iChannelNumber, QWidget *parent, AudioIO *pAudioIO, tSession *ps)
        : QCheckBox(parent)
		, mChannelNumber(iChannelNumber)
	    , mpAudioIO(pAudioIO)
		, mps(ps)

{
   setText(sChannelName);
   setChecked(false);
   connect(this, SIGNAL(toggled(bool)), this, SLOT(activateOutputChannel(bool)));
}

void InputCheckBoxWidget::activateInputChannel(bool bAct)
{
	mpAudioIO->activateInputChannel(bAct, mChannelNumber, mps);
}

void OutputCheckBoxWidget::activateOutputChannel(bool bAct)
{
	mpAudioIO->activateOutputChannel(bAct, mChannelNumber, mps);
}


TabRecordSap::TabRecordSap ( AudioIO *audioIO, QWidget * parent, tSession *ps  )
	: QWidget ( parent )
	, mAudio(audioIO)
	, mps(ps)
{

/*-----------------------------------------------------------------------------------------*
   * Sub layout center
   *-----------------------------------------------------------------------------------------*/
     QVBoxLayout *SubLayoutCenter = new QVBoxLayout(this);
     iChannelNumberInput = 0;
  /*-----------------------------------------------------------------------------------------*
   * Top layout
   *-----------------------------------------------------------------------------------------*/
     mScrollArea = new QScrollArea(this);
     QWidget *mWidget     = new QWidget(mScrollArea);
	 SubLayoutCenter->addWidget(mScrollArea);
     mScrollArea->setWidget(mWidget);
     mScrollArea->setWidgetResizable(true);
   
     TopLayout = new QHBoxLayout(mWidget);
	 mWidget->setLayout(TopLayout);
   
  /*-----------------------------------------------------------------------------------------*
   * Input groupbox
   *-----------------------------------------------------------------------------------------*/
   QVBoxLayout *SubLayoutInput = new QVBoxLayout();
   InputGroupBox  = new QGroupBox(tr("Soundcard inputs"));
   InputLayout    = new QGridLayout(this);
   InputLayout2 = new QGridLayout(this);

   mCheckBoxAudioSoundcardInputGlobal = new QCheckBox(tr("All soundcard inputs"), this);
   mCheckBoxAudioSoundcardInputGlobal->setChecked(true);
   connect(mCheckBoxAudioSoundcardInputGlobal,SIGNAL(toggled(bool)), SLOT(AllSoundcardInputsFlag(bool)));
   mGroupBoxInputDetails = new QGroupBox(tr("Details"));
   mGroupBoxInputDetails->setCheckable(true);
   mGroupBoxInputDetails->setChecked(false);
   mGroupBoxInputDetails->setLayout(InputLayout2);
   connect(mGroupBoxInputDetails,SIGNAL(toggled(bool)), SLOT(showInputDetails(bool)));
   
   for(int i=0; i < ps->psRecord->iLocNumAllIn; i++)
   {
	  sChannelNameInput = QString("Soundcard input %1").arg(i);
	  mInputCheckBoxWidget = new InputCheckBoxWidget(sChannelNameInput ,iChannelNumberInput, mWidget, mAudio, ps);
	  InputLayout2->setRowMinimumHeight( i, 20 );
	  InputLayout2->addWidget(mInputCheckBoxWidget,i,0);
	  iChannelNumberInput++;
   }
   InputLayout2->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), iChannelNumberInput, 2);
   InputGroupBox->setLayout(InputLayout);
   InputLayout->addWidget(mCheckBoxAudioSoundcardInputGlobal,0,0);
   InputLayout->addWidget(mGroupBoxInputDetails);

   SubLayoutInput->addWidget(InputGroupBox);
   TopLayout->addLayout(SubLayoutInput);

  /*-----------------------------------------------------------------------------------------*
   * Output groupbox
   *-----------------------------------------------------------------------------------------*/
   iChannelNumberOutput = 0;
   QVBoxLayout *SubLayoutOutput = new QVBoxLayout();
   QGroupBox   *OutputGroupBox  = new QGroupBox(tr("Soundcard outputs"));
   OutputLayout    = new QGridLayout(this);
   OutputLayout2   = new QGridLayout(this);

   
   mCheckBoxAudioSoundcardOutputGlobal = new QCheckBox(tr("All soundcard outputs"), this);
   mCheckBoxAudioSoundcardOutputGlobal->setChecked(true);
   connect(mCheckBoxAudioSoundcardOutputGlobal,SIGNAL(toggled(bool)), SLOT(AllSoundcardOutputsFlag(bool)));
   mGroupBoxOutputDetails = new QGroupBox(tr("Details"), this);
   mGroupBoxOutputDetails->setLayout(OutputLayout2);
   mGroupBoxOutputDetails->setCheckable(true);
   mGroupBoxOutputDetails->setChecked(false);
   connect(mGroupBoxOutputDetails,SIGNAL(toggled(bool)), SLOT(showOutputDetails(bool)));


   for(int i=0; i < ps->psRecord->iLocNumAllOut; i++)
   {
	   sChannelNameOutput = QString("Soundcard output %1").arg(i);
	   mOutputCheckBoxWidget = new OutputCheckBoxWidget(sChannelNameOutput ,iChannelNumberOutput, mWidget, mAudio, ps);
	   OutputLayout2->setRowMinimumHeight( i, 20 );
	   OutputLayout2->addWidget(mOutputCheckBoxWidget,i,0);
	   iChannelNumberOutput++;
   }
   OutputLayout2->addItem( new QSpacerItem(1,1,QSizePolicy::Expanding, QSizePolicy::Expanding), iChannelNumberOutput, 2);
   OutputGroupBox->setLayout(OutputLayout);
   OutputLayout->addWidget(mCheckBoxAudioSoundcardOutputGlobal,0,0);
   OutputLayout->addWidget(mGroupBoxOutputDetails);

   SubLayoutOutput->addWidget(OutputGroupBox);
   TopLayout->addLayout(SubLayoutOutput);

 /*-----------------------------------------------------------------------------------------*
   * Record-
   buttons
   *-----------------------------------------------------------------------------------------*/
   QHBoxLayout *SubLayoutMid = new QHBoxLayout();

   mPushButtonRecord = new QPushButton(this);
   mPushButtonRecord->setIcon(QIcon("record_icon.bmp"));
   mPushButtonRecord->setIconSize(QSize(11,11));
   mPushButtonRecord->setFixedSize(70,25);
   connect(mPushButtonRecord, SIGNAL(clicked()), this, SLOT(startRecording()));

   mPushButtonStop = new QPushButton(this);
   mPushButtonStop->setIcon(QIcon("stop_icon.bmp"));
   mPushButtonStop->setIconSize(QSize(11,11));
   mPushButtonStop->setFixedSize(70,25);
   mPushButtonStop->setDisabled(true);
   connect(mPushButtonStop, SIGNAL(clicked()), this, SLOT(stopRecording()));

   SubLayoutMid->addStretch();
   SubLayoutMid->addWidget(mPushButtonRecord);
   SubLayoutMid->addWidget(mPushButtonStop);
   SubLayoutMid->addStretch();
   SubLayoutCenter->addLayout(SubLayoutMid);

}

void TabRecordSap::startRecording()
{
	mPushButtonRecord->setDisabled(true);
	mPushButtonStop->setEnabled(true);
	mAudio->startRecording(mps);
}

void TabRecordSap::stopRecording()
{
	mPushButtonRecord->setEnabled(true);
	mPushButtonStop->setDisabled(true);
	mAudio->stopRecording(mps);
}
void TabRecordSap::showInputDetails(bool bAct)
{
    mCheckBoxAudioSoundcardInputGlobal->setChecked(false);
	mAudio->AllSoundcardInputsFlag(!bAct, mps);
}

void TabRecordSap::showOutputDetails(bool bAct)
{
    mCheckBoxAudioSoundcardOutputGlobal->setChecked(false);
	mAudio->AllSoundcardOutputsFlag(!bAct, mps);
}

void TabRecordSap::AllSoundcardInputsFlag(bool bActive)
{
	mAudio->AllSoundcardInputsFlag(bActive, mps);
}

void TabRecordSap::AllSoundcardOutputsFlag(bool bActive)
{
	mAudio->AllSoundcardOutputsFlag(bActive, mps);
}

TabMeasureSap::TabMeasureSap( AudioIO *audioIO, QWidget * parent)
{

}


MeasureRecordDetailsDialog::MeasureRecordDetailsDialog(AudioIO *audioIO, QWidget * parent, tSession *ps )
: QDialog(parent) 
, mAudio(audioIO)
, mps(ps)
{

  /*-----------------------------------------------------------------------------------------*
   * Main layout
   *-----------------------------------------------------------------------------------------*/
   MainLayout = new QVBoxLayout(this);
   setWindowTitle(tr("Record"));
   setFixedSize(800,350);
   mTabWidgetMeasureRecordSelect = new QTabWidget(this);

  /*-----------------------------------------------------------------------------------------*
   * Recording
   *-----------------------------------------------------------------------------------------*/
   mTabRecordSap = new TabRecordSap(mAudio, this, ps);
   mTabWidgetMeasureRecordSelect->addTab(mTabRecordSap, tr("Record"));
   connect(mTabRecordSap, SIGNAL(ResizeTab()), this, SLOT(ResizeTab()));

  /*-----------------------------------------------------------------------------------------*
   * Measurements
   *-----------------------------------------------------------------------------------------*/
   mTabMeasureSap = new TabMeasureSap(mAudio, this);

  /*-----------------------------------------------------------------------------------------*
   * Close button
   *-----------------------------------------------------------------------------------------*/
   QHBoxLayout *SubLayoutBottom = new QHBoxLayout(this);
   QPushButton *PushButtonClose = new QPushButton(tr("&Close"), this);

   PushButtonClose->setMinimumWidth(100);
   connect(PushButtonClose, SIGNAL(clicked()), this, SLOT(close()));
   SubLayoutBottom->addStretch();	
   SubLayoutBottom->addWidget(PushButtonClose);

   MainLayout->addWidget(mTabWidgetMeasureRecordSelect);
   MainLayout->addLayout(SubLayoutBottom);
}


void MeasureRecordDetailsDialog::showRecordingTab()
{
	show();
	mTabWidgetMeasureRecordSelect->setCurrentWidget(mTabRecordSap);
}

void MeasureRecordDetailsDialog::showMeasuringTab()
{
	show();
	mTabWidgetMeasureRecordSelect->setCurrentWidget(mTabMeasureSap);
}
