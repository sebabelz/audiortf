/*-----------------------------------------------------------------------------------------------*
* Real-time framework for audio and speech signal processing
* Aschaffenburg University of Applied Sciences
* Mohammed.Krini@h-ab.de
* (c) 2016-2017
*-----------------------------------------------------------------------------------------------*/

#ifndef RTF_HSAB_AUDIO_SELECT_OUTPUTS_H
#define RTF_HSAB_AUDIO_SELECT_OUTPUTS_H

#include "rtf_hsab_audio_io.h"
#include <QDialog> 
#include <QtGui>
#include <QtWidgets>

class AudioIO;

class AudioDetailsOutChannel: public QGroupBox
{
   Q_OBJECT

   public:
	   AudioDetailsOutChannel(QWidget *parent, int channelNumber, QString string, AudioIO *audioIO, int iNumAllIn);
	   
	   QComboBox				*mComboBoxSoundcardOutputChannelSpec;
	   QCheckBox				*mSoundCardOutputCheckBox;

   private slots:
	   void		selectSoundcardOutputChannel(bool bActive);
	   void		selectGainSoundcardOutputChannel(double dValue);
	   void		setSoundCardOutputChannel(int idx);

   private:
	   T_INT					mChannelNumber;
	   QDoubleSpinBox			*mSpinBoxGainSoundcardOutputChannelSpec;
	   AudioIO					*mAudio;
};


class TabPhoneOutSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabPhoneOutSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsOutChannel*>   mQVectorAudioDetailsOutChannel;
		
    protected:

	signals:


    public slots:

    private:
	
};


class TabLspOutSelectSap : public QWidget
{
    Q_OBJECT

    public:
		TabLspOutSelectSap(tHsabAlgCoreParameters *Params, AudioIO *audioIO, T_INT iChannelNumber, T_INT iNumAllIn);
		QVector<AudioDetailsOutChannel*>   mQVectorAudioDetailsOutChannel;
    protected:


	signals:

    public slots:

    private:


};



class AudioDetailsOutputs : public QWidget
{
	Q_OBJECT
	
   public:
	   AudioDetailsOutputs(AudioIO *audioIO, tHsabAlgCoreParameters *Params, QWidget *parent);

   signals:
		
   public slots:

		void	setGlobalSoundcardOutput( bool bActive );

   private:

	   tHsabAlgCoreParameters		                *mParams;
		QCheckBox									*mCheckBoxAudioSoundcardOutputGlobal;
		QGroupBox									*mGroupBoxMic0;
		QTabWidget									*mTabWidgetMic0;
		AudioIO										*mAudio;
		T_INT										iNumAllOut;
		TabPhoneOutSelectSap						*mPhoneOutSelectTap;
		TabLspOutSelectSap							*mLspOutSelectTap;
		QTabWidget									*mTabWidgetAudioSelect;

};

#endif